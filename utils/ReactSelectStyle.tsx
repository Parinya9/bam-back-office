const customStyles = {
  control: (styles: any, { isDisabled }: { isDisabled: any }) => ({
    ...styles,
    borderRadius: "60px",
    border: "1px solid #CED4DA ",
    fontFamily: "Kanit",
    backgroundColor: isDisabled ? "hsl(0, 0%, 98%);" : "",
  }),
  menu: (base: any) => ({
    ...base,
    fontSize: "14px",
  }),
  singleValue: (base: any) => ({
    ...base,
    fontSize: "14px",
    fontFamily: "Kanit",
  }),
  menuList: (base: any) => ({
    ...base,
    fontSize: "14px",
  }),
  placeholder: (defaultStyles: any) => {
    return {
      ...defaultStyles,
      fontSize: "14px",
    };
  },
};

export { customStyles };
