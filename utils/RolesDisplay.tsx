import React, { FunctionComponent } from "react";
import callApi from "../src/pages/api/callApi";

// interface RoleDisplay {
//   item: string;
// }
// call from reduct

const checkStatusApprove = (role: any, status: any) => {
  switch (status as any) {
    case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
      if (
        role == "dev" ||
        role == "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์" ||
        role == "Administrator"
      ) {
        return true;
      }
      return false;
    case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
      if (
        role == "dev" ||
        role == "จนท.ฝ่ายการตลาด" ||
        role == "Administrator"
      ) {
        return true;
      }
      return false;
    case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
      if (
        role == "dev" ||
        role == "ผจก.ฝ่ายการตลาด" ||
        role == "Administrator"
      ) {
        return true;
      }
      return false;
    default:
      return false;
  }
};

export default {
  checkStatusApprove,
};
