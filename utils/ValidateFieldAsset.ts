const checkFieldRequire = (item: any) => {
  if (checkEmptyField(item?.project_th)) {
    return true;
  }
  if (checkEmptyField(item?.invitation_th)) {
    return true;
  }
  if (checkEmptyField(item?.property_detail)) {
    return true;
  }
  if (checkEmptyField(item?.work_phone)) {
    return true;
  }
  if (checkEmptyField(item?.work_phone_nxt)) {
    return true;
  }
  if (checkEmptyField(item?.property_location)) {
    return true;
  }
  if (checkEmptyField(item?.image_map)) {
    return true;
  }
  if (checkEmptyField(item?.album_property)) {
    return true;
  }
  return false;
};

const checkFieldRequireAssetImage = (item: any) => {
  if (checkEmptyField(item?.image_map)) {
    return true;
  }
  if (checkEmptyField(item?.album_property)) {
    return true;
  }
  return false;
};
const checkFieldRequireAssetDetail = (item: any) => {
  if (checkEmptyField(item?.project_th)) {
    return true;
  }
  if (checkEmptyField(item?.invitation_th)) {
    return true;
  }
  if (checkEmptyField(item?.property_detail)) {
    return true;
  }
  if (checkEmptyField(item?.work_phone)) {
    return true;
  }
  if (checkEmptyField(item?.work_phone_nxt)) {
    return true;
  }
  if (checkEmptyField(item?.property_location)) {
    return true;
  }

  return false;
};
const checkEmptyField = (value: any) => {
  if (value === "" || value === null || value === undefined || value === []) {
    return true;
  }
  return false;
};

export default {
  checkFieldRequire,
  checkFieldRequireAssetImage,
  checkFieldRequireAssetDetail,
};
