import { Request, Response } from "express";
// import utmSource from "./utmSource";

module.exports = (app?: any, server?: any) => {
  const handler = app.getRequestHandler();

  //   server.use(utmSource);

  server.get("/", async (req: Request, res: Response) => {
    return app.render(req, res, "/index");
  });
  server.get("/memberinfo", async (req: Request, res: Response) => {
    return app.render(req, res, "/memberinfo");
  });
  server.get("/reserveProperty", async (req: Request, res: Response) => {
    return app.render(req, res, "/reserveproperty");
  });
  server.get("/kbanktranscation", async (req: Request, res: Response) => {
    return app.render(req, res, "/kbanktranscation");
  });
  server.get("/interestproperty", async (req: Request, res: Response) => {
    return app.render(req, res, "/interestproperty");
  });
  server.get("/grouppropertylist", async (req: Request, res: Response) => {
    return app.render(req, res, "/grouppropertylist");
  });
  server.get("/grouppricelist", async (req: Request, res: Response) => {
    return app.render(req, res, "/grouppricelist");
  });
  server.get("/nearbylist", async (req: Request, res: Response) => {
    return app.render(req, res, "/nearbylist");
  });
  server.get("/propertyforsales", async (req: Request, res: Response) => {
    return app.render(req, res, "/propertyforsales");
  });

  server.get("*", (req: Request, res: Response) => handler(req, res));
};
