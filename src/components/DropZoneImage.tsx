import React, { useRef, FunctionComponent, useCallback } from "react";
import { useDropzone } from "react-dropzone";
import Colors from "../../utils/Colors";
import { TextSemiBold } from "./text";
import { addImageAsset } from "../redux/assetDetail/action";
import { useDispatch } from "react-redux";

interface Dropzone {
  addImage: any;
}

const Dropzone: FunctionComponent<Dropzone> = ({ addImage }) => {
  const inputFile: any = useRef(null);

  const dispatch = useDispatch();

  const onDrop = useCallback((acceptedFiles) => {
    dispatch(addImage(acceptedFiles));
  }, []);

  const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
    accept: "image/jpeg, image/png",
    onDrop,
  });

  return (
    <div
      onClick={() => inputFile.current.click()}
      className="container"
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        border: "none",
      }}
    >
      <div
        {...getRootProps({ className: "dropzone" })}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <input {...getInputProps()} id="dragdropImage" ref={inputFile} />
        <img src="/new/images/Icon feather-upload-cloud.svg" />
        <div className="mt-3">
          <TextSemiBold
            weight="300"
            color={Colors.mainBlackLight}
            fontSize="20px"
            align="center"
          >
            ลากไฟล์และวางที่นี่
          </TextSemiBold>
        </div>

        <div>
          <a>
            <TextSemiBold
              weight="300"
              color={Colors.mainBlackLight}
              fontSize="20px"
            >
              หรือ
              <TextSemiBold
                color={Colors.mainBlue}
                className="ms-1"
                fontSize="20px"
                weight="300"
                style={{ textDecoration: "underline" }}
              >
                คลิก
              </TextSemiBold>{" "}
              เพื่ออัปโหลดไฟล์
            </TextSemiBold>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Dropzone;
