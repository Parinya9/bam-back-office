import styled from "styled-components";
import Colors from "../../utils/Colors";

interface TextSemiBold {
  fontSize?: string;
  align?: string;
  color?: string;
  weight?: string;
  textDecoration?: string;
  mobileSize?: string;
}

interface TextBold {
  fontSize?: string;
  align?: string;
  color?: string;
  weight?: string;
  float?: string;
  mobileSize?: string;
  mobileAlign?: string;
  mobileFloat?: string;
}

interface FooterSpan {
  fontSize?: string;
  mobileSize?: string;
  weight?: string;
}
interface TextActivityBold {
  fontSize?: string;
  align?: string;
  color?: string;
  mobileSize?: string;
  weight?: string;
}

interface TextActivitySemiBold {
  fontSize?: string;
  align?: string;
  color?: string;
  mobileSize?: string;
  weight?: string;
}

export const TextSemiBold = styled.span<TextSemiBold>`
  font-size: ${(props) => props.fontSize || "30px"};
  text-align: ${(props) => props.align || "left"};
  color: ${(props) => props.color || Colors.mainDarkBlue};
  font-family: Kanit;
  font-weight: ${(props) => props.weight || "400"};
  word-break: break-word;

  text-decoration: ${(props) => props.textDecoration || ""};
  @media (max-width: 1024px) {
    font-size: ${(props) => props.mobileSize || "14px"};
  }
`;

export const TextBold = styled.span<TextBold>`
  font-size: ${(props) => props.fontSize || "30px"};
  text-align: ${(props) => props.align || "left"};
  color: ${(props) => props.color || Colors.mainDarkBlue};
  font-family: Kanit;
  font-weight: ${(props) => props.weight || "500"};
  float: ${(props) => props.float || "none"};
  @media (max-width: 1023px) {
    font-size: ${(props) => props.mobileSize || "14px"};
    text-align: ${(props) => props.mobileAlign || "left"};
    float: ${(props) => props.mobileFloat || "none"};
  }
`;

export const SubtitleFooterSpan = styled.span<FooterSpan>`
  font-size: ${(props) => props.fontSize || "18px"};
  font-weight: ${(props) => props.weight || "400"};
  color: ${(props) => props.color || "black"};
  font-family: Kanit;
  margin-top: 5px;
  display: block;
  @media (max-width: 1024px) {
    font-size: ${(props) => props.mobileSize || "14px"};
  }
`;

export const TitleFooterSpan = styled.span<FooterSpan>`
  font-size: ${(props) => props.fontSize || "24px"};
  font-weight: ${(props) => props.weight || "500"};
  color: ${(props) => props.color || "black"};
  display: block;
  font-family: Kanit;
  @media (max-width: 1024px) {
    display: flex;
    justify-content: center;
    font-size: ${(props) => props.mobileSize || "18px"};
  }
`;

export const FooterSpan = styled.span<FooterSpan>`
  font-size: ${(props) => props.fontSize || "16px"};
  font-weight: ${(props) => props.weight || "500"};
  color: ${(props) => props.color || "white"};
  font-family: Kanit;
  @media (max-width: 1024px) {
    justify-content: center;
    display: flex;
    font-size: ${(props) => props.mobileSize || "16px"};
  }
`;

export const TextActivitySemiBold = styled.span<TextActivitySemiBold>`
  font-size: ${(props) => props.fontSize || "30px"};
  text-align: ${(props) => props.align || "left"};
  color: ${(props) => props.color || "red"};
  font-family: Kanit;
  font-weight: ${(props) => props.weight || "400"};
  display: block;
  @media (max-width: 1024px) {
    font-size: ${(props) => props.mobileSize || "16px"};
  }
`;

export const TextActivityBold = styled.span<TextActivityBold>`
  font-size: ${(props) => props.fontSize || "30px"};
  text-align: ${(props) => props.align || "left"};
  color: ${(props) => props.color || "red"};
  font-family: Kanit;
  display: block;
  font-weight: ${(props) => props.weight || "500"};
  @media (max-width: 1024px) {
    font-size: ${(props) => props.mobileSize || "16px"};
  }
`;
