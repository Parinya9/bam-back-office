import { useEffect, FunctionComponent } from "react";
import Colors from "../../../utils/Colors";
import { Flex, Line } from "../layout";
import { TextSemiBold } from "../text";
import dayjs from "dayjs";
import { NavItem } from "react-bootstrap";
import Router from "next/router";

interface CardDetailNotificationType {
  title: any;
  date: any;
  total: any;
  read: boolean;
  marketCode: any;
  asset_id: any;
}

const CardDetailNotification: FunctionComponent<CardDetailNotificationType> = ({
  title,
  date,
  total,
  read,
  marketCode,
  asset_id,
}) => {
  const setTitle = (type: any) => {
    if (type == "ทรัพย์มาใหม่") {
      return "แจ้งเตือนทรัพย์เข้าใหม่";
    } else if (type == "ซ่อนทรัพย์") {
      return "แจ้งเตือนการซ่อนทรัพย์";
    } else {
      return "แจ้งเตือนการตรวจสอบทรัพย์สิน";
    }
  };
  const setSubTitle = (type: any, total: any) => {
    if (type == "ทรัพย์มาใหม่") {
      return `ทรัพย์เข้าใหม่ ${total}  รายการ`;
    } else if (type == "ซ่อนทรัพย์") {
      return `ซ่อนทรัพย์ ${total}  รายการ`;
    } else {
      return setSubTitleAsset(type);
    }
  };
  const assetLink = () => {
    return (
      <a
        href=""
        onClick={(e) => {
          e.preventDefault();
          Router.push({
            pathname: "/propertylist/propertydetail",
            query: {
              mode: "readonly",
              id: asset_id,
              market_code: marketCode,
            },
          });
        }}
      >
        {" "}
        {marketCode}
      </a>
    );
  };
  const setSubTitleAsset = (type: any) => {
    switch (type as any) {
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        return (
          <div>
            ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์ อนุมัติ รหัสทรัพย์สิน &nbsp;
            {assetLink()}
          </div>
        );
      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        return (
          <div>
            จนท.ฝ่ายการตลาด อนุมัติ รหัสทรัพย์สิน &nbsp;
            {assetLink()}
          </div>
        );
      case "ปฏิเสธทรัพย์มาใหม่":
        return (
          <div>
            ปฏิเสธการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์ &nbsp;
            {assetLink()}
          </div>
        );
      case "ปฏิเสธการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return (
          <div>
            จนท.ฝ่ายการตลาด ปฏิเสธการอนุมัติ รหัสทรัพย์สิน &nbsp;
            {assetLink()}
          </div>
        );
      case "ปฏิเสธการตรวจสอบ จนท.ฝ่ายการตลาด":
        return (
          <div>
            ผจก.ฝ่ายการตลาด ปฏิเสธการอนุมัติ รหัสทรัพย์สิน &nbsp;
            {assetLink()}
          </div>
        );
      case "ตรวจสอบแล้ว":
        return (
          <div>
            ผจก.ฝ่ายการตลาด การอนุมัติ รหัสทรัพย์สิน &nbsp;
            {assetLink()}
          </div>
        );
      case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return (
          <div>
            รายการทรัพย์สิน &nbsp;
            {assetLink()}
          </div>
        );
      default:
        return "";
    }
  };

  return (
    <>
      <div
        style={{
          backgroundColor: "white",
          flexDirection: "row",
          display: "grid",
          gridTemplateColumns: "1fr 5fr 5fr",
        }}
      >
        <Flex
          width="100%"
          height="100px"
          className="flex-column justify-content-start align-items-center"
        >
          {!read && (
            <img
              src="/new/images/Ellipse_red.svg"
              width="20px"
              height="20px"
              style={{ marginTop: "30px" }}
            />
          )}
        </Flex>
        <Flex
          className="flex-column justify-content-center px-2 py-2"
          width="100%"
          height="100px"
        >
          <TextSemiBold color={Colors.mainBlack} fontSize="18px">
            {setTitle(title)}
          </TextSemiBold>
          <TextSemiBold color={Colors.mainBlack} fontSize="18px" weight="200">
            {setSubTitle(title, total)}
          </TextSemiBold>
        </Flex>
        <Flex
          width="100%"
          height="100px"
          className="flex-column justify-content-center align-items-end px-3 py-2"
        >
          <TextSemiBold color={Colors.gray9b} fontSize="15px" weight="200">
            {dayjs(date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </TextSemiBold>
        </Flex>
      </div>
      <Line width="100%" color={Colors.gray9b} />
    </>
  );
};

export default CardDetailNotification;
