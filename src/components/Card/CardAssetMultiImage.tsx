import { FunctionComponent, useEffect, useState } from "react";
import { TextBold, TextSemiBold } from "../text";
import styled from "styled-components";
import Colors from "../../../utils/Colors";

import ModalEditImage from "../Modal/ModalEditImage";
import { useSelector, useDispatch } from "react-redux";

interface CardAssetImage {
  className?: string;
  detail: string;
  linkPath?: string;
  onClick?: () => any;
  ref?: any;
  id?: any;
  fileName?: any;
  saveEditImage: any;
  stateEditImage: any;
  editImage: any;
  mode: any;
  display?: any;
  setSortedList?: any;
}
const CardAssetImage: FunctionComponent<CardAssetImage> = ({
  className,
  detail,
  linkPath,
  ref,
  id,
  fileName,
  saveEditImage,
  stateEditImage,
  editImage,
  mode,
  display,
  setSortedList,
}) => {
  const [showTab, setShowTab] = useState(false);
  const [showImage, setShowImage] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [payload, setPayload] = useState(null as any);
  const [data, setData] = useState(null as any);
  const dispatch = useDispatch();

  const onEdit = (id: any, detail: any, linkPath: any) => {
    setData(payload);
    setShowModal(true);
  };

  const onSetDisplay = (id: any, display: any) => {
    setShowImage(!showImage);
    stateEditImage.map((item: any, index: any) => {
      if (index == id) {
        item.display = display;
      }
    });
    dispatch(saveEditImage(stateEditImage));
  };

  const onDelete = (id: any) => {
    let listImage: any = [];
    let count = 0;
    stateEditImage.map((item: any, index: any) => {
      if (index != id) {
        var data = {
          no: 0,
          url: "",
          name: "",
          detail: "",
          id: 0,
          order: 0,
          display: true,
        };
        data.no = count;
        data.id = count;
        data.url = item.url;
        data.name = item.name;
        data.detail = item.detail;
        data.display = item.display;
        data.order = count;
        count = count + 1;
        listImage.push(data);
      }
    });

    setSortedList(listImage);
    dispatch(saveEditImage(listImage));
  };

  useEffect(() => {
    setShowImage(display);
    setPayload({
      id,
      linkPath,
      detail,
      fileName,
      display,
    });
  }, [detail, fileName, display]);
  return (
    <RadiusBox className={className} key={id}>
      <div
        className=" d-flex flex-column justify-content-end px-2"
        style={{
          backgroundImage: `url(${linkPath})`,
          width: "100%",
          height: "220px",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          borderTopLeftRadius: "8px",
          borderTopRightRadius: "8px",
          boxShadow: !showImage ? "inset 0 0 0 2000px rgba(5, 0, 0, 0.25)" : "",
        }}
      >
        {!showImage && (
          <Circle>
            <TextBold color={Colors.mainWhite} fontSize="12px">
              ไม่แสดง
            </TextBold>
          </Circle>
        )}

        {showTab && (
          <div
            style={{
              display: "grid",
              backgroundColor: Colors.black0,
              height: "30px",
              gridTemplateColumns: "1fr 1fr 1fr",

              gridGap: "20px",
              borderRadius: "4px",
            }}
          >
            {!showImage && (
              <>
                <img
                  onClick={() => onSetDisplay(id, !showImage)}
                  src="/new/images/Icon-Inactive-eye.svg"
                  width="15px"
                  height="15px"
                  style={{ alignSelf: "center", justifySelf: "center" }}
                />
              </>
            )}

            {showImage && (
              <img
                onClick={() => onSetDisplay(id, !showImage)}
                src="/new/images/Icon-active-eye.svg"
                width="15px"
                height="15px"
                style={{ alignSelf: "center", justifySelf: "center" }}
              />
            )}

            <img
              onClick={() => onEdit(id, detail, linkPath)}
              src="/new/images/Edit.svg"
              width="15px"
              height="15px"
              style={{ alignSelf: "center", justifySelf: "center" }}
            />
            <img
              onClick={() => onDelete(id)}
              src="/new/images/Trash.svg"
              width="15px"
              height="15px"
              style={{ alignSelf: "center", justifySelf: "center" }}
            />
          </div>
        )}
      </div>

      <div style={{ display: "grid", gridTemplateColumns: "2.5fr 1fr" }}>
        <TextSemiBold
          fontSize="16px"
          color={Colors.mainBlackLight}
          className="mt-3 ms-3 mb-3"
        >
          {detail}
        </TextSemiBold>

        {mode != "readonly" && (
          <button
            type="button"
            className="d-flex flex-row justify-content-center align-items-center"
            style={{ backgroundColor: "transparent", border: "none" }}
            onClick={() => setShowTab(!showTab)}
          >
            <ButtonRounded />
            <ButtonRounded />
            <ButtonRounded />
          </button>
        )}
      </div>
      <ModalEditImage
        show={showModal}
        setShow={setShowModal}
        setDataMap={setData}
        data={payload}
        editImage={saveEditImage}
        stateEditImage={stateEditImage}
      />
    </RadiusBox>
  );
};

export default CardAssetImage;

interface RadiusBox {
  className?: string;
}

const RadiusBox = styled.div.attrs<RadiusBox>(() => ({
  className: "d-flex flex-column justify-content-start px-0 mt-4 py-0 mx-3",
}))`
  background-color: #fff;
  width: 220px;
  height: 250px;
  border-radius: 8px;
  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
  border: none;
`;

const ButtonRounded = styled.div.attrs(() => ({
  className: "ms-1",
}))`
  background-color: #c4c4c4;
  border-radius: 50%;
  width: 3px;
  height: 3px;
`;

const Circle = styled.div.attrs(() => ({
  className:
    "d-flex flex-row justify-content-center align-items-center align-self-center",
}))`
  height: 100px;
  width: 100px;
  background-color: black;
  border-radius: 50%;
  margin-bottom: 50px;
`;
