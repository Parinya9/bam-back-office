import { FunctionComponent } from "react";
import { Flex } from "../layout";
import { TextBold } from "../text";
import styled from "styled-components";
import Colors from "../../../utils/Colors";
import Router from "next/router";

interface CardHome {
  className?: string;
  title: string;
  detail: string;
  linkPath?: string;
}

const CardHome: FunctionComponent<CardHome> = ({
  className,
  title,
  detail,
  linkPath,
}) => {
  return (
    <RadiusBox
      className={className}
      onClick={() => Router.push(linkPath || "/")}
    >
      <Flex flex={0.2}>
        <img src="/new/images/Setting.svg" width="80px" height="80px" />
      </Flex>
      <Flex flex={0.6} className="flex-column ms-3">
        <TextBold fontSize="18px" color={Colors.mainBlack}>
          {title}
        </TextBold>
        <TextBold fontSize="14px" color={Colors.gray9b} className="mt-2">
          {detail}
        </TextBold>
      </Flex>
      <Flex flex={0.2} className="flex-row justify-content-end">
        <TextBold fontSize="4px" color={"rgba(155, 155, 155, 1)"}>
          {"\u2B24"}
        </TextBold>
        <TextBold fontSize="4px" color={"rgba(155, 155, 155, 1)"}>
          {"\u2B24"}
        </TextBold>
        <TextBold fontSize="4px" color={"rgba(155, 155, 155, 1)"}>
          {"\u2B24"}
        </TextBold>
      </Flex>
    </RadiusBox>
  );
};

export default CardHome;

interface RadiusBox {
  className?: string;
}

const RadiusBox = styled.button.attrs<RadiusBox>(() => ({
  className: "d-flex flex-row justify-content-start py-3 px-4 mt-4",
}))`
  background-color: #fff;
  width: 100%;
  height: 120px;
  border-radius: 20px;
  box-shadow: rgba(0, 0, 0, 0.1) 0 0 1px;
  border: none;
`;
