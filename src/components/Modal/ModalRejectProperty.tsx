import { useState, FunctionComponent, useEffect, useRef } from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../Input";
import styled from "styled-components";
import ButtonRightSideRadius from "../Button/ButtonRightSideRadius";
import { saveEditMapImage } from "../../redux/assetDetail/action";
import callApi from "../../pages/api/callApi";
import { useDispatch } from "react-redux";
import TextAreaRadius from "../textarea";
import Cookies from "universal-cookie";

interface ModalRejectProperty {
  show: boolean;
  setShow: any;
  id: any;
  statusApprove: any;
  market_code: any;
}
const cookies = new Cookies();
let role = cookies.get("role");
let firstname = cookies.get("firstname");
let lastname = cookies.get("lastname");
const ModalRejectProperty: FunctionComponent<ModalRejectProperty> = ({
  show,
  setShow,
  id,
  statusApprove,
  market_code,
}) => {
  const {
    control,
    getValues,
    formState: { errors },
  } = useForm({});

  const onSubmit = async () => {
    const notify = {
      asset_id: parseInt(id),
      market_code: market_code,
      roles: setRolesNoti(),
      type: setStateReject(),
    };
    let objLog = {
      asset_id: parseInt(id),
      status_approve: false,
      user: firstname + " " + lastname,
      position: role,
      title: statusApprove,
      note: getValues("desciption"),
    };

    if (setRolesNoti()) {
      const item = await callApi.apiPut(
        "property-detail/update",
        setRejectData()
      );
      await callApi.apiPost("history-log/addLog", objLog);
      const lis = await callApi.apiPost("notification/add", notify);
      setShow(false);
      window.location.reload();
    } else {
      await callApi.apiPost("history-log/addLog", objLog);
      const lis = await callApi.apiPost("notification/add", notify);
      console.log("mush have roles");
      setShow(false);
      window.location.reload();
    }
  };
  const setRejectData = () => {
    var objData = { status_approve: setState(), id: parseInt(id) };
    var desciption = getValues("desciption");
    switch (statusApprove as any) {
      case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        Object.assign(objData, { reason_head_sales: desciption });
        return objData;
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        Object.assign(objData, { reason_office_market: desciption });
        return objData;
      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        Object.assign(objData, { reason_head_market: desciption });
        return objData;
      default:
        return "";
    }
  };

  const setRolesNoti = () => {
    switch (statusApprove as any) {
      case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return "";
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        return "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์";

      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        return "จนท.ฝ่ายการตลาด";
      default:
        return "";
    }
  };
  const setState = () => {
    switch (statusApprove as any) {
      case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return "ปฏิเสธทรัพย์มาใหม่";
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        return "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์";
      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        return "รอการตรวจสอบ จนท.ฝ่ายการตลาด";
      default:
        return "";
    }
  };
  const setStateReject = () => {
    console.log("statusApprove", statusApprove);
    switch (statusApprove as any) {
      case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return "ปฏิเสธทรัพย์มาใหม่";
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        return "ปฏิเสธการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์";
      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        return "ปฏิเสธการตรวจสอบ จนท.ฝ่ายการตลาด";
      default:
        return "";
    }
  };
  const handleClose = () => {
    setShow(false);
  };
  // useEffect(() => {
  // }, []);
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <TextBold fontSize="24px">ไม่อนุมัติการตรวจทรัพย์</TextBold>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Flex className="flex-column " flex={0.49}>
            <TextSemiBold
              fontSize="16px"
              weight="400"
              color={Colors.mainBlackLight}
            >
              รายละเอียด
            </TextSemiBold>

            <Controller
              name="desciption"
              control={control}
              rules={{ required: false }}
              render={({ field }) => (
                <TextAreaRadius
                  width="100%"
                  borderWidth="1px"
                  borderColor="#CED4DA"
                  border="solid"
                  rows="3"
                  field={field}
                />
              )}
            />
            {errors.display_property_desc && (
              <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                กรุณากรอกข้อมูลให้ถูกต้อง
              </TextSemiBold>
            )}
          </Flex>
        </Modal.Body>
        <Modal.Footer>
          <Flex className="flex-row justify-content-center">
            <ButtonRadius
              type="button"
              title="บันทึก"
              onClick={() => onSubmit()}
              backgroundColor={Colors.mainYellow}
              color={Colors.mainDarkBlue}
              width="200px"
              fontSize="18px"
              className="mx-1  mt-1  mb-1 py-1 "
              border="none"
            />
          </Flex>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalRejectProperty;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
