import { useState, FunctionComponent } from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import ButtonRadiusIconL from "../Button/ButtonRadiusIconL";

interface ModalQrCode {
  show: boolean;
  setShow: any;
}

const ModalQrCode: FunctionComponent<ModalQrCode> = ({ show, setShow }) => {
  const {
    register,
    handleSubmit,
    control,
    watch,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: any) => {
    // "data", data;
    setShow(false);
    reset({});
  };

  const handleClose = () => {
    reset({});
    setShow(false);
  };

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title className="d-flex flex-row justify-content-center">
              <TextBold fontSize="32px" align="center">
                QR code ทรัพย์สิน
              </TextBold>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <RadiusBox>
              <img
                src="/new/images/qrcodemock.jpeg"
                width="100%"
                height="300px"
              />
            </RadiusBox>
            <Flex className="flex-row justify-content-center mt-3">
              <TextSemiBold fontSize="18px" color={Colors.mainBlackLight}>
                แสกน QR code เพื่อดูรายละเอียดทรัพย์สิน
              </TextSemiBold>
            </Flex>
          </Modal.Body>
          <Modal.Footer>
            <Flex className="flex-row justify-content-center">
              <ButtonRadiusIconL
                backgroundColor="white"
                title="ดาวน์โหลด QR code"
                border="solid"
                borderColor={Colors.mainBlue}
                fontSize="17px"
                borderWidth="1px"
                className="px-3 ms-2 mx-1 mt-1 mt-3"
                color={Colors.mainBlue}
                iconPath="new/images/Sign_in_squre.svg"
              />
            </Flex>
          </Modal.Footer>
        </form>
      </Modal>
    </>
  );
};

export default ModalQrCode;

interface RadiusBox {
  className?: string;
}

const RadiusBox = styled.button.attrs<RadiusBox>(() => ({
  className:
    "d-flex flex-column justify-content-center align-items-center px-0 mt-4 py-0",
}))`
  background-color: #fff;
  width: 100%;
  height: auto;
  border-radius: 8px;
  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
  border: none;
`;
