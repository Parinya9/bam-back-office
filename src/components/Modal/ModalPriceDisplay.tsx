import {
  useState,
  FunctionComponent,
  useEffect,
  useRef,
  forwardRef,
} from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import callApi from "../../pages/api/callApi";
import TextAreaRadius from "../textarea";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
interface ModalPriceDisplay {
  show: boolean;
  setShow: any;
  id: any;
  display_price: any;
  setDisplayPrice: any;
}
interface CustomInput {
  onClick: any;
  value: string;
  placeholder: string;
}

const CustomInput: FunctionComponent<CustomInput> = forwardRef(
  ({ value, placeholder, onClick }, ref: any) => {
    return (
      <CustomDatePickDiv
        style={{
          background: "white",
          marginTop: "0.5em",
        }}
      >
        <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
          {value || placeholder}

          <img
            src="/new/images/Calendar_navyblue.svg"
            width="25px"
            height="25px"
            style={{ float: "right", marginRight: "10px" }}
          />
        </label>
      </CustomDatePickDiv>
    );
  }
);
CustomInput.displayName = "CustomInput";
const ModalPriceDisplay: FunctionComponent<ModalPriceDisplay> = ({
  show,
  setShow,
  id,
  display_price,
  setDisplayPrice,
}) => {
  const {
    control,
    setValue,
    getValues,
    reset,
    formState: { errors },
  } = useForm({});
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  const onSubmit = async () => {
    var display_center_price_desc = getValues("display_center_price_desc");
    const objEdit = {
      display_center_price_desc: display_center_price_desc,
      status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
      display_price: "false",
      display_center_price_startdate:
        getValues("display_center_price_startdate") != undefined
          ? setFormatDate(getValues("display_center_price_startdate"))
          : getValues("display_center_price_startdate"),
      display_center_price_enddate:
        getValues("display_center_price_enddate") != undefined
          ? setFormatDate(getValues("display_center_price_enddate"))
          : getValues("display_center_price_enddate"),
      id: parseInt(id),
    };
    const item = await callApi.apiPut("property-detail/update", objEdit);
    setShow(false);
  };

  const handleClose = () => {
    setShow(false);
    setDisplayPrice(true);
  };
  useEffect(() => {}, []);
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <TextBold fontSize="24px">ไม่แสดงทรัพย์บนหน้าเว็บไซต์</TextBold>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row ">
            <div className="col-6">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                วันที่เริ่มต้น
              </TextSemiBold>
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="display_center_price_startdate"
                  rules={{ required: false }}
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        dateFormat="dd/MM/yyyy"
                        placeholderText="DD/MM/YYYY"
                        value={value || ""}
                        selected={value}
                        onChange={(date) => {
                          onChange(date);
                        }}
                        customInput={
                          <CustomInput
                            value={value}
                            placeholder="DD/MM/YYYY"
                            onClick={onclick}
                          />
                        }
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
            <div className="col-6">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                วันที่สิ้นสุด
              </TextSemiBold>
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="display_center_price_enddate"
                  rules={{ required: false }}
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        dateFormat="dd/MM/yyyy"
                        placeholderText="DD/MM/YYYY"
                        value={value || ""}
                        selected={value}
                        onChange={(date) => {
                          onChange(date);
                        }}
                        customInput={
                          <CustomInput
                            value={value}
                            placeholder="DD/MM/YYYY"
                            onClick={onclick}
                          />
                        }
                      />
                      {errors &&
                        errors[name] &&
                        errors[name].type === "required" && (
                          <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                            กรุณากรอกข้อมูลให้ถูกต้อง
                          </TextSemiBold>
                        )}
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                รายละเอียด
              </TextSemiBold>

              <Controller
                name="display_center_price_desc"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <TextAreaRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    rows="3"
                    field={field}
                  />
                )}
              />
              {errors.display_property_desc && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Flex className="flex-row justify-content-center">
            <ButtonRadius
              type="button"
              title="บันทึก"
              onClick={() => onSubmit()}
              backgroundColor={Colors.mainYellow}
              color={Colors.mainDarkBlue}
              width="250px"
              fontSize="18px"
              className="mx-1  mt-1  mb-1 py-1 "
              border="none"
            />
          </Flex>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalPriceDisplay;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
