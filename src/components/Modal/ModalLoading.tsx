import { FunctionComponent } from "react";
import { Modal } from "react-bootstrap";

interface ModalLoading {
  visible: boolean;
}
const ModalLoading: FunctionComponent<ModalLoading> = ({ visible }) => {
  return (
    <Modal
      show={visible}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <img src="/new/images/loading.gif" width="100%" />
      </Modal.Body>
    </Modal>
  );
};

export default ModalLoading;
