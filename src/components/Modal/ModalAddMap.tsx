import { useState, FunctionComponent, useEffect, useRef } from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import styled from "styled-components";
import ButtonRightSideRadius from "../Button/ButtonRightSideRadius";
import { saveEditMapImage } from "../../redux/assetDetail/action";
import callApi from "../../pages/api/callApi";
import { useDispatch } from "react-redux";

interface ModalAddMap {
  show: boolean;
  setShow: any;
  setDataMap: any;
  flagTab: any;
}

const ModalAddMap: FunctionComponent<ModalAddMap> = ({
  show,
  setShow,
  setDataMap,
  flagTab,
}) => {
  const {
    register,
    handleSubmit,
    control,
    watch,
    getValues,
    reset,
    setValue,
    formState: { errors },
  } = useForm({});

  const [img, setImg] = useState(null as any);
  const [errorImg, setErrorImg] = useState(false);

  const inputImg: any = useRef<HTMLInputElement>(null);
  const watchImg = watch("uploadImg");
  const dispatch = useDispatch();

  const handleImg = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      // 10485760
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue("uploadImg", {
          name: "",
        });
        setImg("");
        setErrorImg(true);
      } else {
        var listType = ["image/png", "image/jpg", "image/jpeg"];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setImg(event.target.files[0]);
          setErrorImg(false);
        } else {
          alert("Incorrect Type File (Support png , jpg and jpeg only )");
          setValue("uploadImg", "");
          setImg("");
          setErrorImg(true);
        }
      }
    }
  };

  const onSubmit = async () => {
    if (!errorImg && img != null && img != "") {
      flagTab();
      var description = getValues("description");
      var formData = new FormData();
      var imgMap = [];
      formData.append("files", img);
      const item = await callApi.apiPost(
        "property-detail/uploadMulti",
        formData
      );
      imgMap.push({
        detail: description,
        name: item[0].name,
        url: item[0].url,
        display: true,
        id: 0,
      });
      setDataMap(imgMap);
      dispatch(saveEditMapImage(imgMap));
      setShow(false);
    } else {
      setErrorImg(true);
    }
  };
  useEffect(() => {
    setErrorImg(false);
  }, []);
  const handleClose = () => {
    reset({});
    setShow(false);
  };

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title
            style={{
              marginLeft: "30%",
            }}
          >
            <TextBold fontSize="24px">เพิ่มรูปภาพแผนที่</TextBold>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Flex className="flex-column" flex={0.49}>
            <TextSemiBold
              fontSize="17px"
              weight="normal"
              color={Colors.mainBlackLight}
            >
              เลือกไฟล์ &nbsp;
              <TextSemiBold color={Colors.mainRed} fontSize="16px">
                *
              </TextSemiBold>
            </TextSemiBold>

            <RadiusBox>
              <Flex widthMobile="100%" className="justify-content-between">
                <Flex className="ms-3 mt-1">{watchImg?.name || ""}</Flex>
                <ButtonRightSideRadius
                  onClick={() => inputImg?.current.click()}
                  backgroundColor={Colors.mainBlue}
                  title="เลือกไฟล์"
                  width="20%"
                  height="100%"
                  color={Colors.mainWhite}
                />

                <Controller
                  name="uploadImg"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange } }) => (
                    <input
                      onChange={(e: any) => {
                        onChange(e.target.files[0]);
                        handleImg(e);
                      }}
                      type="file"
                      accept="image/png, image/jpg, image/jpeg"
                      style={{ display: "none" }}
                      id="uploadImg"
                      ref={inputImg}
                    />
                  )}
                />
              </Flex>
            </RadiusBox>
            {errorImg && (
              <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                กรุณากรอกข้อมูลให้ถูกต้อง
              </TextSemiBold>
            )}
          </Flex>
          <Flex className="flex-column mt-3" flex={0.49}>
            <TextSemiBold
              fontSize="16px"
              weight="400"
              color={Colors.mainBlackLight}
            >
              คำอธิบาย
            </TextSemiBold>

            <Controller
              name="description"
              control={control}
              render={({ field }) => (
                <InputRadius
                  width="100%"
                  borderWidth="1px"
                  borderColor="#CED4DA"
                  border="solid"
                  field={field}
                />
              )}
            />
          </Flex>
        </Modal.Body>
        <Modal.Footer>
          <Flex className="flex-row justify-content-center">
            <ButtonRadius
              type="button"
              title="บันทึก"
              onClick={() => onSubmit()}
              backgroundColor={Colors.mainYellow}
              color={Colors.mainDarkBlue}
              width="200px"
              fontSize="18px"
              className="mx-1 mt-1 mt-3 py-2 r"
              border="none"
            />
          </Flex>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalAddMap;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
