import { useState, FunctionComponent, useEffect, useRef } from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../Input";
import styled from "styled-components";
import ButtonRightSideRadius from "../Button/ButtonRightSideRadius";
import { saveEditMapImage } from "../../redux/assetDetail/action";
import callApi from "../../pages/api/callApi";
import { useDispatch } from "react-redux";
import TextAreaRadius from "../textarea";
import { apiSyncAssetDetail } from "../../pages/api/elsSync";
import ModalLoading from "../../components/Modal/ModalLoading";

interface ModalPropertyDisplay {
  show: boolean;
  setShow: any;
  setDisplayProperty: any;
  id: any;
  display_property: any;
  reload: any;
  fetchData: any;
  currentPage: any;
}

const ModalPropertyDisplay: FunctionComponent<ModalPropertyDisplay> = ({
  show,
  setShow,
  id,
  display_property,
  reload,
  fetchData,
  currentPage,
  setDisplayProperty,
}) => {
  const {
    control,
    setValue,
    getValues,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      display_property_desc: "",
    },
  });
  const [visible, setVisible] = useState(false);

  const onSubmit = async () => {
    setVisible(true);
    var display_property_desc = getValues("display_property_desc");
    const objEdit = {
      display_property_desc: display_property_desc,
      display_property: display_property ? "true" : "false",
      status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
      id: parseInt(id),
    };
    const notify = {
      asset_id: "",
      market_code: "",
      roles: "Manager_Sale",
      type: "ซ่อนทรัพย์",
    };

    const item = await callApi.apiPut("property-detail/update", objEdit);
    const lis = await callApi.apiPost("notification/add", notify);
    const response = await apiSyncAssetDetail("api/asset-detail");
    if (response.status === "success") {
      alert("Update Success");
      setVisible(false);
    } else {
      alert("Cant'not update asset");
      setVisible(false);
    }
    // setDesciption(display_property_desc);
    setShow(false);

    if (reload) {
      fetchData(currentPage, 10);
    }
  };

  const handleClose = () => {
    setShow(false);
    setDisplayProperty(true);
  };
  useEffect(() => {}, []);
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <form onSubmit={handleSubmit(onSubmit)} id="dddd">
          <Modal.Header closeButton>
            <Modal.Title>
              <TextBold fontSize="24px">ไม่แสดงทรัพย์บนหน้าเว็บไซต์</TextBold>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Flex className="flex-column " flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                รายละเอียด{" "}
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  &nbsp; *
                </TextSemiBold>
              </TextSemiBold>

              <Controller
                name="display_property_desc"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <TextAreaRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    rows="3"
                    field={field}
                  />
                )}
              />
              {errors.display_property_desc && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Modal.Body>
          <Modal.Footer>
            <Flex className="flex-row justify-content-center">
              <ButtonRadius
                type="submit"
                title="บันทึก"
                backgroundColor={Colors.mainYellow}
                color={Colors.mainDarkBlue}
                width="200px"
                fontSize="18px"
                className="mx-1  mt-1  mb-1 py-1 "
                border="none"
              />
            </Flex>
          </Modal.Footer>
        </form>
      </Modal>
      <ModalLoading visible={visible} />
    </>
  );
};

export default ModalPropertyDisplay;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
