import { useEffect, useState, FunctionComponent } from "react";

import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import callApi from "../../pages/api/callApi";

interface ModalPopularSearch {
  show: boolean;
  setShow: any;
  fetchData: any;
  mode: string;
  search_name: string;
  url: string;
  id: string;
}

const ModalPopularSearch: FunctionComponent<ModalPopularSearch> = ({
  show,
  setShow,
  mode,
  search_name,
  url,
  fetchData,
  id,
}) => {
  const {
    register,
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    mode == "edit" ? editData(data) : addData(data);
  };
  const editData = async (data: any) => {
    const objEdit = {
      search_name: data.search_name,
      url: data.url,
    };
    const item = await callApi.apiPut("popularsearch/" + parseInt(id), objEdit);
    if (item.affected == 1) {
      setShow(false);
      fetchData();
      reset({});
    } else {
      alert("Duplicate Word");
    }
  };
  const addData = async (data: any) => {
    const item = await callApi.apiPost("popularsearch", data);
    if (item.id != undefined) {
      setShow(false);
      fetchData();
      reset({});
    } else {
      alert("Duplicate word");
    }
  };
  const handleClose = () => {
    reset({});
    setShow(false);
  };
  const setData = () => {
    if (mode == "edit") {
      setValue("search_name", search_name);
      setValue("url", url);
    } else {
      setValue("search_name", "");
      setValue("url", "");
    }
  };
  useEffect(() => {
    setData();
  }, [mode, show]);
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>
              <TextBold fontSize="32px">สร้าง - คำค้นหา ยอดนิยม</TextBold>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                ชื่อคำค้นหา &nbsp;
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
              </TextSemiBold>

              <Controller
                name="search_name"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
              {errors.search_name && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                URL &nbsp;
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
              </TextSemiBold>

              <Controller
                name="url"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    {...register("url", {
                      pattern:
                        /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi,
                    })}
                  />
                )}
              />
              {errors.url && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Modal.Body>
          <Modal.Footer>
            <Flex className="flex-row justify-content-center">
              <ButtonRadius
                type="submit"
                title="บันทึก"
                backgroundColor={Colors.mainYellow}
                color={Colors.mainDarkBlue}
                width="200px"
                fontSize="18px"
                className="mx-1 mt-1 mt-3 py-2 r"
                border="none"
              />
            </Flex>
          </Modal.Footer>
        </form>
      </Modal>
    </>
  );
};

export default ModalPopularSearch;
