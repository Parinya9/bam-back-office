import {
  useState,
  FunctionComponent,
  useEffect,
  forwardRef,
  useRef,
} from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import styled from "styled-components";
import ButtonRightSideRadius from "../Button/ButtonRightSideRadius";
import dayjs from "dayjs";
import callApi from "../../pages/api/callApi";
import DatePicker from "../../components/Datepicker/DatePicker";

interface ModalPopularSearch {
  show: boolean;
  setShow: any;
  edit: boolean;
  fetchData: any;
  mode: string;
  itemEdit: any;
}

const ModalPopularSearch: FunctionComponent<ModalPopularSearch> = ({
  show,
  setShow,
  edit,
  fetchData,
  mode,
  itemEdit,
}) => {
  // const [inputEvent, setInputEvent] = useState(null as any);
  // const [inputPdf, setInputPdf] = useState(null as any);
  const [image, setImage] = useState(null as any);
  const [pdf, setPdf] = useState(null as any);
  const inputImg: any = useRef<HTMLInputElement>(null);
  const inputPdf: any = useRef<HTMLInputElement>(null);

  const {
    register,
    handleSubmit,
    control,
    watch,
    reset,
    setValue,
    getValues,
    formState: { errors },
  } = useForm();

  const watchImageUpload = watch("uploadImg");
  const watchPDF = watch("uploadPdf");

  const onSubmit = async (data: any) => {
    mode == "edit" ? editData(data) : addData(data);
  };

  const setFormData = (formData: any, key: any, data: any, type: any) => {
    if (type == "date") {
      if (data != undefined) {
        formData.append(key, setFormatDate(data));
      }
    } else if (type == "dropdown") {
      if (data != undefined) {
        formData.append(key, data.value);
      }
    } else {
      formData.append(key, data);
    }
  };
  const genFormData = async (formData: any, data: any) => {
    let listDate = ["start_date", "end_date"];
    let listString = ["link"];

    console.log("image ", image);
    image != null ? formData.append("image", image, image.name) : "";
    image != null ? formData.append("image_name", image.name) : "";
    pdf != null ? formData.append("pdf", pdf, pdf.name) : "";

    // mode == "Edit" ? formData.append("id", id) : "";
    listDate.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "date");
    });

    listString.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "string");
    });
  };
  const addData = async (data: any) => {
    let formData = new FormData();
    genFormData(formData, data);
    const item = await callApi.apiPost("banner/", formData);
    if (item.error) {
      alert("Limit 5 banner slide ");
    } else {
      reset({});
      setShow(false);
      fetchData();
    }
  };
  const editData = async (data: any) => {
    let formData = new FormData();
    genFormData(formData, data);
    await callApi.apiPut("banner/" + parseInt(itemEdit.id), formData);
    setShow(false);
    fetchData();
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    return dayjs(date).format("YYYY-MM-DD");
  };
  const handleClose = () => {
    reset({});
    setShow(false);
  };

  const handleImage = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      // 10485760
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue("uploadImg", {
          name: "",
        });
        setImage("");
      } else {
        var listType = ["image/png", "image/jpg", "image/jpeg"];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setImage(event.target.files[0]);
        } else {
          alert("Incorrect Type File (Support png , jpg and jpeg only )");
          setValue("uploadImg", "");
          setImage("");
        }
      }
      // else {
      //   setImage(event.target.files[0]);
      // }
    }
  };

  const handlePdf = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      if (event.target.files[0].type == "application/pdf") {
        setPdf(event.target.files[0]);
      } else {
        alert("Incorrect Type File (Support pdf only )");
        setPdf("");
        setValue("uploadPdf", "");
      }
    }
  };
  const setData = () => {
    if (mode == "edit" && show) {
      console.log(itemEdit.img_name, itemEdit.pdf_name);
      setValue("link", itemEdit.link);
      setValue("start_date", itemEdit.start_date);
      setValue("end_date", itemEdit.end_date);
      itemEdit.img_name == undefined
        ? setValue("uploadImg", "")
        : setValue("uploadImg", { name: itemEdit.img_name });
      itemEdit.pdf_name == undefined
        ? setValue("uploadPdf", "")
        : setValue("uploadPdf", { name: itemEdit.pdf_name });
    } else {
      setValue("link", "");
      setValue("start_date", "");
      setValue("end_date", "");
      setValue("uploadImg", "");
      setValue("uploadPdf", "");
    }
  };
  useEffect(() => {
    // setValue("start_date", "");
    // setValue("end_date", "");

    setData();
  }, [show]);
  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv>
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";

  return (
    <>
      <Modal
        size="lg"
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>
              <TextBold fontSize="32px">
                {edit ? "แก้ไข - Banner slide" : "สร้าง - Banner slide"}{" "}
              </TextBold>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="17px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                เลือกไฟล์{" "}
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  &nbsp; *
                </TextSemiBold>
              </TextSemiBold>

              <RadiusBox>
                <Flex widthMobile="100%" className="justify-content-between">
                  <Flex className="ms-3 mt-1">
                    {watchImageUpload?.name || ""}
                  </Flex>
                  <ButtonRightSideRadius
                    onClick={() => inputImg.current.click()}
                    backgroundColor={Colors.mainBlue}
                    title="เลือกไฟล์"
                    width="20%"
                    height="100%"
                    color={Colors.mainWhite}
                  />

                  <Controller
                    name="uploadImg"
                    control={control}
                    rules={{ required: true }}
                    render={({ field: { onChange } }) => (
                      <input
                        onChange={(e: any) => {
                          onChange(e.target.files[0]);
                          handleImage(e);
                        }}
                        type="file"
                        accept="image/png, image/jpg, image/jpeg"
                        style={{ display: "none" }}
                        id="uploadImg"
                        ref={inputImg}
                      />
                    )}
                  />
                </Flex>
              </RadiusBox>
              <TextSemiBold
                weight="300"
                fontSize="16px"
                className="mt-2"
                color={Colors.gray9b}
              >
                ไฟล์นามสกุล .png .jpeg ,jpg เท่านั้น และขนาดไฟล์ต้องไม่เกิน 10
                MB รองรับภาพขนาด 1920 x 650 px
              </TextSemiBold>

              {errors.uploadImg && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>

            <Flex className="flex-column mt-4" flex={0.49}>
              <TextSemiBold
                fontSize="17px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                เลือกไฟล์แนบ PDF
              </TextSemiBold>

              <RadiusBox>
                <Flex widthMobile="100%" className="justify-content-between">
                  <Flex className="ms-3 mt-1">{watchPDF?.name || ""}</Flex>
                  <ButtonRightSideRadius
                    onClick={() => inputPdf.current.click()}
                    backgroundColor={Colors.mainBlue}
                    title="เลือกไฟล์"
                    width="20%"
                    height="100%"
                    color={Colors.mainWhite}
                  />

                  <Controller
                    name="uploadPdf"
                    control={control}
                    render={({ field: { onChange } }) => (
                      <input
                        onChange={(e: any) => {
                          onChange(e.target.files[0]);
                          handlePdf(e);
                        }}
                        type="file"
                        accept="application/pdf"
                        style={{ display: "none" }}
                        id="uploadPdf"
                        ref={inputPdf}
                      />
                    )}
                  />
                </Flex>
              </RadiusBox>
              {errors.uploadPdf && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>

            <Flex className="flex-column mt-4" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                เปิดลิงค์ (URL)
              </TextSemiBold>

              <Controller
                name="link"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    placeHolder="ตัวอย่าง http://www.google.com"
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    height="40px"
                    {...register("link", {
                      pattern:
                        /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi,
                    })}
                  />
                )}
              />
              {errors.link && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <div
              className="d-grid gap-5"
              style={{ gridTemplateColumns: "47% 47%" }}
            >
              <Flex className="flex-column mt-4">
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  วันที่เริ่มต้น
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    &nbsp; *
                  </TextSemiBold>
                </TextSemiBold>
                <div className="mt-1 ">
                  <Controller
                    control={control}
                    name="start_date"
                    rules={{ required: true }}
                    render={({
                      field: { onChange, name, value },
                      formState: { errors },
                    }) => (
                      <>
                        <DatePicker
                          value={value || ""}
                          onchange={(date: any) => {
                            console.log("date :", date);
                            onChange(date);
                          }}
                        />

                        {errors &&
                          errors[name] &&
                          errors[name].type === "required" && (
                            <TextSemiBold
                              fontSize="14px"
                              color={Colors.mainRed}
                            >
                              กรุณากรอกข้อมูลให้ถูกต้อง
                            </TextSemiBold>
                          )}
                      </>
                    )}
                  />
                </div>
              </Flex>
              <Flex className="flex-column mt-4">
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  วันที่สิ้นสุด
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    &nbsp; *
                  </TextSemiBold>
                </TextSemiBold>
                <div className="mt-1 ">
                  <Controller
                    control={control}
                    name="end_date"
                    rules={{ required: true }}
                    render={({
                      field: { onChange, name, value },
                      formState: { errors },
                    }) => (
                      <>
                        <DatePicker
                          value={value || ""}
                          onchange={(date: any) => {
                            onChange(date);
                          }}
                        />

                        {errors &&
                          errors[name] &&
                          errors[name].type === "required" && (
                            <TextSemiBold
                              fontSize="14px"
                              color={Colors.mainRed}
                            >
                              กรุณากรอกข้อมูลให้ถูกต้อง
                            </TextSemiBold>
                          )}
                      </>
                    )}
                  />
                </div>
              </Flex>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Flex className="flex-row justify-content-center">
              <ButtonRadius
                type="submit"
                title="บันทึก"
                backgroundColor={Colors.mainYellow}
                color={Colors.mainDarkBlue}
                width="200px"
                fontSize="18px"
                className="mx-1 mt-1 mt-3 py-2 r"
                border="none"
              />
            </Flex>
          </Modal.Footer>
        </form>
      </Modal>
    </>
  );
};

export default ModalPopularSearch;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;

// const DatePickerDiv = styled.div`
//   position: relative;
// `;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
