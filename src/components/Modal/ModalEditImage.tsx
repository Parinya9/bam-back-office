import { useState, FunctionComponent, useEffect, useRef } from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import styled from "styled-components";
import ButtonRightSideRadius from "../Button/ButtonRightSideRadius";
import { useDispatch, useSelector } from "react-redux";
import callApi from "../../pages/api/callApi";

interface ModalEditImage {
  show: boolean;
  setShow: any;
  setDataMap: any;
  data: any;
  editImage: any;
  stateEditImage: any;
}

const ModalEditImage: FunctionComponent<ModalEditImage> = ({
  show,
  setShow,
  setDataMap,
  data,
  editImage,
  stateEditImage,
}) => {
  const dispatch = useDispatch();

  const {
    register: register2,
    handleSubmit: handleSubmit2,
    control,
    watch,
    reset,
    formState: { errors: errors2 },
    setValue,
    getValues,
  } = useForm({});

  const [uploadFile, setUploadFile] = useState({} as any);
  const [errorFile, setErrorFile] = useState(false);
  const [dataEdit, setDataEdit] = useState({} as any);
  const [file, setFile] = useState(null as any);
  const inputFile: any = useRef<HTMLInputElement>(null);

  const watchImageUpload: any = watch("uploadFile");
  useEffect(() => {
    setDataEdit(data);
    setValue("description", data?.detail);
    setValue("uploadFile", { name: data?.fileName });
  }, [data, show]);

  const onSubmit = async () => {
    var uploadFile = getValues("uploadFile");
    var description = getValues("description");
    if (!errorFile && (file != null || file != "")) {
      if (uploadFile?.size != undefined) {
        var formData = new FormData();
        formData.append("files", uploadFile);
        const item = await callApi.apiPost(
          "property-detail/uploadMulti",
          formData
        );

        stateEditImage.map((data: any, index: any) => {
          if (dataEdit.id == index) {
            data.detail = description;
            data.linkPath = item[0].url;
            data.name = item[0].name;
            data.url = item[0].url;
          }
        });
        dispatch(editImage(stateEditImage));
      } else {
        stateEditImage.map((item: any, index: any) => {
          if (dataEdit.id == index) {
            item.detail = description;
          }
        });
        dispatch(editImage(stateEditImage));
      }

      setShow(false);
      reset({});
    }
  };
  const handleClose = () => {
    reset({});
    setShow(false);
  };
  const handleFile = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      // 10485760
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue("uploadFile", {
          name: "",
        });
        setFile("");
        setErrorFile(true);
      } else {
        var listType = ["image/png", "image/jpg", "image/jpeg"];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setFile(event.target.files[0]);
          setErrorFile(false);
        } else {
          alert("Incorrect Type File (Support png , jpg and jpeg only )");
          setValue("uploadFile", "");
          setFile("");
          setErrorFile(true);
        }
      }
    }
  };
  useEffect(() => {
    setErrorFile(false);
  }, []);
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title
            style={{
              marginLeft: "30%",
            }}
          >
            <TextBold fontSize="24px">แก้ไข - รูป</TextBold>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Flex className="flex-column" flex={0.49}>
            <TextSemiBold
              fontSize="17px"
              weight="normal"
              color={Colors.mainBlackLight}
            >
              เลือกไฟล์ &nbsp;
              <TextSemiBold color={Colors.mainRed} fontSize="16px">
                *
              </TextSemiBold>
            </TextSemiBold>

            <RadiusBox>
              <Flex widthMobile="100%" className="justify-content-between">
                <Flex className="ms-3 mt-1">
                  {watchImageUpload?.name || ""}
                </Flex>
                <ButtonRightSideRadius
                  onClick={() => inputFile?.current.click()}
                  backgroundColor={Colors.mainBlue}
                  title="เลือกไฟล์"
                  width="20%"
                  height="100%"
                  color={Colors.mainWhite}
                />
                <Controller
                  name="uploadFile"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange } }) => (
                    <input
                      onChange={(e: any) => {
                        onChange(e.target.files[0]);
                        handleFile(e);
                      }}
                      type="file"
                      style={{ display: "none" }}
                      id="uploadFile"
                      accept="image/png, image/jpg, image/jpeg"
                      ref={inputFile}
                    />
                  )}
                />
              </Flex>
            </RadiusBox>
            {errorFile && (
              <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                กรุณากรอกข้อมูลให้ถูกต้อง
              </TextSemiBold>
            )}
          </Flex>
          <Flex className="flex-column mt-3" flex={0.49}>
            <TextSemiBold
              fontSize="16px"
              weight="400"
              color={Colors.mainBlackLight}
            >
              คำอธิบาย
            </TextSemiBold>

            <Controller
              name="description"
              control={control}
              rules={{ required: true }}
              render={({ field }) => (
                <InputRadius
                  width="100%"
                  borderWidth="1px"
                  borderColor="#CED4DA"
                  border="solid"
                  field={field}
                />
              )}
            />
            {errors2.description && (
              <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                กรุณากรอกข้อมูลให้ถูกต้อง
              </TextSemiBold>
            )}
          </Flex>
        </Modal.Body>
        <Modal.Footer>
          <Flex className="flex-row justify-content-center">
            <ButtonRadius
              title="บันทึก"
              onClick={() => {
                onSubmit();
              }}
              backgroundColor={Colors.mainYellow}
              color={Colors.mainDarkBlue}
              width="200px"
              fontSize="18px"
              className="mx-1 mt-1 mt-3 py-2 r"
              border="none"
            />
          </Flex>
        </Modal.Footer>
        {/* </form> */}
      </Modal>
    </>
  );
};

export default ModalEditImage;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
