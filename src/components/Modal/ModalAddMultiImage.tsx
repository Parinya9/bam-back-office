import { useState, FunctionComponent, useEffect, useRef } from "react";
import { Modal } from "react-bootstrap";
import { TextBold, TextSemiBold } from "../text";
import ButtonRadius from "../Button/ButtonRadius";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import styled from "styled-components";
import DropZoneImage from "../DropZoneImage";
import { useSelector, useDispatch } from "react-redux";

interface ModalAddMultiImage {
  show: boolean;
  setShow: any;
  setDataAsset: any;
  onSubmit: Function;
  onDelete: (data: any) => any;
  imageData: Array<any>;
  addImage: (data: any) => any;
}

const ModalAddMultiImage: FunctionComponent<ModalAddMultiImage> = ({
  show,
  setShow,
  setDataAsset,
  onSubmit,
  onDelete,
  imageData,
  addImage,
}) => {
  const handleClose = () => {
    setShow(false);
  };

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
        size="lg"
        contentClassName="custom-modal-style"
      >
        <Modal.Header closeButton>
          <Modal.Title
            style={{
              marginLeft: "30%",
            }}
          >
            <TextBold fontSize="24px">อัลบั้มรูปภาพทรัพย์</TextBold>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "20px",
            }}
          >
            <RadiusBoxDash style={{ backgroundColor: Colors.lightGrayF6 }}>
              <DropZoneImage addImage={addImage} />
            </RadiusBoxDash>
            <RadiusBoxNormal style={{ backgroundColor: Colors.mainWhite }}>
              <Flex
                className="flex-column mt-3"
                style={{ overflowY: "scroll" }}
              >
                <TextBold
                  color={"rgba(58, 58, 58, 1)"}
                  fontSize="18px"
                  className="ms-3"
                >
                  อัปโหลด
                </TextBold>
                {imageData?.map((item: any, index: number) => {
                  return (
                    <div
                      key={index}
                      style={{
                        display: "grid",
                        gridTemplateColumns: "1fr 3fr 1fr",
                        alignItems: "center",
                      }}
                      className="px-3"
                    >
                      <img
                        src="/new/images/Icon feather-check-circle.svg"
                        width="25px"
                        height="25px"
                      />
                      <Flex className="flex-column">
                        <TextBold fontSize="16px">{item.name}</TextBold>
                        <TextBold fontSize="16px" color={Colors.gray9b}>
                          {(item.size / (1024 * 1024)).toFixed(2)} MB
                        </TextBold>
                      </Flex>
                      <button
                        type="button"
                        style={{
                          backgroundColor: "transparent",
                          border: "none",
                        }}
                        onClick={() => onDelete(item.name)}
                      >
                        <img
                          src="/new/images/closeGray.svg"
                          width="18px"
                          height="18px"
                          style={{ justifySelf: "end" }}
                        />
                      </button>
                    </div>
                  );
                })}
              </Flex>
            </RadiusBoxNormal>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Flex className="flex-row justify-content-center">
            <ButtonRadius
              onClick={() => {
                onSubmit(imageData);
                setShow(false);
              }}
              type="button"
              title="บันทึก"
              backgroundColor={Colors.mainYellow}
              color={Colors.mainDarkBlue}
              width="200px"
              fontSize="18px"
              className="mx-1 mt-1 mt-3 py-2 r"
              border="none"
            />
          </Flex>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalAddMultiImage;

const RadiusBoxNormal = styled.div.attrs(() => ({
  className: "d-flex mt-2 flex-row",
}))`
  width: 100%;
  height: 400px;
  border-radius: 2px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;

const RadiusBoxDash = styled.div.attrs(() => ({
  className:
    "d-flex mt-2 flex-column justify-content-center align-items-center",
}))`
  width: 100%;
  height: 400px;
  border-radius: 2px;
  border-style: dashed;
  border-width: 3px;
  border-color: rgba(206, 212, 218, 1);
`;
