import styled from "styled-components";
import Colors from "../../utils/Colors";

interface Box {
  width?: string;
  bgColor?: string;
  paddingTop?: string;
  borderRadius?: string;
}

interface Line {
  width?: string;
  height?: string;
  bgColor?: string;
  borderTop?: string;
  borderBottom?: string;
  widthMobile?: string;
}

interface Flex {
  flex?: number;
  width?: string;
  height?: string;
  flexMobile?: any;
  widthMobile?: any;
}

export const BoxCard = styled.div.attrs<Box>(() => ({
  className: "d-flex flex-column justify-content-center mx-sm-2 my-2 ",
}))<Box>`
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  width: 100%;
  border-radius: ${(props) => props.borderRadius || "5px"};
`;

export const OvalBlue = styled.div.attrs<Box>(() => ({
  className: "d-flex flex-row justify-content-around",
}))<Box>`
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  width: 40%;
  border-radius: 10px;
  background-color: ${Colors.mainDarkBlue};
`;

export const Oval = styled.div.attrs<Box>(() => ({
  className: "d-flex flex-row justify-content-around",
}))<Box>`
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  width: ${(props) => props.width || "40%"};
  border-radius: 10px;
  background-color: ${(props) => props.bgColor || Colors.mainDarkBlue};
  padding-top: ${(props) => props.paddingTop || "0px"};
`;

export const Line = styled.div<Line>`
  width: ${(props) => props.width || "100%"};
  height: ${(props) => (props.height ? props.height : "2px")};
  background: ${(props) =>
    props.bgColor ? props.bgColor : "rgb(231, 231, 231)"};
  border-top: ${(props) => (props.borderTop ? props.borderTop : "unset")};
  border-bottom: ${(props) =>
    props.borderBottom ? props.borderBottom : "unset"};

  @media (max-width: 576px) {
    background: ${(props) => (props.bgColor ? props.bgColor : "#65c6ef")};
    width: ${(props) => props.widthMobile || "100%"};
  }
`;

export const Flex = styled.div.attrs<Flex>(() => ({
  className: "d-flex",
}))<Flex>`
  flex: ${(props) => props.flex || null};
  width: ${(props) => props.width || "100%"};
  height: ${(props) => props.height};
  @media (max-width: 576px) {
    flex: ${(props) => props.flexMobile || null};
    width: ${(props) => (props.widthMobile ? props.widthMobile : "auto")};
  }
`;

export const LineVertical = styled.div.attrs(() => ({
  className: "mx-2",
}))<Line>`
  border-left: 1px solid ${Colors.lightGray};
  height: 100%;
  position: positive;
  left: 50%;
  margin-left: -3px;
  top: 0;
`;
