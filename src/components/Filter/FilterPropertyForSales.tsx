import { TextSemiBold } from "../text";
import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import InputRadius from "../Input";
import DatePicker from "../../components/Datepicker/DatePicker";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import moment from "moment";
import callApi from "../../pages/api/callApi";
interface propertyForSales {
  onClickSearch: Function;
  closeModal: Function;
  setShow: any;
  show: any;
  count: any;
}
const FilterPropertyForSales: FunctionComponent<propertyForSales> = ({
  onClickSearch,
  closeModal,
  setShow,
  show,
  count,
}) => {
  const {
    control,
    formState: { errors },
    getValues,
    setValue,
  } = useForm();
  const dropdown: any = useRef(null);
  const [dropdownProvince, setDropdownProvince] = useState([]);
  const [dropdownDistrict, setDropdownDistrict] = useState([]);
  const [dropdownAsset, setDropdownAsset] = useState([]);
  const [disabledDistrict, setDisabledDistrict] = useState(true);
  let countFirstModal = 0;

  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }
  const setPropertyDistrict = async (event: any) => {
    var objData = { province: event.value };
    setValue("province", { label: event.value, value: event.value });
    const listDistrict = await callApi.apiPost(
      "master/District/Dropdown/find",

      objData
    );
    setDropdownDistrict(listDistrict);
    setDisabledDistrict(false);
  };
  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv>
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";
  const checkClickDropdown = (event: any) => {
    if (event.target.outerText.length < 100) {
      return true;
    }
    return false;
  };
  const checkClickOnDatePicker = (event: any) => {
    if (
      event.target.textContent == "<" ||
      event.target.textContent == ">" ||
      event.target.outerText.length < 100
    ) {
      return true;
    }
    return false;
  };

  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  function handleClick(event: any) {
    countFirstModal = countFirstModal + count;
    if (event.target.textContent == "ค้นหา") {
      closeModal();
      setShow(false);
    } else {
      if (countFirstModal == 1) {
        setShow(true);
      } else {
        if (
          (dropdown.current && dropdown.current.contains(event.target)) ||
          checkClickOnDatePicker(event) ||
          checkClickDropdown(event)
        ) {
          countFirstModal = 0;
          setShow(true);
        } else {
          countFirstModal = 0;
          setShow(false);
          closeModal();
        }
      }
    }
  }
  const handleClickSearch = () => {
    // closeModal();
    onClickSearch({
      claimant: getValues("claimant"),
      startdate:
        getValues("startdate") != undefined && getValues("startdate") != ""
          ? setFormatDate(getValues("startdate"))
          : getValues("startdate"),
      enddate:
        getValues("enddate") != undefined && getValues("enddate") != ""
          ? setFormatDate(getValues("enddate"))
          : getValues("enddate"),
      asset_type:
        getValues("asset_type") != undefined
          ? getValues("asset_type").value
          : getValues("asset_type"),
      province:
        getValues("province") != undefined
          ? getValues("province").value
          : getValues("province"),
      district:
        getValues("district") != undefined
          ? getValues("district").value
          : getValues("district"),
    });
  };
  const clearData = () => {
    setValue("claimant", "");
    setValue("asset_type", "");
    setValue("province", "");
    setValue("district", "");
    setValue("enddate", "");
    setValue("startdate", "");
  };
  const setDropDown = async () => {
    const listProvince = await callApi.apiGet("master/Province/Dropdown");

    const listAsset = await callApi.apiGet("master/AssetType/Dropdown");
    setDropdownProvince(listProvince);
    setDropdownAsset(listAsset);
  };
  useEffect(() => {
    setDropDown();
    if (show) {
      window.addEventListener("click", handleClick);
      return () => window.removeEventListener("click", handleClick);
    }
  }, [show]);
  return (
    <div
      style={{
        float: "left",
        zIndex: 9999,
        background: "white",
        width: "1050px",
        position: "absolute",
        borderRadius: "10px",
        boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.15)",
        left: "20%",
      }}
      ref={dropdown}
    >
      <div className="container" style={{ margin: "2em" }}>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              โจทก์
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="claimant"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="normal"
              color={Colors.mainBlackLight}
            >
              ประเภททรัพย์สิน
            </TextSemiBold>
            <div className="mt-2">
              <Controller
                name="asset_type"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="type"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={dropdownAsset}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              จังหวัด
            </TextSemiBold>
            <div className="mt-2">
              <Controller
                name="province"
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="province"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    onChange={(e) => {
                      setPropertyDistrict(e);
                    }}
                    options={dropdownProvince}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              อำเภอ
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="district"
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    isDisabled={disabledDistrict}
                    instanceId="district"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={dropdownDistrict}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่ เริ่มต้นประกาศ
            </TextSemiBold>
            <div className="mt-1 ">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="startdate"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่ สิ้นสุดเริ่มต้นประกาศ
            </TextSemiBold>
            <div className="mt-1">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="enddate"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center mt-3 mb-5">
          <ButtonRadius
            type="button"
            title="ล้างค่ากรอง"
            backgroundColor={Colors.mainWhite}
            color={Colors.mainBlack}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderColor={Colors.mainGrayLight}
            borderWidth="1px"
            border="solid"
            onClick={clearData}
          />
          <ButtonRadius
            type="button"
            title="ค้นหา"
            backgroundColor={Colors.mainBlue}
            color={Colors.mainWhite}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderWidth="thin"
            onClick={() => handleClickSearch()}
          />
        </div>
      </div>
    </div>
  );
};

export default FilterPropertyForSales;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row ",
}))`
  background-color: #fff;
  width: 100%;
  height: 60%;
  border-radius: 60px;
  padding-right: 0px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
