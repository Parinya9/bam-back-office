import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import { TextSemiBold } from "../text";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import InputRadius from "../Input";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import { NextPage } from "next";
import DatePicker from "../../components/Datepicker/DatePicker";
import callApi from "../../pages/api/callApi";
import moment from "moment";
interface FilterSearchManageMember {
  onClickSearch: Function;
  closeModal: Function;
  setShow: any;
  show: any;
  count: any;
}
const FilterSearchManageMember: FunctionComponent<FilterSearchManageMember> = ({
  onClickSearch,
  closeModal,
  setShow,
  show,
  count,
}) => {
  const {
    control,
    getValues,
    resetField,
    reset,
    setValue,
    formState: { errors },
  } = useForm();
  let countFirstModal = 0;
  const dropdown: any = useRef(null);
  const [dropdownRoles, setDropdownRoles] = useState([]);
  const [dropdownStatus, setDropDownStatus] = useState([]);
  // const [show, setShow] = useState(false);
  const options = [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" },
  ];

  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv>
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";
  const handleClickSearch = () => {
    // closeModal();
    onClickSearch({
      firstname: getValues("firstname"),
      lastname: getValues("lastname"),
      role_id: getValues("role") == undefined ? "" : getValues("role").value,
      status:
        getValues("status") == undefined
          ? ""
          : convertStatus(getValues("status").value),
    });
  };
  const convertStatus = (status: any) => {
    if (status == "อนุมัติการใช้งาน") {
      return 1;
    }
    return 0;
  };
  const clearData = () => {
    setValue("firstname", "");
    setValue("role", "");
    setValue("lastname", "");
    setValue("status", "");
  };

  const checkClickDropdown = (event: any) => {
    if (event.target.outerText.length < 100) {
      return true;
    }
    return false;
  };
  const checkClickOnDatePicker = (event: any) => {
    if (
      event.target.textContent == "<" ||
      event.target.textContent == ">" ||
      event.target.outerText.length < 20
    ) {
      return true;
    }
    return false;
  };
  function handleClick(event: any) {
    countFirstModal = countFirstModal + count;
    if (event.target.textContent == "ค้นหา") {
      closeModal();
      setShow(false);
    } else {
      if (countFirstModal == 1) {
        setShow(true);
      } else {
        if (
          (dropdown.current && dropdown.current.contains(event.target)) ||
          checkClickOnDatePicker(event) ||
          checkClickDropdown(event)
        ) {
          countFirstModal = 0;
          setShow(true);
        } else {
          countFirstModal = 0;
          setShow(false);
          closeModal();
        }
      }
    }
  }
  const setDropDown = async () => {
    const listRoles = await callApi.apiGet("master/roles/dropdown");
    const listStatus = await callApi.apiGet("master/StatusMember");

    setDropdownRoles(listRoles);
    setDropDownStatus(listStatus);
  };
  useEffect(() => {
    setDropDown();
    if (show) {
      window.addEventListener("click", handleClick);
      return () => window.removeEventListener("click", handleClick);
    }
  }, [show]);
  return (
    <form>
      <div
        style={{
          float: "left",
          background: "white",
          width: "1050px",
          position: "absolute",
          borderRadius: "10px",
          boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.15)",
          left: "20%",
          zIndex: 100000,
        }}
        ref={dropdown}
      >
        <div className="container" style={{ margin: "2em" }}>
          <div className="row mt-3" style={{ width: "950px" }}>
            <div className="col-12 col-lg-3">
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                ชื่อ
              </TextSemiBold>
              <div className="mt-1">
                <Controller
                  name="firstname"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
              </div>
            </div>
            <div className="col-12 col-lg-3">
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                นามสกุล
              </TextSemiBold>
              <div className="mt-1">
                <Controller
                  name="lastname"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
              </div>
            </div>

            <div className="col-12 col-lg-3">
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                Role
              </TextSemiBold>
              <div className="mt-2">
                <Controller
                  name="role"
                  control={control}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      styles={customStyles}
                      instanceId="role"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      options={dropdownRoles}
                    />
                  )}
                />
              </div>
            </div>
            <div className="col-12 col-lg-3">
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                สถานะผู้ใช้งาน
              </TextSemiBold>
              <div className="mt-2">
                <Controller
                  name="status"
                  control={control}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      styles={customStyles}
                      instanceId="position"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      options={dropdownStatus}
                    />
                  )}
                />
              </div>
            </div>
          </div>

          <div className="d-flex justify-content-center mt-3 mb-5">
            <ButtonRadius
              type="button"
              title="ล้างค่ากรอง"
              backgroundColor={Colors.mainWhite}
              color={Colors.mainBlack}
              width="200px"
              fontSize="18px"
              className="mx-1 mt-1 mt-3"
              borderColor={Colors.mainGrayLight}
              borderWidth="1px"
              border="solid"
              onClick={clearData}
            />
            <ButtonRadius
              type="button"
              title="ค้นหา"
              backgroundColor={Colors.mainBlue}
              color={Colors.mainWhite}
              width="200px"
              fontSize="18px"
              className="mx-1 mt-1 mt-3"
              borderWidth="thin"
              onClick={() => handleClickSearch()}
            />
          </div>
        </div>
      </div>
    </form>
  );
};

export default FilterSearchManageMember;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row ",
}))`
  background-color: #fff;
  width: 100%;
  height: 60%;
  border-radius: 60px;
  padding-right: 0px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
