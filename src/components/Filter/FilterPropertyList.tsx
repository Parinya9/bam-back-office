import type { NextPage } from "next";
import { TextSemiBold } from "../text";
import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import InputRadius from "../Input";
import Select, { components } from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import callApi from "../../pages/api/callApi";
import DatePicker from "../../components/Datepicker/DatePicker";
import moment from "moment";
import Cookies from "universal-cookie";

interface propertyList {
  onClickSearch: Function;
  closeModal: Function;
  setShow: any;
  show: any;
  count: any;
}

const FilterPropertyList: FunctionComponent<propertyList> = ({
  onClickSearch,
  closeModal,
  setShow,
  show,
  count,
}) => {
  const {
    control,
    setValue,
    getValues,
    formState: { errors },
  } = useForm();
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  let email = cookies.get("email");
  let deptCode = cookies.get("deptCode");
  const [type, setType] = useState("");
  // const [show, setShow] = useState(false);
  const dropdown: any = useRef(null);
  const dropdownPrice: any = useRef(null);
  const [showListStart, setShowListStart] = useState(false);
  const [showListEnd, setShowListEnd] = useState(false);
  const [showList, setShowList] = useState(false);
  const [listAssetState, setListAssetState] = useState([]);
  const [listAssetType, setListAssetType] = useState([]);
  const [listStar, setListStar] = useState([]);
  const [listAssetGroup, setListAssetGroup] = useState([]);
  const [listRegion, setListRegion] = useState([]);
  const [listProvince, setListProvince] = useState([]);
  const [listDistrict, setListDistrict] = useState([]);
  const [listSubDistrict, setListSubDistrict] = useState([]);
  const [listAoName, setListAoName] = useState([]);
  const [listDept, setListDept] = useState([]);
  const [open, setOpen] = useState(false);
  const [listPrice, setListPrice] = useState([]);
  const [disabledAo, setDisabledAo] = useState(false);
  let countFirstModal = 0;

  function collapse(value: any) {
    if (showListStart) {
      setValue("start", value);
    }
    if (showListEnd) {
      setValue("end", value);
    }
  }
  function expand() {
    setOpen(true);
  }
  function handleClickPrice(event: any) {
    if (dropdownPrice.current && dropdownPrice.current.contains(event.target)) {
      setShow(true);
    } else {
      var start = getValues("start");
      var end = getValues("end");
      if (end != undefined && end != "" && start != undefined && start != "") {
        setValue("price_range", {
          value: start + " ถึง " + end,
          label: start + " ถึง " + end,
        });
      } else {
        if (end != undefined && end.toString() != "") {
          setValue("price_range", {
            value: "ต่ำกว่า  " + end,
            label: "ต่ำกว่า  " + end,
          });
        } else if (start != undefined && start.toString() != "") {
          setValue("price_range", {
            value: "สูงกว่า " + start,
            label: "สูงกว่า " + start,
          });
        }
      }
      setOpen(false);

      // closeModal();
    }
  }
  const listSpecialPrice = [
    { value: "ใช่", label: "ใช่" },
    {
      value: "ไม่ใช่",
      label: "ไม่ใช่",
    },
  ];

  const listShowWebsite = [
    { value: "แสดง", label: "แสดง" },
    { value: "ไม่แสดง", label: "ไม่แสดง" },
  ];
  const listExpiry = [
    { value: "หมดอายุ", label: "หมดอายุ" },
    { value: "ไม่หมดอายุ", label: "ไม่หมดอายุ" },
  ];
  const listHiddenAsset = [
    { value: "ซ่อนทรัพย์", label: "ซ่อนทรัพย์" },
    { value: "แสดงทรัพย์", label: "แสดงทรัพย์" },
  ];
  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const listStatusApprove = [
    { value: "ตรวจสอบแล้ว", label: "ตรวจสอบแล้ว", color: "#7AC66E" },
    {
      value: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
      label: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
      color: "#F0AD4E",
    },
    {
      value: "รอการตรวจสอบ จนท.ฝ่ายการตลาด",
      label: "รอการตรวจสอบ จนท.ฝ่ายการตลาด",
      color: "#FF69B4",
    },
    {
      value: "รอการตรวจสอบ ผจก.ฝ่ายการตลาด",
      label: "รอการตรวจสอบ ผจก.ฝ่ายการตลาด",
      color: "#0092FF",
    },
    {
      value: "การบันทึกข้อมูลยังไม่สมบูรณ์",
      label: "การบันทึกข้อมูลยังไม่สมบูรณ์",
      color: "#FF2727",
    },
    {
      value: "ทรัพย์มาใหม่",
      label: "ทรัพย์มาใหม่",
      color: "#CED4DA",
    },
  ];
  const { Option } = components;
  const IconOption = (props: any) => (
    <Option {...props}>
      <span
        style={{
          height: "8px",
          width: "8px",
          borderRadius: "50%",
          display: "inline-block",
          backgroundColor: props.data.color,
          marginRight: "10px",
        }}
      ></span>
      {props.data.label}
    </Option>
  );
  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv>
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";
  const checkClickDropdown = (event: any) => {
    if (event.target.outerText.length < 100) {
      return true;
    }
    return false;
  };
  const checkClickOnDatePicker = (event: any) => {
    if (
      event.target.textContent == "<" ||
      event.target.textContent == ">" ||
      event.target.outerText.length < 20
    ) {
      return true;
    }
    return false;
  };
  function handleClick(event: any) {
    countFirstModal = countFirstModal + count;
    if (event.target.textContent == "ค้นหา") {
      closeModal();
      setShow(false);
    } else {
      if (countFirstModal == 1) {
        setShow(true);
      } else {
        if (
          (dropdown.current && dropdown.current.contains(event.target)) ||
          checkClickOnDatePicker(event) ||
          checkClickDropdown(event)
        ) {
          countFirstModal = 0;
          setShow(true);
        } else {
          countFirstModal = 0;
          setShow(false);
          closeModal();
        }
      }
    }
  }

  const clearData = () => {
    if (role !== "จำหน่ายทรัพย์ (Sale)") {
      setValue("ao_name", "");
    }

    setValue("asset_state", "");
    setValue("property_star", "");
    setValue("display_special_price", "");
    setValue("npa_type", "");
    setValue("group_property", "");
    setValue("evaluate_date", "");
    setValue("display_property", "");
    setValue("status_hidden_property", "");
    setValue("dept_name", "");

    setValue("status_approve", "");
    setValue("add_district", "");
    setValue("city_name", "");
    setValue("physical_zone", "");
    setValue("province_name", "");
    setValue("sale_price_spc_from_date", "");
    setValue("sale_price_spc_to_date", "");
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  const handleClickSearch = () => {
    closeModal();
    let ao_name = getValues("ao_name");
    var start = getValues("start");
    var end = getValues("end");
    let objSearch = {
      start_price: start,
      end_price: end,
      dept_name:
        getValues("dept_name") != undefined ? getValues("dept_name").value : "",
      ao_name:
        getValues("ao_name") != undefined ? getValues("ao_name").value : "",
      status_approve:
        getValues("status_approve") != undefined
          ? getValues("status_approve").value
          : "",
      add_district:
        getValues("add_district") != undefined
          ? getValues("add_district").value
          : "",
      city_name:
        getValues("city_name") != undefined ? getValues("city_name").value : "",
      display_special_price:
        getValues("display_special_price") != undefined
          ? getValues("display_special_price").value
          : "",

      asset_state:
        getValues("asset_state") != undefined
          ? getValues("asset_state").value
          : "",
      property_star:
        getValues("property_star") != undefined
          ? getValues("property_star").value
          : "",
      physical_zone:
        getValues("physical_zone") != undefined
          ? getValues("physical_zone").value
          : "",
      province_name:
        getValues("province_name") != undefined
          ? getValues("province_name").value
          : "",
      sale_price_spc_from_date:
        getValues("sale_price_spc_from_date") != undefined &&
        getValues("sale_price_spc_from_date") != ""
          ? setFormatDate(getValues("sale_price_spc_from_date"))
          : getValues("sale_price_spc_from_date"),
      sale_price_spc_to_date:
        getValues("sale_price_spc_to_date") != undefined &&
        getValues("sale_price_spc_to_date") != ""
          ? setFormatDate(getValues("sale_price_spc_to_date"))
          : getValues("sale_price_spc_to_date"),

      npa_type:
        getValues("npa_type") != undefined ? getValues("npa_type").value : "",
      group_property:
        getValues("group_property") != undefined
          ? getValues("group_property").value
          : "",
      evaluate_date:
        getValues("evaluate_date") != undefined
          ? getValues("evaluate_date").value
          : "",
      display_property:
        getValues("display_property") != undefined
          ? getValues("display_property").value
          : "",
      status_hidden_property:
        getValues("status_hidden_property") != undefined
          ? getValues("status_hidden_property").value
          : "",
    };
    if (ao_name === undefined || ao_name === "") {
      if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
        Object.assign(objSearch, {
          dept_code: deptCode,
        });
      } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
        Object.assign(objSearch, {
          v_name: firstname + " " + lastname,
        });
      }
    }
    onClickSearch(objSearch);
  };
  const setPropertyDistrict = async (event: any) => {
    var objData = { province: event.value };
    const listDistrict = await callApi.apiPost(
      "master/District/Dropdown/find",
      objData
    );

    setValue("province_name", {
      value: event.value,
      label: event.value,
    });
    setValue("city_name", {
      value: "",
      label: "",
    });
    setListDistrict(listDistrict);
  };
  const setPropertySubDistrict = async (event: any) => {
    var objData = { district: event.value };
    const listSubDistrict = await callApi.apiPost(
      "master/Subdistrict/Dropdown/find",
      objData
    );
    setListSubDistrict(listSubDistrict);

    setValue("city_name", {
      value: event.value,
      label: event.value,
    });
    setValue("add_district", {
      value: "",
      label: "",
    });
  };
  const setDropDown = async () => {
    const dropdownAssetState = await callApi.apiGet(
      "master/propertystatus/dropdown"
    );
    const dropdownAssetType = await callApi.apiGet("master/AssetType/Dropdown");
    const dropdownStar = await callApi.apiGet("master/Star");
    const dropdownAssetGroup = await callApi.apiGet(
      "master/Propertygroup/dropdown"
    );
    const dropdownDept = await callApi.apiGet("master/Department/Dropdown");
    const dropdownProvince = await callApi.apiGet("master/Province/Dropdown");
    const dropdownRegion = await callApi.apiGet("master/PhysicalZone/Dropdown");
    const dropdownPrice = await callApi.apiGet("price-list/price/dropdown");

    if (role === "จำหน่ายทรัพย์ (Sale)") {
      setValue("ao_name", {
        value: firstname + " " + lastname,
        label: firstname + " " + lastname,
      });
      setDisabledAo(true);
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      const dropdownAoName = await callApi.apiPost(
        "master/AoName/Manager/Dropdown",
        {
          email: email,
        }
      );
      setListAoName(dropdownAoName);
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      const dropdownAoName = await callApi.apiPost(
        "master/AoName/Director/Dropdown",
        {
          email: email,
        }
      );

      setListAoName(dropdownAoName);
    } else {
      const dropdownAoName = await callApi.apiGet("master/AoName/Dropdown");
      setListAoName(dropdownAoName);
    }
    setListAssetState(dropdownAssetState);
    setListAssetType(dropdownAssetType);
    setListStar(dropdownStar);
    setListAssetGroup(dropdownAssetGroup);
    setListDept(dropdownDept);
    setListProvince(dropdownProvince);

    setListRegion(dropdownRegion);
    setListPrice(dropdownPrice);
  };
  const onFocusStart = () => {
    setShowListStart(true);
    setShowListEnd(false);
    setShowList(true);
  };
  const onFocusEnd = () => {
    setShowListStart(false);
    setShowListEnd(true);
    setShowList(true);
  };
  useEffect(() => {
    countFirstModal = 0;
    setDropDown();
    setShowList(false);
    setShowListStart(false);
    setShowListEnd(false);
    if (open) {
      window.addEventListener("click", handleClickPrice);
      return () => window.removeEventListener("click", handleClickPrice);
    }

    if (show) {
      window.addEventListener("click", handleClick);
      return () => window.removeEventListener("click", handleClick);
    }
  }, [show, open]);
  return (
    <div
      style={{
        float: "left",
        zIndex: 100000,

        background: "white",
        width: "1050px",
        position: "absolute",
        borderRadius: "10px",
        boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.15)",
        left: "20%",

        // top: "50px",
      }}
      ref={dropdown}
    >
      <div className="container" style={{ margin: "2em" }}>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              สถานะการตรวจสอบ
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="status_approve"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="status_approve"
                    options={listStatusApprove}
                    components={{
                      Option: IconOption,
                      IndicatorSeparator: () => null,
                    }}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ส่วนงานที่รับผิดชอบ
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="dept_name"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="dept_name"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listDept}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              สถานะทรัพย์สิน
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="asset_state"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="asset_state"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listAssetState}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              เจ้าหน้าที่รับผิดชอบ
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="ao_name"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    isDisabled={disabledAo}
                    styles={customStyles}
                    instanceId="ao_name"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listAoName}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              เรียงตามดาว
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="property_star"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="property_star"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listStar}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ทรัพย์ราคาพิเศษ
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="display_special_price"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    instanceId="display_special_price"
                    options={listSpecialPrice}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่เริ่มต้นราคาพิเศษ
            </TextSemiBold>
            <div className="mt-1 ">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="sale_price_spc_from_date"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่สิ้นสุดราคาพิเศษ
            </TextSemiBold>
            <div className="mt-1">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="sale_price_spc_to_date"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ประเภททรัพย์สิน
            </TextSemiBold>
            <div className="mt-1 mb-3">
              <Controller
                name="npa_type"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="npa_type"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listAssetType}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              กลุ่มทรัพย์สิน
            </TextSemiBold>
            <div className="mt-1 mb-3">
              <Controller
                name="group_property"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="group_property"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listAssetGroup}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ระดับราคา
            </TextSemiBold>
            <div className="mt-1 mb-3" onClick={expand}>
              <Controller
                name="price_range"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    openMenuOnClick={false}
                    instanceId="price_range"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    // options={listPrice}
                  />
                )}
              />
              {open && (
                <div tabIndex={0}>
                  <div
                    style={{
                      zIndex: 1,
                      float: "left",
                      overflow: "hidden",
                      background: "white",
                      width: "30%",
                      position: "absolute",
                      borderRadius: "8px",
                      minHeight: "250px",
                      boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.15)",
                    }}
                    ref={dropdownPrice}
                  >
                    <ul
                      style={{
                        zIndex: 1,
                        listStyleType: "none",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                        marginTop: "10px",
                      }}
                    >
                      <li
                        style={{
                          width: "45%",
                          float: "left",
                          clear: "left",
                        }}
                      >
                        <Controller
                          name="start"
                          control={control}
                          rules={{ required: false }}
                          render={({ field }) => (
                            <InputRadius
                              field={field}
                              fontSize="17px"
                              style={{
                                border: "1px solid #CED4DA",
                                textAlign: "center",
                              }}
                              onFocus={onFocusStart}
                              placeHolder="ขั้นต่ำ"
                            />
                          )}
                        />
                      </li>
                      <li
                        style={{
                          left: "0px",
                          position: "absolute",
                          textAlign: "center",
                          top: "25px",
                          width: "10%",
                          marginLeft: "45%",
                          zIndex: 0,
                          color: "#CED4DA",
                        }}
                      >
                        -
                      </li>

                      <li
                        style={{
                          width: "45%",
                          float: "right",
                          clear: "right",
                        }}
                      >
                        <Controller
                          name="end"
                          control={control}
                          rules={{ required: false }}
                          render={({ field }) => (
                            <InputRadius
                              field={field}
                              fontSize="17px"
                              style={{
                                border: "1px solid #CED4DA",
                                textAlign: "center",
                              }}
                              onFocus={onFocusEnd}
                              placeHolder="สูงสุด"
                            />
                          )}
                        />
                      </li>
                    </ul>

                    <ul
                      style={{
                        zIndex: 1,
                        listStyleType: "none",
                        padding: "30px",
                        paddingTop: "0px",
                        float: showListStart ? "left" : "right",
                        maxHeight: "200px",
                        overflowY: "auto",
                        display: showList ? "initial" : "none",
                      }}
                    >
                      {listPrice.map((item: any, index) => (
                        <li
                          key={index}
                          style={{ marginTop: "1em", cursor: "pointer" }}
                          onClick={() => {
                            collapse(item.value);
                          }}
                        >
                          <TextSemiBold
                            className="mt-1"
                            fontSize="18px"
                            weight="300"
                            color={Colors.mainBlackLight}
                          >
                            {item.label}
                          </TextSemiBold>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ราคาประเมินหมดอายุ
            </TextSemiBold>
            <div className="mt-1 mb-3">
              <Controller
                name="evaluate_date"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="evaluate_date"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listExpiry}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="row mt-1" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ภาค
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="physical_zone"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="physical_zone"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listRegion}
                  />
                )}
              />
            </div>
          </div>

          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              จังหวัด
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="province_name"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="province_name"
                    onChange={(e) => {
                      setPropertyDistrict(e);
                    }}
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listProvince}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              อำเภอ/เขต
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="city_name"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="city_name"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    onChange={(e) => {
                      setPropertySubDistrict(e);
                    }}
                    options={listDistrict}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ตำบล/แขวง
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="add_district"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="add_district"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listSubDistrict}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              แสดงที่หน้าเว็บ
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="display_property"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="display_property"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listShowWebsite}
                  />
                )}
              />
            </div>
          </div>

          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              สถานะซ่อนทรัพย์
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="status_hidden_property"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="department"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listHiddenAsset}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-3 mb-5">
          <ButtonRadius
            type="button"
            title="ล้างค่ากรอง"
            backgroundColor={Colors.mainWhite}
            color={Colors.mainBlack}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderColor={Colors.mainGrayLight}
            borderWidth="1px"
            border="solid"
            onClick={clearData}
          />
          <ButtonRadius
            type="button"
            title="ค้นหา"
            backgroundColor={Colors.mainBlue}
            color={Colors.mainWhite}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderWidth="thin"
            onClick={() => handleClickSearch()}
          />
        </div>
      </div>
    </div>
  );
};

export default FilterPropertyList;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row ",
}))`
  background-color: #fff;
  width: 100%;
  height: 60%;
  border-radius: 60px;
  padding-right: 0px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
