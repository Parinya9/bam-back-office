import type { NextPage } from "next";
import { TextSemiBold } from "../text";
import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import InputRadius from "../Input";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import callApi from "../../pages/api/callApi";
import DatePicker from "../../components/Datepicker/DatePicker";
import moment from "moment";
interface property {
  onClickSearch: Function;
  closeModal: Function;
  setShow: any;
  show: any;
  count: any;
}

const FilterProperty: FunctionComponent<property> = ({
  onClickSearch,
  closeModal,
  setShow,
  show,
  count,
}) => {
  const {
    control,
    setValue,
    getValues,
    formState: { errors },
  } = useForm();
  let countFirstModal = 0;

  const dropdown: any = useRef(null);
  const [listStatusPayment, setListStatusPayment] = useState([]);
  const [listStatusApprove, setListStatusApprove] = useState([]);
  const [listDepartment, setListDepartment] = useState([]);
  const [listAsset, setListAsset] = useState([]);
  const [listPrice, setListPrice] = useState([]);

  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv>
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";
  const checkClickDropdown = (event: any) => {
    if (event.target.outerText.length < 100) {
      return true;
    }
    return false;
  };
  const checkClickOnDatePicker = (event: any) => {
    if (
      event.target.textContent == "<" ||
      event.target.textContent == ">" ||
      event.target.outerText.length < 20
    ) {
      return true;
    }
    return false;
  };
  function handleClick(event: any) {
    countFirstModal = countFirstModal + count;
    if (event.target.textContent == "ค้นหา") {
      closeModal();
      setShow(false);
    } else {
      if (countFirstModal == 1) {
        setShow(true);
      } else {
        if (
          (dropdown.current && dropdown.current.contains(event.target)) ||
          checkClickOnDatePicker(event) ||
          checkClickDropdown(event)
        ) {
          countFirstModal = 0;
          setShow(true);
        } else {
          countFirstModal = 0;
          setShow(false);
          closeModal();
        }
      }
    }
  }
  const openModalFilter = () => {
    setShow(true);
  };
  const clearData = () => {
    setValue("department", "");
    setValue("startdate", "");
    setValue("enddate", "");
    setValue("price_range", "");
    setValue("fullname", "");
    setValue("asset_id", "");
    setValue("status_payment", "");
    setValue("status_approve", "");
    setValue("asset_type", "");
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  const handleClickSearch = () => {
    onClickSearch({
      fullname: getValues("fullname"),
      department:
        getValues("department") != undefined
          ? getValues("department").value
          : getValues("department"),
      price_range:
        getValues("price_range") != undefined
          ? getValues("price_range").value
          : getValues("price_range"),
      startdate:
        getValues("startdate") != undefined && getValues("startdate") != ""
          ? setFormatDate(getValues("startdate"))
          : getValues("startdate"),
      enddate:
        getValues("enddate") != undefined && getValues("enddate") != ""
          ? setFormatDate(getValues("enddate"))
          : getValues("enddate"),
      status_payment:
        getValues("status_payment") != undefined
          ? convertStatusPayment(getValues("status_payment").value)
          : getValues("status_payment"),
      status_approve:
        getValues("status_approve") != undefined
          ? getValues("status_approve").value
          : getValues("status_approve"),
      asset_type:
        getValues("asset_type") != undefined
          ? getValues("asset_type").value
          : getValues("asset_type"),
      asset_id: getValues("asset_id"),
    });
    setShow(false);
  };
  const convertStatusPayment = (status: string) => {
    if (status === "รอชำระเงิน") {
      return "pending";
    } else if (status === "ไม่ชำระเงิน") {
      return "fail";
    } else if (status === "ชำระเงินเรียบร้อย") {
      return "success";
    }
  };
  const setDropDown = async () => {
    const dropdownDepartment = await callApi.apiGet(
      "master/Department/Dropdown"
    );
    const dropdownStatusPayment = await callApi.apiGet("master/StatusPayment");
    const dropdownStatusApprove = await callApi.apiGet("master/StatusApprove");
    const dropdownAssetType = await callApi.apiGet("master/AssetType/Dropdown");
    const dropdownPrice = await callApi.apiGet(
      "price-list/pricerange/dropdown"
    );
    setListDepartment(dropdownDepartment);
    setListStatusApprove(dropdownStatusApprove);
    setListStatusPayment(dropdownStatusPayment);
    setListAsset(dropdownAssetType);
    setListPrice(dropdownPrice);
  };
  useEffect(() => {
    setDropDown();
    if (show) {
      window.addEventListener("click", handleClick);
      return () => window.removeEventListener("click", handleClick);
    }
  }, [show]);
  return (
    <div
      style={{
        float: "left",
        background: "white",
        width: "1050px",
        position: "absolute",
        borderRadius: "10px",
        boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.15)",
        left: "20%",
        zIndex: 100000,
      }}
      ref={dropdown}
    >
      <div className="container" style={{ margin: "2em" }}>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ชื่อลูกค้า
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="fullname"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              รหัสทรัพย์
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="asset_id"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              สถานะการชำระเงิน
            </TextSemiBold>
            <div className="mt-2">
              <Controller
                name="status_payment"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="status_payment"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listStatusPayment}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              สถานะการอนุมัติ
            </TextSemiBold>
            <div className="mt-2">
              <Controller
                name="status_approve"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="status_approve"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listStatusApprove}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ประเภททรัพย์
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="asset_type"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="asset_type"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listAsset}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ฝ่ายงาน
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="department"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="department"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listDepartment}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่เริ่มต้น
            </TextSemiBold>
            <div className="mt-1 ">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="startdate"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่สิ้นสุด
            </TextSemiBold>
            <div className="mt-1">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="enddate"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ระดับราคา
            </TextSemiBold>
            <div className="mt-1 mb-3">
              <Controller
                name="price_range"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="price_range"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listPrice}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-3 mb-5">
          <ButtonRadius
            type="button"
            title="ล้างค่ากรอง"
            backgroundColor={Colors.mainWhite}
            color={Colors.mainBlack}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderColor={Colors.mainGrayLight}
            borderWidth="1px"
            border="solid"
            onClick={clearData}
          />
          <ButtonRadius
            type="button"
            title="ค้นหา"
            backgroundColor={Colors.mainBlue}
            color={Colors.mainWhite}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderWidth="thin"
            onClick={() => handleClickSearch()}
          />
        </div>
      </div>
    </div>
  );
};

export default FilterProperty;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row ",
}))`
  background-color: #fff;
  width: 100%;
  height: 60%;
  border-radius: 60px;
  padding-right: 0px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
