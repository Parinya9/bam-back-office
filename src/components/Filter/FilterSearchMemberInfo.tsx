import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import { TextSemiBold } from "../text";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import InputRadius from "../Input";
import DatePicker from "../../components/Datepicker/DatePicker";

import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import moment from "moment";
import callApi from "../../pages/api/callApi";
interface searchMemberInfo {
  onClickSearch: Function;
  closeModal: Function;
  setShow: any;
  show: any;
  count: any;
}

const FilterSearchMemberInfo: FunctionComponent<searchMemberInfo> = ({
  onClickSearch,
  closeModal,
  setShow,
  show,
  count,
}) => {
  const {
    control,
    setValue,
    getValues,
    formState: { errors },
  } = useForm();
  const dropdown: any = useRef(null);
  const btnSearch: any = useRef(null);

  let countFirstModal = 0;

  const [dropdownStatusMember, setDropdownStatusMember] = useState([]);
  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv>
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";

  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };

  const handleClickSearch = () => {
    var status;
    if (getValues("status") != undefined && getValues("status") != "") {
      status = getValues("status").value == "อนุมัติการใช้งาน" ? true : false;
    }
    onClickSearch({
      fullname: getValues("fullname") != undefined ? getValues("fullname") : "",
      email: getValues("email"),
      create_date:
        getValues("create_date") != undefined && getValues("create_date") != ""
          ? setFormatDate(getValues("create_date"))
          : getValues("create_date"),
      status: status,
      refno: getValues("refno"),
    });
    setShow(false);
  };
  function handleClick(event: any) {
    countFirstModal = countFirstModal + count;
    if (event.target.textContent == "ค้นหา") {
      closeModal();
      setShow(false);
    } else {
      if (countFirstModal == 1) {
        setShow(true);
      } else {
        if (
          (dropdown.current && dropdown.current.contains(event.target)) ||
          checkClickOnDatePicker(event) ||
          checkClickDropdown(event)
        ) {
          countFirstModal = 0;
          setShow(true);
        } else {
          countFirstModal = 0;
          setShow(false);
          closeModal();
        }
      }
    }
  }
  const checkClickDropdown = (event: any) => {
    if (
      event.target.textContent == "โปรดเลือก" ||
      event.target.textContent == "อนุมัติการใช้งาน" ||
      event.target.textContent == "ไม่อนุมัติการใช้งาน"
    ) {
      return true;
    }
    return false;
  };
  const checkClickOnDatePicker = (event: any) => {
    if (
      event.target.textContent == "<" ||
      event.target.textContent == ">" ||
      event.target.outerText.length < 100
    ) {
      return true;
    }
    return false;
  };
  const clearData = () => {
    setValue("refno", "");
    setValue("fullname", "");
    setValue("email", "");
    setValue("create_date", "");
    setValue("status", "");
  };
  const setDropDown = async () => {
    const item = await callApi.apiGet("master/statusMember");
    setDropdownStatusMember(item);
  };
  useEffect(() => {
    countFirstModal = 0;

    setDropDown();
    if (show) {
      window.addEventListener("click", handleClick);
      return () => window.removeEventListener("click", handleClick);
    }
  }, [show]);
  return (
    <div
      style={{
        float: "left",
        background: "white",
        width: "1050px",
        position: "absolute",
        borderRadius: "10px",
        boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.15)",
        left: "20%",
        zIndex: 100000,
      }}
      ref={dropdown}
    >
      <div className="container" style={{ margin: "2em" }}>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              Ref.
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="refno"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ชื่อ - นามสกุล
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="fullname"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              อีเมล
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="email"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              สถานะการใช้งาน
            </TextSemiBold>
            <div className="mt-2">
              <Controller
                name="status"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="status"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={dropdownStatusMember}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่ลงทะเบียน
            </TextSemiBold>
            <div className="mt-1">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="create_date"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center mt-3 mb-5">
          <ButtonRadius
            type="button"
            title="ล้างค่ากรอง"
            backgroundColor={Colors.mainWhite}
            color={Colors.mainBlack}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderColor={Colors.mainGrayLight}
            borderWidth="1px"
            border="solid"
            onClick={clearData}
          />
          <ButtonRadius
            type="button"
            title="ค้นหา"
            backgroundColor={Colors.mainBlue}
            color={Colors.mainWhite}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderWidth="thin"
            onClick={() => handleClickSearch()}
          />
        </div>
      </div>
    </div>
  );
};

export default FilterSearchMemberInfo;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row ",
}))`
  background-color: #fff;
  width: 100%;
  height: 60%;
  border-radius: 60px;
  padding-right: 0px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
