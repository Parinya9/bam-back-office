import type { NextPage } from "next";
import { TextSemiBold } from "../text";
import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components";
import InputRadius from "../Input";
import Select, { components } from "react-select";
import DatePicker from "../../components/Datepicker/DatePicker";
import { customStyles } from "../../../utils/ReactSelectStyle";

import moment from "moment";
import callApi from "../../pages/api/callApi";
interface FilterInterestPropetry {
  onClickSearch: Function;
  closeModal: Function;
  setShow: any;
  show: any;
  count: any;
}
const FilterInterestPropetry: FunctionComponent<FilterInterestPropetry> = ({
  onClickSearch,
  closeModal,
  setShow,
  show,
  count,
}) => {
  const {
    control,
    handleSubmit,
    getValues,
    setValue,
    formState: { errors },
  } = useForm();
  let countFirstModal = 0;

  const dropdown: any = useRef(null);
  const [listAoName, setListAoName] = useState([]);
  const [listDept, setListDept] = useState([]);
  const [listWorkGroup, setListWorkGroup] = useState([]);

  const onSubmit = () => {};

  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv>
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";
  const handleClickSearch = () => {
    // closeModal();
    onClickSearch({
      asset_id: getValues("asset_id"),
      work_group:
        getValues("work_group") == undefined
          ? getValues("work_group")
          : getValues("work_group").value,
      admin_name:
        getValues("ao_name") == undefined
          ? getValues("ao_name")
          : getValues("ao_name").value,
      department:
        getValues("dept_name") == undefined
          ? getValues("dept_name")
          : getValues("dept_name").value,
      startdate:
        getValues("startdate") != undefined && getValues("startdate") != ""
          ? setFormatDate(getValues("startdate"))
          : getValues("startdate"),
      enddate:
        getValues("enddate") != undefined && getValues("enddate") != ""
          ? setFormatDate(getValues("enddate"))
          : getValues("enddate"),
    });
  };
  const clearData = () => {
    setValue("asset_id", "");
    setValue("work_group", "");
    setValue("ao_name", "");
    setValue("dept_name", "");
    setValue("enddate", "");
    setValue("startdate", "");
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  const checkClickDropdown = (event: any) => {
    if (event.target.outerText.length < 100) {
      return true;
    }
    return false;
  };
  const checkClickOnDatePicker = (event: any) => {
    if (
      event.target.textContent == "<" ||
      event.target.textContent == ">" ||
      event.target.outerText.length < 20
    ) {
      return true;
    }
    return false;
  };
  function handleClick(event: any) {
    countFirstModal = countFirstModal + count;
    if (event.target.textContent == "ค้นหา") {
      closeModal();
      setShow(false);
    } else {
      if (countFirstModal == 1) {
        setShow(true);
      } else {
        if (
          (dropdown.current && dropdown.current.contains(event.target)) ||
          checkClickOnDatePicker(event) ||
          checkClickDropdown(event)
        ) {
          countFirstModal = 0;
          setShow(true);
        } else {
          countFirstModal = 0;
          setShow(false);
          closeModal();
        }
      }
    }
  }
  const setDropDown = async () => {
    const dropdownDept = await callApi.apiGet("master/Department/Dropdown");
    const dropdownAoName = await callApi.apiGet("master/AoName/Dropdown");
    const dropdownWorkgroup = await callApi.apiGet("master/Workgroup/Dropdown");
    setListDept(dropdownDept);
    setListAoName(dropdownAoName);
    setListWorkGroup(dropdownWorkgroup);
  };
  useEffect(() => {
    setDropDown();
    if (show) {
      window.addEventListener("click", handleClick);
      return () => window.removeEventListener("click", handleClick);
    }
  }, [show]);
  return (
    <div
      style={{
        float: "left",
        background: "white",
        width: "1050px",
        position: "absolute",
        borderRadius: "10px",
        boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.15)",
        left: "20%",
        zIndex: 100000,
      }}
      ref={dropdown}
    >
      <div className="container" style={{ margin: "2em" }}>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              เจ้าหน้าที่
            </TextSemiBold>
            <div className="mt-2">
              <Controller
                name="ao_name"
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="ao_name"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listAoName}
                  />
                )}
              />
            </div>
          </div>

          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              กลุ่มงาน
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="dept_name"
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="dept_name"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listDept}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              ฝ่ายงาน
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="work_group"
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="โปรดเลือก"
                    styles={customStyles}
                    instanceId="work_group"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    options={listWorkGroup}
                  />
                )}
              />
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              รหัสทรัพย์
            </TextSemiBold>
            <div className="mt-1">
              <Controller
                name="asset_id"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
            </div>
          </div>
        </div>
        <div className="row mt-3" style={{ width: "950px" }}>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่เริ่มต้น
            </TextSemiBold>
            <div className="mt-1 ">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="startdate"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <TextSemiBold
              fontSize="16px"
              weight="500"
              color={Colors.mainBlackLight}
            >
              วันที่สิ้นสุด
            </TextSemiBold>
            <div className="mt-1">
              <DatePickerDiv>
                <Controller
                  control={control}
                  name="enddate"
                  render={({
                    field: { onChange, name, value },
                    formState: { errors },
                  }) => (
                    <>
                      <DatePicker
                        value={value || ""}
                        onchange={(date: any) => {
                          onChange(date);
                        }}
                      />
                    </>
                  )}
                />
              </DatePickerDiv>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-3 mb-5">
          <ButtonRadius
            type="button"
            title="ล้างค่ากรอง"
            backgroundColor={Colors.mainWhite}
            color={Colors.mainBlack}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderColor={Colors.mainGrayLight}
            borderWidth="1px"
            border="solid"
            onClick={clearData}
          />
          <ButtonRadius
            type="button"
            title="ค้นหา"
            backgroundColor={Colors.mainBlue}
            color={Colors.mainWhite}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3"
            borderWidth="thin"
            onClick={() => handleClickSearch()}
          />
        </div>
      </div>
    </div>
  );
};

export default FilterInterestPropetry;

const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
