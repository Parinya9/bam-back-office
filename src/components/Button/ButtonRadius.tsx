import { useState, FunctionComponent } from "react";
import styled from "styled-components";
import Colors from "../../../utils/Colors";
import { TextSemiBold } from "../text";
import Color from "../../../utils/Colors";

interface ButtonRadiusType {
  onClick?: () => any;
  title?: string;
  backgroundColor?: string;
  color?: string;
  width?: string;
  height?: string;
  radius?: string;
  border?: string;
  iconPath?: string;
  className?: string;
  borderColor?: string;
  borderWidth?: string;
  fontSize?: string;
  mobileWidth?: string;
  background?: string;
  mobileSize?: string;
  type?: any;
  disable?: boolean;
  imgWidth?: string;
  imgHeight?: string;
  imgStyle?: any;
}

const ButtonRadius: FunctionComponent<ButtonRadiusType> = ({
  onClick,
  title,
  backgroundColor,
  color,
  width,
  height,
  iconPath,
  className,
  border,
  borderColor,
  borderWidth,
  fontSize,
  mobileWidth,
  background,
  mobileSize,
  radius,
  type = "button",
  disable,
  imgWidth,
  imgHeight,
  imgStyle,
}) => {
  return (
    <BtnRadius
      type={type}
      className={className}
      onClick={onClick}
      backgroundColor={backgroundColor}
      color={color}
      width={width}
      height={height}
      border={border}
      borderColor={borderColor}
      borderWidth={borderWidth}
      fontSize={fontSize}
      mobileWidth={mobileWidth}
      background={background}
      disabled={disable}
      radius={radius}
      imgWidth={imgWidth}
      imgHeight={imgHeight}
      imgStyle={imgStyle}
    >
      <TextSemiBold
        fontSize={fontSize ? fontSize : "15px"}
        mobileSize={mobileSize ? mobileSize : "15px"}
        color={color ? color : Colors.mainDarkBlue}
      >
        {title || ""}
      </TextSemiBold>
      {iconPath && (
        <img
          src={iconPath}
          width={imgWidth ? imgWidth : "15px"}
          height={imgHeight ? imgHeight : "15px"}
          style={imgStyle ? imgStyle : { marginLeft: "10px" }}
        />
      )}
    </BtnRadius>
  );
};

export default ButtonRadius;

const BtnRadius = styled.button.attrs<ButtonRadiusType>(() => ({
  className: "",
}))<ButtonRadiusType>`
  background-color: ${(props) =>
    props.backgroundColor ? props.backgroundColor : Colors.mainDarkBlue};
  color: ${(props) => (props.color ? props.color : Colors.mainGray)};
  width: ${(props) => (props.width ? props.width : "auto")};
  height: ${(props) => (props.height ? props.height : "auto")};
  border-radius: ${(props) => (props.radius ? props.radius : "30px")};
  border-style: ${(props) => (props.border ? props.border : "none")};
  border-color: ${(props) =>
    props.borderColor ? props.borderColor : Color.mainDarkBlue};
  border-width: ${(props) => (props.borderWidth ? props.borderWidth : "none")};
  @media (max-width: 1024px) {
    width: ${(props) => props.mobileWidth || "auto"};
  }
  background: ${(props) => (props.background ? props.background : "")};
`;
