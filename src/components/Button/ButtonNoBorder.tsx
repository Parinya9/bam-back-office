import { useState, FunctionComponent } from "react";
import styled from "styled-components";
import Colors from "../../../utils/Colors";
import { TextSemiBold } from "../text";
import Color from "../../../utils/Colors";

interface ButtonRadiusType {
  onClick?: () => any;
  title?: string;
  backgroundColor?: string;
  color?: string;
  width?: string;
  height?: string;
  radius?: string;
  border?: string;
  iconPath?: string;
  className?: string;
  borderColor?: string;
  background?: string;
  fontSize?: string;
  mobileSize?: string;
  imgWidth?: string;
  imgHeight?: string;
  imgMarginB?: string;
  form?: any;
  type?: any;
}

const ButtonRadius: FunctionComponent<ButtonRadiusType> = ({
  onClick,
  title,
  backgroundColor,
  color,
  width,
  height,
  iconPath,
  className,
  border,
  borderColor,
  background,
  mobileSize,
  fontSize,
  imgWidth,
  imgHeight,
  imgMarginB,
  form = "",
  type = "button",
}) => {
  return (
    <BtnRadius
      className={className}
      onClick={onClick}
      backgroundColor={backgroundColor}
      color={color}
      width={width}
      height={height}
      border={border}
      borderColor={borderColor}
      background={background}
      form={form}
      type={type}
    >
      {iconPath && (
        <img
          src={iconPath}
          width={imgWidth ? imgWidth : "15px"}
          height={imgHeight ? imgHeight : "15px"}
          style={{
            marginRight: "5px",
            marginBottom: imgMarginB ? imgMarginB : "0px",
          }}
        />
      )}
      <TextSemiBold
        fontSize={fontSize ? fontSize : "15px"}
        mobileSize={mobileSize ? mobileSize : "15px"}
        color={color}
      >
        {title || ""}
      </TextSemiBold>
    </BtnRadius>
  );
};

export default ButtonRadius;

const BtnRadius = styled.button.attrs<ButtonRadiusType>(() => ({
  className: "",
}))<ButtonRadiusType>`
  background-color: ${(props) =>
    props.backgroundColor ? props.backgroundColor : "red"};
  color: ${(props) => (props.color ? props.color : Colors.mainDarkBlue)};
  width: ${(props) => (props.width ? props.width : "auto")};
  height: ${(props) => (props.height ? props.height : "auto")};
  border-radius: ${(props) => (props.radius ? props.radius : "30px")};
  background: ${(props) =>
    props.background
      ? props.background
      : "linear-gradient(90deg, #008ED2 0%, #003C78 100%)"};
  border-color: ${(props) =>
    props.borderColor
      ? props.borderColor
      : "linear-gradient(90deg, #008ED2 0%, #003C78 100%)"};
  &:focus {
    outline: none;
  }
  border: none;
`;
