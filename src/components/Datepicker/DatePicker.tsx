import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import "react-datetime/css/react-datetime.css";
import ModernDatepicker from "react-datetime";
import { readFileSync } from "fs";
import moment from "moment";

interface InputDate {
  disabled?: any;
  isValidDate?: any;
  value?: any;
  onchange?: any;
}

const DatePicker: FunctionComponent<InputDate> = ({
  isValidDate,
  disabled = false,
  value,
  onchange,
}) => {
  let inputProps = {
    placeholder: "DD/MM/YYYY",
    disabled: disabled,
  };

  // const setOnChange = (e: any) => {
  //   var dateFormat = moment(e._d).format("DD/MM/YYYY");
  //   onchange(dateFormat);
  // };

  return (
    <div
      className="div-datepicker"
      style={{ position: "relative", width: "100%", margin: 0, padding: 0 }}
    >
      <ModernDatepicker
        dateFormat="DD/MM/YYYY"
        value={value || ""}
        timeFormat={false}
        closeOnSelect={true}
        inputProps={inputProps}
        onChange={onchange}
        renderInput={(props) => {
          return (
            <input
              {...props}
              value={value ? moment(value).format("DD/MM/YYYY") : ""}
            />
          );
        }}
      />
      <img
        src="/new/images/Calendar_navyblue.svg"
        width="25px"
        height="25px"
        style={{
          position: "absolute",
          top: "5px",
          right: "20px",
          height: "25px",
        }}
      />
    </div>
  );
};
export default DatePicker;
