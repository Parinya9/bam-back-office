import { FunctionComponent } from "react";
import { TextSemiBold } from "../text";
import { Flex, LineVertical } from "../../components/layout";
import Colors from "../../../utils/Colors";
import styled from "styled-components";

interface InpuutWithTitleType {
  title?: string;
  flex?: number;
  placeholder?: string;
}

const InputWithTitle: FunctionComponent<InpuutWithTitleType> = ({
  title,
  flex,
  placeholder,
}) => {
  return (
    <Flex className="flex-column px-1" flex={flex} style={{ width: "100px " }}>
      <TextSemiBold
        fontSize="13px"
        color={Colors.mainDarkBlue}
        className="mt-2"
      >
        {title || "BAM"}
      </TextSemiBold>
      <Input
        placeholder={placeholder}
        className="mt-1"
        style={{ height: "40px" }}
      />
    </Flex>
  );
};

export default InputWithTitle;

const Input = styled.input.attrs(() => ({}))`
  font-size: 0.6em;
  border-width: 0px;
  border: none;
  &:focus {
    outline: none;
  }
`;
