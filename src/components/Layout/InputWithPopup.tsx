import { FunctionComponent } from "react";
import { TextSemiBold } from "../text";
import { Flex } from "../../components/layout";
import Colors from "../../../utils/Colors";
import styled from "styled-components";

interface InpuutWithTitleType {
  title?: string;
  flex?: number;
  placeholder?: string;
  withIcon?: boolean;
}

const InputWithPopup: FunctionComponent<InpuutWithTitleType> = ({
  title,
  flex,
  placeholder,
  withIcon,
}) => {
  return (
    <Flex className="flex-column px-1" flex={flex} style={{ width: "100px" }}>
      <TextSemiBold
        fontSize="13px"
        color={Colors.mainDarkBlue}
        className="mt-2"
      >
        {title || "BAM"}
      </TextSemiBold>
      <Flex className="flex-row">
        {withIcon ? <img src="new/images/location.svg" /> : null}
        <Input
          placeholder={placeholder}
          // className={withIcon ? "mx-2" : null}
          style={{ height: "auto" }}
        />
      </Flex>
    </Flex>
  );
};

export default InputWithPopup;

const Input = styled.input.attrs(() => ({}))`
  font-size: 0.6em;
  border-width: 0px;
  border: none;
  &:focus {
    outline: none;
  }
`;
