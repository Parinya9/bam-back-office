import { useState, FunctionComponent } from "react";
import styled from "styled-components";
import Colors from "../../../utils/Colors";

interface InputType {
  title?: string;
  backgroundColor?: string;
  color?: string;
  width?: string;
  height?: string;
  radius?: string;
  border?: string;
  iconPath?: string;
  className?: string;
  borderColor?: string;
  borderWidth?: string;
  placeHolder?: string;
  fontSize?: string;
  field?: any;
  disabled?: boolean;
  style?: any;
  onChange?: any;
  value?: any;
}

const InputRadiusAsset: FunctionComponent<InputType> = ({
  backgroundColor,
  color,
  width,
  height,
  className,
  border,
  borderColor,
  borderWidth,
  placeHolder,
  fontSize,
  field,
  disabled,
  style,
  onChange,
  value,
}) => {
  return (
    <InputField
      {...field}
      className={className}
      placeholder={placeHolder}
      backgroundColor={backgroundColor}
      color={color}
      width={width}
      height={height}
      disabled={disabled ? disabled : false}
      border={border}
      borderColor={borderColor}
      borderWidth={borderWidth}
      fontSize={fontSize}
      style={style}
      onChange={onChange}
      value={value}
    ></InputField>
  );
};

export default InputRadiusAsset;

const InputField = styled.input.attrs<InputType>(() => ({
  className: "mt-2 mb-2 pl-2",
}))<InputType>`
  color: ${(props) => (props.color ? props.color : "black")};
  width: ${(props) => (props.width ? props.width : "100%")};
  height: ${(props) => (props.height ? props.height : "auto")};
  border-radius: ${(props) => (props.radius ? props.radius : "30px")};
  border: ${(props) => (props.border ? props.border : "")};
  placeholder: ${(props) => (props.placeHolder ? props.placeHolder : "")};
  border-width: ${(props) => (props.borderWidth ? props.borderWidth : "none")};
  padding-left: 10px;
  padding-top: 5px;
  padding-bottom: 5px;
  font-size: ${(props) => (props.fontSize ? props.fontSize : "14px")};
  border-color: ${(props) => (props.borderColor ? props.borderColor : "")};
`;
