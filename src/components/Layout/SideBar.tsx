import React, { FunctionComponent, useEffect, useState } from "react";
import Link from "next/link";

import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";
import sidebarBg from "./assets/bg2.jpg";
import { TextBold, TextSemiBold } from "../text";
import Colors from "../../../utils/Colors";
import callApi from "../../pages/api/callApi";
import Cookies from "universal-cookie";

interface Asdie {
  image?: string;
  collapsed: boolean;
  rtl: any;
  toggled: boolean;
  handleToggleSidebar: any;
}

const Aside: FunctionComponent<Asdie> = ({
  image,
  collapsed,
  rtl,
  toggled,
  handleToggleSidebar,
}) => {
  const cookies = new Cookies();
  let role = cookies.get("role");
  const [listMenu, setListMenu] = useState([]);
  const checkSubMenu = (menu: any) => {
    let flag = false;
    listMenu.map((item: any, index: any) => {
      if (menu == item.subMenu) {
        flag = true;
        return;
      }
    });
    return flag;
  };
  const checkMenu = (menu: any) => {
    let flag = false;
    listMenu.map((item: any, index: any) => {
      if (menu == item.menu) {
        flag = true;
        return;
      }
    });
    return flag;
  };
  const getMenuByRoles = async () => {
    if (role != undefined) {
      const item = await callApi.apiPost("rolepermission/find", { role: role });
      setListMenu(item);
    }
  };
  useEffect(() => {
    getMenuByRoles();
  }, [role]);
  // const intl = useIntl();
  return (
    <ProSidebar
      // image={image ? sidebarBg : false}
      rtl={rtl}
      collapsed={collapsed}
      toggled={toggled}
      breakPoint="md"
      onToggle={handleToggleSidebar}
    >
      <SidebarHeader>
        <div
          className="d-flex flex-row  align-items-center "
          style={{
            padding: "30px",
            overflow: "hidden",
          }}
        >
          <img src="/new/images/bam_logo.png" width="50px" height="50px" />
          <TextBold fontSize="20px" color={Colors.mainWhite} className="ms-2">
            ระบบบริหาร จัดการเว็บไซต์
          </TextBold>
        </div>
      </SidebarHeader>

      <SidebarContent>
        {(checkMenu("สนใจทรัพย์สิน") ||
          checkMenu("จองซื้อทรัพย์") ||
          checkMenu("ทรัพย์สิน") ||
          checkMenu("ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี")) && (
          <Menu>
            <MenuItem>
              <TextBold fontSize="14px" color="rgba(0, 146, 255, 1)">
                จัดการทรัพย์
              </TextBold>
            </MenuItem>
          </Menu>
        )}

        <Menu>
          {checkSubMenu("สนใจทรัพย์สิน") && (
            <Link href="/interestproperty">
              <MenuItem>
                <img
                  src="/new/images/Home-white.svg"
                  width="20px"
                  height="20px"
                  style={{ marginRight: "15px", marginLeft: "10px" }}
                />
                <TextBold fontSize="14px" color={Colors.mainWhite}>
                  สนใจทรัพย์สิน
                </TextBold>
              </MenuItem>
            </Link>
          )}
          <SubMenu
            title={
              (
                <TextBold fontSize="14px" color={Colors.mainWhite}>
                  <img
                    src="/new/images/Home-white.svg"
                    width="20px"
                    height="20px"
                    style={{ marginRight: "15px", marginLeft: "10px" }}
                  />{" "}
                  ทรัพย์สิน
                </TextBold>
              ) as any
            }
          >
            {checkSubMenu("ทรัพย์สิน") && (
              <Link href="/propertylist">
                <MenuItem>
                  <TextBold
                    fontSize="14px"
                    color={Colors.mainWhite}
                    className="ms-4"
                  >
                    • &nbsp; ทรัพย์สิน
                  </TextBold>
                </MenuItem>
              </Link>
            )}
            {checkSubMenu("ข้อมูลประเภททรัพย์") && (
              <Link href="/grouppropertylist">
                <MenuItem>
                  <TextBold
                    fontSize="14px"
                    color={Colors.mainWhite}
                    className="ms-4"
                  >
                    • &nbsp; ข้อมูลประเภททรัพย์
                  </TextBold>
                </MenuItem>
              </Link>
            )}
            {checkSubMenu("ข้อมูลระดับราคา") && (
              <Link href="/grouppricelist">
                <MenuItem>
                  <TextBold
                    fontSize="14px"
                    color={Colors.mainWhite}
                    className="ms-4"
                  >
                    • &nbsp; ข้อมูลระดับราคา
                  </TextBold>
                </MenuItem>
              </Link>
            )}
            {checkSubMenu("ข้อมูลสถานที่ใกล้เคียง") && (
              <Link href="/nearbylist">
                <MenuItem>
                  <TextBold
                    fontSize="14px"
                    color={Colors.mainWhite}
                    className="ms-4"
                  >
                    • &nbsp; ข้อมูลสถานที่ใกล้เคียง
                  </TextBold>
                </MenuItem>
              </Link>
            )}
          </SubMenu>
          {checkMenu("จองซื้อทรัพย์") && (
            <SubMenu
              title={
                (
                  <TextBold fontSize="14px" color={Colors.mainWhite}>
                    <img
                      src="/new/images/Home-white.svg"
                      width="20px"
                      height="20px"
                      style={{ marginRight: "15px", marginLeft: "10px" }}
                    />{" "}
                    จองซื้อทรัพย์
                  </TextBold>
                ) as any
              }
            >
              {checkSubMenu("ข้อมูลจองซื้อทรัพย์") && (
                <Link href="/reserveproperty">
                  <MenuItem>
                    <TextBold
                      fontSize="14px"
                      color={Colors.mainWhite}
                      className="ms-4"
                    >
                      • &nbsp; ข้อมูลจองซื้อทรัพย์
                    </TextBold>
                  </MenuItem>
                </Link>
              )}
              {checkSubMenu("Transaction KBANK") && (
                <Link href="/kbanktranscation">
                  <MenuItem>
                    <TextBold
                      fontSize="14px"
                      color={Colors.mainWhite}
                      className="ms-4"
                    >
                      • &nbsp; Transaction KBANK
                    </TextBold>
                  </MenuItem>
                </Link>
              )}
            </SubMenu>
          )}
          {checkSubMenu("Log จองซื้อทรัพย์") && (
            <MenuItem>
              <img
                src="/new/images/Home-white.svg"
                width="20px"
                height="20px"
                style={{ marginRight: "15px", marginLeft: "10px" }}
              />{" "}
              <TextBold fontSize="14px" color={Colors.mainWhite}>
                Log จองซื้อทรัพย์
              </TextBold>
            </MenuItem>
          )}
          {checkSubMenu("ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี") && (
            <Link href="/propertyforsales">
              <MenuItem>
                <img
                  src="/new/images/Home-white.svg"
                  width="20px"
                  height="20px"
                  style={{ marginRight: "15px", marginLeft: "10px" }}
                />{" "}
                <TextBold fontSize="14px" color={Colors.mainWhite}>
                  ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี
                </TextBold>
              </MenuItem>
            </Link>
          )}
        </Menu>
        {(checkSubMenu("จัดการ HomePage") ||
          checkSubMenu("ข้อมูลสมาชิก") ||
          checkSubMenu("จัดการผู้ใช้งาน")) && (
          <Menu>
            <MenuItem>
              <TextBold fontSize="14px" color="rgba(0, 146, 255, 1)">
                การจัดการ
              </TextBold>
            </MenuItem>
          </Menu>
        )}
        <Menu>
          {checkSubMenu("จัดการ HomePage") && (
            <Link href="/home">
              <MenuItem>
                <img
                  src="/new/images/Home-white.svg"
                  width="20px"
                  height="20px"
                  style={{ marginRight: "15px", marginLeft: "10px" }}
                />{" "}
                <TextBold fontSize="14px" color={Colors.mainWhite}>
                  จัดการ HomePage
                </TextBold>
              </MenuItem>
            </Link>
          )}
          {checkSubMenu("ข้อมูลสมาชิก") && (
            <Link href="/memberinfo">
              <MenuItem>
                <img
                  src="/new/images/Home-white.svg"
                  width="20px"
                  height="20px"
                  style={{ marginRight: "15px", marginLeft: "10px" }}
                />{" "}
                <TextBold fontSize="14px" color={Colors.mainWhite}>
                  ข้อมูลสมาชิก
                </TextBold>
              </MenuItem>
            </Link>
          )}
          {checkSubMenu("จัดการผู้ใช้งาน") && (
            <Link href="/managemember">
              <MenuItem>
                <TextBold fontSize="14px" color={Colors.mainWhite}>
                  <img
                    src="/new/images/Home-white.svg"
                    width="20px"
                    height="20px"
                    style={{ marginRight: "15px", marginLeft: "10px" }}
                  />{" "}
                  จัดการผู้ใช้งาน
                </TextBold>
              </MenuItem>
            </Link>
          )}
          {checkSubMenu("ข้อมูลผู้รับข่าวสาร") && (
            <Link href="/managereceivenews">
              <MenuItem>
                <img
                  src="/new/images/Home-white.svg"
                  width="20px"
                  height="20px"
                  style={{ marginRight: "15px", marginLeft: "10px" }}
                />{" "}
                <TextBold fontSize="14px" color={Colors.mainWhite}>
                  ข้อมูลผู้รับข่าวสาร
                </TextBold>
              </MenuItem>
            </Link>
          )}
        </Menu>
      </SidebarContent>
    </ProSidebar>
  );
};

export default Aside;
