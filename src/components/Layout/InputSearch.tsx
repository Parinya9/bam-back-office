import { FunctionComponent } from "react";
import styled from "styled-components";
import { ControllerRenderProps } from "react-hook-form";
interface InputRadiusType {
  field?: any;
  placeholder?: string;
  maxLength?: number;
}

const InputWithTitle: FunctionComponent<InputRadiusType> = ({
  placeholder,
  maxLength,
  field,
}) => {
  return (
    <Input
      className="mt-1"
      placeholder={placeholder}
      {...field}
      maxLength={maxLength}
    />
  );
};

export default InputWithTitle;

const Input = styled.input.attrs(() => ({}))`
  font-size: 17px;
  width: 100%;
  border-width: 0px;
  border: none;
  &:focus {
    outline: none;
  }
`;
