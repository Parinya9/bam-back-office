import { FunctionComponent } from "react";
import styled from "styled-components";
import { Flex } from "../../components/layout";
import { ControllerRenderProps } from "react-hook-form";

interface InputRadiusType {
  field?: ControllerRenderProps;
  title?: string;
  flex?: number;
  placeholder?: string;
  maxLength?: number;
  txtAlign?: string;
  fontSize?: string;
  type?: string;
}

const InputWithTitle: FunctionComponent<InputRadiusType> = ({
  title,
  flex,
  placeholder,
  maxLength,
  field,
  txtAlign,
  fontSize,
  type = "text",
}) => {
  return (
    <RadiusBox>
      <Flex className="flex-column px-1" flex={flex}>
        <Input
          className="mt-1"
          placeholder={placeholder}
          {...field}
          maxLength={maxLength}
          type={type}
          // fontSize={fontSize}
          // style={{ textAlign: txtAlign ? txtAlign : "left" }}
        />
      </Flex>
    </RadiusBox>
  );
};

export default InputWithTitle;

const Input = styled.input.attrs(() => ({}))`
  font-size: ${(props: any) => (props.fontSize ? props.fontSize : "0.7em")};
  width: 100%;
  border-width: 0px;
  border: none;
  &:focus {
    outline: none;
  }
`;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row px-4 py-2",
}))`
  background-color: #fff;
  width: 100%;
  height: 25%;
  border-radius: 60px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
