import { FunctionComponent } from "react";
import { TextSemiBold } from "../text";
import { Flex } from "../../components/layout";
import Colors from "../../../utils/Colors";
import styled from "styled-components";

interface InputRadiusWithRSpan {
  title?: string;
  flex?: number;
  placeholder?: string;
  withIcon?: boolean;
  disabled?: boolean;
  field?: any;
  onChange?: any;
  value?: any;
}

const InputRadiusWithRSpan: FunctionComponent<InputRadiusWithRSpan> = ({
  title,
  flex,
  placeholder,
  withIcon,
  field,
  disabled,
  onChange,
  value,
}) => {
  return (
    <RadiusBox style={{ backgroundColor: disabled ? "#e8e8e8" : "#fff" }}>
      <div className="col-10 px-1 mt-1">
        <Input
          {...field}
          disabled={disabled ? disabled : false}
          onChange={onChange}
          value={value}
        />
      </div>
      <div className="col-2 mt-1" style={{ paddingRight: "1.5em" }}>
        <TextSemiBold
          fontSize="12px"
          color={Colors.mainGray}
          weight="400"
          className="mt-1"
          style={{ float: "right" }}
        >
          {title}
        </TextSemiBold>
      </div>
    </RadiusBox>
  );
};

export default InputRadiusWithRSpan;
const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
const Input = styled.input.attrs(() => ({}))`
  font-size: 14px;
  text-align: left;
  weight: 400;
  padding-left: 7px;
  width: 100%;
  border-width: 0px;
  border: none;
  &:focus {
    outline: none;
  }
`;
