import { forwardRef, useEffect, FunctionComponent, useState } from "react";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import { TextBold } from "../../components/text";
import Dropdown from "react-bootstrap/Dropdown";
import callApi from "../../pages/api/callApi";
import moment from "moment";
import Router from "next/router";
import Cookies from "universal-cookie";
import dayjs from "dayjs";
import axios from "axios";
interface Wrapper {
  children: any;
  id?: string;
  stepIcon: any;
  title: string;
  iconWidth?: string;
  iconHeight?: string;
  isLogout?: boolean;
  isDisplayTitle?: boolean;
  onClick?: () => any;
  titleIcon?: any;
}
interface CustomToggle {
  children: any;
  onClick: any;
  ref: any;
}
const Wrapper: FunctionComponent<Wrapper> = ({
  children,
  stepIcon,
  title,
  titleIcon,
  iconWidth,
  iconHeight,
  isDisplayTitle = true,
  isLogout = false,
  onClick,
}) => {
  const [listNotification, setListNotification] = useState([]);
  const [triggerRead, setTriggerRead] = useState(false);
  const [triggerIcon, setTriggerIcon] = useState(false);
  const [name, setName] = useState("");
  const [roleDisplay, setRoleDisplay] = useState("");
  const cookies = new Cookies();
  let email = cookies.get("email");
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  const CustomToggle: FunctionComponent<CustomToggle> = forwardRef(
    ({ children, onClick }, ref) => (
      <a
        href=""
        ref={ref as any}
        onClick={(e) => {
          e.preventDefault();
          // await setReadNoti();
          // onClick(e);
          Router.push({ pathname: "/notification" });
        }}
      >
        {children}
        {triggerIcon ? (
          <img
            src="/new/images/Bell_red.svg"
            width="30px"
            height="30px"
            // onClick={}
          />
        ) : (
          <img
            src="/new/images/Bell.svg"
            width="30px"
            height="30px"
            // onClick={}
          />
        )}
      </a>
    )
  );
  CustomToggle.displayName = "CustomToggle";

  const getNotification = async () => {
    const condition = { email: email };
    const response = await callApi.apiPost(
      "notification/findByEmail",
      condition
    );
    if (response.length > 0) {
      setListNotification(response);
      checkTriggerReadIcon(response);
    }
  };
  const setPersonalInfo = () => {
    setRoleDisplay(role);
    setName(firstname);
  };
  // const setReadNoti = async () => {
  //   if (!triggerRead) {
  //     setTriggerRead(!triggerRead);
  //     const condition = { email: email };
  //     await callApi.apiPost("notification/upateNoti", condition);
  //     setTriggerIcon(false);
  //   } else {
  //     getNotification();
  //   }
  // };
  const setTitle = (type: any) => {
    if (type == "ทรัพย์มาใหม่") {
      return "แจ้งเตือนทรัพย์เข้าใหม่";
    } else if (type == "ซ่อนทรัพย์") {
      return "แจ้งเตือนการซ่อนทรัพย์";
    } else {
      return "แจ้งเตือนการตรวจสอบทรัพย์สิน";
    }
  };
  const setSubTitle = (type: any, total: any) => {
    if (type == "ทรัพย์มาใหม่") {
      return `ทรัพย์เข้าใหม่ ${total}  รายการ`;
    } else if (type == "ซ่อนทรัพย์") {
      return `ซ่อนทรัพย์ ${total}  รายการ`;
    } else {
      return setSubTitleAsset(type);
    }
  };
  const setSubTitleAsset = (type: any) => {
    switch (type as any) {
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        return "ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์ อนุมัติ รหัสทรัพย์สิน  ";
      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        return "จนท.ฝ่ายการตลาด อนุมัติ รหัสทรัพย์สิน ";
      case "ปฏิเสธการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return "จนท.ฝ่ายการตลาด ปฏิเสธการอนุมัติ รหัสทรัพย์สิน  ";
      case "ปฏิเสธการตรวจสอบ จนท.ฝ่ายการตลาด":
        return "ผจก.ฝ่ายการตลาด ปฏิเสธการอนุมัติ รหัสทรัพย์สิน ";
      case "ตรวจสอบแล้ว":
        return "ผจก.ฝ่ายการตลาด การอนุมัติ รหัสทรัพย์สิน ";
      case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return "ผจก.ฝ่ายการตลาด การอนุมัติ รหัสทรัพย์สิน ";
      default:
        return "";
      // รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์
    }
  };
  const checkTriggerReadIcon = (item: any) => {
    for (var i = 0; i < item.length; ++i) {
      if (!item[i].read) {
        setTriggerIcon(true);
        return;
      } else {
        setTriggerIcon(false);
      }
    }
  };
  const genListNotification = () => {
    if (listNotification.length > 0) {
      return (
        <Dropdown.Menu
          className="Wrapper"
          style={{
            borderRadius: "15px",
            boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.1)",
          }}
        >
          {listNotification.map((item: any, index: number) => {
            return (
              <Dropdown.Item className="mt-2" key={index}>
                <div
                  className="row"
                  style={{
                    minWidth: "400px",
                  }}
                >
                  <div className="col-6">
                    {item.read ? (
                      ""
                    ) : (
                      <img
                        src="/new/images/dot_red.svg"
                        width="8px"
                        height="8px"
                        style={{ marginRight: "5px" }}
                      />
                    )}
                    <TextBold
                      color={Colors.mainBlack}
                      fontSize="16px"
                      weight="500"
                    >
                      {setTitle(item.type)}
                    </TextBold>
                  </div>
                  <div className="col-6">
                    <TextBold
                      color={Colors.mainGray}
                      fontSize="16px"
                      weight="300"
                      style={{ float: "right" }}
                    >
                      {dayjs(
                        item.update_date.replace("Z", "").replace("T", "")
                      ).format("DD/MM/YYYY HH:mm")}
                    </TextBold>
                  </div>
                </div>
                <TextBold
                  fontSize="16px"
                  weight="300"
                  color={Colors.mainBlack}
                  style={{
                    display: "block",
                    borderBottom:
                      listNotification.length != index + 1
                        ? "1px solid #E0E0EE"
                        : "",
                    paddingBottom:
                      listNotification.length != index + 1 ? "10px" : "",
                  }}
                >
                  {setSubTitle(item.type, item.total)}
                  <a
                    href="#"
                    onClick={() =>
                      Router.push({
                        pathname: "/propertylist/propertydetail",
                        query: {
                          mode: "view",
                          id: item.asset_id,
                          market_code: item.market_code,
                        },
                      })
                    }
                  >
                    {item.market_code}
                  </a>
                </TextBold>
              </Dropdown.Item>
            );
          })}
        </Dropdown.Menu>
      );
    } else {
      return (
        <Dropdown.Menu
          className="WrapperZeroNoti"
          style={{
            borderRadius: "15px",
            boxShadow: "0px 2px 15px rgba(0, 0, 0, 0.1)",
          }}
        >
          <Dropdown.Item className="mt-2">
            <div
              className="row"
              style={{
                minWidth: "400px",
              }}
            >
              <TextBold
                fontSize="16px"
                weight="300"
                color={Colors.mainBlack}
                align="center"
              >
                ไม่พบข้อมูลการแจ้งเตือน
              </TextBold>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      );
    }
  };
  const logout = async () => {
    const cookies = new Cookies();
    cookies.remove("token");
    cookies.remove("role");
    cookies.remove("firstname");
    cookies.remove("lastname");
    cookies.remove("email");
    cookies.remove("deptCode");

    Router.push({
      pathname: `https://login.microsoftonline.com/${process.env.NEXT_PUBLIC_AZURE_TENANT_ID}/oauth2/v2.0/logout?post_logout_redirect_uri=${process.env.NEXT_PUBLIC_BAM_BO_WEB}`,
    });
  };
  useEffect(() => {
    setPersonalInfo();
    getNotification();
  }, [email]);
  return (
    <div
      className="container"
      style={{
        height: "100vh",
        overflowY: "scroll",
      }}
    >
      <Flex className="mt-3" style={{ height: "100px" }}>
        <Flex className="flex-column" flex={0.7}>
          {isDisplayTitle && (
            <Flex className="flex-row align-items-center">{stepIcon}</Flex>
          )}
          {isDisplayTitle && (
            <TextBold fontSize="32px" className="mt-3">
              {titleIcon && (
                <img
                  src={titleIcon}
                  width={iconWidth ? iconWidth : "30px"}
                  height={iconHeight ? iconHeight : "30px"}
                  style={{ marginRight: "1rem" }}
                  onClick={onClick}
                />
              )}
              {title}
            </TextBold>
          )}
        </Flex>
        {isLogout && (
          <Flex
            className="flex-row justify-content-around align-items-center"
            flex={0.3}
            style={{
              borderRadius: "10px",
              backgroundColor: Colors.mainWhite,
              height: "80%",
            }}
          >
            <Flex className="justify-content-center" flex={0.2}>
              <Dropdown autoClose="outside">
                <Dropdown.Toggle as={CustomToggle} />

                {genListNotification()}
              </Dropdown>
            </Flex>
            <Flex
              className="flex-row align-items-center justify-content-center"
              width="auto"
              style={{ height: "100%" }}
              flex={0.45}
            >
              <Flex className="flex-column">
                <TextBold fontSize="18px" color={Colors.mainBlackLight}>
                  {name}
                </TextBold>
                <TextBold
                  fontSize="18px"
                  weight="300"
                  color={Colors.mainBlackLight}
                >
                  {roleDisplay}
                </TextBold>
              </Flex>
            </Flex>
            <Flex flex={0.25} className="justify-content-center">
              <img
                src="/new/images/Sign-Out.svg"
                width="30px"
                height="30px"
                onClick={() => {
                  logout();
                }}
              />
            </Flex>
          </Flex>
        )}
      </Flex>
      {children}
    </div>
  );
};

export default Wrapper;
