import React, { useState } from "react";
import styled from "styled-components";
import { menuList } from "../../../utils/mockData";
import ButtonRadius from "../Button/ButtonRadius";
import { Flex } from "../layout";

interface Menu {
  isOpen: boolean;
}

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [activeLang, setActiveLang] = useState("th");
  return (
    <Nav>
      <Logo href="">
        <img src={"new/images/bam_logo.png"} width="50px" height="50px" />
      </Logo>
      <Hamburger onClick={() => setIsOpen(!isOpen)}>
        <span />
        <span />
        <span />
      </Hamburger>
      <Menu isOpen={isOpen}>
        {menuList.map((item: any, index: number) => {
          return (
            <MenuLink href="" key={index + Math.random()}>
              {item}
            </MenuLink>
          );
        })}
        <Flex
          className="flex-row d-block d-sm-none justify-content-between"
          widthMobile="100%"
        >
          <ButtonRadius
            className="d-block d-sm-none"
            width="auto"
            height="40px"
            backgroundColor="white"
            title="เข้าสู่ระบบ/สมัครสมาชิก"
          />
          <Flex className="flex-row">
            <button
              style={{ border: "none", backgroundColor: "transparent" }}
              onClick={() => setActiveLang("th")}
            >
              <img
                src="new/images/th.png"
                width="30px"
                height="20px"
                style={{
                  filter: activeLang === "th" ? "none" : "grayscale(100%)",
                }}
              />
            </button>
            <button
              style={{ border: "none", backgroundColor: "transparent" }}
              onClick={() => setActiveLang("en")}
            >
              <img
                src="new/images/en.png"
                width="30px"
                height="20px"
                style={{
                  filter: activeLang === "en" ? "none" : "grayscale(100%)",
                }}
              />
            </button>
            <button
              style={{ border: "none", backgroundColor: "transparent" }}
              onClick={() => setActiveLang("ch")}
            >
              <img
                src="new/images/ch.png"
                width="30px"
                height="20px"
                style={{
                  filter: activeLang === "ch" ? "none" : "grayscale(100%)",
                }}
              />
            </button>
          </Flex>
        </Flex>
      </Menu>
      <ButtonRadius
        className="d-none d-sm-block"
        width="auto"
        height="40px"
        backgroundColor="white"
        title="เข้าสู่ระบบ/สมัครสมาชิก"
      />

      <button
        className="d-none d-sm-block"
        style={{ border: "none", backgroundColor: "transparent" }}
        onClick={() => setActiveLang("th")}
      >
        <img
          src="new/images/th.png"
          width="30px"
          height="20px"
          style={{ filter: activeLang === "th" ? "none" : "grayscale(100%)" }}
        />
      </button>
      <button
        className="d-none d-sm-block"
        style={{ border: "none", backgroundColor: "transparent" }}
        onClick={() => setActiveLang("en")}
      >
        <img
          src="new/images/en.png"
          width="30px"
          height="20px"
          style={{ filter: activeLang === "en" ? "none" : "grayscale(100%)" }}
        />
      </button>
      <button
        className="d-none d-sm-block"
        style={{ border: "none", backgroundColor: "transparent" }}
        onClick={() => setActiveLang("ch")}
      >
        <img
          src="new/images/ch.png"
          width="30px"
          height="20px"
          style={{ filter: activeLang === "ch" ? "none" : "grayscale(100%)" }}
        />
      </button>
    </Nav>
  );
};

export default Navbar;

const MenuLink = styled.a`
  padding: 1rem 1rem;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  color: white;
  transition: all 0.3s ease-in;
  font-size: 0.9rem;
  &:hover {
    color: #7b7fda;
  }
`;

const Nav = styled.div`
  padding: 0 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  background: #004c85;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  @media (max-width: 576px) {
    padding: 0 1rem;
    display: flex;
    justify-content: space-between;
    position: fixed;
  }
`;

const Logo = styled.a`
  padding: 1rem 0;
  color: #7b7fda;
  text-decoration: none;
  font-weight: 800;
  font-size: 1.7rem;
  span {
    font-weight: 300;
    font-size: 1.3rem;
  }
`;

const Menu = styled.div<Menu>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  @media (max-width: 768px) {
    overflow: hidden;
    flex-direction: column;
    max-height: ${({ isOpen }) => (isOpen ? "auto" : "0")};
    transition: max-height 0.3s ease-in;
    width: 100%;
    padding-bottom: ${({ isOpen }) => (isOpen ? "20px" : "auto")};
  }
`;

const Hamburger = styled.div`
  display: none;
  flex-direction: column;
  cursor: pointer;
  span {
    height: 2px;
    width: 25px;
    background: white;
    margin-bottom: 4px;
    border-radius: 5px;
  }
  @media (max-width: 768px) {
    display: flex;
  }
`;
