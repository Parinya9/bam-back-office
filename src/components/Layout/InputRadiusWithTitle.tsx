import { FunctionComponent } from "react";
import styled from "styled-components";
import { TextSemiBold } from "../text";
import { Flex } from "../../components/layout";
import Colors from "../../../utils/Colors";

interface InpuutWithTitleType {
  title?: string;
  flex?: number;
  placeholder?: string;
}

const InputWithTitle: FunctionComponent<InpuutWithTitleType> = ({
  title,
  flex,
  placeholder,
}) => {
  return (
    <RadiusBox>
      <Flex className="flex-column px-1" flex={flex}>
        <TextSemiBold
          fontSize="13px"
          color={Colors.mainDarkBlue}
          className="mt-2"
        >
          {title || "BAM"}
        </TextSemiBold>
        <Input placeholder={placeholder} className="mt-1" />
      </Flex>
    </RadiusBox>
  );
};

export default InputWithTitle;

const Input = styled.input.attrs(() => ({}))`
  font-size: 0.6em;
  width: 100%;
  border-width: 0px;
  border: none;
  &:focus {
    outline: none;
  }
`;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row px-4 py-2",
}))`
  background-color: #fff;
  width: 100%;
  height: 25%;
  border-radius: 30px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
