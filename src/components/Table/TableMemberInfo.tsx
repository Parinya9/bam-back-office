import React, { FunctionComponent, useEffect, useState } from "react";
import { Table } from "react-bootstrap";
// import Pagination from "./Pagination";
import Pagination from "./pagination/Pagination";

import Router from "next/router";
import dayjs from "dayjs";
interface Table {
  data: any;
  onClickDelete: Function;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
  // exportExcel: any;
}
const TableMemberInfo: FunctionComponent<Table> = ({
  data,
  onClickDelete,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
}) => {
  const header = [
    "Ref.",
    "ชื่อ - นามสกุล",
    "ชื่อเข้าใช้งาน / อีเมล",
    "เบอร์โทรศัพท์",
    "สถานะการใช้งาน",
    "วันที่ลงทะเบียน",
    "วันที่เข้าใช้งานล่าสุด",
    "",
    "",
  ];

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.refno}</td>
          <td>{item.firstname + " " + item.lastname}</td>
          <td>{item.email}</td>
          <td>{item.phone}</td>
          <td>
            {item.status ? (
              <img
                src="new/images/Ellipse-green.svg"
                width="10px"
                height="10px"
                style={{
                  marginLeft: "4px",
                  marginRight: "4px",
                  marginBottom: "4px",
                }}
              />
            ) : (
              <img
                src="new/images/Ellipse_gray.svg"
                width="10px"
                height="10px"
                style={{
                  marginLeft: "4px",
                  marginRight: "4px",
                  marginBottom: "4px",
                }}
              />
            )}
            {item.status ? "อนุมัติการใช้งาน" : "ไม่อนุมัติการใช้งาน"}
          </td>
          <td>
            {dayjs(item.create_date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>
          <td>
            {" "}
            {dayjs(item.update_date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>
          <td>
            <img
              src="new/images/Edit.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() =>
                Router.push({
                  pathname: `memberinfo/addmemberinfo`,
                  query: {
                    id: item.id,
                    mode: "Edit",
                  },
                })
              }
            />
          </td>
          <td>
            <img
              src="new/images/Trash.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickDelete(item.id)}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableMemberInfo;
