import React, { FunctionComponent, useState } from "react";
import { Table } from "react-bootstrap";
import Pagination from "./pagination/Pagination";

import moment from "moment";
import PurchaseComplete from "../../pages/pdf/PurchaseComplete";
import Colors from "../../../utils/Colors";
import dayjs from "dayjs";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import callApi from "../../pages/api/callApi";
import { apiSyncAssetDetail } from "../../pages/api/elsSync";
import Cookies from "universal-cookie";
import ModalLoading from "../Modal/ModalLoading";

interface Table {
  data: any;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
}

const TableProperty: FunctionComponent<Table> = ({
  data,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
}) => {
  const cookies = new Cookies();
  let role = cookies.get("role");
  const [visible, setVisible] = useState(false);

  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const header = [
    "#",
    "รหัสการจอง",
    "แพ็คเกจรีโนเวท",
    "วันที่ทำการจอง",
    "รหัสทรัพย์สิน",
    "ชื่อ - นามสกุล",
    "อีเมล",
    "เบอร์โทรศัพท์",
    "สถานะการชำระเงิน",
    "สถานะ",
    "",
    "",
  ];
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const setDateDisplay = (date: any) => {
    var monthNamesThai = [
      "ม.ค.",
      "ก.พ.",
      "มี.ค.",
      "เม.ษ",
      "พ.ค.",
      "มิ.ย.",
      "ก.ค.",
      "ส.ค.",
      "ก.ย.",
      "ต.ค.",
      "พ.ย.",
      "ธ.ค.",
    ];
    let dateSplitTime = date;
    dateSplitTime = dateSplitTime.split(" ");
    let dateSplitDay = dateSplitTime[0].split("/");
    let year = parseInt(dateSplitDay[2]) + 543;
    let fullDateTime =
      dateSplitDay[0] +
      " " +
      monthNamesThai[dateSplitDay[1] - 1] +
      " " +
      year +
      " " +
      dateSplitTime[1];

    return fullDateTime;
  };
  const genHeader = () => {
    return (
      <tr>
        {header.map(function (item, i) {
          return <th key={item}>{item}</th>;
        })}
      </tr>
    );
  };
  const mapStatusPayment = (status: any) => {
    let listStatusPayment = [
      { th: "ชำระเงินเรียบร้อย", en: "success" },
      { th: "ไม่ชำระเงิน", en: "fail" },
      { th: "รอชำระเงิน", en: "pending" },
    ];
    for (let index = 0; index < listStatusPayment.length; index++) {
      if (listStatusPayment[index].en === status) {
        return listStatusPayment[index].th;
      }
    }
    return status;
  };
  const setDisplayStatus = (item: any) => {
    let statusPayment = mapStatusPayment(item.status_payment);
    if (statusPayment === "ชำระเงินเรียบร้อย") {
      return (
        <div>
          <img
            src="new/images/Ellipse-green.svg"
            width="10px"
            height="10px"
            style={{
              marginLeft: "4px",
              marginRight: "4px",
              marginBottom: "4px",
            }}
          />
          <span style={{ color: Colors.greenLight, display: "block" }}>
            {statusPayment}
          </span>
          <span style={{ color: Colors.mainBlack, display: "block" }}>
            {item.reserve_type === "credit"
              ? "[บัตรเครดิตและเดบิต]"
              : "[QR Code]"}
          </span>
          <span style={{ color: Colors.mainGrayLight, display: "block" }}>
            {setDateDisplay(
              dayjs(item.create_date.replace("Z", "").replace("T", "")).format(
                "DD/MM/YYYY HH:mm"
              )
            )}
          </span>
        </div>
      );
    } else {
      return (
        <div>
          <img
            src="new/images/Ellipse_gray.svg"
            width="10px"
            height="10px"
            style={{
              marginLeft: "4px",
              marginRight: "4px",
              marginBottom: "4px",
            }}
          />
          <span style={{ color: Colors.mainGrayLight }}>{statusPayment}</span>
        </div>
      );
    }
  };
  const rejectAsset = async (reserveId: any, assetId: any) => {
    let listData = {
      idReserve: parseInt(reserveId),
      statusPayment: "ไม่อนุมัติ",
      statusAsset: "ทรัพย์พร้อมขาย",
      market_code: assetId,
    };

    try {
      setVisible(true);
      var item = { market_code: assetId, start: 0, limit: 10 };
      const assetItem = await callApi.apiPost("property-detail/find", item);
      let objLog = {
        asset_id: parseInt(assetItem[0].id),
        status_approve: false,
        user: firstname + " " + lastname,
        position: role,
        title: "ชำระเงิน",
        note: "",
      };
      await callApi.apiPut("reserve-asset/updateStatusPayment", listData);
      await callApi.apiPost("history-log/addLog", objLog);

      const response = await apiSyncAssetDetail("api/asset-detail");
      if (response.status === "success") {
        alert("Update Success");
        setVisible(false);
      } else {
        alert("Cant'not update asset");
        setVisible(false);
      }
      fetchData();
    } catch (error: any) {
      setVisible(false);

      console.log("Log Error From Reject Asset ", error);
    }
  };
  const approveAsset = async (reserveId: any, assetId: any) => {
    var item = { market_code: assetId, start: 0, limit: 10 };

    let listData = {
      idReserve: parseInt(reserveId),
      statusPayment: "อนุมัติ",
      statusAsset: "อยู่ระหว่างการทำสัญญา",
      market_code: assetId,
    };
    try {
      const assetItem = await callApi.apiPost("property-detail/find", item);
      let objLog = {
        asset_id: parseInt(assetItem[0].id),
        status_approve: true,
        user: firstname + " " + lastname,
        position: role,
        title: "ชำระเงิน",
        note: "",
      };
      await callApi.apiPut("reserve-asset/updateStatusPayment", listData);
      await callApi.apiPost("history-log/addLog", objLog);

      const response = await apiSyncAssetDetail("api/asset-detail");
      if (response.status === "success") {
        alert("Update Success");
      } else {
        alert("Cant'not update asset");
      }
      fetchData();
    } catch (error: any) {
      console.log("Log Error From Approve Asset ", error);
    }

    //call api update status reserve & status asest
    //อนุมัติ อยู่ระหว่างการทำสัญญา
  };
  const buttonApproveStatus = (status: any, assetId: any, reserveId: any) => {
    if (status === "รอการอนุมัติ") {
      return (
        <div className="">
          <ButtonRadius
            type="button"
            title="ไม่อนุมัติ"
            background="#FF2727"
            color={Colors.mainWhite}
            width="85px"
            fontSize="18px"
            className="mx-1 py-2"
            border="none"
            onClick={() => {
              rejectAsset(reserveId, assetId);
            }}
          />{" "}
          <ButtonRadius
            type="button"
            title="อนุมัติ"
            onClick={() => {
              approveAsset(reserveId, assetId);
            }}
            background={Colors.mainBlue}
            color={Colors.mainWhite}
            width="85px"
            fontSize="18px"
            className="mx-1 py-2"
            border="none"
          />
        </div>
      );
    }
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.no}</td>
          <td>{item.reserve_no}</td>
          <td>{item.package_renovate}</td>
          <td>
            {" "}
            {dayjs(item.create_date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>
          <td>{item.asset_id}</td>
          <td>{item.fullname}</td>
          <td>{item.email}</td>
          <td>{item.phone}</td>
          <td>{setDisplayStatus(item)}</td>
          <td>{item.status_approve}</td>

          <td>
            {buttonApproveStatus(item.status_approve, item.asset_id, item.id)}
          </td>
          <td>
            {item.status_payment == "success" ? (
              <PurchaseComplete
                reserveno={item.reserve_no}
                assetNo={item.asset_id}
                fullname={item.fullname}
                currentDate={moment().format("DD/MM/YYYY")}
                reserveDate={moment(item.create_date).format("DD/MM/YYYY")}
                price={"10000"}
                adminName={item.admin_name}
              ></PurchaseComplete>
            ) : (
              ""
            )}
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
      <ModalLoading visible={visible} />
    </>
  );
};
export default TableProperty;
