import React, { FunctionComponent } from "react";
import { Table } from "react-bootstrap";
// import Pagination from "./Pagination";
import Pagination from "./pagination/Pagination";
import dayjs from "dayjs";
import Cookies from "universal-cookie";

interface Table {
  data: any;
  onClickDelete: Function;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
  // exportExcel: any;
}

const TableInterestProperty: FunctionComponent<Table> = ({
  data,
  onClickDelete,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
}) => {
  const cookies = new Cookies();
  let role = cookies.get("role");
  const header = [
    "#",
    "รหัสทรัพย์สิน",
    "ทรัพย์สิน",
    "อำเภอทรัพย์",
    "ชื่อ - นามสกุล",
    "อีเมล",
    "เบอร์โทรศัพท์",
    "เรื่อง",
    "ชื่อ ผู้ดูแล",
    "ฝ่ายงาน",
    "กลุ่มงาน",
    "วันที่ติดต่อ",
  ];
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const genHeader = () => {
    return (
      <tr>
        {header.map(function (item, i) {
          return <th key={item}>{item}</th>;
        })}
      </tr>
    );
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.no}</td>
          <td>{item.asset_id}</td>
          <td>{item.asset_name}</td>
          <td>{item.district}</td>
          <td>{item.fullname}</td>
          <td>{item.email}</td>
          <td>{item.phone}</td>
          <td>{item.title}</td>
          <td>{item.admin_name}</td>
          <td>{item.work_group}</td>
          <td>{item.department}</td>

          <td>
            {dayjs(item.created_at.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>

          <td>
            {role !== "จำหน่ายทรัพย์ (Sale)" && (
              <img
                src="new/images/Trash.svg"
                width="20px"
                height="20px"
                style={{ margin: "5px" }}
                onClick={() => onClickDelete(item.id)}
              />
            )}
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
          minWidth={"1500px"}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableInterestProperty;
