import { useState, FunctionComponent, useMemo, useEffect } from "react";
import { Flex } from "../layout";
import { Table } from "react-bootstrap";
import Colors from "../../../utils/Colors";

interface Pagination {
  data: any;
  RenderComponent: any;
  header: any;

  pageLimit: number;
  dataLimit: number;
  minWidth?: number;
}

const Pagination: FunctionComponent<Pagination> = ({
  data,
  RenderComponent,
  pageLimit,
  dataLimit,
  header,
  minWidth = "",
}) => {
  const [pages, setPages] = useState(Math.ceil(data.length / dataLimit));
  const [currentPage, setCurrentPage] = useState(1);

  function goToNextPage() {
    setCurrentPage((page) => page + 1);
  }

  function goToPreviousPage() {
    setCurrentPage((page) => page - 1);
  }

  function changePage(event: any) {
    const pageNumber = Number(event.target.textContent);
    setCurrentPage(pageNumber);
  }

  const getPaginatedData = () => {
    const startIndex = currentPage * dataLimit - dataLimit;
    const endIndex = startIndex + dataLimit;
    return data.slice(startIndex, endIndex);
  };

  const getPaginationGroup = () => {
    let start = Math.floor((currentPage - 1) / pageLimit) * pageLimit;
    return new Array(pageLimit).fill(null).map((_, idx) => start + idx + 1);
  };
  useEffect(() => {
    setPages(Math.ceil(data.length / dataLimit));
    setCurrentPage(1);
  }, [data]);
  return (
    <>
      <div style={{ overflowX: "scroll" }}>
        <Table
          style={{
            backgroundColor: "white",
            borderRadius: "10px",
            minWidth: minWidth,
          }}
          className="mt-3"
        >
          <thead>{header}</thead>
          <tbody style={{ borderTop: "none" }}>
            {getPaginatedData().map((d: any, idx: any) => (
              <RenderComponent key={idx} item={d} i={idx} />
            ))}
          </tbody>
        </Table>
      </div>
      {data.length <= 10 ? null : (
        <Flex
          className="flex-row justify-content-center align-items-center mb-3"
          style={{ backgroundColor: "transparent" }}
        >
          <button
            onClick={goToPreviousPage}
            className="me-3"
            style={{
              backgroundColor:
                currentPage === 1 ? "transparent" : Colors.mainDarkBlue,
              borderRadius: "50%",
              width: "45px",
              height: "45px",
              borderColor:
                currentPage === 1 ? Colors.borderGray : "transparent",
              borderWidth: "1px",
              borderStyle: "solid",
            }}
            disabled={currentPage === 1 ? true : false}
            //   className={`prev ${currentPage === 1 ? "disabled" : ""}`}
          >
            <img
              src="/new/images/arrow-left.svg"
              width="10px"
              height="10px"
              className={currentPage === 1 ? "filter-gray" : ""}
            />
          </button>

          {getPaginationGroup().map((item, index) => (
            <button
              key={index}
              onClick={changePage}
              className={`paginationItem ${
                currentPage === item ? "active" : "inactive"
              }`}
            >
              <span>{`${item}`}</span>
            </button>
          ))}

          <button
            onClick={goToNextPage}
            className={`next ${currentPage === pages ? "disabled" : ""}`}
            // className="ms-3"
            style={{
              backgroundColor:
                currentPage === pages ? "transparent" : Colors.mainDarkBlue,
              borderRadius: "50%",
              width: "45px",
              height: "45px",
              borderColor:
                currentPage === pages ? Colors.borderGray : "transparent",
              borderWidth: "1px",
              borderStyle: "solid",
            }}
            disabled={currentPage === pages ? true : false}
          >
            <img
              src="/new/images/arrow-right.svg"
              width="10px"
              height="10px"
              className={currentPage === pages ? "filter-gray" : ""}
            />
          </button>
        </Flex>
      )}
    </>
  );
};

export default Pagination;
