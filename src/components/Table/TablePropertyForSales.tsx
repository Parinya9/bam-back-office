import Router from "next/router";
import React, { forwardRef, FunctionComponent, useState } from "react";
import { Table } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import Pagination from "./pagination/Pagination";

interface Table {
  data: any;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
  onClickDownloadQR: Function;
}

const TablePropertyForSales: FunctionComponent<Table> = ({
  data,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
  onClickDownloadQR,
}) => {
  interface CustomToggle {
    children: any;
    onClick: any;
    ref: any;
  }
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const CustomToggle: FunctionComponent<CustomToggle> = forwardRef(
    ({ children, onClick }, ref) => (
      <a
        href=""
        ref={ref as any}
        onClick={(e) => {
          e.preventDefault();
          onClick(e);
        }}
      >
        {children}
        <img src="new/images/Dots_blue.svg" width="20px" height="20px" />
      </a>
    )
  );
  CustomToggle.displayName = "CustomToggle";

  const header = [
    "#",
    "คดีแดงเลขที่",
    "เลขที่เอกสารสิทธิ์",
    "จังหวัด",
    "อำเภอ",
    "ประเภททรัพย์สิน",
    "ราคาประเมิน",
    "สถานที่ขายทอดตลาด",
    "ติดต่อเจ้าหน้าที่",
    "",
  ];

  const [openOption, setOpenOption] = useState(false);
  const [row, setRow] = useState(999);
  const mockProvince = [
    {
      title: "ดูรายละเอียดทรัพย์",
      type: "readonly",
      txtTitle: "คดีแดงเลขที่ - 791/2545",
    },
    {
      title: "แก้ไขรายละเอียดทรัพย์",
      type: "edit",
      txtTitle: "คดีแดงเลขที่ - 791/2545",
    },
    { title: "ดาวน์โหลด QR Code", type: "qrcode" },
  ];

  const genHeader = () => {
    return (
      <tr>
        {header.map(function (item, i) {
          return <th key={item}>{item}</th>;
        })}
      </tr>
    );
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <tr key={i}>
        <td>{item.no}</td>
        <td style={{ color: "#FF2727" }}>{item.caseno}</td>
        <td>{item.licenseno}</td>
        <td>{item.province}</td>
        <td>{item.district}</td>
        <td>{item.asset_type}</td>
        <td>
          {" "}
          {parseFloat(item.price_estimate_of_legal_officer).toLocaleString()}
        </td>
        <td style={{ width: "200px" }}>{item.place_auction}</td>
        <td>{item.contact}</td>
        <td style={{ verticalAlign: "middle" }}>
          <button style={{ backgroundColor: "transparent", border: "none" }}>
            <Dropdown>
              <Dropdown.Toggle as={CustomToggle} />
              <Dropdown.Menu title="">
                <Dropdown.Item
                  className="mt-2"
                  onClick={() =>
                    Router.push({
                      pathname: "/propertyforsales/addpropertyforsales",
                      query: {
                        id: item.id,
                        mode: "readonly",
                      },
                    })
                  }
                >
                  ดูรายละเอียดทรัพย์
                </Dropdown.Item>
                <Dropdown.Item
                  className="mt-2"
                  onClick={() =>
                    Router.push({
                      pathname: "/propertyforsales/addpropertyforsales",
                      query: {
                        id: item.id,
                        mode: "Edit",
                      },
                    })
                  }
                >
                  แก้ไขรายละเอียดทรัพย์
                </Dropdown.Item>
                <Dropdown.Item
                  className="mt-2"
                  onClick={() => {
                    onClickDownloadQR(item.asset_url, item.caseno);
                  }}
                >
                  ดาวน์โหลด QR code
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </button>
        </td>
      </tr>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TablePropertyForSales;
