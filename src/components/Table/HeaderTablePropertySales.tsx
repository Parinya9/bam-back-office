import { TextBold } from "../text";
import React, { FunctionComponent } from "react";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";

interface Header {
  number: number;
  caseno: string;
}
const HeaderTablePropertySales: FunctionComponent<Header> = ({
  number,
  caseno,
}) => {
  return (
    <Flex className=" flex-row">
      <Flex className="flex-row" flex={0.3}>
        <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
          ข้อมูลทรัพย์
        </TextBold>
        <TextBold fontSize="16px" weight="400" color={Colors.mainBlue}>
          &nbsp;&nbsp;
          {number}
        </TextBold>
        <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
          &nbsp; รายการ
        </TextBold>
      </Flex>
      {number > 0 && caseno != "" ? (
        <Flex className="flex-row" flex={0.3}>
          <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
            ทรัพย์ขายทอดของรหัส
          </TextBold>
          <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
            &nbsp; {caseno}
          </TextBold>
          <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
            &nbsp; มี
          </TextBold>
          <TextBold fontSize="16px" weight="400" color={Colors.mainBlue}>
            &nbsp; {number}
          </TextBold>
          <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
            &nbsp; คดี
          </TextBold>
        </Flex>
      ) : (
        ""
      )}
    </Flex>
  );
};

export default HeaderTablePropertySales;
