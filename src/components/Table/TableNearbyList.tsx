import Router from "next/router";
import React, { FunctionComponent } from "react";
import { Table } from "react-bootstrap";
import Pagination from "./pagination/Pagination";

interface Table {
  data: any;
  onClickDelete: Function;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
}

const TableNearbyList: FunctionComponent<Table> = ({
  data,
  onClickDelete,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
}) => {
  const header = ["หัวข้อ (TH)", "หัวข้อ (EN)", "", ""];

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={item.id}>
          <td>{item.title_th}</td>
          <td>{item.title_en}</td>

          <td>
            <img
              src="new/images/Edit.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() =>
                Router.push({
                  pathname: `/nearbylist/addnearbylist`,
                  query: {
                    id: item.id,
                    title_th: item.title_th,
                    title_en: item.title_en,
                    mode: "Edit",
                    show_display: item.show_display,
                  },
                })
              }
            />
          </td>
          <td>
            <img
              src="new/images/Trash.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickDelete(item.id)}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableNearbyList;
