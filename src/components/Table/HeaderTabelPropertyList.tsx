import { TextBold } from "../text";
import React, { FunctionComponent } from "react";
import Colors from "../../../utils/Colors";
import { Flex } from "../layout";
import ButtonRadius from "../Button/ButtonRadius";
import { TextSemiBold } from "../text";
import callApi from "../../pages/api/callApi";
import Cookies from "universal-cookie";

interface Header {
  number: string;
  setSearch: any;
  setData: any;
  setTotal: any;
  setCurrentPage: any;
}
const HeaderTabelPropertyList: FunctionComponent<Header> = ({
  number,
  setSearch,
  setData,
  setTotal,
  setCurrentPage,
}) => {
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  let deptCode = cookies.get("deptCode");
  const onClickStatusApprove = async (status: string) => {
    var item = { status_approve: status, start: 0, limit: 10 };
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(item, {
        ao_name: firstname + " " + lastname,
      });
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      Object.assign(item, {
        dept_code: deptCode,
      });
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      Object.assign(item, {
        v_name: firstname + " " + lastname,
      });
    }
    setSearch(item);
    const response = await callApi.apiPost("property-detail/find", item);
    if (response.length > 0) {
      setData(response);
      setTotal(response[0].total);
      setCurrentPage(1);
    } else {
      setData([]);
      setTotal(0);
      setCurrentPage(0);
    }
  };
  return (
    <Flex className=" flex-row">
      <Flex className="flex-row" flex={0.22}>
        <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
          ข้อมูล
        </TextBold>
        <TextBold fontSize="16px" weight="400" color={Colors.mainBlue}>
          &nbsp; {number}
        </TextBold>
        <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
          &nbsp; รายการ
        </TextBold>
      </Flex>
      <Flex className="flex-row justify-content-around" flex={0.78}>
        <ButtonRadius
          width="auto"
          title="ทรัพย์มาใหม่"
          color={Colors.mainWhite}
          backgroundColor="#CED4DA"
          fontSize="13px"
          onClick={() => onClickStatusApprove("ทรัพย์มาใหม่")}
        />
        <ButtonRadius
          width="auto"
          title="รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์"
          color={Colors.mainWhite}
          backgroundColor="rgba(240, 173, 78, 1)"
          fontSize="13px"
          onClick={() =>
            onClickStatusApprove(
              "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์"
            )
          }
        />
        <ButtonRadius
          width="auto"
          title="รอการตรวจสอบ จนท.ฝ่ายการตลาด"
          color={Colors.mainWhite}
          backgroundColor="rgba(255, 105, 180, 1)"
          fontSize="13px"
          onClick={() => onClickStatusApprove("รอการตรวจสอบ จนท.ฝ่ายการตลาด")}
        />
        <ButtonRadius
          width="auto"
          title="รอการตรวจสอบ ผจก.ฝ่ายการตลาด"
          color={Colors.mainWhite}
          backgroundColor="rgba(51, 122, 183, 1)"
          fontSize="13px"
          onClick={() => onClickStatusApprove("รอการตรวจสอบ ผจก.ฝ่ายการตลาด")}
        />
        <ButtonRadius
          width="auto"
          title="ตรวจสอบแล้ว"
          color={Colors.mainWhite}
          backgroundColor="rgba(92, 184, 92, 1)"
          fontSize="13px"
          onClick={() => onClickStatusApprove("ตรวจสอบแล้ว")}
        />
      </Flex>
    </Flex>
  );
};

export default HeaderTabelPropertyList;
