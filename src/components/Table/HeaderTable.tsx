import { TextBold } from "../text";
import React, { FunctionComponent } from "react";
import Colors from "../../../utils/Colors";

interface Header {
  number: any;
}
const HeaderTable: FunctionComponent<Header> = ({ number }) => {
  return (
    <>
      <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
        ข้อมูล{" "}
      </TextBold>
      <TextBold fontSize="16px" weight="400" color={Colors.mainBlue}>
        {number}{" "}
      </TextBold>
      <TextBold fontSize="16px" weight="400" color={Colors.mainBlackLight}>
        รายการ
      </TextBold>
    </>
  );
};

export default HeaderTable;
