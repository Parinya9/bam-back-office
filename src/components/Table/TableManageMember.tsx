import { FunctionComponent } from "react";
import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import Router from "next/router";
import Pagination from "./Pagination";
import dayjs from "dayjs";
interface Table {
  data: any;
  onClickDelete: Function;
}
const TableMemberInfo: FunctionComponent<Table> = ({ data, onClickDelete }) => {
  const header = [
    "No.",
    "ชื่อ",
    "นามสกุล",
    "Role",
    "Email",
    "สถานะผู้ใช้งาน",
    "",
  ];

  // const data = [
  //   {
  //     no: "1",
  //     nameLastName: "สมชาย ใจดี",
  //     role: "AM",
  //     position: "Section - Manager",
  //     organization: "BAM website",
  //     createdate: "16/10/2021  10:30",
  //     lastupdate: "20/10/2021  20:30",
  //   },
  //   {
  //     no: "2",
  //     nameLastName: "สมชาย ใจดี",
  //     role: "Viewer",
  //     position: "Section - Manager",
  //     organization: "BAM website",
  //     createdate: "16/10/2021  10:30",
  //     lastupdate: "20/10/2021  20:30",
  //   },
  //   {
  //     no: "3",
  //     nameLastName: "สมชาย ใจดี",
  //     role: "Viewer",
  //     position: "Section - Manager",
  //     organization: "BAM website",
  //     createdate: "16/10/2021  10:30",
  //     lastupdate: "20/10/2021  20:30",
  //   },
  // ];

  const onClickEditBtn = (no: string) => {
    Router.push({
      pathname: "/managemember/addmember",
      query: {
        no,
      },
    });
  };

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.no}</td>
          <td>{item.firstname}</td>
          <td>{item.lastname}</td>
          <td>{item.role}</td>
          <td>{item.email}</td>

          <td>
            <button style={{ backgroundColor: "transparent", border: "none" }}>
              <img
                src="new/images/Edit.svg"
                width="20px"
                height="20px"
                style={{ margin: "5px" }}
                onClick={() =>
                  Router.push({
                    pathname: `managemember/addmember`,
                    query: {
                      id: item.id,
                      mode: "edit",
                    },
                  })
                }
              />
            </button>
          </td>
          <td>
            <img
              src="new/images/Trash.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickDelete(item.id)}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          pageLimit={Math.ceil(data.length / 10)}
          dataLimit={10}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableMemberInfo;
