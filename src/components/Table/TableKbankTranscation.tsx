import React, { FunctionComponent } from "react";
import { Table } from "react-bootstrap";
import Pagination from "./pagination/Pagination";

import Colors from "../../../utils/Colors";
import dayjs from "dayjs";
interface Table {
  data: any;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
}
const TableKbankTranscation: FunctionComponent<Table> = ({
  data,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
}) => {
  const header = [
    "#",
    "รหัสการจอง",
    "วันที่ทำการจอง",
    "รหัสทรัพย์สิน",
    "ชื่อ - นามสกุล ผู้จอง",
    "อีเมล",
    "เบอร์โทรศัพท์",
    "สถานะการชำระเงิน",
    "วันที่ยืนยัน",
  ];
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const genHeader = () => {
    return (
      <tr>
        {header.map(function (item, i) {
          return <th key={item}>{item}</th>;
        })}
      </tr>
    );
  };
  const mapStatusPayment = (status: any) => {
    let listStatusPayment = [
      { th: "ชำระเงินเรียบร้อย", en: "success" },
      { th: "ไม่ชำระเงิน", en: "fail" },
      { th: "รอชำระเงิน", en: "pending" },
    ];
    for (let index = 0; index < listStatusPayment.length; index++) {
      if (listStatusPayment[index].en === status) {
        return listStatusPayment[index].th;
      }
    }
    return status;
  };
  const setDisplayStatus = (item: any) => {
    let statusPayment = mapStatusPayment(item.status_payment);
    if (statusPayment === "ชำระเงินเรียบร้อย") {
      return (
        <div>
          <img
            src="new/images/Ellipse-green.svg"
            width="10px"
            height="10px"
            style={{
              marginLeft: "4px",
              marginRight: "4px",
              marginBottom: "4px",
            }}
          />
          <span style={{ color: Colors.greenLight, display: "block" }}>
            {statusPayment}
          </span>
        </div>
      );
    } else {
      return (
        <div>
          <img
            src="new/images/Ellipse_gray.svg"
            width="10px"
            height="10px"
            style={{
              marginLeft: "4px",
              marginRight: "4px",
              marginBottom: "4px",
            }}
          />
          <span style={{ color: Colors.mainGrayLight }}>{statusPayment}</span>
        </div>
      );
    }
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.no}</td>
          <td>{item.reserve_no}</td>
          <td>
            {" "}
            {dayjs(item.reserve_date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>
          <td>{item.asset_id}</td>
          <td>{item.fullname}</td>
          <td>{item.email}</td>
          <td>{item.phone}</td>

          <td>{setDisplayStatus(item)}</td>
          <td>
            {" "}
            {dayjs(item.create_date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableKbankTranscation;
