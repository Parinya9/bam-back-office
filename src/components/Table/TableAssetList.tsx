import type { NextPage } from "next";
import Router from "next/router";
import React, { forwardRef, FunctionComponent, useState } from "react";
import { Table } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import Switch from "react-switch";
import { Flex } from "../layout";
import Colors from "../../../utils/Colors";
import Pagination from "./pagination/Pagination";
import callApi from "../../pages/api/callApi";
import { useForm, Controller } from "react-hook-form";
import ModalPropertyDisplay from "../Modal/ModalPropertyDisplay";
import RoleDisplay from "../../../utils/RolesDisplay";
import Cookies from "universal-cookie";
import { apiSyncAssetDetail } from "../../pages/api/elsSync";
import ModalLoading from "../../components/Modal/ModalLoading";

interface Table {
  data: any;
  total: any;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  exportExcel: any;
  setData: any;
  setTotal: any;
}
const TableAssetList: FunctionComponent<Table> = ({
  data,
  total,
  currentPage,
  setCurrentPage,
  fetchData,
  exportExcel,
  setData,
  setTotal,
}) => {
  const [showDisplayProperty, setShowDisplayProperty] = useState(false);
  const [show, setShow] = useState(false);
  const [visible, setVisible] = useState(false);
  const [idItem, setIdItem] = useState(0);
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const {
    control,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = useForm();
  const header = [
    "ประเภททรัพย์สิน",
    "รหัสทรัพย์สิน",
    "ชื่อทรัพย์สิน",
    "ชื่อผู้ดูแล",
    "ละติจูด",
    "ลองติจูด",
    "อำเภอ",
    "จังหวัด",
    "ราคา",
    "สถานะทรัพย์สิน",
    "",
    "",
  ];

  interface CustomToggle {
    children: any;
    onClick: any;
    ref: any;
  }
  const updateDisplayProperty = async (id: any, display_property: any) => {
    if (display_property) {
      let objEdit = {
        display_property: "true",
        display_property_desc: "",
        status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        id: parseInt(id),
      };
      setVisible(true);
      const item = await callApi.apiPut("property-detail/update", objEdit);
      const response = await apiSyncAssetDetail("api/asset-detail");
      if (response.status === "success") {
        setVisible(false);

        alert("Update Success");
      } else {
        setVisible(false);

        alert("Cant'not update asset");
      }
      fetchData(setStartData(currentPage), 10);
    } else {
      setIdItem(id);
      setShowDisplayProperty(true);
    }
  };

  const UpdateDisplayHomeCondo = async (id: any, display_home_condo: any) => {
    let objEdit = {
      display_home_condo: "",
      status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
      id: parseInt(id),
    };
    if (display_home_condo) {
      objEdit.display_home_condo = "true";
    } else {
      objEdit.display_home_condo = "false";
    }
    const item = await callApi.apiPut("property-detail/update", objEdit);
    const response = await apiSyncAssetDetail("api/asset-detail");
    if (response.status === "success") {
      alert("Update Success");
    } else {
      alert("Cant'not update asset");
    }
    fetchData(setStartData(currentPage), 10);
  };

  const CustomToggle: FunctionComponent<CustomToggle> = forwardRef(
    ({ children, onClick }, ref) => (
      <a
        href=""
        ref={ref as any}
        onClick={(e) => {
          e.preventDefault();
          onClick(e);
        }}
      >
        {children}
        <img src="new/images/Dots_blue.svg" width="20px" height="20px" />
      </a>
    )
  );
  CustomToggle.displayName = "CustomToggle";

  const getImageStatus = (id: any, status: string, market_code: string) => {
    if (
      status === "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์" &&
      RoleDisplay.checkStatusApprove(role, status)
    ) {
      return (
        <img
          src="new/images/Status_orange.svg"
          width="40px"
          height="40px"
          onClick={() => {
            approveAsset(
              id,
              market_code,
              "รอการตรวจสอบ จนท.ฝ่ายการตลาด",
              "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์"
            );
          }}
        />
      );
    } else if (
      status === "รอการตรวจสอบ จนท.ฝ่ายการตลาด" &&
      RoleDisplay.checkStatusApprove(role, status)
    ) {
      return (
        <img
          src="new/images/Status-pink.svg"
          width="40px"
          height="40px"
          onClick={() => {
            approveAsset(
              id,
              market_code,
              "รอการตรวจสอบ ผจก.ฝ่ายการตลาด",
              "รอการตรวจสอบ จนท.ฝ่ายการตลาด"
            );
          }}
        />
      );
    } else if (
      status === "รอการตรวจสอบ ผจก.ฝ่ายการตลาด" &&
      RoleDisplay.checkStatusApprove(role, status)
    ) {
      return (
        <img
          src="new/images/Status-blue.svg"
          width="40px"
          height="40px"
          onClick={() => {
            approveAsset(
              id,
              market_code,
              "ตรวจสอบแล้ว",
              "รอการตรวจสอบ ผจก.ฝ่ายการตลาด"
            );
          }}
        />
      );
    }
  };
  const setRolesNoti = (status: any) => {
    switch (status) {
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        return "จนท.ฝ่ายการตลาด";

      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        return "ผจก.ฝ่ายการตลาด";
      default:
        return "";
    }
  };
  const approveAsset = async (
    id: any,
    market_code: any,
    status: any,
    oldStatus: any
  ) => {
    const notify = {
      asset_id: parseInt(id),
      market_code: market_code,
      roles: setRolesNoti(status),
      type: status,
    };
    let reSearch = { status_approve: oldStatus, start: 0, limit: 10 };
    let objLog = {
      asset_id: parseInt(id),
      status_approve: true,
      user: firstname + " " + lastname,
      position: role,
      title: oldStatus,
      note: "",
    };
    if (status == "ตรวจสอบแล้ว") {
      let objData = {
        status_approve: status,
        id: parseInt(id),
        display_property: "true",
      };
      await callApi.apiPut("property-detail/update", objData);
    } else {
      let objData = {
        status_approve: status,
        id: parseInt(id),
      };
      await callApi.apiPut("property-detail/update", objData);
    }

    await callApi.apiPost("notification/add", notify);
    await callApi.apiPost("history-log/addLog", objLog);

    const response = await callApi.apiPost("property-detail/find", reSearch);
    if (response.length > 0) {
      setData(response);
      setTotal(response[0].total);
      setCurrentPage(1);
    } else {
      setData([]);
      setTotal(0);
      setCurrentPage(0);
    }
  };
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const checkStatus = (status: string) => {
    if (status === "ทรัพย์พร้อมขาย") {
      return (
        <img
          src="new/images/Ellipse-green.svg"
          width="10px"
          height="10px"
          style={{
            marginLeft: "4px",
            marginRight: "4px",
            marginBottom: "4px",
          }}
        />
      );
    } else if (status === "ขายแล้ว") {
      return (
        <img
          src="new/images/Ellipse-yellow.svg"
          width="10px"
          height="10px"
          style={{
            marginLeft: "4px",
            marginRight: "4px",
            marginBottom: "4px",
          }}
        />
      );
    } else if (status === "ระหว่างดำเนินการ") {
      return (
        <img
          src="new/images/Ellipse-red.svg"
          width="10px"
          height="10px"
          style={{
            marginLeft: "4px",
            marginRight: "4px",
            marginBottom: "4px",
          }}
        />
      );
    }
  };

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const downloadQR = async (id: any, market_code: any) => {
    const url = process.env.NEXT_PUBLIC_BAM_WEB + "propertydetail/" + id;
    const dataQR = { url: url };
    const response = await callApi.apiPost("property-list/qrcode", dataQR);
    const link = document.createElement("a");
    link.href = response;
    link.setAttribute("download", "Qrcode_" + market_code + ".png");

    document.body.appendChild(link);
    link.click();
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.npa_type}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.display_property ? (
              <a
                href="#"
                onClick={() =>
                  window.open(
                    process.env.NEXT_PUBLIC_BAM_WEB + "preview?id=" + item.id,
                    "_blank"
                  )
                }
              >
                {item.market_code}
              </a>
            ) : (
              item.market_code
            )}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.project_th}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.ao_name}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.gps_lat1}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.gps_long1}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.city_name}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {item.province_name}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {parseFloat(item.center_price).toLocaleString()}
          </td>
          <td style={{ opacity: item.display_property ? 1 : 0.5 }}>
            {checkStatus(item.asset_state)}
            {item.asset_state}
          </td>
          <td>
            <Dropdown autoClose="outside">
              <Dropdown.Toggle as={CustomToggle} />
              <Dropdown.Menu>
                <Dropdown.Header>
                  {getImageStatus(
                    item.id,
                    item.status_approve,
                    item.market_code
                  )}
                </Dropdown.Header>
                <Dropdown.Item
                  className="mt-2"
                  onClick={() =>
                    Router.push({
                      pathname: "/propertylist/propertydetail",
                      query: {
                        mode: "readonly",
                        id: item.id,
                        market_code: item.market_code,
                      },
                    })
                  }
                >
                  ดูรายละเอียดทรัพย์
                </Dropdown.Item>
                {item.asset_state === "ทรัพย์พร้อมขาย" ? (
                  <Dropdown.Item
                    className="mt-2"
                    onClick={() =>
                      Router.push({
                        pathname: "/propertylist/propertydetail",
                        query: {
                          mode: "edit",
                          id: item.id,
                          market_code: item.market_code,
                        },
                      })
                    }
                  >
                    แก้ไขรายละเอียดทรัพย์
                  </Dropdown.Item>
                ) : (
                  ""
                )}
                <Flex className="flex-row">
                  <Dropdown.Item className="mt-2">
                    สถานะการแสดงทรัพย์ บนหน้าเว็บไซต์
                  </Dropdown.Item>
                  <Dropdown.Item className="mt-2">
                    <Controller
                      name="display44"
                      control={control}
                      rules={{ required: false }}
                      render={({ field }) => (
                        <Switch
                          className="ms-2"
                          onChange={
                            () =>
                              updateDisplayProperty(
                                item.id,
                                !item.display_property
                              )
                            // setApproveStatus(!approveStatus)
                          }
                          checked={item.display_property}
                          checkedIcon={false}
                          uncheckedIcon={false}
                          onColor={Colors.green}
                        />
                      )}
                    />
                  </Dropdown.Item>
                </Flex>
                <Flex className="flex-row">
                  <Dropdown.Item className="mt-2">
                    ส่งไปแสดงหน้า Homecondocenter.com
                  </Dropdown.Item>
                  <Dropdown.Item className="mt-2">
                    <Switch
                      className="ms-2"
                      onChange={() =>
                        UpdateDisplayHomeCondo(
                          item.id,
                          !item.display_home_condo
                        )
                      }
                      checked={item.display_home_condo}
                      checkedIcon={false}
                      uncheckedIcon={false}
                      onColor={Colors.green}
                    />
                  </Dropdown.Item>
                </Flex>

                <Dropdown.Item
                  className="mt-2"
                  onClick={() => downloadQR(item.id, item.market_code)}
                >
                  ดาวน์โหลด QR code
                </Dropdown.Item>
                <Dropdown.Item
                  className="mt-2"
                  onClick={() => exportExcel(item)}
                >
                  Export to Excel
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
      <ModalPropertyDisplay
        show={showDisplayProperty}
        setShow={setShowDisplayProperty}
        setDisplayProperty={() => {}}
        display_property={false}
        id={idItem}
        reload={true}
        fetchData={fetchData}
        currentPage={setStartData(currentPage)}
      />
      <ModalLoading visible={visible} />
    </>
  );
};
export default TableAssetList;
