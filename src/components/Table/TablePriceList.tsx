import Router from "next/router";
import React, { FunctionComponent } from "react";
import { Table } from "react-bootstrap";
import Pagination from "./pagination/Pagination";

interface Table {
  data: any;
  onClickDelete: Function;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
}
const TablePriceList: FunctionComponent<Table> = ({
  data,
  onClickDelete,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
}) => {
  const header = ["ระดับราคาตั้งขาย", "ระดับราคาสิ้นสุด", "", ""];

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.price_start}</td>
          <td>{item.price_end}</td>
          <td>
            <img
              src="new/images/Edit.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() =>
                Router.push({
                  pathname: `/grouppricelist/addprice`,
                  query: {
                    id: item.id,
                    price_start: item.price_start,
                    price_end: item.price_end,
                    mode: "Edit",
                  },
                })
              }
            />
          </td>
          <td>
            <img
              src="new/images/Trash.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickDelete(item.id)}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TablePriceList;
