import React, { FunctionComponent } from "react";
import classnames from "classnames";
import { usePagination, DOTS } from "./usePagination";
import { Table } from "react-bootstrap";
// import "./pagination.css";
interface Pagination {
  data: any;
  RenderComponent: any;
  header: any;
  onPageChange: any;
  totalCount: any;
  currentPage: any;
  pageSize: any;
  className: any;
  siblingCount: any;
  minWidth?: any;
}

const Pagination: FunctionComponent<Pagination> = ({
  data,
  RenderComponent,
  header,
  onPageChange,
  totalCount,
  currentPage,
  pageSize,
  className,
  siblingCount = 1,
  minWidth,
}) => {
  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  }) as any;

  if (currentPage === 0 || paginationRange?.length < 2) {
    // return null;
  }
  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };
  const getPaginatedData = () => {
    return data;
  };

  let lastPage = 0;

  if (paginationRange != undefined) {
    lastPage = paginationRange[paginationRange.length - 1];
  }
  return (
    <div>
      <Table
        style={{
          backgroundColor: "white",
          borderRadius: "10px",
          minWidth: minWidth,
        }}
        className="mt-3"
      >
        <thead>{header}</thead>
        <tbody style={{ borderTop: "none" }}>
          {getPaginatedData().map((d: any, idx: any) => (
            <RenderComponent key={idx} item={d} i={idx} />
          ))}
        </tbody>
      </Table>
      <ul
        className={classnames("pagination-container", {
          [className]: className,
        })}
      >
        <li
          className={classnames("pagination-item", {
            disabled: currentPage === 1,
          })}
          onClick={onPrevious}
        >
          <div className="arrow left" />
        </li>
        {paginationRange != undefined
          ? paginationRange.map((pageNumber: any, index: any) => {
              if (pageNumber === DOTS) {
                return (
                  <li key={index} className="pagination-item dots">
                    &#8230;
                  </li>
                );
              }

              return (
                <li
                  key={index}
                  className={classnames("pagination-item", {
                    selected: pageNumber === currentPage,
                  })}
                  onClick={() => onPageChange(pageNumber)}
                >
                  {pageNumber}
                </li>
              );
            })
          : ""}
        <li
          className={classnames("pagination-item", {
            disabled: currentPage === lastPage,
          })}
          onClick={onNext}
        >
          <div className="arrow right" />
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
