import type { NextPage } from "next";
import React, { FunctionComponent, useEffect, useState } from "react";
import callApi from "../../pages/api/callApi";
import Pagination from "./Pagination";
import dayjs from "dayjs";

interface TableHistoryLog {
  id: string;
}
const TableHistoryLog: FunctionComponent<TableHistoryLog> = ({ id }) => {
  const [listHistoryLog, setListHistoryLog] = useState([]);
  let header = [
    "ชื่อผู้ใช้งาน",
    "หัวข้อ",
    "สถานะการอนุมัติ",
    "จัดการโดย",
    "วันที่แก้ไขล่าสุด",
    "หมายเหตุ",
  ];
  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const setDisplayStatus = (status: any) => {
    if (status) {
      return "อนุมัติ";
    }
    return "ไม่อนุมัติ";
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.user}</td>
          <td>{item.title}</td>
          <td>{setDisplayStatus(item.status_approve)}</td>
          <td>{item.position}</td>
          <td>
            {dayjs(item.modify_date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>

          <td>{item.note}</td>
        </tr>
      </>
    );
  };
  const setDataHistoryLog = async () => {
    let obj = { asset_id: id };
    let historyLog = await callApi.apiPost("history-log/findAllByAssetId", obj);
    setListHistoryLog(historyLog);
  };
  useEffect(() => {
    setDataHistoryLog();
  }, []);
  return (
    <>
      {listHistoryLog.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={listHistoryLog}
          RenderComponent={genData}
          pageLimit={Math.ceil(listHistoryLog.length / 10)}
          dataLimit={10}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableHistoryLog;
