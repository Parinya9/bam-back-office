import React, { FunctionComponent } from "react";
import { Table } from "react-bootstrap";
import Colors from "../../../utils/Colors";
import { TextSemiBold } from "../text";
import Pagination from "./Pagination";
interface Table {
  data: any;
  onClickDelete: Function;
  onClickEdit: Function;
}

const TableMangePopularSearch: FunctionComponent<Table> = ({
  data,
  onClickDelete,
  onClickEdit,
}) => {
  const header = ["#", "ชื่อคำค้นหา", "URL", "", ""];

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.no}</td>
          <td>{item.search_name}</td>
          <td>
            <TextSemiBold color={Colors.mainBlue} fontSize="16px">
              {item.url}
            </TextSemiBold>
          </td>
          <td>
            <img
              src="/new/images/Edit.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickEdit(item.search_name, item.url, item.id)}
            />
          </td>
          <td>
            <img
              src="/new/images/Trash.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickDelete(item.id)}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          pageLimit={Math.ceil(data.length / 10)}
          dataLimit={10}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableMangePopularSearch;
