import { FunctionComponent } from "react";
import { Table } from "react-bootstrap";
import Colors from "../../../utils/Colors";
import { TextSemiBold } from "../text";
import Pagination from "./Pagination";

interface TableBannerSlide {
  data: any;
  onClickDelete: Function;
  onClickEdit: Function;
}

const TableBannerSlide: FunctionComponent<TableBannerSlide> = ({
  onClickEdit,
  onClickDelete,
  data,
}) => {
  const header = ["#", "ตัวอย่างรูป", "รูปภาพ", "URL", "", ""];

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{i + 1}</td>
          <td>
            <img src={item?.image_url?.url} width="120px" height="50px" />
          </td>
          <td>{item?.image_url?.name}</td>
          <td>
            <TextSemiBold color={Colors.mainBlue} fontSize="16px">
              {item?.link}
            </TextSemiBold>
          </td>
          <td>
            <button style={{ backgroundColor: "transparent", border: "none" }}>
              <img
                src="/new/images/Edit.svg"
                width="20px"
                height="20px"
                style={{ margin: "5px" }}
                // onClick={onClickEdit}
                onClick={() =>
                  onClickEdit(
                    item?.id,
                    item?.image_url?.name,
                    item?.pdf_url?.name,
                    item?.link,
                    item?.start_date,
                    item?.end_date
                  )
                }
              />
            </button>
          </td>
          <td>
            <img
              src="/new/images/Trash.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickDelete(item.id)}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          pageLimit={Math.ceil(data.length / 10)}
          dataLimit={10}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableBannerSlide;
