import Router from "next/router";
import React, { FunctionComponent, useState } from "react";
import { Table } from "react-bootstrap";
import Pagination from "./pagination/Pagination";
import { useForm } from "react-hook-form";
import Cookies from "universal-cookie";
import dayjs from "dayjs";
interface Table {
  data: any;
  total: any;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  setData: any;
  setTotal: any;
}
const TableManageReceiveNews: FunctionComponent<Table> = ({
  data,
  total,
  currentPage,
  setCurrentPage,
  fetchData,
  setData,
  setTotal,
}) => {
  const [showDisplayProperty, setShowDisplayProperty] = useState(false);
  const [show, setShow] = useState(false);
  const [idItem, setIdItem] = useState(0);
  const cookies = new Cookies();
  let role = cookies.get("role");

  const {
    control,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = useForm();
  const header = [
    "วันที่ติดต่อ",
    "ชื่อ-นามสกุล",
    "เบอร์โทรศัพท์",
    "อีเมล",
    "ประเภททรัพย์สิน",
    "ช่วงราคา",
    "จังหวัด",
    "อำเภอ",
    "ที่อยู่",
    "",
  ];

  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };

  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const viewData = (id: any) => {
    Router.push({
      pathname: "/managereceivenews/info",
      query: {
        id: id,
        mode: "View",
      },
    });
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>
            {dayjs(item.create_date.replace("Z", "").replace("T", "")).format(
              "DD/MM/YYYY HH:mm"
            )}
          </td>

          <td>{item.firstname + " " + item.lastname}</td>
          <td>{"0" + item.telephone}</td>
          <td>{item.email}</td>
          <td>{item.asset_type}</td>
          <td>{item.price_range}</td>
          <td>{item.province}</td>
          <td>{item.district}</td>
          <td>{item.address}</td>
          <td>
            <img
              onClick={() => viewData(item.id)}
              src="/new/images/Icon feather-eye-blue.svg"
              width="15px"
              height="15px"
              style={{
                alignSelf: "center",
                justifySelf: "center",
              }}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TableManageReceiveNews;
