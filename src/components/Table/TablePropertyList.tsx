import Router from "next/router";
import React, { FunctionComponent } from "react";
import { Table } from "react-bootstrap";
import Pagination from "./pagination/Pagination";

interface Table {
  data: any;
  onClickDelete: Function;
  currentPage: any;
  setCurrentPage: any;
  fetchData: any;
  total: any;
  setTotal: any;
}
const TablePropertyList: FunctionComponent<Table> = ({
  data,
  onClickDelete,
  currentPage,
  setCurrentPage,
  fetchData,
  total,
  setTotal,
}) => {
  const header = [
    "ประเภททรัพย์สิน (TH)",
    "ประเภททรัพย์สิน (EN)",
    "หมวดหมู่",
    "",
    "",
  ];
  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    fetchData(setStartData(page), 10);
  };
  const setStartData = (page: any) => {
    return (page - 1) * 10;
  };
  const genHeader = () => {
    return (
      <tr>
        {header.map((item, i) => {
          return <th key={i}>{item}</th>;
        })}
      </tr>
    );
  };
  const genData = ({ item, i }: { item: any; i: any }) => {
    return (
      <>
        <tr key={i}>
          <td>{item.asset_name_th}</td>
          <td>{item.asset_name_en}</td>
          <td>{item.group}</td>

          <td>
            <img
              src="new/images/Edit.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() =>
                Router.push({
                  pathname: `/grouppropertylist/addproperty`,
                  query: {
                    id: item.id,
                    asset_name_th: item.asset_name_th,
                    asset_name_en: item.asset_name_en,
                    mode: "Edit",
                    asset_no: item.asset_no,
                    group: item.group,
                  },
                })
              }
            />
          </td>
          <td>
            <img
              src="new/images/Trash.svg"
              width="20px"
              height="20px"
              style={{ margin: "5px" }}
              onClick={() => onClickDelete(item.id)}
            />
          </td>
        </tr>
      </>
    );
  };

  return (
    <>
      {data.length > 0 ? (
        <Pagination
          header={genHeader()}
          data={data}
          RenderComponent={genData}
          siblingCount={1}
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={total}
          pageSize={10}
          onPageChange={(page: any) => onPageChange(page)}
        />
      ) : (
        ""
      )}
    </>
  );
};
export default TablePropertyList;
