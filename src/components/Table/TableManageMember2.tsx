import * as React from "react";
import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";
import Router from "next/router";
import Colors from "../../../utils/Colors";
import classnames from "classnames";
import { usePagination, DOTS } from "../Table/pagination/usePagination";
import { useState } from "react";

//
interface Table {
  data: any;
  fetchData: any;
  totalCount: any;
  currentPage: any;
  setCurrentPage: any;
  pageSize: any;
  siblingCount: any;
  tableType: any;
  filterSearch: any;
  actionSearch: any;
  paramSearch: any;
}
const TableMemberInfo: React.FunctionComponent<Table> = ({
  data,
  fetchData,
  totalCount,
  currentPage,
  setCurrentPage,
  pageSize,
  siblingCount,
  tableType,
  filterSearch,
  actionSearch,
  paramSearch,
}) => {
  function Row(props: { row: any }) {
    const { row } = props;
    const [open, setOpen] = useState(false);
    return (
      <React.Fragment>
        <TableRow
          style={{
            background: open ? Colors.bamNavy : Colors.mainWhite,
          }}
          sx={{ "& > *": { borderBottom: "unset" } }}
        >
          <TableCell>
            {row.person && (
              <IconButton
                style={{ color: open ? Colors.mainWhite : "#4268AB" }}
                aria-label="expand row"
                size="small"
                onClick={() => setOpen(!open)}
              >
                {open ? <RemoveIcon /> : <AddIcon />}
              </IconButton>
            )}
          </TableCell>
          <TableCell
            style={{
              fontWeight: "300",
              color: open ? Colors.mainWhite : "#000000de",
            }}
          >
            {row.firstname}
          </TableCell>
          <TableCell
            style={{
              fontWeight: "300",
              color: open ? Colors.mainWhite : "#000000de",
            }}
          >
            {row.lastname}
          </TableCell>
          <TableCell
            style={{
              fontWeight: "300",
              color: open ? Colors.mainWhite : "#000000de",
            }}
          >
            {row.role}
          </TableCell>
          <TableCell
            style={{
              fontWeight: "300",
              color: open ? Colors.mainWhite : "#000000de",
            }}
          >
            {row.email}
          </TableCell>
          <TableCell
            style={{
              fontWeight: "300",
              color: open ? Colors.mainWhite : "#000000de",
            }}
          >
            {row.status == 1 ? "เปิดใช้งาน" : "ปิดใช้งาน"}
          </TableCell>
          <TableCell style={{ textAlign: "center" }}>
            <button style={{ backgroundColor: "transparent", border: "none" }}>
              <img
                src="new/images/Edit.svg"
                width="20px"
                height="20px"
                style={{ margin: "5px" }}
                onClick={() =>
                  Router.push({
                    pathname: `managemember/addmember`,
                    query: {
                      id: row.id,
                      mode: "edit",
                    },
                  })
                }
              />
            </button>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ padding: 0 }} colSpan={7}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 0 }}>
                <Table size="small" aria-label="collapsible table">
                  <TableBody>
                    {row.person &&
                      row.person.map((item: any) => (
                        <Row2 key={item.id} row={item} previousOpen={open} />
                      ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </React.Fragment>
    );
  }

  function Row2(props: { row: any; previousOpen: any }) {
    const { row, previousOpen } = props;
    const [open, setOpen] = useState(false);
    return (
      <React.Fragment>
        <TableRow
          style={{
            background: previousOpen ? "#41AAFA" : Colors.mainWhite,
          }}
          sx={{ "& > *": { borderBottom: "unset" } }}
        >
          <TableCell
            style={{
              paddingLeft: row.person ? "50px" : "85px",
              width: "50px",
            }}
          >
            {row.person && (
              <IconButton
                style={{ color: previousOpen ? Colors.mainWhite : "#4268AB" }}
                aria-label="expand row"
                size="small"
                onClick={() => setOpen(!open)}
              >
                {open ? <RemoveIcon /> : <AddIcon />}
              </IconButton>
            )}
          </TableCell>

          <TableCell style={{ width: "200px", fontWeight: "300" }}>
            {row.firstname}
          </TableCell>
          <TableCell style={{ width: "200px", fontWeight: "300" }}>
            {row.lastname}
          </TableCell>
          <TableCell style={{ width: "200px", fontWeight: "300" }}>
            {row.role}
          </TableCell>
          <TableCell style={{ width: "200px", fontWeight: "300" }}>
            {row.email}
          </TableCell>
          <TableCell style={{ width: "200px", fontWeight: "300" }}>
            {row.status == 1 ? "เปิดใช้งาน" : "ปิดใช้งาน"}
          </TableCell>
          <TableCell style={{ textAlign: "center" }}>
            <button style={{ backgroundColor: "transparent", border: "none" }}>
              <img
                src="new/images/Edit.svg"
                width="20px"
                height="20px"
                style={{ margin: "5px" }}
                onClick={() =>
                  Router.push({
                    pathname: `managemember/addmember`,
                    query: {
                      id: row.id,
                      mode: "edit",
                    },
                  })
                }
              />
            </button>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ padding: 0 }} colSpan={7}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 0 }}>
                <Table size="small" aria-label="collapsible table">
                  <TableBody>
                    {row.person &&
                      row.person.map((person: any) => (
                        <TableRow
                          style={{
                            background: open ? "#E3F3FF" : Colors.mainWhite,
                          }}
                          key={person.id}
                        >
                          <TableCell style={{ width: "100px" }} />
                          <TableCell
                            style={{ width: "200px", fontWeight: "300" }}
                          >
                            {person.firstname}
                          </TableCell>
                          <TableCell
                            style={{ width: "200px", fontWeight: "300" }}
                          >
                            {person.lastname}
                          </TableCell>
                          <TableCell
                            style={{ width: "200px", fontWeight: "300" }}
                          >
                            {person.role}
                          </TableCell>
                          <TableCell
                            style={{ width: "200px", fontWeight: "300" }}
                          >
                            {person.email}
                          </TableCell>
                          <TableCell
                            style={{ width: "200px", fontWeight: "300" }}
                          >
                            {person.status == 1 ? "เปิดใช้งาน" : "ปิดใช้งาน"}
                          </TableCell>
                          <TableCell style={{ textAlign: "center" }}>
                            <button
                              style={{
                                backgroundColor: "transparent",
                                border: "none",
                              }}
                            >
                              <img
                                src="new/images/Edit.svg"
                                width="20px"
                                height="20px"
                                style={{ margin: "5px" }}
                                onClick={() =>
                                  Router.push({
                                    pathname: `managemember/addmember`,
                                    query: {
                                      id: person.id,
                                      mode: "edit",
                                    },
                                  })
                                }
                              />
                            </button>
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </React.Fragment>
    );
  }

  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  }) as any;

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  const onPageChange = async (page: any) => {
    setCurrentPage(page);
    if (tableType === "1") {
      fetchData(page);
    } else {
      if (paramSearch?.typeSearch === "1") {
        filterSearch(paramSearch.data, page);
      } else {
        actionSearch(paramSearch.data, page);
      }
    }
  };

  let lastPage = 0;
  if (paginationRange != undefined) {
    lastPage = paginationRange[paginationRange.length - 1];
  }

  return (
    <>
      {data?.length > 0 ? (
        <>
          <TableContainer
            style={{ borderRadius: "10px", margin: "16px 0px 16px" }}
            component={Paper}
          >
            <Table size="small" aria-label="collapsible table">
              <TableHead>
                <TableRow style={{ height: "72px" }}>
                  <TableCell style={{ width: "100px" }} />
                  <TableCell style={{ width: "200px" }}>ชื่อ</TableCell>
                  <TableCell style={{ width: "200px" }}>นามสกุล</TableCell>
                  <TableCell style={{ width: "200px" }}>Role</TableCell>
                  <TableCell style={{ width: "200px" }}>Email</TableCell>
                  <TableCell style={{ width: "200px" }}>
                    สถานะผู้ใช้งาน
                  </TableCell>
                  <TableCell />
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row: any) => (
                  <Row key={row.id} row={row} />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <ul
            className={classnames("pagination-container", {
              ["pagination-bar"]: "pagination-bar",
            })}
          >
            <li
              className={classnames("pagination-item", {
                disabled: currentPage === 1,
              })}
              onClick={onPrevious}
            >
              <div className="arrow left" />
            </li>
            {paginationRange != undefined
              ? paginationRange.map((pageNumber: any, index: any) => {
                  if (pageNumber === DOTS) {
                    return (
                      <li key={index} className="pagination-item dots">
                        &#8230;
                      </li>
                    );
                  }
                  return (
                    <li
                      key={index}
                      className={classnames("pagination-item", {
                        selected: pageNumber === currentPage,
                      })}
                      onClick={() => onPageChange(pageNumber)}
                    >
                      {pageNumber}
                    </li>
                  );
                })
              : ""}
            <li
              className={classnames("pagination-item", {
                disabled: currentPage === lastPage,
              })}
              onClick={onNext}
            >
              <div className="arrow right" />
            </li>
          </ul>
        </>
      ) : (
        ""
      )}
    </>
  );
};
export default TableMemberInfo;
