import { useState, useEffect, FunctionComponent } from "react";
import { Flex } from "../layout";
import { TextSemiBold } from "../text";
import CardAssetImage from "../Card/CardAssetImage";
import Colors from "../../../utils/Colors";
import Switch from "react-switch";
import styled from "styled-components";
import InputRadiusAsset from "../../components/Layout/InputRadiusAsset";
import ButtonRadiusIconL from "../Button/ButtonRadiusIconL";
import ModalAddMap from "../Modal/ModalAddMap";
import ModalData360 from "../Modal/ModalData360";
import ModalAddMultiImage from "../Modal/ModalAddMultiImage";
import { useDispatch, useSelector } from "react-redux";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import validateFieldAsset from "../../../utils/ValidateFieldAsset";

import {
  saveImageAsset,
  deleteImageAsset,
  addImageAsset,
  saveEditAssetImage,
  saveEdit360Image,
  saveEditMapImage,
  clearAssetImage,
  clearMapImage,
  clear360Image,
  clearEditAssetImage,
} from "../../redux/assetDetail/action";
import moment from "moment";
import { ListManager } from "react-beautiful-dnd-grid";
import { Controller, useForm } from "react-hook-form";
import callApi from "../../pages/api/callApi";
import Router from "next/router";
import Cookies from "universal-cookie";

import {
  apiSyncAssetDetail,
  apiSyncAssetDetailAuction,
} from "../../pages/api/elsSync";
import ModalLoading from "../../components/Modal/ModalLoading";
import CardAssetMultiImage from "../Card/CardAssetMultiImage";

const dataCard = [
  {
    id: 1,
    detail: "จัดการ รูปภาพหน้าปก1",
    linkPath: "/new/images/Commercial-building-3.png",
    order: 1,
  },
  {
    id: 2,
    detail: "จัดการข้อมูล คำค้นหา ยอดนิยม2",
    linkPath: "/new/images/Commercial-building-3.png",
    order: 2,
  },
  {
    id: 3,
    detail: "จัดการ รูปโฆษณา3",
    linkPath: "/new/images/Commercial-building-3.png",
    order: 3,
  },
  {
    id: 4,
    detail: "จัดการ รูปโฆษณา4",
    linkPath: "/new/images/Commercial-building-3.png",
    order: 4,
  },
  {
    id: 5,
    detail: "จัดการ รูปโฆษณา5",
    linkPath: "/new/images/Commercial-building-3.png",
    order: 5,
  },
];

interface AssetImage {
  mode: string;
  id: string;
  checkSubmit: any;
  flagTab: any;
  setFlagTab: any;
  display_property: any;
}
const AssetImage: FunctionComponent<AssetImage> = ({
  id,
  mode,
  checkSubmit,
  flagTab,
  setFlagTab,
  display_property,
}) => {
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const [assetItem, setAssetItem] = useState([]);
  const [visible, setVisible] = useState(false);

  const {
    control,
    handleSubmit,
    setValue,
    getValues,
    watch,
    register,
    formState: { errors },
  } = useForm();
  const stateEditAssetImage = useSelector(
    (state: any) => state?.assetDetail?.stateEditAssetImage
  );
  const stateEditImgMap = useSelector(
    (state: any) => state?.assetDetail?.stateEditImgMap
  );
  const stateEditImg360 = useSelector(
    (state: any) => state?.assetDetail?.stateEditImg360
  );

  const stateAssetImage = useSelector(
    (state: any) => state?.assetDetail?.stateAssetImage
  );
  const stateEditImageRenovate1 = useSelector(
    (state: any) => state?.assetDetail?.stateEditImageRenovate1
  );
  const [displayYoutube, setDisplayYoutube] = useState(false);
  const [displaySketchup, setDisplaySketchup] = useState(false);
  const [statusField, setStatusField] = useState(true);
  const [approveStatus, setApproveStatus] = useState(false);
  const [showMap, setShowMap] = useState(false);
  const [show360, setShow360] = useState(false);
  const [showAssetImage, setShowAssetImage] = useState(false);
  const [dataMap, setDataMap] = useState([]);
  const [data360, setData360] = useState([]);
  const [dataAsset, setDataAsset] = useState([]);

  const [sortedList, setSortedList] = useState(sortList(dataCard));

  const dispatch = useDispatch();
  const onChangeFieldSetFlagTab = () => {
    if (!flagTab) {
      setFlagTab(true);
    }
  };
  const assetImage = useSelector(
    (state: any) => state?.assetDetail?.stateAssetImage
  );
  const listImage = useSelector((state: any) => state?.assetDetail?.listImage);

  const onDelete = (data: any) => {
    dispatch(deleteImageAsset(data));
  };

  const [assetData, updateAssetData] = useState(dataCard);

  function sortList(list: any) {
    return list.slice().sort((a: any, b: any) => a.order - b.order);
  }
  function setIdList(list: any) {
    list.map((item: any, index: number) => {
      item.no = index;
      item.id = index;
    });
    return list;
  }
  const reorderList = (sourceIndex: any, destinationIndex: any) => {
    if (destinationIndex === sourceIndex) {
      return;
    }

    const list = sortedList;
    if (destinationIndex === 0) {
      list[sourceIndex].order = parseInt(list[0].order) - 1;

      const newList = sortList(sortedList);
      setSortedList(reSetOrderImage(newList));

      dispatch(saveEditAssetImage(setIdList(reSetOrderImage(newList))));

      return;
    }
    if (destinationIndex === list.length - 1) {
      list[sourceIndex].order = parseInt(list[list.length - 1].order) + 1;
      const newList = sortList(sortedList);
      setSortedList(reSetOrderImage(newList));
      dispatch(saveEditAssetImage(setIdList(reSetOrderImage(newList))));
      return;
    }
    if (destinationIndex < sourceIndex) {
      list[sourceIndex].order =
        (parseInt(list[destinationIndex].order) +
          parseInt(list[destinationIndex - 1].order)) /
        2;
      const newList = sortList(sortedList);
      dispatch(saveEditAssetImage(setIdList(reSetOrderImage(newList))));

      setSortedList(reSetOrderImage(newList));

      return;
    }

    list[destinationIndex].order;
    list[sourceIndex].order =
      (parseInt(list[destinationIndex].order) +
        parseInt(list[destinationIndex + 1].order)) /
      2;

    const newList = sortList(sortedList);
    dispatch(saveEditAssetImage(setIdList(reSetOrderImage(newList))));

    setSortedList(reSetOrderImage(newList));
  };
  const onSubmit = (data: any) => {
    if (stateEditAssetImage.length > 0) {
      editData(data);
    } else {
      alert("กรุณาใส่ รูปภาพ ในอัลบั้มรูปภาพอย่างน้อย 1 รูป");
    }
  };
  const editData = async (data: any) => {
    let formData = new FormData();
    setVisible(true);
    if (!validateFieldAsset.checkFieldRequireAssetDetail(assetItem)) {
      let objLog = {
        asset_id: parseInt(id),
        status_approve: true,
        user: firstname + " " + lastname,
        position: role,
        title: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        note: "",
      };
      formData.append(
        "status_approve",
        "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์"
      );
      await callApi.apiPost("history-log/addLog", objLog);
    }
    genFormData(formData, data);
    setFlagTab(false);
    const item = await callApi.apiPut("property-detail/update/form", formData);

    const response = await apiSyncAssetDetail("api/asset-detail");
    if (item.error) {
      alert("error");
      setVisible(false);
    }

    if (response.status === "success") {
      setVisible(false);

      alert("Update success");
    } else {
      setVisible(false);

      alert("Cant'not update asset");
    }
  };

  const genFormData = async (formData: any, data: any) => {
    let listString = ["link_youtube", "ember_code_th", "ember_code_en"];
    formData.append("display_sketchup", displaySketchup ? "true" : "false");
    formData.append("display_youtube", displayYoutube ? "true" : "false");
    formData.append("id", id);

    formData.append("album_property", setJsonImgAsset(stateEditAssetImage));
    formData.append("image_360", setJsonImgAsset(stateEditImg360));
    formData.append("image_map", setJsonImgAsset(stateEditImgMap));
    formData.append("display_property", display_property ? "true" : "false");

    listString.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "string");
    });
  };
  const setJsonImgAsset = (data: any) => {
    var imgAssetJson: any = [];
    data.map((item: any, index: any) => {
      imgAssetJson.push({
        no: index,
        id: index,
        order: item.order,
        url: item.url,
        detail: item.detail,
        name: item.name,
        display: item.display,
      });
    });

    return JSON.stringify(imgAssetJson);
  };
  const setFormData = (formData: any, key: any, data: any, type: any) => {
    if (type == "date") {
      if (data != undefined) {
        formData.append(key, setFormatDate(data));
      }
    } else if (type == "dropdown") {
      if (data != undefined) {
        formData.append(key, data.value);
      }
    } else {
      if (data != null) {
        formData.append(key, data);
      }
    }
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  const setData = async () => {
    const conditionSearch = { id: parseInt(id) };
    const response = await callApi.apiPost(
      "property-detail/findKey",
      conditionSearch
    );
    if (response[0] != undefined) {
      setAssetItem(response[0]);
      setValue("link_youtube", response[0].link_youtube);
      setValue("ember_code_th", response[0].ember_code_th);
      setValue("ember_code_en", response[0].ember_code_en);

      setDisplaySketchup(response[0].display_sketchup);
      setDisplayYoutube(response[0].display_youtube);
      response[0].image_360 != null
        ? dispatch(saveEdit360Image(response[0].image_360))
        : dispatch(clear360Image([]));
      response[0].image_360 != null ? setData360(response[0].image_360) : "";
      response[0].image_map != null
        ? dispatch(saveEditMapImage(response[0].image_map))
        : dispatch(clearMapImage([]));
      response[0].image_map != null ? setDataMap(response[0].image_map) : "";

      response[0].album_property != null
        ? dispatch(
            saveEditAssetImage(reSetOrderImage(response[0].album_property))
          )
        : dispatch(clearEditAssetImage([]));
      response[0].album_property != null
        ? setDataAsset(reSetOrderImage(response[0].album_property))
        : "";
      response[0].album_property != null
        ? setSortedList(sortList(reSetOrderImage(response[0].album_property)))
        : "";
    }
  };
  const reSetOrderImage = (data: any) => {
    for (let i = 0; i < data.length; ++i) {
      data[i].order = i;
    }
    return data;
  };
  const onDelete1 = (data: any) => {
    dispatch(deleteImageAsset(data));
  };
  const uploadImg = async (
    saveImageRenovate: any,
    imgRenovate: any,
    setData: any,
    saveEditImageRenovate: any,
    stateEditImageRenovate: any
  ) => {
    onChangeFieldSetFlagTab();
    dispatch(saveImageRenovate(imgRenovate));
    var formData = new FormData();
    if (imgRenovate.length > 0) {
      imgRenovate.map((item: any, index: number) => {
        formData.append("files", item);
      });
      const item = await callApi.apiPost(
        "property-detail/uploadMulti",
        formData
      );

      if (stateEditImageRenovate != undefined) {
        item.map((data: any, index: number) => {
          stateEditImageRenovate.push({
            detail: "",
            name: data.name,
            url: data.url,
            id: stateEditImageRenovate.length,
            no: stateEditImageRenovate.length,
            display: true,
            order: stateEditImageRenovate.length,
          });
        });
        setData(stateEditImageRenovate);
        dispatch(saveEditImageRenovate(stateEditImageRenovate));
        setSortedList(sortList(stateEditImageRenovate));
      } else {
        item.map((data: any, index: number) => {
          stateEditImageRenovate.push({
            detail: "",
            name: data.name,
            url: data.url,
            id: index,
            no: index,
            display: true,
            order: index,
          });
        });
        setData(item);
        dispatch(saveEditImageRenovate(item));
      }
    }
  };
  const setField = () => {
    mode == "readonly" ? setStatusField(true) : setStatusField(false);
  };

  useEffect(() => {
    setField();
    setData();
  }, []);
  const onClickAlbum = () => {
    setShowAssetImage(true);
    dispatch(clearAssetImage([]));
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} id="assetform">
      <div className="container mt-5">
        <Flex className="flex-column">
          <Flex className="flex-row justify-content-between">
            <TextSemiBold fontSize="24px">อัลบั้มรูปภาพทรัพย์</TextSemiBold>
            {mode == "edit" && (
              <ButtonRadiusIconL
                onClick={() => onClickAlbum()}
                backgroundColor="white"
                title="เพิ่มรูปภาพ"
                border="solid"
                borderColor={Colors.mainBlue}
                fontSize="17px"
                borderWidth="1px"
                className="px-3 ms-2 mx-1 mt-1 mt-3 mb-3"
                color={Colors.mainBlue}
                iconPath="/new/images/Add.svg"
              />
            )}
          </Flex>
          <div
            className="px-3 py-4"
            style={{
              backgroundColor: Colors.lightGrayF6,
              height: assetData.length > 0 ? "auto" : "220px",
              display: "grid",
            }}
          >
            <ListManager
              items={stateEditAssetImage}
              direction="horizontal"
              maxItems={4}
              render={(item: any) => (
                <CardAssetMultiImage
                  linkPath={item.url}
                  id={item.id}
                  detail={item.detail}
                  fileName={item.name}
                  display={item.display}
                  saveEditImage={saveEditAssetImage}
                  stateEditImage={stateEditAssetImage}
                  editImage={saveEditAssetImage}
                  mode={mode}
                  setSortedList={setSortedList}
                />
              )}
              onDragEnd={reorderList}
            />
          </div>
          <TextSemiBold
            weight="300"
            fontSize="16px"
            className="mt-2"
            color={Colors.gray9b}
          >
            กรุณาใส่รูปภาพทรัพย์อย่างน้อย 7 รูป
          </TextSemiBold>
        </Flex>
        <Flex className="flex-column mt-5">
          <Flex className="flex-row justify-content-between">
            <TextSemiBold fontSize="24px">รูปภาพแผนที่</TextSemiBold>
            {stateEditImgMap.length == 0 && mode == "edit" && (
              <ButtonRadiusIconL
                backgroundColor="white"
                title="เพิ่มรูปภาพ"
                border="solid"
                borderColor={Colors.mainBlue}
                fontSize="17px"
                borderWidth="1px"
                className="px-3 ms-2 mx-1 mt-1 mt-3 mb-3"
                color={Colors.mainBlue}
                iconPath="/new/images/Add.svg"
                onClick={() => setShowMap(true)}
              />
            )}
          </Flex>
          <div
            className="px-3 py-4"
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr 1fr 1fr",
              backgroundColor: Colors.lightGrayF6,
              gridGap: "10px",
              marginTop: 30,
              height: stateEditImgMap.length > 0 ? "auto" : "220px",
            }}
          >
            {stateEditImgMap.length > 0 &&
              stateEditImgMap.map((item: any, index: number) => {
                return (
                  <CardAssetImage
                    key={index}
                    linkPath={item.url}
                    id={item.id}
                    detail={item.detail}
                    fileName={item.name}
                    display={item.display}
                    saveEditImage={saveEditMapImage}
                    stateEditImage={stateEditImgMap}
                    editImage={saveEditMapImage}
                    mode={mode}
                  />
                );
              })}
          </div>
        </Flex>

        <Flex className="flex-column mt-5">
          <Flex className="flex-row justify-content-between">
            <TextSemiBold fontSize="24px">มุมมอง 360</TextSemiBold>
            {stateEditImg360.length <= 0 && mode == "edit" && (
              <ButtonRadiusIconL
                onClick={() => setShow360(true)}
                backgroundColor="white"
                title="เพิ่มรูปภาพ"
                border="solid"
                borderColor={Colors.mainBlue}
                fontSize="17px"
                borderWidth="1px"
                className="px-3 ms-2 mx-1 mt-1 mt-3 mb-3"
                color={Colors.mainBlue}
                iconPath="/new/images/Add.svg"
              />
            )}
          </Flex>
          <div
            className="px-3 py-4"
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr 1fr 1fr",
              backgroundColor: Colors.lightGrayF6,
              gridGap: "10px",
              marginTop: 30,
              height: stateEditImg360.length > 0 ? "auto" : "220px",
            }}
          >
            {stateEditImg360.map((item: any, index: number) => {
              return (
                <CardAssetImage
                  key={index}
                  linkPath={item.url}
                  id={item.id}
                  detail={item.detail}
                  fileName={item.name}
                  display={item.display}
                  saveEditImage={saveEdit360Image}
                  stateEditImage={stateEditImg360}
                  editImage={saveEdit360Image}
                  mode={mode}
                />
              );
            })}
          </div>
        </Flex>

        <Flex className="flex-column mt-5">
          <TextSemiBold fontSize="24px">Youtube</TextSemiBold>
          <Flex className="flex-column mt-3">
            <div style={{ display: "grid", gridTemplateColumns: "1fr 2fr" }}>
              <Flex>
                <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
                  {displayYoutube ? "แสดง " : "ไม่แสดง "}
                </TextSemiBold>
              </Flex>
              <Flex>
                <Controller
                  name="display_youtube"
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                    <Switch
                      onChange={() => {
                        setDisplayYoutube(!displayYoutube);
                        onChangeFieldSetFlagTab();
                      }}
                      checked={displayYoutube}
                      checkedIcon={false}
                      uncheckedIcon={false}
                      disabled={statusField}
                      onColor={Colors.green}
                    />
                  )}
                />
              </Flex>
            </div>
            <Flex className="flex-column mt-3">
              <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
                ลิงค์ Youtube{" "}
              </TextSemiBold>
              <Controller
                name="link_youtube"
                control={control}
                rules={{ required: displayYoutube }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      placeHolder="https://www.youtube.com/watch?v=J6-ASlRWV6M"
                      width="100%"
                      disabled={statusField}
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      {...register("link_youtube", {
                        pattern:
                          /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi,
                      })}
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                    />
                  );
                }}
              />
              {errors.link_youtube && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Flex>
        </Flex>

        <Flex className="flex-column mt-5">
          <TextSemiBold fontSize="24px">Sketchup / Virtual Tour</TextSemiBold>
          <Flex className="flex-column mt-3">
            <div style={{ display: "grid", gridTemplateColumns: "1fr 2fr" }}>
              <Flex>
                <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
                  {displaySketchup ? "แสดง " : "ไม่แสดง "}
                </TextSemiBold>
              </Flex>
              <Flex>
                <Controller
                  name="display_sketchup"
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                    <Switch
                      onChange={() => {
                        setDisplaySketchup(!displaySketchup);
                        onChangeFieldSetFlagTab();
                      }}
                      checked={displaySketchup}
                      checkedIcon={false}
                      uncheckedIcon={false}
                      disabled={statusField}
                      onColor={Colors.green}
                    />
                  )}
                />
              </Flex>
            </div>
            <Flex className="flex-column mt-3">
              <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
                Embec code (ไทย){" "}
              </TextSemiBold>

              <Controller
                name="ember_code_th"
                control={control}
                rules={{ required: displaySketchup }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      placeHolder="Link URL"
                      width="100%"
                      disabled={statusField}
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      {...register("ember_code_th", {
                        pattern:
                          /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi,
                      })}
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                    />
                  );
                }}
              />
              {errors.ember_code_th && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex className="flex-column mt-3">
              <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
                Embec code (อังกฤษ){" "}
              </TextSemiBold>

              <Controller
                name="ember_code_en"
                control={control}
                rules={{ required: displaySketchup }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      placeHolder="Link URL"
                      width="100%"
                      disabled={statusField}
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      {...register("ember_code_en", {
                        pattern:
                          /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi,
                      })}
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                    />
                  );
                }}
              />
              {errors.ember_code_en && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Flex>
        </Flex>

        <ModalAddMap
          show={showMap}
          setShow={setShowMap}
          setDataMap={setDataMap}
          flagTab={onChangeFieldSetFlagTab}
        />
        <ModalData360
          show={show360}
          setShow={setShow360}
          setData360={setData360}
          flagTab={onChangeFieldSetFlagTab}
        />
        <ModalAddMultiImage
          show={showAssetImage}
          setShow={setShowAssetImage}
          setDataAsset={setDataAsset}
          onSubmit={() =>
            uploadImg(
              saveImageAsset,
              stateAssetImage,
              setDataAsset,
              saveEditAssetImage,
              stateEditAssetImage
            )
          }
          onDelete={onDelete1}
          imageData={stateAssetImage}
          addImage={addImageAsset}
        />
      </div>
      {checkSubmit("buttonSubmitAsset") && !statusField && (
        <Flex className="justify-content-center mt-5">
          <ButtonRadius
            type="submit"
            title="บันทึกข้อมูล"
            form="assetform"
            iconPath="/new/images/Vector.png"
            background={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 py-2"
            border="none"
          />
        </Flex>
      )}
      <ModalLoading visible={visible} />
    </form>
  );
};

export default AssetImage;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex mt-2 flex-row px-2 align-items-center",
}))`
  background-color: #ffff;
  width: 100%;
  height: 60px;
  border-radius: 8px;
  border-style: solid;
  border-width: 2px;
  border-color: #ced4da;
  overflow-x: scroll;
`;

const Circle = styled.div.attrs(() => ({
  className: "",
}))`
  height: 25px;
  width: 25px;
  background-color: black;
  border-radius: 50%;
  display: inline-block;
`;
