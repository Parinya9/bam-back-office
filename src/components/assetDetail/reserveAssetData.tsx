import { forwardRef, FunctionComponent, useEffect, useState } from "react";
import { Flex } from "../layout";
import { TextSemiBold } from "../text";
import Colors from "../../../utils/Colors";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../Input";
import styled from "styled-components";
import DatePicker from "react-datepicker";
import callApi from "../../pages/api/callApi";
import moment from "moment";
import { apiSyncAssetDetail } from "../../pages/api/elsSync";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import Cookies from "universal-cookie";
import validateFieldAsset from "../../../utils/ValidateFieldAsset";
import ModalLoading from "../../components/Modal/ModalLoading";

interface ReserveAssetData {
  mode: string;
  id: string;
  statusField: any;
  checkSubmit: any;
  flagTab: any;
  setFlagTab: any;
  display_property: any;
}
const ReserveAssetData: FunctionComponent<ReserveAssetData> = ({
  id,
  mode,
  statusField,
  checkSubmit,
  flagTab,
  setFlagTab,
  display_property,
}) => {
  const {
    register,
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv
          style={{
            background: Colors.lightGrayF6,
            marginTop: "0.5em",
          }}
        >
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";
  const [fieldDisabled, setFieldDisabled] = useState(false);
  const cookies = new Cookies();
  const [assetItem, setAssetItem] = useState([]);
  const [visible, setVisible] = useState(false);

  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const [radioHighlight, setRadioHighlight] = useState("");
  const onChangeFieldSetFlagTab = () => {
    if (!flagTab) {
      setFlagTab(true);
    }
  };
  const onChangeHighLightRadio = (event: any) => {
    onChangeFieldSetFlagTab();
    setRadioHighlight(event.target.value);
  };

  const onSubmit = (data: any) => {
    editData(data);
  };
  const editData = async (data: any) => {
    let objEdit;
    setVisible(true);
    if (validateFieldAsset.checkFieldRequire(assetItem)) {
      objEdit = {
        property_highlight: radioHighlight == "ใช่" ? true : false,
        display_property: display_property ? "true" : "false",
        id: id,
      };
    } else {
      objEdit = {
        property_highlight: radioHighlight == "ใช่" ? true : false,
        status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        display_property: display_property ? "true" : "false",
        id: id,
      };
      let objLog = {
        asset_id: parseInt(id),
        status_approve: true,
        user: firstname + " " + lastname,
        position: role,
        title: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        note: "",
      };
      await callApi.apiPost("history-log/addLog", objLog);
    }

    const item = await callApi.apiPut("property-detail/update", objEdit);

    const response = await apiSyncAssetDetail("api/asset-detail");
    if (item.affected == 1) {
      if (response.status === "success") {
        setVisible(false);

        alert("Update Success");
      } else {
        setVisible(false);

        alert("Cant'not update asset");
      }
    } else {
      setVisible(false);
    }
  };
  const setData = async () => {
    const conditionSearch = { id: parseInt(id) };
    const response = await callApi.apiPost(
      "property-detail/findKey",
      conditionSearch
    );
    if (response.length > 0) {
      setValue("npa_type", response[0].npa_type);
      setAssetItem(response[0]);
      if (response[0].property_highlight != null) {
        setRadioHighlight(response[0].property_highlight ? "ใช่" : "ไม่");
      }
      response[0].reserve_start_date != null
        ? setValue(
            "reserve_start_date",
            new Date(
              moment(response[0].reserve_start_date).format("yyyy-MM-DD")
            )
          )
        : "";
      response[0].reserve_end_date != null
        ? setValue(
            "reserve_end_date",
            new Date(moment(response[0].reserve_end_date).format("yyyy-MM-DD"))
          )
        : "";
    }
  };
  const setField = () => {
    mode == "readonly" ? setFieldDisabled(true) : setFieldDisabled(false);
  };
  useEffect(() => {
    setField();
    setData();
  }, []);
  return (
    <form onSubmit={handleSubmit(onSubmit)} id="assetform">
      <Flex className="flex-column mt-5">
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            gridGap: "40px",
          }}
        >
          <Flex className="flex-column">
            <TextSemiBold
              fontSize="16px"
              weight="400"
              color={Colors.mainBlackLight}
            >
              ประเภททรัพย์
            </TextSemiBold>

            <Controller
              name="npa_type"
              control={control}
              rules={{ required: false }}
              render={({ field }) => (
                <InputRadius
                  width="100%"
                  borderWidth="1px"
                  borderColor="#CED4DA"
                  border="solid"
                  field={field}
                  disabled
                />
              )}
            />
          </Flex>
        </div>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            gridGap: "40px",
          }}
          className="mt-3"
        >
          <Flex className="flex-column">
            <TextSemiBold
              fontSize="16px"
              weight="normal"
              color={Colors.mainBlackLight}
            >
              วันที่เริ่มต้น
            </TextSemiBold>
            <DatePickerDiv>
              <Controller
                control={control}
                name="reserve_start_date"
                rules={{ required: false }}
                render={({
                  field: { onChange, name, value },
                  formState: { errors },
                }) => (
                  <>
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      placeholderText="DD/MM/YYYY"
                      value={value || ""}
                      selected={value}
                      disabled={true}
                      onChange={(date) => {
                        onChange(date);
                      }}
                      customInput={
                        <CustomInput
                          value={value}
                          placeholder="DD/MM/YYYY"
                          onClick={onclick}
                        />
                      }
                    />
                  </>
                )}
              />
            </DatePickerDiv>
          </Flex>

          <Flex className="flex-column">
            <TextSemiBold
              fontSize="16px"
              weight="normal"
              color={Colors.mainBlackLight}
            >
              วันที่สิ้นสุด
            </TextSemiBold>
            <DatePickerDiv>
              <Controller
                control={control}
                name="reserve_end_date"
                rules={{ required: false }}
                render={({
                  field: { onChange, name, value },
                  formState: { errors },
                }) => (
                  <>
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      placeholderText="DD/MM/YYYY"
                      value={value || ""}
                      selected={value}
                      disabled={true}
                      onChange={(date) => {
                        onChange(date);
                      }}
                      customInput={
                        <CustomInput
                          value={value}
                          placeholder="DD/MM/YYYY"
                          onClick={onclick}
                        />
                      }
                    />
                  </>
                )}
              />
            </DatePickerDiv>
          </Flex>
          <Flex className="flex-column">
            <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
              ทรัพย์ Highlight
            </TextSemiBold>
            <Flex className="flex-row mt-3">
              <Flex width="100px">
                <input
                  className="form-check-input"
                  type="radio"
                  value="ใช่"
                  checked={radioHighlight === "ใช่"}
                  onChange={onChangeHighLightRadio}
                  aria-label="..."
                  width="100%"
                  disabled={fieldDisabled}
                />
                <TextSemiBold fontSize="16px" className="ms-3">
                  ใช่
                </TextSemiBold>
              </Flex>
              <Flex width="100px">
                <input
                  className="form-check-input"
                  type="radio"
                  value="ไม่"
                  checked={radioHighlight === "ไม่"}
                  onChange={onChangeHighLightRadio}
                  aria-label="..."
                  width="100%"
                  disabled={fieldDisabled}
                />
                <TextSemiBold fontSize="16px" className="ms-3">
                  ไม่
                </TextSemiBold>
              </Flex>
            </Flex>
          </Flex>
        </div>
      </Flex>
      {checkSubmit("buttonSubmitAsset") && !statusField && (
        <Flex className="justify-content-center mt-5">
          <ButtonRadius
            type="submit"
            title="บันทึกข้อมูล"
            form="assetform"
            iconPath="/new/images/Vector.png"
            background={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 py-2"
            border="none"
          />
        </Flex>
      )}
      <ModalLoading visible={visible} />
    </form>
  );
};

export default ReserveAssetData;

const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
