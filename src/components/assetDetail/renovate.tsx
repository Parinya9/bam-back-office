import { useState, useRef, FunctionComponent, useEffect } from "react";
import { Flex } from "../layout";
import { TextSemiBold } from "../text";
import CardAssetImage from "../Card/CardAssetImage";
import Colors from "../../../utils/Colors";
import styled from "styled-components";
import InputRadiusAsset from "../../components/Layout/InputRadiusAsset";
import TextAreaRadiusInput from "../../components/textAreaInput";
import ButtonRadiusIconL from "../Button/ButtonRadiusIconL";
import ModalAddMultiImage from "../Modal/ModalAddMultiImage";
import { toggle } from "slidetoggle";
import ButtonRightSideRadius from "../Button/ButtonRightSideRadius";
import { useForm, Controller } from "react-hook-form";
import Cookies from "universal-cookie";
import { useSelector, useDispatch } from "react-redux";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import validateFieldAsset from "../../../utils/ValidateFieldAsset";

import {
  deleteImageRenovate1,
  deleteImageRenovate2,
  deleteImageRenovate3,
  addImageRenovate1,
  addImageRenovate2,
  addImageRenovate3,
  saveImageRenovate1,
  saveImageRenovate2,
  saveImageRenovate3,
  editImageRenovate1,
  editImageRenovate2,
  editImageRenovate3,
  saveEditImageRenovate1,
  saveEditImageRenovate2,
  saveEditImageRenovate3,
  clearImageRenovate1,
  clearImageRenovate2,
  clearImageRenovate3,
  clearEditImageRenovate1,
  clearEditImageRenovate2,
  clearEditImageRenovate3,
} from "../../redux/assetDetail/action";
import callApi from "../../pages/api/callApi";
import moment from "moment";
import { apiSyncAssetDetail } from "../../pages/api/elsSync";
import ModalLoading from "../../components/Modal/ModalLoading";

interface TrackingLog {
  mode: string;
  id: string;
  statusField: any;
  checkSubmit: any;
  flagTab: any;
  setFlagTab: any;
  display_property: any;
}
const Renovate: FunctionComponent<TrackingLog> = ({
  id,
  mode,
  statusField,
  checkSubmit,
  flagTab,
  setFlagTab,
  display_property,
}) => {
  const [visible, setVisible] = useState(false);
  const [showAssetImage, setShowAssetImage] = useState(false);
  const [showAssetImage2, setShowAssetImage2] = useState(false);
  const [showAssetImage3, setShowAssetImage3] = useState(false);
  const [dataAsset, setDataAsset] = useState([]);
  const [dataAsset2, setDataAsset2] = useState([]);
  const [dataAsset3, setDataAsset3] = useState([]);
  const [disabledField, setDisabledField] = useState(false);
  const [slideProperty1, setSlideProperty1] = useState(true);
  const [slideProperty2, setSlideProperty2] = useState(true);
  const [slideProperty3, setSlideProperty3] = useState(true);
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const dispatch = useDispatch();
  const [assetItem, setAssetItem] = useState([]);

  const stateImageRenovate1 = useSelector(
    (state: any) => state?.assetDetail?.stateImageRenovate1
  );
  const stateImageRenovate2 = useSelector(
    (state: any) => state?.assetDetail?.stateImageRenovate2
  );
  const stateImageRenovate3 = useSelector(
    (state: any) => state?.assetDetail?.stateImageRenovate3
  );
  const stateEditImageRenovate1 = useSelector(
    (state: any) => state?.assetDetail?.stateEditImageRenovate1
  );
  const stateEditImageRenovate2 = useSelector(
    (state: any) => state?.assetDetail?.stateEditImageRenovate2
  );
  const stateEditImageRenovate3 = useSelector(
    (state: any) => state?.assetDetail?.stateEditImageRenovate3
  );

  const onDelete1 = (data: any) => {
    dispatch(deleteImageRenovate1(data));
  };

  const onDelete2 = (data: any) => {
    dispatch(deleteImageRenovate2(data));
  };

  const onDelete3 = (data: any) => {
    dispatch(deleteImageRenovate3(data));
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    watch,
    reset,
    setError,
    clearErrors,
    getValues,
    formState: { errors },
  } = useForm({});

  const toggleDivProperty = () => {
    setSlideProperty1(!slideProperty1);
    toggle("div.toggle-divproperty", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const onChangeFieldSetFlagTab = () => {
    if (!flagTab) {
      setFlagTab(true);
    }
  };
  const toggleDivProperty2 = () => {
    setSlideProperty2(!slideProperty2);
    toggle("div.toggle-divproperty2", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };

  const toggleDivProperty3 = () => {
    setSlideProperty3(!slideProperty3);
    toggle("div.toggle-divproperty3", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const [file, setFile] = useState(null as any);
  const [file2, setFile2] = useState(null as any);
  const [file3, setFile3] = useState(null as any);
  const inputFile: any = useRef<HTMLInputElement>(null);
  const inputFile2: any = useRef<HTMLInputElement>(null);
  const inputFile3: any = useRef<HTMLInputElement>(null);
  const watchFile: any = watch("uploadFile1");
  const watchFile2: any = watch("uploadFile2");
  const watchFile3: any = watch("uploadFile3");
  const textFieldPrice = register("price_package_1", {
    pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
  });

  const handleFile = (event: any, setValueFile: any, keyFile: any) => {
    if (event.target.files && event.target.files[0]) {
      setValueFile(event.target.files[0]);
    }
    if (event.target.files && event.target.files[0]) {
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue(keyFile, {
          name: "",
        });
        setValueFile("");
      } else {
        var listType = [
          "application/pdf",
          "application/msword",
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setValueFile(event.target.files[0]);
          onChangeFieldSetFlagTab();
        } else {
          alert("Incorrect Type File (Support pdf , doc and docx only )");
          setValue(keyFile, "");
          setValueFile("");
        }
      }
    }
  };

  const onSubmit = (data: any) => {
    if (
      checkDetailPackage(
        "detail_package_1",
        "price_package_1",
        "uploadFile1",
        "renovate_price_package_1"
      ) &&
      checkDetailPackage(
        "detail_package_2",
        "price_package_2",
        "uploadFile2",
        "renovate_price_package_2"
      ) &&
      checkDetailPackage(
        "detail_package_3",
        "price_package_3",
        "uploadFile3",
        "renovate_price_package_3"
      )
    ) {
      editData(data);
    }
  };
  const checkEmpty = (data: any) => {
    if (data == "" || data == null || data == undefined) {
      return true;
    }
    return false;
  };
  const checkDetailPackage = (
    detail: any,
    price: any,
    upload: any,
    renovate_price: any
  ) => {
    var type = true;
    var detailPackage = getValues(detail);
    var pricePackage = getValues(price);
    var uploadPackage = getValues(upload);
    var renovate_price_package = getValues(renovate_price);
    if (!checkEmpty(detailPackage)) {
      if (checkEmpty(pricePackage)) {
        type = false;
        setError(price, {
          type: "manual",
          message: "",
        });
      }
      if (uploadPackage == null || uploadPackage == "") {
        type = false;

        setError(upload, {
          type: "manual",
          message: "",
        });
      }
      if (checkEmpty(renovate_price_package)) {
        type = false;

        setError(renovate_price, {
          type: "manual",
          message: "",
        });
      }
      return type;
    }

    return type;
  };
  const editData = async (data: any) => {
    setVisible(true);
    let formData = new FormData();
    if (!validateFieldAsset.checkFieldRequire(assetItem)) {
      let objLog = {
        asset_id: parseInt(id),
        status_approve: true,
        user: firstname + " " + lastname,
        position: role,
        title: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        note: "",
      };
      formData.append(
        "status_approve",
        "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์"
      );
      await callApi.apiPost("history-log/addLog", objLog);
    }

    genFormData(formData, data);
    setFlagTab(false);
    const item = await callApi.apiPut("property-detail/update/form", formData);
    const response = await apiSyncAssetDetail("api/asset-detail");

    if (item.error) {
      alert("error");
      setVisible(false);
    }

    if (response.status === "success") {
      setVisible(false);

      alert("Update success");
    } else {
      setVisible(false);

      alert("Cant'not update asset");
    }
  };
  const setJsonImgAsset = (data: any) => {
    var imgAssetJson: any = [];
    data.map((item: any, index: any) => {
      imgAssetJson.push({
        no: index,
        url: item.url,
        detail: item.detail,
        name: item.name,
        display: item.display,
      });
    });

    return JSON.stringify(imgAssetJson);
  };
  const setFormData = (formData: any, key: any, data: any, type: any) => {
    if (type == "date") {
      if (data != undefined) {
        formData.append(key, setFormatDate(data));
      }
    } else if (type == "dropdown") {
      if (data != undefined) {
        formData.append(key, data.value);
      }
    } else {
      if (data != null) {
        formData.append(key, data);
      }
    }
  };
  const genFormData = async (formData: any, data: any) => {
    let listString = [
      "title_renovate",
      "sub_title_renovate",
      "renovate_price_package_1",
      "detail_package_1",
      "price_package_1",
      "renovate_price_package_2",
      "detail_package_2",
      "price_package_2",
      "renovate_price_package_3",
      "detail_package_3",
      "price_package_3",
    ];
    file != null ? formData.append("attchment_package_1", file, file.name) : "";
    file2 != null
      ? formData.append("attchment_package_2", file2, file2.name)
      : "";
    file3 != null
      ? formData.append("attchment_package_3", file3, file3.name)
      : "";

    formData.append(
      "album_package_1",
      setJsonImgAsset(stateEditImageRenovate1)
    );
    formData.append(
      "album_package_2",
      setJsonImgAsset(stateEditImageRenovate2)
    );
    formData.append(
      "album_package_3",
      setJsonImgAsset(stateEditImageRenovate3)
    );

    formData.append("id", id);
    formData.append("display_property", display_property ? "true" : "false");

    listString.map((item: any, index: number) => {
      if (data[item] != null) {
        setFormData(formData, item, data[item], "string");
      }
    });
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  const setData = async () => {
    const conditionSearch = { id: parseInt(id) };
    const response = await callApi.apiPost(
      "property-detail/findKey",
      conditionSearch
    );

    if (response[0] != undefined) {
      setAssetItem(response[0]);
      response[0].album_package_1 != null
        ? dispatch(saveEditImageRenovate1(response[0].album_package_1))
        : dispatch(clearEditImageRenovate1([]));
      response[0].album_package_2 != null
        ? dispatch(saveEditImageRenovate2(response[0].album_package_2))
        : dispatch(clearEditImageRenovate2([]));
      response[0].album_package_3 != null
        ? dispatch(saveEditImageRenovate3(response[0].album_package_3))
        : dispatch(clearEditImageRenovate3([]));

      response[0].album_package_1 != null
        ? setDataAsset(response[0].album_package_1)
        : "";
      response[0].album_package_2 != null
        ? setDataAsset2(response[0].album_package_2)
        : "";
      response[0].album_package_3 != null
        ? setDataAsset3(response[0].album_package_3)
        : "";

      response[0].attchment_package_1 != null
        ? setValue("uploadFile1", {
            name: response[0].attchment_package_1.name,
          })
        : "";
      response[0].attchment_package_2 != null
        ? setValue("uploadFile2", {
            name: response[0].attchment_package_2.name,
          })
        : "";
      response[0].attchment_package_3 != null
        ? setValue("uploadFile3", {
            name: response[0].attchment_package_3.name,
          })
        : "";
      setValue("title_renovate", response[0].title_renovate);
      setValue("sub_title_renovate", response[0].sub_title_renovate);
      setValue(
        "renovate_price_package_1",
        response[0].renovate_price_package_1
      );
      setValue("detail_package_1", response[0].detail_package_1);
      setValue("price_package_1", response[0].price_package_1);
      setValue(
        "renovate_price_package_2",
        response[0].renovate_price_package_2
      );
      setValue("detail_package_2", response[0].detail_package_2);
      setValue("price_package_2", response[0].price_package_2);
      setValue(
        "renovate_price_package_3",
        response[0].renovate_price_package_3
      );
      setValue("detail_package_3", response[0].detail_package_3);
      setValue("price_package_3", response[0].price_package_3);
    }
  };
  const setField = () => {
    mode == "readonly" ? setDisabledField(true) : setDisabledField(false);
  };

  useEffect(() => {
    setData();
    setField();
  }, []);
  const onClickAlbum1 = () => {
    setShowAssetImage(true);
    dispatch(clearImageRenovate1([]));
  };
  const onClickAlbum2 = () => {
    setShowAssetImage2(true);
    dispatch(clearImageRenovate2([]));
  };
  const onClickAlbum3 = () => {
    setShowAssetImage3(true);
    dispatch(clearImageRenovate3([]));
  };
  const uploadImgRenovate = async (
    saveImageRenovate: any,
    imgRenovate: any,
    setData: any,
    saveEditImageRenovate: any,
    stateEditImageRenovate: any
  ) => {
    dispatch(saveImageRenovate(imgRenovate));

    var formData = new FormData();

    if (imgRenovate.length > 0) {
      imgRenovate.map((item: any, index: number) => {
        formData.append("files", item);
      });
      const item = await callApi.apiPost(
        "property-detail/uploadMulti",
        formData
      );

      if (stateEditImageRenovate.length > 0) {
        item.map((data: any, index: number) => {
          stateEditImageRenovate.push({
            detail: "",
            name: data.name,
            url: data.url,
            no: stateEditImageRenovate.length,
            display: true,
          });
        });
        setData(stateEditImageRenovate);
        dispatch(saveEditImageRenovate(stateEditImageRenovate));
      } else {
        item.map((data: any, index: number) => {
          stateEditImageRenovate.push({
            detail: "",
            name: data.name,
            url: data.url,
            no: index,
            display: true,
          });
        });
        setData(stateEditImageRenovate);
        dispatch(saveEditImageRenovate(stateEditImageRenovate));
      }
    }
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} id="assetform">
      <div className="container mt-5">
        <Flex className="flex-column ">
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                หัวข้อ &nbsp;
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
              </TextSemiBold>

              <Controller
                name="title_renovate"
                control={control}
                rules={{ required: true }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      disabled={disabledField}
                    />
                  );
                }}
              />

              {errors.title_renovate && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                หัวข้อย่อย &nbsp;
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
              </TextSemiBold>

              <Controller
                name="sub_title_renovate"
                control={control}
                rules={{ required: true }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      disabled={disabledField}
                    />
                  );
                }}
              />
              {errors.sub_title_renovate && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </div>
        </Flex>
        <div className="row mt-3 mx-1">
          <div
            style={{
              borderBottom: " 1px solid #CED4DA",
              marginTop: "1.5rem",
              paddingBottom: "0.5rem",
              paddingLeft: "0px",
              paddingRight: "0px",
            }}
          >
            <TextSemiBold fontSize="24px">
              แพ็กเกจที่ 1
              {slideProperty1 ? (
                <img
                  src="/new/images/collaspe.svg"
                  width="20px"
                  height="20px"
                  style={{ float: "right" }}
                  onClick={toggleDivProperty}
                />
              ) : (
                <img
                  src="/new/images/expand.svg"
                  width="20px"
                  height="20px"
                  style={{ float: "right" }}
                  onClick={toggleDivProperty}
                />
              )}
            </TextSemiBold>
          </div>
        </div>

        <Flex className="flex-column toggle-divproperty">
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                ข้อความ &nbsp;
              </TextSemiBold>
              <Controller
                name="detail_package_1"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextAreaRadiusInput
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      rows="5"
                      disabled={disabledField}
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                    />
                  );
                }}
              />
            </Flex>
          </div>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                เอกสารแนบ (รายการประกอบแบบ) &nbsp;
              </TextSemiBold>
              <RadiusBox>
                <Flex widthMobile="100%" className="justify-content-between">
                  <Flex className="ms-3 mt-1">{watchFile?.name || ""}</Flex>
                  <ButtonRightSideRadius
                    onClick={() => inputFile.current.click()}
                    backgroundColor={Colors.mainBlue}
                    title="เลือกไฟล์"
                    width="20%"
                    height="100%"
                    color={Colors.mainWhite}
                  />

                  <Controller
                    name="uploadFile1"
                    control={control}
                    rules={{ required: false }}
                    render={({ field: { onChange } }) => (
                      <input
                        onChange={(e: any) => {
                          onChange(e.target.files[0]);
                          handleFile(e, setFile, "uploadFile1");
                        }}
                        type="file"
                        disabled={disabledField}
                        style={{ display: "none" }}
                        id="uploadFile1"
                        accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                        ref={inputFile}
                      />
                    )}
                  />
                </Flex>
              </RadiusBox>
              <TextSemiBold
                weight="300"
                fontSize="16px"
                className="mt-2"
                color={Colors.gray9b}
              >
                เฉพาะไฟล์ .doc , .pdf , .docx เท่านั้น ขนาดไฟล์ไม่เกิน 10 MB
              </TextSemiBold>
              {errors.uploadFile1 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </div>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                ราคาตั้งขาย &nbsp;
              </TextSemiBold>

              <Controller
                name="price_package_1"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      value={value}
                      {...register("price_package_1", {
                        pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                      })}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      disabled={disabledField}
                    />
                  );
                }}
              />

              {errors.price_package_1 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                ราคาค่าปรับปรุงทรัพย์ (ราคาประเมินเบื้องต้น) &nbsp;
              </TextSemiBold>

              <Controller
                name="renovate_price_package_1"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      value={value}
                      {...register("renovate_price_package_1", {
                        pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                      })}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      disabled={disabledField}
                    />
                  );
                }}
              />
              {errors.renovate_price_package_1 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex />
          </div>
          <Flex className="flex-row justify-content-between mt-4">
            <TextSemiBold fontSize="24px">อัลบั้มรูปภาพทรัพย์</TextSemiBold>
            {mode != "readonly" && (
              <ButtonRadiusIconL
                onClick={() => onClickAlbum1()}
                backgroundColor="white"
                title="เพิ่มรูปภาพ"
                border="solid"
                borderColor={Colors.mainBlue}
                fontSize="17px"
                borderWidth="1px"
                className="px-3 ms-2 mx-1 mt-1 mt-3"
                color={Colors.mainBlue}
                iconPath="/new/images/Add.svg"
              />
            )}
          </Flex>
          <div
            className="px-3 py-4"
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr 1fr 1fr",
              backgroundColor: Colors.lightGrayF6,
              gridGap: "15px",
              marginTop: 30,
              height: stateEditImageRenovate1.length > 0 ? "auto" : "220px",
            }}
          >
            {stateEditImageRenovate1.length > 0 &&
              stateEditImageRenovate1.map((item: any, index: number) => {
                return (
                  <CardAssetImage
                    key={index}
                    linkPath={item.url}
                    id={index}
                    detail={item.detail}
                    fileName={item.name}
                    display={item.display}
                    saveEditImage={saveEditImageRenovate1}
                    stateEditImage={stateEditImageRenovate1}
                    editImage={editImageRenovate1}
                    mode={mode}
                  />
                );
              })}
          </div>
        </Flex>

        {/* 2 */}

        <div className="row mt-3 mx-1">
          <div
            style={{
              borderBottom: " 1px solid #CED4DA",
              marginTop: "1.5rem",
              paddingBottom: "0.5rem",
              paddingLeft: "0px",
              paddingRight: "0px",
            }}
          >
            <TextSemiBold fontSize="24px">
              แพ็กเกจที่ 2
              {slideProperty2 ? (
                <img
                  src="/new/images/collaspe.svg"
                  width="20px"
                  height="20px"
                  style={{ float: "right" }}
                  onClick={toggleDivProperty2}
                />
              ) : (
                <img
                  src="/new/images/expand.svg"
                  width="20px"
                  height="20px"
                  style={{ float: "right" }}
                  onClick={toggleDivProperty2}
                />
              )}
            </TextSemiBold>
          </div>
        </div>

        <Flex className="flex-column toggle-divproperty2">
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                ข้อความ &nbsp;
              </TextSemiBold>
              <Controller
                name="detail_package_2"
                control={control}
                rules={{ required: false }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextAreaRadiusInput
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      rows="5"
                      disabled={disabledField}
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                    />
                  );
                }}
              />
            </Flex>
          </div>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                เอกสารแนบ (รายการประกอบแบบ) &nbsp;
              </TextSemiBold>
              <RadiusBox>
                <Flex widthMobile="100%" className="justify-content-between">
                  <Flex className="ms-3 mt-1">{watchFile2?.name || ""}</Flex>
                  <ButtonRightSideRadius
                    onClick={() => inputFile2.current.click()}
                    backgroundColor={Colors.mainBlue}
                    title="เลือกไฟล์"
                    width="20%"
                    height="100%"
                    color={Colors.mainWhite}
                  />

                  <Controller
                    name="uploadFile2"
                    control={control}
                    rules={{ required: false }}
                    render={({ field: { onChange } }) => (
                      <input
                        onChange={(e: any) => {
                          onChange(e.target.files[0]);
                          // handleFile2(e);
                          handleFile(e, setFile2, "uploadFile2");
                        }}
                        type="file"
                        disabled={disabledField}
                        style={{ display: "none" }}
                        id="uploadFile2"
                        accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                        ref={inputFile2}
                      />
                    )}
                  />
                </Flex>
              </RadiusBox>
              <TextSemiBold
                weight="300"
                fontSize="16px"
                className="mt-2"
                color={Colors.gray9b}
              >
                เฉพาะไฟล์ .doc , .pdf , .docx เท่านั้น ขนาดไฟล์ไม่เกิน 10 MB
              </TextSemiBold>
              {errors.uploadFile2 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </div>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                ราคาตั้งขาย &nbsp;
              </TextSemiBold>

              <Controller
                name="price_package_2"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      value={value}
                      {...register("price_package_2", {
                        pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                      })}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      disabled={disabledField}
                    />
                  );
                }}
              />
              {errors.price_package_2 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                ราคาค่าปรับปรุงทรัพย์ (ราคาประเมินเบื้องต้น) &nbsp;
              </TextSemiBold>
              <Controller
                name="renovate_price_package_2"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      value={value}
                      {...register("renovate_price_package_2", {
                        pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                      })}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      disabled={disabledField}
                    />
                  );
                }}
              />

              {errors.renovate_price_package_2 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex />
          </div>
          <Flex className="flex-row justify-content-between mt-4">
            <TextSemiBold fontSize="24px">อัลบั้มรูปภาพทรัพย์</TextSemiBold>
            {mode != "readonly" && (
              <ButtonRadiusIconL
                onClick={() => onClickAlbum2()}
                backgroundColor="white"
                title="เพิ่มรูปภาพ"
                border="solid"
                borderColor={Colors.mainBlue}
                fontSize="17px"
                borderWidth="1px"
                className="px-3 ms-2 mx-1 mt-1 mt-3"
                color={Colors.mainBlue}
                iconPath="/new/images/Add.svg"
              />
            )}
          </Flex>
          <div
            className="px-3 py-4"
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr 1fr 1fr",
              backgroundColor: Colors.lightGrayF6,
              gridGap: "15px",
              marginTop: 30,
              height: stateEditImageRenovate2.length > 0 ? "auto" : "220px",
            }}
          >
            {stateEditImageRenovate2.length > 0 &&
              stateEditImageRenovate2.map((item: any, index: number) => {
                return (
                  <CardAssetImage
                    key={index}
                    linkPath={item.url}
                    display={item.display}
                    id={index}
                    detail={item.detail}
                    fileName={item.name}
                    saveEditImage={saveEditImageRenovate2}
                    stateEditImage={stateEditImageRenovate2}
                    editImage={editImageRenovate2}
                    mode={mode}
                  />
                );
              })}
          </div>
        </Flex>

        <div className="row mt-3 mx-1">
          <div
            style={{
              borderBottom: " 1px solid #CED4DA",
              marginTop: "1.5rem",
              paddingBottom: "0.5rem",
              paddingLeft: "0px",
              paddingRight: "0px",
            }}
          >
            <TextSemiBold fontSize="24px">
              แพ็กเกจที่ 3
              {slideProperty3 ? (
                <img
                  src="/new/images/collaspe.svg"
                  width="20px"
                  height="20px"
                  style={{ float: "right" }}
                  onClick={toggleDivProperty3}
                />
              ) : (
                <img
                  src="/new/images/expand.svg"
                  width="20px"
                  height="20px"
                  style={{ float: "right" }}
                  onClick={toggleDivProperty3}
                />
              )}
            </TextSemiBold>
          </div>
        </div>

        <Flex className="flex-column toggle-divproperty3">
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                ข้อความ &nbsp;
              </TextSemiBold>
              <Controller
                name="detail_package_3"
                control={control}
                rules={{ required: false }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextAreaRadiusInput
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      rows="5"
                      disabled={disabledField}
                      value={value}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                    />
                  );
                }}
              />
            </Flex>
          </div>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                เอกสารแนบ (รายการประกอบแบบ) &nbsp;
              </TextSemiBold>
              <RadiusBox>
                <Flex widthMobile="100%" className="justify-content-between">
                  <Flex className="ms-3 mt-1">{watchFile3?.name || ""}</Flex>
                  <ButtonRightSideRadius
                    onClick={() => inputFile3.current.click()}
                    backgroundColor={Colors.mainBlue}
                    title="เลือกไฟล์"
                    width="20%"
                    height="100%"
                    color={Colors.mainWhite}
                  />

                  <Controller
                    name="uploadFile3"
                    control={control}
                    rules={{ required: false }}
                    render={({ field: { onChange } }) => (
                      <input
                        onChange={(e: any) => {
                          onChange(e.target.files[0]);
                          handleFile(e, setFile3, "uploadFile3");
                        }}
                        type="file"
                        disabled={disabledField}
                        style={{ display: "none" }}
                        id="uploadFile3"
                        accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                        ref={inputFile3}
                      />
                    )}
                  />
                </Flex>
              </RadiusBox>
              <TextSemiBold
                weight="300"
                fontSize="16px"
                className="mt-2"
                color={Colors.gray9b}
              >
                เฉพาะไฟล์ .doc , .pdf , .docx เท่านั้น ขนาดไฟล์ไม่เกิน 10 MB
              </TextSemiBold>
              {errors.uploadFile3 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </div>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "50px",
            }}
          >
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                ราคาตั้งขาย &nbsp;
              </TextSemiBold>

              <Controller
                name="price_package_3"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      {...register("price_package_3", {
                        pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                      })}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      value={value}
                      disabled={disabledField}
                    />
                  );
                }}
              />
              {errors.price_package_3 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex className="flex-column mt-3">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                ราคาค่าปรับปรุงทรัพย์ (ราคาประเมินเบื้องต้น) &nbsp;
              </TextSemiBold>

              <Controller
                name="renovate_price_package_3"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <InputRadiusAsset
                      height="36px"
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      value={value}
                      {...register("renovate_price_package_3", {
                        pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                      })}
                      onChange={(event: any) => {
                        onChangeFieldSetFlagTab();
                        onChange(event.target.value);
                      }}
                      disabled={disabledField}
                    />
                  );
                }}
              />
              {errors.renovate_price_package_3 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex />
          </div>
          <Flex className="flex-row justify-content-between mt-4">
            <TextSemiBold fontSize="24px">อัลบั้มรูปภาพทรัพย์</TextSemiBold>
            {mode != "readonly" && (
              <ButtonRadiusIconL
                onClick={() => onClickAlbum3()}
                backgroundColor="white"
                title="เพิ่มรูปภาพ"
                border="solid"
                borderColor={Colors.mainBlue}
                fontSize="17px"
                borderWidth="1px"
                className="px-3 ms-2 mx-1 mt-1 mt-3"
                color={Colors.mainBlue}
                iconPath="/new/images/Add.svg"
              />
            )}
          </Flex>

          <div
            className="px-3 py-4"
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr 1fr 1fr",
              backgroundColor: Colors.lightGrayF6,
              gridGap: "15px",
              marginTop: 30,
              height: stateEditImageRenovate3.length > 0 ? "auto" : "220px",
            }}
          >
            {stateEditImageRenovate3.length > 0 &&
              stateEditImageRenovate3.map((item: any, index: number) => {
                return (
                  <CardAssetImage
                    key={index}
                    linkPath={item.url}
                    id={index}
                    display={item.display}
                    detail={item.detail}
                    fileName={item.name}
                    saveEditImage={saveEditImageRenovate3}
                    stateEditImage={stateEditImageRenovate3}
                    editImage={editImageRenovate3}
                    mode={mode}
                  />
                );
              })}
          </div>
        </Flex>

        <ModalAddMultiImage
          show={showAssetImage}
          setShow={setShowAssetImage}
          setDataAsset={setDataAsset}
          // onSubmit={() => dispatch(saveImageRenovate1(stateImageRenovate1))}
          onSubmit={() =>
            uploadImgRenovate(
              saveImageRenovate1,
              stateImageRenovate1,
              setDataAsset,
              saveEditImageRenovate1,
              stateEditImageRenovate1
            )
          }
          onDelete={onDelete1}
          imageData={stateImageRenovate1}
          addImage={addImageRenovate1}
        />

        <ModalAddMultiImage
          show={showAssetImage2}
          setShow={setShowAssetImage2}
          setDataAsset={setDataAsset2}
          // onSubmit={() => dispatch(saveImageRenovate1(stateImageRenovate1))}
          onSubmit={() =>
            uploadImgRenovate(
              saveImageRenovate2,
              stateImageRenovate2,
              setDataAsset2,
              saveEditImageRenovate2,
              stateEditImageRenovate2
            )
          }
          onDelete={onDelete2}
          imageData={stateImageRenovate2}
          addImage={addImageRenovate2}
        />

        <ModalAddMultiImage
          show={showAssetImage3}
          setShow={setShowAssetImage3}
          setDataAsset={setDataAsset3}
          // onSubmit={() => dispatch(saveImageRenovate3(stateImageRenovate3))}
          onSubmit={() =>
            uploadImgRenovate(
              saveImageRenovate3,
              stateImageRenovate3,
              setDataAsset3,
              saveEditImageRenovate3,
              stateEditImageRenovate3
            )
          }
          onDelete={onDelete3}
          imageData={stateImageRenovate3}
          addImage={addImageRenovate3}
        />
      </div>
      {checkSubmit("buttonSubmitAsset") && !statusField && (
        <Flex className="justify-content-center mt-5">
          <ButtonRadius
            type="submit"
            title="บันทึกข้อมูล"
            form="assetform"
            iconPath="/new/images/Vector.png"
            background={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 py-2"
            border="none"
          />
        </Flex>
      )}
      <ModalLoading visible={visible} />
    </form>
  );
};

export default Renovate;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
