import React, { FunctionComponent, useEffect, useState } from "react";
import { TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { useForm, Controller } from "react-hook-form";
import InputRadiusAsset from "../../components/Layout/InputRadiusAsset";
import TextAreaRadiusInput from "../../components/textAreaInput";
import "react-datepicker/dist/react-datepicker.css";
import styled from "styled-components";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import { toggle } from "slidetoggle";
import InputRadiusWithSpan from "../Layout/InputRadiusWithSpan";
import InputRadiusWithRSpan from "../Layout/InputRadiusWithRSpan";
import callApi from "../../pages/api/callApi";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import { Flex } from "../layout";
import validateFieldAsset from "../../../utils/ValidateFieldAsset";
import ModalLoading from "../../components/Modal/ModalLoading";

import Cookies from "universal-cookie";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";
import {
  apiSyncAssetDetail,
  apiSyncAssetDetailAuction,
} from "../../pages/api/elsSync";
import InputRadiusArea from "../Layout/InputRadiusArea";

interface AssetDetail {
  mode: string;
  id: string;
  checkSubmit: any;
  flagTab: any;
  setFlagTab: any;
  display_property: any;
}

const AssetDetail: FunctionComponent<AssetDetail> = ({
  id,
  mode,
  checkSubmit,
  flagTab,
  setFlagTab,
  display_property,
}) => {
  const {
    control,
    handleSubmit,
    watch,
    getValues,
    register,
    setValue,
    formState: { errors },
  } = useForm();

  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const token = cookies.get("token");
  const [visible, setVisible] = useState(false);
  const [statusField, setStatusField] = useState(true);
  const [slideProperty, setSlideProperty] = useState(true);
  const [slideLocation, setSlideLocation] = useState(true);
  const [slideEtc, setSlideEtc] = useState(true);
  const [dropdownLicense, setDropDownLicense] = useState([]);
  const [nearbyList, setNearbyList] = useState<any>([]);
  const [lati, setLati] = useState(0);
  const [longti, setLongti] = useState(0);
  const [assetItem, setAssetItem] = useState([]);
  const [disabledBedRoom, setDisabledBedRoom] = useState(false);
  const [disabledStudio, setDisabledStudio] = useState(false);
  const MapWithAMarker = withScriptjs(
    withGoogleMap((props) => (
      <GoogleMap defaultZoom={8} defaultCenter={{ lat: lati, lng: longti }}>
        <Marker position={{ lat: lati, lng: longti }} />
      </GoogleMap>
    ))
  );
  const toggleDivProperty = () => {
    setSlideProperty(!slideProperty);
    toggle("div.toggle-divproperty", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const onChangeFieldSetFlagTab = () => {
    if (!flagTab) {
      setFlagTab(true);
    }
  };
  const toggleDivLocation = () => {
    setSlideLocation(!slideLocation);
    toggle("div.toggle-divlocation", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const toggleDivEtc = () => {
    setSlideEtc(!slideEtc);
    toggle("div.toggle-divetc", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const setDropDown = async () => {
    const listLicense = await callApi.apiGet("master/License");
    setDropDownLicense(listLicense);
  };
  const setNearby = (listData: any) => {
    if (listData != null) {
      listData.map((item: any, index: any) => {
        setValue("checkbox" + item.place, true);
        setValue(item.place, item.detail);
      });
    }
  };

  useEffect(() => {
    setData();
  }, []);

  const onSubmit = (data: any) => {
    editData(data);
  };

  const editData = async (data: any) => {
    setVisible(true);
    let objEdit;
    if (validateFieldAsset.checkFieldRequireAssetImage(assetItem)) {
      objEdit = {
        project_th: data.project_th,
        project_en: data.project_en,
        size_build: data.size_build,
        invitation_th: data.invitation_th,
        property_detail: data.property_detail,
        note: data.note,
        col_no: data.col_no,
        gps_lat1: data.gps_lat1,
        gps_long1: data.gps_long1,
        property_location: data.property_location,
        bedroom: data.bedroom,
        bathroom: data.bathroom,
        livingroom: data.livingroom,
        studio: data.studio,
        parking: data.parking,
        kitchen: data.kitchen,
        usabled_area: data.usabled_area,
        location: data.location,
        license: data.license != undefined ? data.license.value : "",
        internal_record: data.internal_record,
        nearby: setNearbyItem(data),
        property_source: data.property_source,
        telephone: data.telephone,
        work_phone: data.work_phone,
        work_phone_nxt: data.work_phone_nxt,
        ref_debt: data.ref_debt,
        id: id,
        display_property: display_property ? "true" : "false",
      };
    } else {
      objEdit = {
        project_th: data.project_th,
        project_en: data.project_en,
        size_build: data.size_build,
        invitation_th: data.invitation_th,
        property_detail: data.property_detail,
        note: data.note,
        col_no: data.col_no,
        gps_lat1: data.gps_lat1,
        gps_long1: data.gps_long1,
        property_location: data.property_location,
        bedroom: data.bedroom,
        bathroom: data.bathroom,
        livingroom: data.livingroom,
        studio: data.studio,
        parking: data.parking,
        kitchen: data.kitchen,
        usabled_area: data.usabled_area,
        location: data.location,
        license: data.license != undefined ? data.license.value : "",
        internal_record: data.internal_record,
        nearby: setNearbyItem(data),
        property_source: data.property_source,
        telephone: data.telephone,
        work_phone: data.work_phone,
        work_phone_nxt: data.work_phone_nxt,
        ref_debt: data.ref_debt,
        id: id,
        display_property: display_property ? "true" : "false",
        status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
      };
      let objLog = {
        asset_id: parseInt(id),
        status_approve: true,
        user: firstname + " " + lastname,
        position: role,
        title: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        note: "",
      };

      const notify = {
        asset_id: parseInt(id),
        market_code: "",
        roles: "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        type: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
      };
      await callApi.apiPost("history-log/addLog", objLog);
      await callApi.apiPost("notification/add", notify);
    }

    setFlagTab(false);
    const item = await callApi.apiPut("property-detail/update", objEdit);

    if (item.affected == 1) {
      const response = await apiSyncAssetDetail("api/asset-detail");
      if (response.status === "success") {
        setVisible(false);

        alert("Update Success");
      } else {
        setVisible(false);

        alert("Cant'not update asset");
      }
    } else {
      setVisible(false);
    }
  };
  const setNearbyItem = (data: any) => {
    var near: any = [];
    nearbyList.map((item: any, index: any) => {
      if (data["checkbox" + item.title_th]) {
        near.push({ place: item.title_th, detail: data[item.title_th] });
      }
    });
    return near;
  };
  const setDetailAo = (aoDetail: any) => {
    if (aoDetail.length > 0) {
      setValue("telephone", aoDetail[0].mobile_number);
      setValue("work_phone", aoDetail[0].tel_number);
      setValue("work_phone_nxt", aoDetail[0].txt);
    }
  };
  const setData = async () => {
    if (mode == "edit") {
      setStatusField(false);
    } else {
      setStatusField(true);
      setDisabledBedRoom(true);
      setDisabledStudio(true);
    }
    setDropDown();
    const conditionSearch = { id: parseInt(id) };
    const response = await callApi.apiPost(
      "property-detail/findKey",
      conditionSearch
    );
    const listNearby = await callApi.apiGet("nearby-list/");

    if (response[0] != undefined) {
      setAssetItem(response[0]);
      if (
        response[0].ao_name !== null &&
        response[0].ao_name !== undefined &&
        response[0].ao_name !== ""
      ) {
        let aoNameSplit = response[0].ao_name.split(" ");
        if (aoNameSplit.length > 1) {
          let objFind = { firstname: aoNameSplit[0], lastname: aoNameSplit[1] };
          const aoDetail = await callApi.apiPost("admin/find", objFind, token);
          setDetailAo(aoDetail);
        }
      }

      setNearby(response[0].nearby);
      setNearbyList(listNearby);
      setLongti(parseFloat(response[0].gps_long1));
      setLati(parseFloat(response[0].gps_lat1));
      setValue("project_th", response[0].project_th);
      setValue("project_en", response[0].project_en);
      setValue("size_build", response[0].size_build);
      setValue("invitation_th", response[0].invitation_th);
      setValue("property_source", response[0].property_source);
      setValue("license", response[0].license);
      setValue("note", response[0].note);
      setValue("col_no", response[0].col_no);
      setValue("gps_lat1", response[0].gps_lat1);
      setValue("gps_long1", response[0].gps_long1);
      setValue("property_location", response[0].property_location);
      setValue("location", response[0].location);
      setValue("npa_type", response[0].npa_type);
      setValue("grade", response[0].grade);
      setValue("dept_name", response[0].dept_name);
      setValue("property_detail", response[0].property_detail);
      setValue("rai", parseInt(response[0].rai));
      setValue("ngan", parseInt(response[0].ngan));
      setValue("wa", response[0].wa);
      setValue("area_meter", response[0].area_meter);
      setValue("internal_record", response[0].internal_record);
      if (
        response[0].studio !== 0 &&
        response[0].studio != undefined &&
        response[0].studio != null &&
        response[0].studio != "" &&
        mode == "edit"
      ) {
        setDisabledStudio(true);
        setDisabledBedRoom(false);
      }
      if (
        response[0].bathroom !== 0 &&
        response[0].bathroom != undefined &&
        response[0].bathroom != null &&
        response[0].bathroom != "" &&
        mode == "edit"
      ) {
        setDisabledBedRoom(true);
        setDisabledStudio(false);
      }
      setValue("bathroom", response[0].bathroom);
      setValue("bedroom", response[0].bedroom);
      setValue("livingroom", response[0].livingroom);
      setValue("studio", response[0].studio);
      setValue("parking", response[0].parking);
      setValue("kitchen", response[0].kitchen);
      setValue("usabled_area", response[0].usabled_area);

      setValue("ref_debt", response[0].ref_debt);
      setValue("province_name", {
        value: response[0].province_name,
        label: response[0].province_name,
      });
      setValue("physical_zone", {
        value: response[0].physical_zone,
        label: response[0].physical_zone,
      });
      setValue("city_name", {
        value: response[0].city_name,
        label: response[0].city_name,
      });
      setValue("add_district", {
        value: response[0].add_district,
        label: response[0].add_district,
      });
      setValue("license", {
        value: response[0].license,
        label: response[0].license,
      });
    }
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} id="assetform">
      <div
        className="container pb-5"
        style={{
          backgroundColor: "white",
          minHeight: "450px",
        }}
        key={"assetDetail"}
      >
        <div>
          {" "}
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                ข้อมูลทรัพย์{" "}
                {slideProperty ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivProperty}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivProperty}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-divproperty" style={{ width: "100%" }}>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ชื่อโครงการ (TH) &nbsp;
                </TextSemiBold>
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
                <Controller
                  name="project_th"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        disabled={statusField}
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
                {errors.project_th && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ชื่อโครงการ (EN) &nbsp;
                </TextSemiBold>

                <Controller
                  name="project_en"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                        disabled={statusField}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ขนาดอาคาร
                </TextSemiBold>

                <Controller
                  name="size_build"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                        disabled={statusField}
                      />
                    );
                  }}
                />
              </div>
            </div>

            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  คำเชิญชวน (TH) &nbsp; &nbsp;
                </TextSemiBold>
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
                <Controller
                  name="invitation_th"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextAreaRadiusInput
                        width="100%"
                        disabled={statusField}
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        rows="6"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />

                {errors.invitation_th && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6" style={{ marginTop: "2.2rem" }}>
                <TextSemiBold
                  fontSize="16px"
                  color={Colors.mainBlackLight}
                  style={{ display: "block" }}
                >
                  ตัวอย่างคำเชิญชวน &nbsp;
                </TextSemiBold>

                <TextSemiBold
                  fontSize="16px"
                  weight="300"
                  color={Colors.mainBlackLight}
                >
                  บ้านเดี่ยว 2 ชั้น / ทาวน์เฮ้าส์ 3 ชั้น /
                  ทำเลดีโครงการติดถนนใหญ่ (ติด/ใกล้ถนนอะไร,ใกล้ทางด่วน)
                  เดินทางเข้า-ออกสะดวก อยู่ใกล้สถานที่สำคัญ (สถานที่ราชการ,
                  ห้างสรรพสินค้า, โรงเรียน, etc) &nbsp;
                </TextSemiBold>
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  รายละเอียดของทรัพย์ (TH) &nbsp;
                </TextSemiBold>
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>

                <Controller
                  name="property_detail"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextAreaRadiusInput
                        width="100%"
                        disabled={statusField}
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        rows="6"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
                {errors.property_detail && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6" style={{ marginTop: "2.2em " }}>
                <TextSemiBold
                  fontSize="16px"
                  color={Colors.mainBlackLight}
                  style={{ display: "block" }}
                >
                  ตัวอย่างรายละเอียด &nbsp;
                </TextSemiBold>

                <TextSemiBold
                  fontSize="16px"
                  weight="300"
                  color={Colors.mainBlackLight}
                >
                  บ้านเดี่ยว 2 ชั้น, 3 ห้องนอน, 3 ห้องน้ำ, 1 ห้องครัว, 1
                  ห้องนั่งเล่น, 2 ที่จอดรถ ตกแต่งพร้อมอยู่
                  ภายในกว้างขวางด้วยพื้นที่ใช้สอยขนาด _ ตร.ม.
                  พร้อมสิ่งอำนวยความสะดวกครบครัน และพื้นที่ ส่วนกลาง อาทิ
                  ฟิตเนส, สระว่ายน้ำ พร้อมระบบรักษาความปลอดภัย รปภ. 24 ชม.
                  คีย์การ์ดเข้า-ออก โครงการ
                </TextSemiBold>
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  หมายเหตุ (TH) &nbsp;
                </TextSemiBold>

                <Controller
                  name="note"
                  control={control}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextAreaRadiusInput
                        width="100%"
                        disabled={statusField}
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        rows="3"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6 mt-1">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  เอกสารสิทธิ์ &nbsp;
                </TextSemiBold>

                <Controller
                  name="license"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <Select
                        placeholder="โปรดเลือก"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange({ value: event.value, label: event.label });
                        }}
                        styles={customStyles}
                        instanceId="type"
                        isDisabled={statusField}
                        options={dropdownLicense}
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                      />
                    );
                  }}
                />
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  เลขที่เอกสารสิทธิ์ &nbsp;
                </TextSemiBold>
                <Controller
                  // col_no = เลขทีั่เอกสารสิทธิ์ field จาก bam
                  name="col_no"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        disabled={true}
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  เนื้อที่ (ไร่-งาน-วา) &nbsp;
                </TextSemiBold>
                <div className="row" style={{ textAlign: "right" }}>
                  <div className="col-3">
                    <Controller
                      name="rai"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusArea
                            disabled={true}
                            title="ไร่"
                            value={value}
                          ></InputRadiusArea>
                        );
                      }}
                    />
                  </div>
                  <div className="col-3">
                    <Controller
                      name="ngan"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusArea
                            disabled={true}
                            title="งาน"
                            value={value}
                          ></InputRadiusArea>
                        );
                      }}
                    />
                  </div>
                  <div className="col-3">
                    <Controller
                      name="wa"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusArea
                            disabled={true}
                            title="วา"
                            value={value}
                          ></InputRadiusArea>
                        );
                      }}
                    />
                  </div>
                  <div className="col-3">
                    <Controller
                      name="area_meter"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusArea
                            disabled={true}
                            title="ตร.ม."
                            value={value}
                          ></InputRadiusArea>
                        );
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  พื้นที่ใช้สอย &nbsp;
                </TextSemiBold>
                <Controller
                  name="usabled_area"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusWithRSpan
                        title="ตร.ม."
                        disabled={statusField}
                        {...register("usabled_area", {
                          pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                        })}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      ></InputRadiusWithRSpan>
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  สิ่งอำนวยความสะดวก &nbsp;
                </TextSemiBold>

                <div className="row">
                  <div className="col-3">
                    <Controller
                      name="bedroom"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusWithSpan
                            title="ห้องนอน"
                            disabled={disabledBedRoom}
                            {...register("bedroom", {
                              pattern: /^[0-9]*$/,
                            })}
                            value={value}
                            onChange={(event: any) => {
                              if (
                                event.target.value !== 0 &&
                                event.target.value != undefined &&
                                event.target.value != null &&
                                event.target.value != ""
                              ) {
                                setDisabledStudio(true);
                              } else {
                                setDisabledStudio(false);
                              }
                              onChangeFieldSetFlagTab();
                              onChange(event.target.value);
                            }}
                          ></InputRadiusWithSpan>
                        );
                      }}
                    />
                    {errors.bedroom && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                  </div>
                  <div className="col-3">
                    <Controller
                      name="studio"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusWithSpan
                            disabled={disabledStudio}
                            title="ห้องสตูดิโอ"
                            {...register("studio", {
                              pattern: /^[0-9]*$/,
                            })}
                            value={value}
                            onChange={(event: any) => {
                              console.log(event.target.value);
                              if (
                                event.target.value !== 0 &&
                                event.target.value != undefined &&
                                event.target.value != null &&
                                event.target.value != ""
                              ) {
                                setDisabledBedRoom(true);
                              } else {
                                setDisabledBedRoom(false);
                              }
                              onChangeFieldSetFlagTab();
                              onChange(event.target.value);
                            }}
                          ></InputRadiusWithSpan>
                        );
                      }}
                    />
                    {errors.studio && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                  </div>
                  <div className="col-3">
                    <Controller
                      name="bathroom"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusWithSpan
                            disabled={statusField}
                            title="ห้องน้ำ"
                            {...register("bathroom", {
                              pattern: /^[0-9]*$/,
                            })}
                            value={value}
                            onChange={(event: any) => {
                              onChangeFieldSetFlagTab();
                              onChange(event.target.value);
                            }}
                          ></InputRadiusWithSpan>
                        );
                      }}
                    />
                    {errors.bathroom && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                  </div>
                  <div className="col-3">
                    <Controller
                      name="livingroom"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusWithSpan
                            disabled={statusField}
                            title="ห้องนั่งเล่น"
                            {...register("livingroom", {
                              pattern: /^[0-9]*$/,
                            })}
                            value={value}
                            onChange={(event: any) => {
                              onChangeFieldSetFlagTab();
                              onChange(event.target.value);
                            }}
                          ></InputRadiusWithSpan>
                        );
                      }}
                    />
                    {errors.livingroom && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                  </div>
                </div>
                <div className="row">
                  <div className="col-3">
                    <Controller
                      name="kitchen"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusWithSpan
                            disabled={statusField}
                            title="ห้องครัว"
                            {...register("kitchen", {
                              pattern: /^[0-9]*$/,
                            })}
                            value={value}
                            onChange={(event: any) => {
                              onChangeFieldSetFlagTab();
                              onChange(event.target.value);
                            }}
                          ></InputRadiusWithSpan>
                        );
                      }}
                    />
                    {errors.kitchen && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                  </div>
                  <div className="col-3">
                    <Controller
                      name="parking"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <InputRadiusWithSpan
                            disabled={statusField}
                            title="ที่จอดรถ"
                            {...register("parking", {
                              pattern: /^[0-9]*$/,
                            })}
                            value={value}
                            onChange={(event: any) => {
                              onChangeFieldSetFlagTab();
                              onChange(event.target.value);
                            }}
                          ></InputRadiusWithSpan>
                        );
                      }}
                    />
                    {errors.parking && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ส่วนงานที่รับผิดชอบ
                </TextSemiBold>
                <Controller
                  name="dept_name"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
              <div className="col-6">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ที่มาของทรัพย์สิน
                </TextSemiBold>
                <Controller
                  name="property_source"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={statusField}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  เกรดทรัพย์สิน
                </TextSemiBold>
                <Controller
                  name="grade"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
              <div className="col-6">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ประเภททรัพย์สิน
                </TextSemiBold>
                <Controller
                  name="npa_type"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  เบอร์โทรศัพท์ &nbsp;
                </TextSemiBold>
                <Controller
                  name="telephone"
                  control={control}
                  // rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
                {/* {errors.telephone && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )} */}
              </div>
              <div className="col-3">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  เบอร์โทรศัพท์สำนักงาน{" "}
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="work_phone"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
                {errors.work_phone && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div
                style={{
                  padding: "0px",
                  marginTop: "2rem",
                  flex: "0 0 auto",
                  width: "3%",
                }}
              >
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  {" "}
                </TextSemiBold>
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ต่อ{" "}
                </TextSemiBold>
              </div>
              <div className="col-2 mt-4" style={{ paddingLeft: "0px" }}>
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                ></TextSemiBold>
                <Controller
                  name="work_phone_nxt"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
                {errors.work_phone_nxt && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  อ้างอิงรหัสลูกหนี้
                </TextSemiBold>
                <Controller
                  name="ref_debt"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={statusField}
                        {...register("ref_debt", {
                          pattern: /^[0-9]*$/,
                        })}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
                {errors.ref_debt && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
          </div>
        </div>
        <div>
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                ที่ตั้งทรัพย์{" "}
                {slideLocation ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivLocation}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivLocation}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-divlocation" style={{ width: "100%" }}>
            {lati != null && longti != null ? (
              <MapWithAMarker
                googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
              />
            ) : (
              ""
            )}
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ทำเล &nbsp;
                </TextSemiBold>
                <Controller
                  name="location"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={statusField}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ละติจูด
                </TextSemiBold>
                <Controller
                  name="gps_lat1"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
              <div className="col-6">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ลองติจูด
                </TextSemiBold>
                <Controller
                  name="gps_long1"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ที่ตั้งทรัพย์สิน (TH) &nbsp;
                </TextSemiBold>
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
                <Controller
                  name="property_location"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextAreaRadiusInput
                        width="100%"
                        rows="6"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={statusField}
                        // {...register("property_location", {
                        //   pattern:
                        //     /^[0-9ก-๙!@#\r\n\$%\^\&*\)\(\{\}\[\]\~\-\?\<\>\,\/\'\"\:\;\\\|\=\-\+\.\`\_ ]+$/,
                        // })}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                      />
                    );
                  }}
                />
                {errors.property_location && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6" style={{ marginTop: "2.2em " }}>
                <TextSemiBold
                  fontSize="16px"
                  color={Colors.mainBlackLight}
                  style={{ display: "block" }}
                >
                  ตัวอย่างที่ตั้งทรัพย์สิน &nbsp;
                </TextSemiBold>

                <TextSemiBold
                  fontSize="16px"
                  color={Colors.mainBlackLight}
                  style={{ display: "block", marginLeft: "1.5em" }}
                >
                  <li>ประเภทมีสิ่งปลูกสร้าง</li>
                </TextSemiBold>
                <TextSemiBold
                  fontSize="16px"
                  weight="300"
                  color={Colors.mainBlackLight}
                  style={{ display: "block" }}
                >
                  เลขที่ 51/832 มุมซอย 7/4 ห่างจากถนนรังสิต-นครนายก ประมาณ 1.8
                  กม.
                </TextSemiBold>
                <TextSemiBold
                  fontSize="16px"
                  color={Colors.mainBlackLight}
                  style={{ display: "block", marginLeft: "1.5em" }}
                >
                  <li>ประเภทที่ดินเปล่า</li>
                </TextSemiBold>
                <TextSemiBold
                  fontSize="16px"
                  weight="300"
                  color={Colors.mainBlackLight}
                  style={{ display: "block" }}
                >
                  ถนนนวมินทร์ ซอยนวมินทร์ 26 แยก 1 เข้าไปประมาณ 600 ม.
                  ทรัพย์อยู่ด้านขวามือ แขวงคลองกลุ่ม
                </TextSemiBold>
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6 mt-1">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ภาค
                </TextSemiBold>
                <Controller
                  name="physical_zone"
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      isDisabled={true}
                      styles={customStyles}
                      instanceId="type"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                    />
                  )}
                />
              </div>
              <div className="col-6 mt-1">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  จังหวัด
                </TextSemiBold>
                <Controller
                  name="province_name"
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      isDisabled={true}
                      styles={customStyles}
                      instanceId="type"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                    />
                  )}
                />
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6 mt-1">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  อำเภอ / เขต
                </TextSemiBold>
                <Controller
                  name="city_name"
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      isDisabled={true}
                      styles={customStyles}
                      instanceId="type"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                    />
                  )}
                />
              </div>
              <div className="col-6 mt-1">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ตำบล / แขวง
                </TextSemiBold>
                <Controller
                  name="add_district"
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      isDisabled={true}
                      styles={customStyles}
                      instanceId="type"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                    />
                  )}
                />
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6 mt-1">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  สถานที่ใกล้เคียง
                </TextSemiBold>
              </div>
            </div>
            {nearbyList.map((item: any, index: any) =>
              index % 2 == 0 ? (
                <div className="row" key={index}>
                  <div className="col-6">
                    <div className="row">
                      <div className="col-1 mt-3">
                        {/* <label className="labelCheckbox">
                          <input
                            type="checkbox"
                            style={{ width: "20px", height: "20px" }}
                          />
                          <span className="spanCheckbox"></span>
                        </label> */}
                        <Controller
                          control={control}
                          name={"checkbox" + item.title_th}
                          render={({
                            field: { onChange, onBlur, value, name, ref },
                          }) => (
                            <label className="labelCheckbox">
                              <input
                                type="checkbox"
                                onBlur={onBlur} // notify when input is touched
                                onChange={(event: any) => {
                                  onChangeFieldSetFlagTab();
                                  onChange(event.target.checked);
                                }}
                                checked={value}
                                style={{ width: "20px", height: "20px" }}
                                disabled={statusField}
                              />
                              <span className="spanCheckbox"></span>
                            </label>
                          )}
                        />
                      </div>
                      <div className="col-5 mt-3">
                        <TextSemiBold
                          className="mt-3"
                          fontSize="16px"
                          mobileSize="16px"
                          weight="300"
                          color={Colors.mainBlackLight}
                        >
                          {item.title_th}
                        </TextSemiBold>
                      </div>
                      {item.show_display ? (
                        <div className="col-6">
                          <Controller
                            name={item.title_th}
                            control={control}
                            rules={{ required: false }}
                            render={({ field: { onChange, value } }) => {
                              return (
                                <InputRadiusAsset
                                  width="100%"
                                  borderWidth="1px"
                                  borderColor="#CED4DA"
                                  border="solid"
                                  disabled={statusField}
                                  onChange={(event: any) => {
                                    onChangeFieldSetFlagTab();
                                    onChange(event.target.value);
                                  }}
                                  value={value}
                                />
                              );
                            }}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                  {nearbyList.length > index + 1 ? (
                    <div className="col-6">
                      <div className="row">
                        <div className="col-1 mt-3">
                          <Controller
                            control={control}
                            name={"checkbox" + nearbyList[index + 1].title_th}
                            render={({
                              field: { onChange, onBlur, value, name, ref },
                              fieldState: {
                                invalid,
                                isTouched,
                                isDirty,
                                error,
                              },
                              formState,
                            }) => (
                              <label className="labelCheckbox">
                                <input
                                  type="checkbox"
                                  onBlur={onBlur} // notify when input is touched
                                  onChange={(event: any) => {
                                    onChangeFieldSetFlagTab();
                                    onChange(event.target.checked);
                                  }}
                                  checked={value}
                                  disabled={statusField}
                                  style={{ width: "20px", height: "20px" }}
                                />
                                <span className="spanCheckbox"></span>
                              </label>
                            )}
                          />
                        </div>
                        <div className="col-5 mt-3">
                          <TextSemiBold
                            className="mt-3"
                            fontSize="16px"
                            mobileSize="16px"
                            weight="300"
                            color={Colors.mainBlackLight}
                          >
                            {nearbyList[index + 1].title_th}
                          </TextSemiBold>
                        </div>
                        {nearbyList[index + 1].show_display ? (
                          <div className="col-6">
                            <Controller
                              name={nearbyList[index + 1].title_th}
                              control={control}
                              rules={{ required: false }}
                              render={({ field: { onChange, value } }) => {
                                return (
                                  <InputRadiusAsset
                                    width="100%"
                                    borderWidth="1px"
                                    borderColor="#CED4DA"
                                    border="solid"
                                    onChange={(event: any) => {
                                      onChangeFieldSetFlagTab();
                                      onChange(event.target.value);
                                    }}
                                    value={value}
                                    disabled={statusField}
                                  />
                                );
                              }}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              ) : (
                ""
              )
            )}
          </div>
        </div>
        <div>
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                อื่นๆ{" "}
                {slideEtc ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivEtc}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivEtc}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-divetc" style={{ width: "100%" }}>
            <div className="row mt-3">
              {" "}
              <div className="col-12">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  บันทึกภายใน ระหว่างเจ้าหน้าที่ขายกับแอดมิน &nbsp;
                </TextSemiBold>
                <Controller
                  name="internal_record"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextAreaRadiusInput
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        rows="2"
                        disabled={statusField}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                        value={value}
                      />
                    );
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {checkSubmit("buttonSubmitAsset") && !statusField && (
        <Flex className="justify-content-center mt-5">
          <ButtonRadius
            type="submit"
            title="บันทึกข้อมูล"
            form="assetform"
            iconPath="/new/images/Vector.png"
            background={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 py-2"
            border="none"
          />
        </Flex>
      )}
      <ModalLoading visible={visible} />
    </form>
  );
};

export default AssetDetail;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
export async function getServerSideProps({ query }: { query: any }) {
  return { props: { mode: query.mode || null, title: query.title || null } };
}
