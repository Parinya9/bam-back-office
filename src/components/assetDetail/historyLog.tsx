import { FunctionComponent } from "react";
import TableHistoryLog from "../../components/Table/TableHistoryLog";

interface HistoryLog {
  id: string;
}
const HistoryLog: FunctionComponent<HistoryLog> = ({ id }) => {
  return (
    <div>
      <TableHistoryLog id={id} />
    </div>
  );
};

export default HistoryLog;
