import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import { useForm, Controller } from "react-hook-form";
import InputRadiusAsset from "../../components/Layout/InputRadiusAsset";
import { toggle } from "slidetoggle";
import "react-datepicker/dist/react-datepicker.css";
import styled from "styled-components";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import ButtonRightSideRadius from "../../components/Button/ButtonRightSideRadius";
import Switch from "react-switch";
import DatePicker from "../../components/Datepicker/DatePicker";
import InputRadiusWithRSpan from "../Layout/InputRadiusWithRSpan";
import callApi from "../../pages/api/callApi";
import moment from "moment";
import ModalPriceDisplay from "../Modal/ModalPriceDisplay";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import Cookies from "universal-cookie";
import validateFieldAsset from "../../../utils/ValidateFieldAsset";
import ModalLoading from "../../components/Modal/ModalLoading";

import { apiSyncAssetDetail } from "../../pages/api/elsSync";
interface AssetStatus {
  mode: string;
  id: string;
  checkSubmit: any;
  flagTab: any;
  setFlagTab: any;
  display_property: any;
}
const AssetStatus: FunctionComponent<AssetStatus> = ({
  id,
  mode,
  checkSubmit,
  flagTab,
  setFlagTab,
  display_property,
}) => {
  const {
    control,
    handleSubmit,
    setValue,
    register,
    getValues,
    setError,
    clearErrors,
    watch,
    formState: { errors },
  } = useForm();
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const [visible, setVisible] = useState(false);
  const [displayPrice, setDisplayPrice] = useState(false);
  const [displaySpecialPrice, setDisplaySpecialPrice] = useState(false);
  const [statusField, setStatusField] = useState(true);
  const mockPropertySp = [
    { type: "ทรัพย์ราคาพิเศษ", show_des: false, description: "ฮัลโหลเทส" },
  ];
  const [showDisplayPrice, setShowDisplayPrice] = useState(false);
  const [assetGroup, setAssetGroup] = useState([]);
  const [pdf, setPdf] = useState(null as any);
  const inputPdf: any = useRef<HTMLInputElement>(null);
  const [slideStatusProperty, setSlideStatusProperty] = useState(true);
  const [slideAuction, setSlideAuction] = useState(true);
  const [slidePrice, setSlidePrice] = useState(true);
  const [slideSpeicalPrice, setSlideSpeicalPrice] = useState(true);
  const [dropdownStar, setDropDownStar] = useState([]);
  const [dropdownPropertyStatus, setDropdownPropertyStatus] = useState([]);
  const [flagRequiredSpecalprice, setFlagRequiredSpecalprice] = useState(false);
  const watchPDF = watch("uploadPdf");
  const [assetItem, setAssetItem] = useState([]);

  const onChangeFieldSetFlagTab = () => {
    if (!flagTab) {
      setFlagTab(true);
    }
  };
  const toggleDivStatusProperty = () => {
    setSlideStatusProperty(!slideStatusProperty);
    toggle("div.toggle-divstatusproperty", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const toggleDivAuction = () => {
    setSlideAuction(!slideAuction);
    toggle("div.toggle-divauction", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const toggleDivPrice = () => {
    setSlidePrice(!slidePrice);
    toggle("div.toggle-divprice", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const toggleDivSpecialPrice = () => {
    setSlideSpeicalPrice(!slideSpeicalPrice);
    toggle("div.toggle-divspecialprice", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const onSubmit = (data: any) => {
    if (checkSpecialPrice()) {
      editData(data);
    }
  };
  const compareDate = (date1: any, date2: any) => {
    if (typeof date1 == "string") {
      date1 = setDate(date1);
    }
    if (typeof date2 == "string") {
      date2 = setDate(date2);
    }
    var d1 = new Date(date1);
    var d2 = new Date(date2);
    if (d1.getTime() < d2.getTime()) {
      return true;
    }
    return false;
  };
  const setDate = (date: any) => {
    var dateSplit = date.split("/");
    return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
  };
  const checkEmpty = (data: any) => {
    if (data == "" || data == null || data == undefined) {
      return true;
    }
    return false;
  };
  const checkSpecialPrice = () => {
    var spPrice = getValues("sale_price_spc_amt");
    var sale_price_spc_from_date = getValues("sale_price_spc_from_date");
    var sale_price_spc_to_date = getValues("sale_price_spc_to_date");
    var type = true;
    if (!checkEmpty(spPrice)) {
      if (checkEmpty(sale_price_spc_from_date) && displaySpecialPrice) {
        type = false;
        setError("sale_price_spc_from_date", {
          type: "manual",
          message: "",
        });
      }
      if (checkEmpty(sale_price_spc_to_date) && displaySpecialPrice) {
        type = false;

        setError("sale_price_spc_to_date", {
          type: "manual",
          message: "",
        });
      }

      if (
        type &&
        displaySpecialPrice &&
        !compareDate(sale_price_spc_from_date, sale_price_spc_to_date)
      ) {
        type = false;

        setError("sale_price_spc_to_date", {
          type: "manual",
          message: "",
        });
      }
    }

    return type;
    // if (spPrice == "" || spPrice == null || spPrice == undefined) {
    //   clearErrors("sale_price_spc_from_date");
    //   clearErrors("sale_price_spc_to_date");

    //   return false;
    // }
    // setError("sale_price_spc_from_date", {
    //   type: "manual",
    //   message: "",
    // });
    // setError("sale_price_spc_to_date", {
    //   type: "manual",
    //   message: "",
    // });
    // return true;
  };
  const editData = async (data: any) => {
    setVisible(true);
    let formData = new FormData();
    if (!validateFieldAsset.checkFieldRequire(assetItem)) {
      let objLog = {
        asset_id: parseInt(id),
        status_approve: true,
        user: firstname + " " + lastname,
        position: role,
        title: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        note: "",
      };
      formData.append(
        "status_approve",
        "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์"
      );
      await callApi.apiPost("history-log/addLog", objLog);
    }

    genFormData(formData, data);
    setFlagTab(false);
    const item = await callApi.apiPut("property-detail/update/form", formData);
    const response = await apiSyncAssetDetail("api/asset-detail");
    if (item.error) {
      setVisible(false);

      alert("error");
    }
    if (response.status === "success") {
      setVisible(false);

      alert("Update success");
    } else {
      setVisible(false);

      alert("Cant'not update asset");
    }
  };

  const setJsonAssetGroup = (data: any) => {
    var assetJson: any = [];
    assetGroup.map((item: any, index: any) => {
      if (data["checkbox" + item.type]) {
        assetJson.push({ group: item.type, description: data[item.type] });
      }
    });
    mockPropertySp.map((item: any, index: any) => {
      if (data["checkbox" + item.type]) {
        assetJson.push({ group: item.type, description: data[item.type] });
      }
    });
    return JSON.stringify(assetJson);
  };
  const setData = async () => {
    const conditionSearch = { id: parseInt(id) };
    const response = await callApi.apiPost(
      "property-detail/findKey",
      conditionSearch
    );
    if (response[0] != undefined) {
      setAssetItem(response[0]);
      setAssetField(response[0].group_property);
      setValue(
        "evaluate_amt",
        parseFloat(response[0].evaluate_amt).toLocaleString("en-US")
      );
      setValue("asset_state", {
        value: response[0].asset_state,
        label: response[0].asset_state,
      });
      setValue("property_star", {
        value: response[0].property_star,
        label: response[0].property_star,
      });

      setValue(
        "cost_asset_amt",
        parseFloat(response[0].cost_asset_amt).toLocaleString("en-US")
      );
      setValue(
        "center_price",
        parseFloat(response[0].center_price).toLocaleString("en-US")
      );
      setValue(
        "maintenance_price",
        response[0].maintenance_price != null
          ? parseFloat(response[0].maintenance_price).toLocaleString("en-US")
          : ""
      );
      setValue(
        "sale_price_spc_amt",
        parseFloat(response[0].sale_price_spc_amt).toLocaleString("en-US")
      );
      setValue(
        "asset_state_pending",
        response[0].status_approve != null ? response[0].status_approve : ""
      );
      setDisplayPrice(response[0].display_price);
      setDisplaySpecialPrice(response[0].display_special_price);
      response[0].document_auction != null
        ? setValue("uploadPdf", { name: response[0].document_auction.name })
        : "";
      response[0].sale_price_spc_from_date != null
        ? setValue(
            "sale_price_spc_from_date",
            new Date(
              moment(response[0].sale_price_spc_from_date).format("yyyy-MM-DD")
            )
          )
        : "";
      response[0].date_auction != null
        ? setValue(
            "date_auction",
            new Date(moment(response[0].date_auction).format("yyyy-MM-DD"))
          )
        : "";
      response[0].sale_price_spc_to_date != null
        ? setValue(
            "sale_price_spc_to_date",
            new Date(
              moment(response[0].sale_price_spc_to_date).format("yyyy-MM-DD")
            )
          )
        : "";
      response[0].evaluate_date != null
        ? setValue(
            "evaluate_date",
            new Date(moment(response[0].evaluate_date).format("yyyy-MM-DD"))
          )
        : "";
    }
  };
  const setField = () => {
    mode == "readonly" ? setStatusField(true) : setStatusField(false);
  };
  const setAssetField = (listData: any) => {
    if (listData != null) {
      listData.map((item: any, index: any) => {
        setValue("checkbox" + item.group, true);
        setValue(item.group, item.description);
      });
    }
  };
  const setDropDown = async () => {
    const listPropertyStatus = await callApi.apiGet(
      "master/propertystatus/dropdown"
    );
    const listStar = await callApi.apiGet("master/Star");
    setDropDownStar(listStar);
    setDropdownPropertyStatus(listPropertyStatus);
  };
  const setFieldCheckBox = async () => {
    const listAsset = await callApi.apiGet("master/Propertygroup");
    setAssetGroup(listAsset);
  };
  const setFormData = (formData: any, key: any, data: any, type: any) => {
    if (type == "date") {
      if (data != undefined) {
        formData.append(key, setFormatDate(data));
      }
    } else if (type == "dropdown") {
      if (data != undefined) {
        formData.append(key, data.value);
      }
    } else {
      if (data != undefined && data !== 0 && data !== "") {
        formData.append(key, data);
      }
    }
  };
  const genFormData = async (formData: any, data: any) => {
    let listDate = [
      "date_auction",
      "sale_price_spc_from_date",
      "sale_price_spc_to_date",
    ];
    let listString = ["maintenance_price"];
    let listDropDown = ["property_star"];

    pdf != null ? formData.append("pdf", pdf, pdf.name) : "";

    formData.append("group_property", setJsonAssetGroup(data));

    formData.append("display_property", display_property ? "true" : "false");

    formData.append(
      "display_special_price",
      displaySpecialPrice ? "true" : "false"
    );
    formData.append("id", id);

    listDate.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "date");
    });
    listDropDown.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "dropdown");
    });
    listString.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "string");
    });
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    return moment(date).format("yyyy-MM-DD");
  };
  const handlePdf = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      // 10485760
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue("uploadPdf", {
          name: "",
        });
        setPdf("");
      } else {
        var listType = [
          "application/pdf",
          "application/msword",
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setPdf(event.target.files[0]);
          onChangeFieldSetFlagTab();
        } else {
          alert("Incorrect Type File (Support pdf , doc and docx only )");
          setValue("uploadPdf", "");
          setPdf("");
        }
      }
    }
  };
  const onChangeDisplayPrice = async () => {
    setDisplayPrice(!displayPrice);
    if (!displayPrice == false) {
      setShowDisplayPrice(true);
      setDisplaySpecialPrice(false);
    } else {
      const objEdit = {
        display_price: "true",
        status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        id: parseInt(id),
      };
      const item = await callApi.apiPut("property-detail/update", objEdit);
    }
  };
  useEffect(() => {
    setField();
    setData();
    setDropDown();
    setFieldCheckBox();
  }, []);

  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }

  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv
          style={{
            background: statusField ? "#e8e8e8" : "white",
            marginTop: "0.5em",
          }}
        >
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";

  return (
    <form onSubmit={handleSubmit(onSubmit)} id="assetform">
      <div
        className="container pb-5"
        style={{
          backgroundColor: "white",
          minHeight: "450px",
        }}
      >
        <div>
          {" "}
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                สถานะทรัพย์{" "}
                {slideStatusProperty ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivStatusProperty}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivStatusProperty}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-divstatusproperty" style={{ width: "100%" }}>
            <div className="row mt-3">
              {" "}
              <div className="col-6 ">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  สถานะทรัพย์สิน &nbsp;
                </TextSemiBold>
                <Controller
                  name="asset_state"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <Select
                        placeholder="โปรดเลือก"
                        isDisabled={true}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange({ value: event.value, label: event.label });
                        }}
                        styles={customStyles}
                        instanceId="type"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        options={dropdownPropertyStatus}
                      />
                    );
                  }}
                />
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  สถานะรอการตรวจสอบ &nbsp;
                </TextSemiBold>

                <Controller
                  name="asset_state_pending"
                  control={control}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusAsset
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                        disabled={true}
                      />
                    );
                  }}
                />
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  กลุ่มทรัพย์สิน &nbsp;
                </TextSemiBold>
                {assetGroup.map((item: any, index) => (
                  <div className="row" key={item.type}>
                    <div className="col-12">
                      <div className="row">
                        <div className="col-1 mt-3">
                          <Controller
                            control={control}
                            name={"checkbox" + item.type}
                            render={({
                              field: { onChange, onBlur, value, name, ref },
                              fieldState: {
                                invalid,
                                isTouched,
                                isDirty,
                                error,
                              },
                              formState,
                            }) => (
                              <label className="labelCheckbox">
                                <input
                                  type="checkbox"
                                  onBlur={onBlur} // notify when input is touched
                                  // onChange={onChange} // send value to hook form
                                  onChange={(event: any) => {
                                    onChangeFieldSetFlagTab();
                                    onChange(event.target.checked);
                                  }}
                                  checked={value}
                                  style={{ width: "20px", height: "20px" }}
                                  disabled={statusField}
                                />
                                <span className="spanCheckbox"></span>
                              </label>
                            )}
                          />
                        </div>
                        <div className="col-5 mt-3">
                          <TextSemiBold
                            className="mt-3"
                            fontSize="16px"
                            mobileSize="16px"
                            weight="300"
                            color={Colors.mainBlackLight}
                          >
                            {item.type}
                          </TextSemiBold>
                        </div>
                        {item.show_des ? (
                          <div className="col-6">
                            <div className="row">
                              <div
                                className="col-3 mt-3"
                                style={{ paddingRight: "0px" }}
                              >
                                <TextSemiBold
                                  fontSize="16px"
                                  mobileSize="16px"
                                  weight="300"
                                  color={Colors.mainBlackLight}
                                >
                                  URL
                                </TextSemiBold>
                              </div>
                              <div
                                className="col-9"
                                style={{ paddingLeft: "0px" }}
                              >
                                <Controller
                                  name={item.type}
                                  control={control}
                                  rules={{ required: false }}
                                  render={({ field: { onChange, value } }) => {
                                    return (
                                      <InputRadiusAsset
                                        width="100%"
                                        borderWidth="1px"
                                        borderColor="#CED4DA"
                                        border="solid"
                                        value={value}
                                        onChange={(event: any) => {
                                          onChangeFieldSetFlagTab();
                                          onChange(event.target.value);
                                        }}
                                        disabled={statusField}
                                      />
                                    );
                                  }}
                                />
                              </div>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ทรัพย์พิเศษ &nbsp;
                </TextSemiBold>
                {mockPropertySp.map((item: any, index) =>
                  index % 2 == 0 ? (
                    <div className="row" key={index}>
                      <div className="col-12">
                        <div className="row">
                          <div className="col-1 mt-3">
                            <Controller
                              control={control}
                              name={"checkbox" + item.type}
                              render={({
                                field: { onChange, onBlur, value, name, ref },
                                fieldState: {
                                  invalid,
                                  isTouched,
                                  isDirty,
                                  error,
                                },
                                formState,
                              }) => (
                                <label className="labelCheckbox">
                                  <input
                                    type="checkbox"
                                    onBlur={onBlur} // notify when input is touched
                                    onChange={(event: any) => {
                                      onChangeFieldSetFlagTab();
                                      onChange(event.target.checked);
                                    }}
                                    checked={value}
                                    style={{ width: "20px", height: "20px" }}
                                    disabled={statusField}
                                  />
                                  <span className="spanCheckbox"></span>
                                </label>
                              )}
                            />
                          </div>
                          <div className="col-5 mt-3">
                            <TextSemiBold
                              className="mt-3"
                              fontSize="16px"
                              mobileSize="16px"
                              weight="300"
                              color={Colors.mainBlackLight}
                            >
                              {item.type}
                            </TextSemiBold>
                          </div>
                          {item.show_des ? (
                            <div className="col-6">
                              <Controller
                                name={item.type}
                                control={control}
                                rules={{ required: false }}
                                render={({ field: { onChange, value } }) => {
                                  return (
                                    <InputRadiusAsset
                                      width="100%"
                                      borderWidth="1px"
                                      borderColor="#CED4DA"
                                      border="solid"
                                      value={value}
                                      onChange={(event: any) => {
                                        onChangeFieldSetFlagTab();
                                        onChange(event.target.value);
                                      }}
                                      disabled={statusField}
                                    />
                                  );
                                }}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )
                )}
              </div>
            </div>
          </div>
        </div>

        <div>
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                การประมูล{" "}
                {slideAuction ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivAuction}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivAuction}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-divauction" style={{ width: "100%" }}>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  วันที่ยื่นซองประมูล
                </TextSemiBold>

                <DatePickerDiv>
                  <Controller
                    control={control}
                    name="date_auction"
                    rules={{ required: false }}
                    render={({
                      field: { onChange, name, value },
                      formState: { errors },
                    }) => (
                      <>
                        <DatePicker
                          value={value || ""}
                          disabled={statusField}
                          onchange={(date: any) => {
                            onChange(date);
                            onChangeFieldSetFlagTab();
                          }}
                        />
                      </>
                    )}
                  />
                </DatePickerDiv>
              </div>
              <div className="col-6">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  เอกสารประกอบการประมูล
                </TextSemiBold>
                <RadiusBox>
                  <Flex widthMobile="100%" className="justify-content-between">
                    <Flex className="ms-3 mt-1">{watchPDF?.name || ""}</Flex>
                    <ButtonRightSideRadius
                      onClick={() => inputPdf.current.click()}
                      backgroundColor={Colors.mainBlue}
                      title="เลือกไฟล์"
                      width="20%"
                      disable={statusField}
                      height="100%"
                      color={Colors.mainWhite}
                    />

                    <Controller
                      name="uploadPdf"
                      control={control}
                      rules={{ required: false }}
                      render={({ field: { onChange } }) => (
                        <input
                          onChange={(e: any) => {
                            onChange(e.target.files[0]);
                            handlePdf(e);
                          }}
                          type="file"
                          disabled={statusField}
                          accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                          style={{ display: "none" }}
                          id="uploadImgPdf"
                          ref={inputPdf}
                        />
                      )}
                    />
                  </Flex>
                </RadiusBox>
                <TextSemiBold
                  weight="300"
                  fontSize="16px"
                  className="mt-2"
                  color={Colors.gray9b}
                >
                  เฉพาะไฟล์ .doc , .pdf , .docx เท่านั้น ขนาดไฟล์ไม่เกิน 10 MB
                </TextSemiBold>
              </div>
            </div>
          </div>
        </div>

        <div>
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                ราคาทรัพย์สิน{" "}
                {slidePrice ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivPrice}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivPrice}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-divprice" style={{ width: "100%" }}>
            <div className="row mt-3 mb-4">
              <Flex className="flex-column">
                <Flex className="flex-row mt-2">
                  <TextBold
                    fontSize="16px"
                    weight="400"
                    color={Colors.mainBlackLight}
                    style={{ paddingRight: "2em" }}
                  >
                    แสดงราคาตั้งขายในหน้าเว็บไซต์
                  </TextBold>
                  <Controller
                    name="display_price"
                    control={control}
                    rules={{ required: false }}
                    render={({ field }) => (
                      <Switch
                        // onChange={() => setDisplayPrice(!displayPrice)}
                        onChange={() => {
                          onChangeDisplayPrice();
                          onChangeFieldSetFlagTab();
                        }}
                        checked={displayPrice}
                        checkedIcon={false}
                        uncheckedIcon={false}
                        disabled={statusField}
                        onColor={Colors.green}
                      />
                    )}
                  />

                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                    className="ms-3"
                  >
                    แสดง
                  </TextSemiBold>
                </Flex>
              </Flex>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ต้นทุนทรัพย์สิน
                </TextSemiBold>
                <Controller
                  name="cost_asset_amt"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusWithRSpan
                        value={value}
                        title="บาท"
                        disabled={true}
                      ></InputRadiusWithRSpan>
                    );
                  }}
                />
              </div>
              <div className="col-6">
                <div style={{ marginBottom: "5px" }}>
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ทรัพย์ติดดาว
                  </TextSemiBold>
                </div>
                <Controller
                  name="property_star"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <Select
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange({ value: event.value, label: event.label });
                        }}
                        placeholder="โปรดเลือก"
                        isDisabled={statusField}
                        styles={customStyles}
                        instanceId="type"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        options={dropdownStar}
                      />
                    );
                  }}
                />
              </div>
              {/* <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ค่าใช้จ่ายในการดูแล
                </TextSemiBold>

                <Controller
                  name="maintenance_price"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusWithRSpan
                        title="บาท"
                        {...register("maintenance_price", {
                          pattern: /(^[0-9]+(.[0-9]{1,2})?$)|(^\s*$)/gm,
                        })}
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                        disabled={statusField}
                      ></InputRadiusWithRSpan>
                    );
                  }}
                />
                {errors.maintenance_price && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div> */}
            </div>

            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ราคาประเมิน
                </TextSemiBold>
                <Controller
                  name="evaluate_amt"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusWithRSpan
                        value={value}
                        title="บาท"
                        disabled={true}
                      ></InputRadiusWithRSpan>
                    );
                  }}
                />
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  วันที่ประเมิน
                </TextSemiBold>{" "}
                <DatePickerDiv>
                  <Controller
                    control={control}
                    name="evaluate_date"
                    rules={{ required: false }}
                    render={({
                      field: { onChange, name, value },
                      formState: { errors },
                    }) => (
                      <>
                        <DatePicker
                          value={value || ""}
                          disabled={true}
                          onchange={(date: any) => {
                            onChange(date);
                            onChangeFieldSetFlagTab();
                          }}
                        />

                        {errors &&
                          errors[name] &&
                          errors[name].type === "required" && (
                            <TextSemiBold
                              fontSize="14px"
                              color={Colors.mainRed}
                            >
                              กรุณากรอกข้อมูลให้ถูกต้อง
                            </TextSemiBold>
                          )}
                      </>
                    )}
                  />
                </DatePickerDiv>
              </div>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ราคาตั้งขาย
                </TextSemiBold>
                <Controller
                  name="center_price"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusWithRSpan
                        value={value}
                        title="บาท"
                        disabled={true}
                      ></InputRadiusWithRSpan>
                    );
                  }}
                />
              </div>
            </div>
          </div>
        </div>

        <div>
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                ราคาพิเศษ{" "}
                {slideSpeicalPrice ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivSpecialPrice}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDivSpecialPrice}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-divspecialprice" style={{ width: "100%" }}>
            <div className="row mt-3 mb-4">
              <Flex className="flex-column">
                <Flex className="flex-row mt-2">
                  <TextBold
                    fontSize="16px"
                    weight="400"
                    color={Colors.mainBlackLight}
                    style={{ paddingRight: "2em" }}
                  >
                    แสดงราคาพิเศษ
                  </TextBold>
                  <Controller
                    name="display_special_price"
                    control={control}
                    render={({ field }) => (
                      <Switch
                        {...field}
                        disabled={statusField}
                        checked={displaySpecialPrice}
                        checkedIcon={false}
                        onChange={() => {
                          setDisplaySpecialPrice(!displaySpecialPrice);
                          onChangeFieldSetFlagTab();
                        }}
                        uncheckedIcon={false}
                        onColor={Colors.green}
                      />
                    )}
                  />

                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                    className="ms-3"
                  >
                    แสดง
                  </TextSemiBold>
                </Flex>
              </Flex>
            </div>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ราคาพิเศษ
                </TextSemiBold>

                <Controller
                  name="sale_price_spc_amt"
                  control={control}
                  rules={{ required: false }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <InputRadiusWithRSpan
                        title="บาท"
                        value={value}
                        onChange={(event: any) => {
                          onChangeFieldSetFlagTab();
                          onChange(event.target.value);
                        }}
                        disabled={true}
                      ></InputRadiusWithRSpan>
                    );
                  }}
                />
                {errors.sale_price_spc_amt && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  วันที่เริ่มต้น
                </TextSemiBold>
                <DatePickerDiv>
                  <Controller
                    control={control}
                    name="sale_price_spc_from_date"
                    render={({
                      field: { onChange, name, value },
                      formState: { errors },
                    }) => (
                      <>
                        <DatePicker
                          value={value || ""}
                          disabled={statusField}
                          onchange={(date: any) => {
                            onChange(date);
                            onChangeFieldSetFlagTab();
                          }}
                        />
                      </>
                    )}
                  />
                  {errors.sale_price_spc_from_date && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </DatePickerDiv>
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  วันที่สิ้นสุด
                </TextSemiBold>
                <DatePickerDiv>
                  <Controller
                    control={control}
                    name="sale_price_spc_to_date"
                    render={({
                      field: { onChange, name, value },
                      formState: { errors },
                    }) => (
                      <>
                        <DatePicker
                          value={value || ""}
                          disabled={statusField}
                          onchange={(date: any) => {
                            onChange(date);
                            onChangeFieldSetFlagTab();
                          }}
                          {...register("sale_price_spc_to_date", {})}
                        />
                      </>
                    )}
                  />
                  {errors.sale_price_spc_to_date && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </DatePickerDiv>
              </div>
            </div>
          </div>
        </div>
        {checkSubmit("buttonSubmitAsset") && !statusField && (
          <Flex className="justify-content-center mt-5">
            <ButtonRadius
              type="submit"
              title="บันทึกข้อมูล"
              form="assetform"
              iconPath="/new/images/Vector.png"
              background={Colors.mainYellow}
              color={Colors.mainDarkBlue}
              width="200px"
              fontSize="18px"
              className="mx-1 py-2"
              border="none"
            />
          </Flex>
        )}
        <ModalPriceDisplay
          show={showDisplayPrice}
          setShow={setShowDisplayPrice}
          display_price={displayPrice}
          setDisplayPrice={setDisplayPrice}
          id={id}
        />
      </div>
      <ModalLoading visible={visible} />
    </form>
  );
};

export default AssetStatus;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
export async function getServerSideProps({ query }: { query: any }) {
  return { props: { mode: query.mode || null, title: query.title || null } };
}
