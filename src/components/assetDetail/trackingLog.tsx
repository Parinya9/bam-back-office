import TextAreaRadiusInput from "../../components/textAreaInput";
import { TextSemiBold } from "../text";
import { Flex } from "../../components/layout";
import Colors from "../../../utils/Colors";
import { Controller, useForm } from "react-hook-form";
import { FunctionComponent, useEffect, useState } from "react";
import callApi from "../../pages/api/callApi";
import validateFieldAsset from "../../../utils/ValidateFieldAsset";
import { apiSyncAssetDetail } from "../../pages/api/elsSync";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import Cookies from "universal-cookie";
import ModalLoading from "../../components/Modal/ModalLoading";

interface TrackingLog {
  mode: string;
  id: string;
  statusField: any;
  checkSubmit: any;
  flagTab: any;
  setFlagTab: any;
  display_property: any;
}
const TrackingLog: FunctionComponent<TrackingLog> = ({
  id,
  mode,
  statusField,
  checkSubmit,
  flagTab,
  setFlagTab,
  display_property,
}) => {
  const {
    control,
    formState: { errors },
    setValue,
    handleSubmit,
  } = useForm();
  const [visible, setVisible] = useState(false);
  const [fieldDisabled, setFieldDisabled] = useState(false);
  const [assetItem, setAssetItem] = useState([]);
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const onSubmit = (data: any) => {
    editData(data);
  };

  const editData = async (data: any) => {
    let objEdit = {};
    setVisible(true);
    if (validateFieldAsset.checkFieldRequire(assetItem)) {
      objEdit = {
        tracking_log_detail: data.tracking_log_detail,
        id: id,
        display_property: display_property ? "true" : "false",
      };
    } else {
      let objLog = {
        asset_id: parseInt(id),
        status_approve: true,
        user: firstname + " " + lastname,
        position: role,
        title: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        note: "",
      };
      objEdit = {
        tracking_log_detail: data.tracking_log_detail,
        status_approve: "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์",
        id: id,
        display_property: display_property ? "true" : "false",
      };
      await callApi.apiPost("history-log/addLog", objLog);
    }

    setFlagTab(false);
    const item = await callApi.apiPut("property-detail/update", objEdit);
    const response = await apiSyncAssetDetail("api/asset-detail");

    if (item.affected == 1) {
      if (response.status === "success") {
        setVisible(false);

        alert("Update Success");
      } else {
        setVisible(false);

        alert("Cant'not update asset");
      }
    } else {
      setVisible(false);
    }
  };
  const setData = async () => {
    const conditionSearch = { id: parseInt(id) };
    const response = await callApi.apiPost(
      "property-detail/findKey",
      conditionSearch
    );
    if (response.length > 0) {
      setAssetItem(response[0]);
      setValue("tracking_log_detail", response[0].tracking_log_detail);
    }
  };
  const setField = () => {
    mode == "readonly" ? setFieldDisabled(true) : setFieldDisabled(false);
  };
  const onChangeFieldSetFlagTab = () => {
    if (!flagTab) {
      setFlagTab(true);
    }
  };
  useEffect(() => {
    setField();
    setData();
  }, []);
  return (
    <form onSubmit={handleSubmit(onSubmit)} id="assetform">
      <Flex className="flex-column mt-5">
        <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
          เพิ่มรายละเอียด
        </TextSemiBold>

        <Controller
          name="tracking_log_detail"
          control={control}
          rules={{ required: false }}
          render={({ field: { onChange, value } }) => {
            return (
              <TextAreaRadiusInput
                borderColor={"rgba(206, 212, 218, 1)"}
                backgroundColor={Colors.lightGrayF6}
                disabled={fieldDisabled}
                value={value}
                onChange={(event: any) => {
                  onChangeFieldSetFlagTab();
                  onChange(event.target.value);
                }}
              />
            );
          }}
        />
      </Flex>
      {checkSubmit("buttonSubmitAsset") && !statusField && (
        <Flex className="justify-content-center mt-5">
          <ButtonRadius
            type="submit"
            title="บันทึกข้อมูล"
            form="assetform"
            iconPath="/new/images/Vector.png"
            background={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 py-2"
            border="none"
          />
        </Flex>
      )}
      <ModalLoading visible={visible} />
    </form>
  );
};

export default TrackingLog;
