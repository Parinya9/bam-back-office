import { TextBold, TextSemiBold } from "../text";
import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import { Flex } from "../layout";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../Button/ButtonRadius";
import ButtonRadiusIconL from "../Button/ButtonRadiusIconL";
import { useForm, Controller } from "react-hook-form";
import InputSearch from "../Layout/InputSearch";
import styled from "styled-components";
import "react-datepicker/dist/react-datepicker.css";
import FilterProperty from "../Filter/FilterProperty";
import FilterInterestPropetry from "../Filter/FilterInterestProperty";

interface SearchWithNoFilter {
  typeProperty: string;
  isFilter?: boolean;
  onClickSearch: Function;
  onClickAddBtn?: () => any;
}

const SearchWithNoFilter: FunctionComponent<SearchWithNoFilter> = ({
  onClickAddBtn,
  onClickSearch,
  typeProperty,
  isFilter = false,
}) => {
  const {
    control,
    getValues,
    formState: { errors },
  } = useForm();

  const styleIcon = { marginLeft: "10px", marginRight: "10px" };
  const [type, setType] = useState("");
  const [show, setShow] = useState(false);
  const dropdown: any = useRef(null);

  function handleClick(event: any) {
    if (dropdown.current && !dropdown.current.contains(event.target)) {
      setShow(true);
    } else {
      setShow(false);
    }
  }
  const handleSearch = () => {
    onClickSearch(
      getValues("searchField") === undefined ? "" : getValues("searchField")
    );
  };
  const openModalFilter = () => {
    setShow(true);
  };
  useEffect(() => {
    setType(typeProperty);
    if (show) {
      window.addEventListener("click", handleClick);
      return () => window.removeEventListener("click", handleClick);
    }
  }, [show]);

  return (
    <div
      style={{
        backgroundColor: "white",
        borderRadius: "15px",
        marginBottom: "2em",
      }}
    >
      <div style={{ paddingTop: "2em" }}>
        <TextBold fontSize="24px" weight="400" style={{ paddingLeft: "1em" }}>
          <img
            src="/new/images/Search2.svg"
            width="20px"
            height="20px"
            style={{ margin: "10px" }}
          />
          ค้นหาข้อมูล...
        </TextBold>
      </div>

      <div
        className="row mt-3"
        style={{ paddingLeft: "1.5em", paddingRight: "1.5em" }}
      >
        <div className="col-lg-5">
          <RadiusBox>
            <Flex className="px-1 justify-content-between">
              <Controller
                name="searchField"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputSearch placeholder="ค้นหาที่นี่..." field={field} />
                )}
              />
            </Flex>
            <ButtonRadius
              backgroundColor={Colors.mainBlue}
              color={Colors.mainWhite}
              imgWidth="30px"
              imgHeight="20px"
              radius="30px"
              imgStyle={styleIcon}
              iconPath="/new/images/Search_white.svg"
              onClick={handleSearch}
            />
          </RadiusBox>
        </div>
        <div className="col-lg-1 mt-3">
          {isFilter && (
            <div
              style={{
                height: "38px",
                width: "38px",
                backgroundColor: "white",
                borderRadius: "50%",
                color: "#E2E4E8",
                border: "1px solid",
                marginLeft: "0.5em",
              }}
              onClick={openModalFilter}
            >
              <img
                src="/new/images/Filter.svg"
                width="26px"
                height="26px"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  paddingRight: "3px",
                }}
              />
            </div>
          )}
        </div>
        <div
          className="col-lg-6 mt-1"
          ref={dropdown}
          style={{ textAlign: "end", paddingRight: "0px" }}
        >
          <ButtonRadius
            onClick={onClickAddBtn}
            background={Colors.mainYellow}
            height="35px"
            width="200px"
            backgroundColor="white"
            title="เพิ่มข้อมูล"
            border="none"
            fontSize="17px"
            borderWidth="1px"
            className="px-3 ms-2 mx-1 mt-1 mt-3"
            color={Colors.mainDarkBlue}
          />
        </div>
      </div>
    </div>
  );
};

export default SearchWithNoFilter;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex container mt-3 flex-row ",
}))`
  background-color: #fff;
  width: 100%;
  height: 60%;
  border-radius: 60px;
  padding-right: 0px;
  box-shadow: rgba(0, 0, 0, 0.2) 0 0 2px;
`;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
