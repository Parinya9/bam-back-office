import { AnyAction } from "redux";
import { takeLatest } from "redux-saga/effects";
import { ActionType } from "./types";

function* callWatchSetInitial(action: AnyAction) {
  try {
    console.log("saga intial");
  } catch (error) {
    console.log(
      "DEBUG ~ file: saga.tsx ~ line 7 ~ function*callWatchSetInitial ~ error",
      error
    );
  }
}

export function* watchSetInitial() {
  yield takeLatest(ActionType.SET_INITIAL, callWatchSetInitial);
}

export default [watchSetInitial];
