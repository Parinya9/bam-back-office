export interface ModelPayload {
  value: string;
}

export interface ModelInitialState {
  initial: null;
}

export enum ActionType {
  SET_INITIAL = "INITIAL@SET_INITIAL",
}

export type Action = ActionType;

export interface ModelActionInterface {
  type: Action;
  payload: any;
}
