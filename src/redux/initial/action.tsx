import { ActionType } from "./types";

export const setInitial = (value: any) => ({
  type: ActionType.SET_INITIAL,
  payload: {
    value,
  },
});
