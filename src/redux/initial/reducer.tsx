import { AnyAction } from "redux";
import { ActionType, ModelInitialState } from "./types";

const initialState: ModelInitialState = {
  initial: null,
};

const reducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case ActionType.SET_INITIAL:
      return {
        ...state,
        initial: action.payload.value,
      };
    default:
      return state;
  }
};

export default reducer;
