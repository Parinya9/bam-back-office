import { ActionType } from "./types";

export const addImageAsset = (payload: any) => ({
  type: ActionType.ADD_ASSET_IMAGE,
  payload,
});

export const saveImageAsset = (payload: any) => ({
  type: ActionType.SAVE_ASSET_IMAGE,
  payload,
});

export const deleteImageAsset = (name: string) => ({
  type: ActionType.DELETE_ASSET_IMAGE,
  payload: {
    name,
  },
});
export const clearAssetImage = (payload: any) => ({
  type: ActionType.CLEAR_ASSET_IMAGE,
  payload,
});
export const saveEditAssetImage = (payload: any) => ({
  type: ActionType.SET_EDIT_ASSET_IMAGE,
  payload,
});
export const clearEditAssetImage = (payload: any) => ({
  type: ActionType.CLEAR_EDIT_ASSET_IMAGE,
  payload,
});
export const clear360Image = (payload: any) => ({
  type: ActionType.CLEAR_360_IMAGE,
  payload,
});
export const saveEdit360Image = (payload: any) => ({
  type: ActionType.SET_EDIT_360_IMAGE,
  payload,
});
export const clearMapImage = (payload: any) => ({
  type: ActionType.CLEAR_MAP_IMAGE,
  payload,
});
export const saveEditMapImage = (payload: any) => ({
  type: ActionType.SET_EDIT_MAP_IMAGE,
  payload,
});
export const addImageRenovate1 = (payload: any) => ({
  type: ActionType.ADD_IMAGE_RENOVATE1,
  payload,
});

export const clearImageRenovate1 = (payload: any) => ({
  type: ActionType.CLEAR_IMAGE_RENOVATE1,
  payload,
});
export const clearEditImageRenovate1 = (payload: any) => ({
  type: ActionType.CLEAR_EDIT_IMAGE_RENOVATE1,
  payload,
});
export const saveImageRenovate1 = (payload: any) => ({
  type: ActionType.SAVE_IMAGE_RENOVATE1,
  payload,
});
export const editImageRenovate1 = (payload: any) => ({
  type: ActionType.EDIT_IMAGE_RENOVATE1,
  payload,
});
export const saveEditImageRenovate1 = (payload: any) => ({
  type: ActionType.SET_EDIT_IMAGE_RENOVATE1,
  payload,
});

export const deleteImageRenovate1 = (name: string) => ({
  type: ActionType.DELETE_IMAGE_RENOVATE1,
  payload: {
    name,
  },
});

export const saveEditImageRenovate2 = (payload: any) => ({
  type: ActionType.SET_EDIT_IMAGE_RENOVATE2,
  payload,
});

export const editImageRenovate2 = (name: string) => ({
  type: ActionType.EDIT_IMAGE_RENOVATE2,
  payload: {
    name,
  },
});
export const clearEditImageRenovate2 = (payload: any) => ({
  type: ActionType.CLEAR_EDIT_IMAGE_RENOVATE2,
  payload,
});
export const clearImageRenovate2 = (payload: any) => ({
  type: ActionType.CLEAR_IMAGE_RENOVATE2,
  payload,
});

export const addImageRenovate2 = (payload: any) => ({
  type: ActionType.ADD_IMAGE_RENOVATE2,
  payload,
});

export const saveImageRenovate2 = (payload: any) => ({
  type: ActionType.SAVE_IMAGE_RENOVATE2,
  payload,
});

export const deleteImageRenovate2 = (name: string) => ({
  type: ActionType.DELETE_IMAGE_RENOVATE2,
  payload: {
    name,
  },
});

export const addImageRenovate3 = (payload: any) => ({
  type: ActionType.ADD_IMAGE_RENOVATE3,
  payload,
});
export const saveEditImageRenovate3 = (payload: any) => ({
  type: ActionType.SET_EDIT_IMAGE_RENOVATE3,
  payload,
});

export const editImageRenovate3 = (name: string) => ({
  type: ActionType.EDIT_IMAGE_RENOVATE3,
  payload: {
    name,
  },
});
export const clearEditImageRenovate3 = (payload: any) => ({
  type: ActionType.CLEAR_EDIT_IMAGE_RENOVATE3,
  payload,
});
export const clearImageRenovate3 = (payload: any) => ({
  type: ActionType.CLEAR_IMAGE_RENOVATE3,
  payload,
});
export const saveImageRenovate3 = (payload: any) => ({
  type: ActionType.SAVE_IMAGE_RENOVATE3,
  payload,
});
export const deleteImageRenovate3 = (name: string) => ({
  type: ActionType.DELETE_IMAGE_RENOVATE3,
  payload: {
    name,
  },
});
