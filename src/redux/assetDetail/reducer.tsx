import { AnyAction } from "redux";
import { ActionType } from "./types";

const initialState = {
  stateAssetImage: [],
  listImage: [],
  stateImageRenovate1: [],
  stateImageRenovate2: [],
  stateImageRenovate3: [],
  stateEditImageRenovate1: [],
  stateEditImageRenovate2: [],
  stateEditImageRenovate3: [],
  stateEditAssetImage: [],
  stateEditImg360: [],
  stateEditImgMap: [],
  listEditImageRenovate1: [],
  listEditImageRenovate2: [],
  listEditImageRenovate3: [],
  listImageRenovate1: [],
  listImageRenovate2: [],
  listImageRenovate3: [],
};

const reducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case ActionType.ADD_ASSET_IMAGE:
      return {
        ...state,
        stateAssetImage: [...state.stateAssetImage, ...action.payload],
      };
    case ActionType.SAVE_ASSET_IMAGE:
      return {
        ...state,
        listImage: [...state.listImage, ...action.payload],
      };
    case ActionType.CLEAR_ASSET_IMAGE:
      return {
        ...state,
        stateAssetImage: [...action.payload],
      };
    case ActionType.DELETE_ASSET_IMAGE:
      return {
        ...state,
        stateAssetImage: state.stateAssetImage.filter(
          (item: any) => item.name !== action.payload.name
        ),
      };
    case ActionType.SET_EDIT_ASSET_IMAGE:
      return {
        ...state,
        stateEditAssetImage: [...action.payload],
      };
    case ActionType.CLEAR_EDIT_ASSET_IMAGE:
      return {
        ...state,
        stateEditAssetImage: [...action.payload],
      };
    case ActionType.SET_EDIT_360_IMAGE:
      return {
        ...state,
        stateEditImg360: [...action.payload],
      };
    case ActionType.CLEAR_360_IMAGE:
      return {
        ...state,
        stateEditImg360: [...action.payload],
      };
    case ActionType.SET_EDIT_MAP_IMAGE:
      return {
        ...state,
        stateEditImgMap: [...action.payload],
      };
    case ActionType.CLEAR_MAP_IMAGE:
      return {
        ...state,
        stateEditImgMap: [...action.payload],
      };
    case ActionType.CLEAR_IMAGE_RENOVATE1:
      return {
        ...state,
        stateImageRenovate1: [...action.payload],
      };

    case ActionType.ADD_IMAGE_RENOVATE1:
      return {
        ...state,
        stateImageRenovate1: [...state.stateImageRenovate1, ...action.payload],
      };
    case ActionType.SAVE_IMAGE_RENOVATE1:
      return {
        ...state,
        listImageRenovate1: [...state.listImageRenovate1, ...action.payload],
      };
    case ActionType.DELETE_IMAGE_RENOVATE1:
      return {
        ...state,
        stateImageRenovate1: state.stateImageRenovate1.filter((item: any) => {
          item.name !== action.payload.name;
        }),
      };
    case ActionType.SET_EDIT_IMAGE_RENOVATE1:
      return {
        ...state,
        stateEditImageRenovate1: [...action.payload],
      };
    case ActionType.CLEAR_EDIT_IMAGE_RENOVATE1:
      return {
        ...state,
        stateEditImageRenovate1: [...action.payload],
      };
    case ActionType.ADD_IMAGE_RENOVATE2:
      return {
        ...state,
        stateImageRenovate2: [...state.stateImageRenovate2, ...action.payload],
      };
    case ActionType.SAVE_IMAGE_RENOVATE2:
      return {
        ...state,
        listImageRenovate2: [...state.listImageRenovate2, ...action.payload],
      };
    case ActionType.DELETE_IMAGE_RENOVATE2:
      return {
        ...state,
        stateImageRenovate2: state.stateImageRenovate2.filter(
          (item: any) => item.name !== action.payload.name
        ),
      };
    case ActionType.SET_EDIT_IMAGE_RENOVATE2:
      return {
        ...state,
        stateEditImageRenovate2: [...action.payload],
      };
    case ActionType.CLEAR_IMAGE_RENOVATE2:
      return {
        ...state,
        stateImageRenovate2: [...action.payload],
      };
    case ActionType.CLEAR_EDIT_IMAGE_RENOVATE2:
      return {
        ...state,
        stateEditImageRenovate2: [...action.payload],
      };
    case ActionType.SET_EDIT_IMAGE_RENOVATE3:
      return {
        ...state,
        stateEditImageRenovate3: [...action.payload],
      };
    case ActionType.CLEAR_EDIT_IMAGE_RENOVATE3:
      return {
        ...state,
        stateEditImageRenovate3: [...action.payload],
      };
    case ActionType.ADD_IMAGE_RENOVATE3:
      return {
        ...state,
        stateImageRenovate3: [...state.stateImageRenovate3, ...action.payload],
      };
    case ActionType.SAVE_IMAGE_RENOVATE3:
      return {
        ...state,
        listImageRenovate3: [...state.listImageRenovate3, ...action.payload],
      };
    case ActionType.DELETE_IMAGE_RENOVATE3:
      return {
        ...state,
        stateImageRenovate3: state.stateImageRenovate3.filter(
          (item: any) => item.name !== action.payload.name
        ),
      };
    case ActionType.CLEAR_IMAGE_RENOVATE3:
      return {
        ...state,
        stateImageRenovate3: [...action.payload],
      };

    default:
      return state;
  }
};

export default reducer;
