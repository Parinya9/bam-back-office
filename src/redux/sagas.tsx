import { all, fork } from "redux-saga/effects";
import initialSaga from "./initial/saga";

export default function* rootSaga() {
  yield all([...initialSaga].map(fork));
}
