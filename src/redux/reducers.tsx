import { combineReducers } from "redux";
import initialReducer from "./initial/reducer";
import assetDetailReducer from "./assetDetail/reducer";

const appReducer = combineReducers({
  initial: initialReducer,
  assetDetail: assetDetailReducer,
});

const rootReducer = (state: any, action: any) => {
  return appReducer(state, action);
};

export default rootReducer;
