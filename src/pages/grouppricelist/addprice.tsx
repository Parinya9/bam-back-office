import React, { FunctionComponent, useEffect, useState } from "react";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import Router from "next/router";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import { toggle } from "slidetoggle";
import ButtonRadius from "../../components/Button/ButtonRadius";
import callApi from "../api/callApi";
import Auth from "../auth/index";

interface AddPriceList {
  id: string;
  price_start: string;
  price_end: string;
  mode: string;
}

const AddPriceList: FunctionComponent<AddPriceList> = ({
  id,
  price_start,
  price_end,
  mode,
}) => {
  const {
    control,
    handleSubmit,
    setValue,
    watch,
    register,
    formState: { errors },
  } = useForm();
  const [slide, setSlide] = useState(true);
  const [title, setTitle] = useState("เพิ่มข้อมูล - ระดับราคา");
  const [wrapText, setWrapText] = useState("เพิ่มข้อมูลระดับราคา");
  const returnPage = () => {
    Router.push("/grouppricelist");
  };
  const onSubmit = (data: any) => {
    mode == "Edit" ? editData(data) : addData(data);
  };
  const editData = async (data: any) => {
    const objEdit = {
      price_start: parseInt(data.price_start),
      price_end: parseInt(data.price_end),
      id: parseInt(id),
    };
    const item = await callApi.apiPut("price-List/update", objEdit);
    if (item.affected == 1) {
      returnPage();
    } else {
      alert("Duplicate Item");
    }
  };
  const addData = async (data: any) => {
    const item = await callApi.apiPost("price-List/add", data);
    if (item.id != undefined) {
      returnPage();
    } else {
      alert("Duplicate Item");
    }
  };
  const setData = () => {
    setValue("price_start", price_start);
    setValue("price_end", price_end);
    setTitle("แก้ไขข้อมูล - ระดับราคา");
    setWrapText("แก้ไขข้อมูล");
  };
  const toggleDiv = () => {
    setSlide(!slide);
    toggle("div.toggle-div", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    mode == "Edit" ? setData() : "";
  }, []);
  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลระดับราคา
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            {wrapText}
          </TextBold>
        </>
      }
      title={title}
      titleIcon="/new/images/Back.svg"
      onClick={returnPage}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <div
          className="container"
          style={{
            backgroundColor: "white",
            height: "450px",
            overflowY: "auto",
          }}
        >
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                ระดับราคา{" "}
                {slide ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDiv}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDiv}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-div" style={{ width: "100%" }}>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ระดับราคาตั้งขาย &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="price_start"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("price_start", {
                        pattern: /^[0-9]+$/,
                      })}
                    />
                  )}
                />
                {errors.price_start && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ระดับราคาสิ้นสุด &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="price_end"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("price_end", {
                        pattern: /^[0-9]+$/,
                        validate: (value) =>
                          parseInt(value) >= parseInt(watch("price_start")),
                      })}
                    />
                  )}
                />
                {errors.price_end && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
          </div>
        </div>
        <Flex
          style={{ height: "180px" }}
          className="flex-row justify-content-center align-items-center"
        >
          <ButtonRadius
            type="button"
            title="ยกเลิก"
            onClick={returnPage}
            backgroundColor="transparent"
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            borderColor={Colors.mainDarkBlue}
            borderWidth="1px"
            border="solid"
          />
          <ButtonRadius
            type="submit"
            title="บันทึก"
            backgroundColor={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            border="none"
          />
        </Flex>
      </form>
    </Wrapper>
  );
};

export default AddPriceList;

export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      id: query.id || null,
      mode: query.mode || "add",
      price_start: query.price_start || null,
      price_end: query.price_end || null,
    },
  };
}
