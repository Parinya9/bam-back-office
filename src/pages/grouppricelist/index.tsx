import type { NextPage } from "next";
import Router from "next/router";
import { TextBold, TextSemiBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../../components/Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Layout/InputRadius";
import Wrapper from "../../components/Layout/Wrapper";
import { Flex } from "../../components/layout";
import SearchProperty from "../../components/Search/SearchProperty";
import HeaderTable from "../../components/Table/HeaderTable";
import TablePriceList from "../../components/Table/TablePriceList";
import callApi from "../api/callApi";
import Auth from "../auth/index";

const GroupPriceList: NextPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState({});
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const [conditionExport, setConditionExport] = useState([] as any);
  const fetchData = async (start: any, limit: any) => {
    let item = search;
    Object.assign(item, {
      start: start,
      limit: limit,
    });
    const response = await callApi.apiPost("price-List/find", item);
    setData(response.item);
    setTotal(response.total);
  };
  const setDataForExport = () => {
    var listDataExport = [];
    for (var i = 0; i < data.length; ++i) {
      var objData = {
        ระดับราคาตั้งขาย: data[i]["price_start"],
        ระดับราคาสิ้นสุด: data[i]["price_end"],
      };
      listDataExport.push(objData);
    }
    return listDataExport;
  };
  const actionSearch = async (data: string) => {
    const conditionSearch = { price_start: data, price_end: data };
    Object.assign(conditionSearch, { start: 0, limit: 10 });
    setSearch(conditionSearch);
    setConditionExport(conditionSearch);
    const response = await callApi.apiPost("price-List/find", conditionSearch);
    setData(response.item);
    setTotal(response.total);
  };
  const exportExcel = async () => {
    const response = await callApi.apiExport(
      "price-list/export",
      conditionExport
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "PriceList" + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };
  const deleteData = async (id: number) => {
    if (confirm("ยืนยันการลบข้อมูล")) {
      await callApi.apiDelete("price-List/delete/" + id);
      fetchData((currentPage - 1) * 10, 10);
    }
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);
  return (
    <Wrapper
      isLogout={true}
      stepIcon={
        <>
          <img src="new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลระดับราคา
          </TextBold>
        </>
      }
      title="ข้อมูลระดับราคา"
    >
      <SearchProperty
        onClickAddBtn={() => Router.push("/grouppricelist/addprice")}
        onClickSearch={actionSearch}
        onClickExport={exportExcel}
        onClickSearchFilter={() => {}}
      />
      <HeaderTable number={total.toString()} />

      <TablePriceList
        data={data}
        onClickDelete={deleteData}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        fetchData={fetchData}
        total={total}
        setTotal={setTotal}
      />
    </Wrapper>
  );
};

export default GroupPriceList;
