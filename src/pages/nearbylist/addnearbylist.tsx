import React, { FunctionComponent, useEffect, useState } from "react";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import Router from "next/router";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import ButtonRadius from "../../components/Button/ButtonRadius";
import Switch from "react-switch";
import { toggle } from "slidetoggle";
import callApi from "../api/callApi";
import { apiSyncAssetDetail } from "../api/elsSync";
import Auth from "../auth/index";
import ModalLoading from "../../components/Modal/ModalLoading";

interface AddNearbyList {
  id: string;
  title_th: string;
  title_en: string;
  mode: string;
  show_display: boolean;
}

const AddNearbyList: FunctionComponent<AddNearbyList> = ({
  id,
  title_th,
  title_en,
  mode,
  show_display,
}) => {
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
    watch,
    register,
  } = useForm();
  const [visible, setVisible] = useState(false);
  const [approve, setApprove] = useState(false);
  const [slide, setSlide] = useState(true);
  const [title, setTitle] = useState("เพิ่มข้อมูล - สถานที่ใกล้เคียง");
  const [oldNearby, setOldNearby] = useState("");
  const [wrapText, setWrapText] = useState("เพิ่มข้อมูลสถานที่ใกล้เคียง");
  const returnPage = () => {
    Router.back();
  };
  const onSubmit = (data: any) => {
    mode == "Edit" ? editData(data) : addData(data);
  };
  const addData = async (data: any) => {
    data.show_display = approve;

    const item = await callApi.apiPost("nearby-list/add", data);
    if (item.id != undefined) {
      setVisible(true);

      setTimeout(async () => {
        const resNearby = await apiSyncAssetDetail(
          "api/mastersearchasset/location"
        );
        const response = await apiSyncAssetDetail("api/asset-detail");

        if (response.status === "success" && resNearby.status === "success") {
          setVisible(false);
          alert("Update Success");

          returnPage();
        } else {
          setVisible(false);

          alert("Cant'not update asset");
        }
      }, 2000);
    } else {
      alert("Duplicate Title");
    }
  };
  const editData = async (data: any) => {
    data.show_display = approve;
    const objEdit = {
      title_th: data.title_th,
      title_en: data.title_en,
      old_nearby: oldNearby,
      id: parseInt(id),
      show_display: Boolean(data.show_display),
    };
    const item = await callApi.apiPut("nearby-list/update", objEdit);
    if (item.affected == 1) {
      setTimeout(async () => {
        const resNearby = await apiSyncAssetDetail(
          "api/mastersearchasset/location"
        );
        const response = await apiSyncAssetDetail("api/asset-detail");

        if (response.status === "success" && resNearby.status === "success") {
          alert("Update Success");
        } else {
          alert("Cant'not update asset");
        }
      }, 2000);

      // returnPage();
    } else {
      alert("Duplicate Title");
    }
  };
  const toggleDiv = () => {
    setSlide(!slide);
    toggle("div.toggle-div", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const setData = () => {
    setValue("title_th", title_th);
    setValue("title_en", title_en);
    setApprove(!!parseInt(show_display.toString()));
    setTitle("แก้ไขข้อมูล - สถานที่ใกล้เคียง");
    setWrapText("แก้ไขข้อมูล");
    setOldNearby(title_th);
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    mode == "Edit" ? setData() : "";
  }, []);
  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลสถานที่ใกล้เคียง
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            {wrapText}
          </TextBold>
        </>
      }
      title={title}
      titleIcon="/new/images/Back.svg"
      onClick={returnPage}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <div
          className="container"
          style={{
            backgroundColor: "white",
            height: "450px",
            overflowY: "auto",
          }}
        >
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                สถานที่ใกล้เคียง{" "}
                {slide ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDiv}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDiv}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-div" style={{ width: "100%" }}>
            <div className="row mt-3 mb-4">
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="400"
                  color={Colors.mainBlackLight}
                >
                  หัวข้อ (TH) &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="title_th"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
                {errors.title_th && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="400"
                  color={Colors.mainBlackLight}
                >
                  หัวข้อ (EN) &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="title_en"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("title_en", {
                        pattern:
                          /^[a-zA-Z0-9!@#\$%\^\&*\)\(+._-\{\}\[\]\~\-\?\<\>\,\/\'\"\:\;\\\|\=/\s/ ]+$/g,
                      })}
                    />
                  )}
                />
                {errors.title_en && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
            <div className="row mt-3 mb-4">
              <TextSemiBold
                fontSize="16px"
                weight="400"
                color={Colors.mainBlackLight}
              >
                แสดงช่องสำหรับกรอกข้อมูลเพิ่มเติม &nbsp;
              </TextSemiBold>
            </div>
            <div className="row mt-3 mb-4">
              <Flex className="flex-column">
                <Flex className="flex-row mt-2">
                  <Controller
                    name="show_display"
                    control={control}
                    rules={{ required: false }}
                    render={({ field }) => (
                      <Switch
                        {...field}
                        onChange={() => {
                          setApprove(!approve);
                        }}
                        checked={approve}
                        checkedIcon={false}
                        uncheckedIcon={false}
                        onColor={Colors.mainBlue}
                      />
                    )}
                  />

                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                    className="ms-3"
                  >
                    แสดง
                  </TextSemiBold>
                </Flex>
              </Flex>
            </div>
          </div>
        </div>
        <Flex
          style={{ height: "180px" }}
          className="flex-row justify-content-center align-items-center"
        >
          <ButtonRadius
            type="button"
            title="ยกเลิก"
            backgroundColor="transparent"
            color={Colors.mainDarkBlue}
            onClick={returnPage}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            borderColor={Colors.mainDarkBlue}
            borderWidth="1px"
            border="solid"
          />
          <ButtonRadius
            type="submit"
            title="บันทึก"
            backgroundColor={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            border="none"
          />
        </Flex>
      </form>
      <ModalLoading visible={visible} />
    </Wrapper>
  );
};

export default AddNearbyList;
export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      id: query.id || null,
      mode: query.mode || "add",
      title_th: query.title_th || null,
      title_en: query.title_en || null,
      show_display: query.show_display || true,
    },
  };
}
