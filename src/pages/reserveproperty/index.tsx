import type { NextPage } from "next";
import { TextBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import SearchProperty from "../../components/Search/SearchProperty";
import TableProperty from "../../components/Table/TableProperty";
import Wrapper from "../../components/Layout/Wrapper";
import HeaderTable from "../../components/Table/HeaderTable";
import callApi from "../api/callApi";
import moment from "moment";
import Auth from "../auth/index";
import Router from "next/router";
import Cookies from "universal-cookie";

const ReserveProperty: NextPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState({});
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const [conditionExport, setConditionExport] = useState([] as any);
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const fetchData = async (start: any, limit: any) => {
    let item = search;
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(item, {
        ao_name: firstname + " " + lastname,
      });
    }
    Object.assign(item, { start: start, limit: limit });
    setSearch(item);
    setConditionExport(item);
    const response = await callApi.apiPost("reserve-asset/find", item);
    setTotal(response.total);
    setData(genRunningNo(response.item, start));
  };
  const genRunningNo = (data: any, start: any = 0) => {
    if (data.length > 0) {
      for (var i = 0; i < data.length; ++i) {
        data[i].no = start + i + 1;
      }

      return data;
    }
    return data;
  };
  const actionSearch = async (data: string) => {
    const conditionSearch = {
      fullname: "",
      reserve_no: data,
      start: 0,
      limit: 10,
    };
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(conditionSearch, {
        ao_name: firstname + " " + lastname,
      });
    }
    setSearch(conditionSearch);
    setConditionExport(conditionSearch);
    const response = await callApi.apiPost(
      "reserve-asset/find",
      conditionSearch
    );

    setTotal(response.total);
    setData(genRunningNo(response.item));
  };
  const filterSearch = async (data: any) => {
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(data, {
        ao_name: firstname + " " + lastname,
      });
    }
    Object.assign(data, {
      start: 0,
      limit: 10,
    });
    setSearch(data);
    setConditionExport(data);
    const response = await callApi.apiPost("reserve-asset/find", data);
    setTotal(response.total);
    setData(genRunningNo(response.item));
  };
  const setDataForExport = () => {
    var listDataExport = [];
    for (var i = 0; i < data.length; ++i) {
      var objData = {
        "รหัสการจอง ": data[i]["reserve_no"],
        "แพ็คเกจรีโนเวท ": data[i]["package_renovate"],
        "วันที่ทำการจอง ": moment(data[i]["reserve_date"]).format(
          "DD/MM/YYYY HH:mm"
        ),
        "รหัสทรัพย์สิน ": data[i]["asset_id"],
        "ชื่อ - นามสกุล ": data[i]["fullname"],
        "อีเมล ": data[i]["email"],
        "เบอร์โทรศัพท์ ": data[i]["phone"],
        "สถานะการชำระเงิน ": convertStatusPayment(data[i]["status_payment"]),
        "สถานะ	": data[i]["status_approve"],
      };
      listDataExport.push(objData);
    }
    return listDataExport;
  };
  const convertStatusPayment = (status: string) => {
    if (status === "pending") {
      return "รอชำระเงิน";
    } else if (status === "fail") {
      return "ไม่ชำระเงิน";
    } else if (status === "success") {
      return "ชำระเงินเรียบร้อย";
    }
  };
  const exportExcel = async () => {
    const response = await callApi.apiExport(
      "reserve-asset/export",
      conditionExport
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "ReserveAsset" + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);
  return (
    <>
      <Wrapper
        isLogout={true}
        stepIcon={
          <>
            <img src="new/images/Home2.svg" width="20px" height="20px" />
            <img
              src="new/images/Expand_right_double_light.svg"
              width="20px"
              height="20px"
              className="ms-2"
            />
            <TextBold
              fontSize="16px"
              color={Colors.mainGrayLight}
              className="ms-2"
            >
              ข้อมูลจองซื้อทรัพย์
            </TextBold>
          </>
        }
        title="ข้อมูลจองซื้อทรัพย์"
      >
        <SearchProperty
          isFilter={true}
          typeProperty="searchProperty"
          onClickSearch={actionSearch}
          onClickExport={exportExcel}
          onClickSearchFilter={filterSearch}
        />

        <HeaderTable number={total.toString()} />

        <TableProperty
          data={data}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          fetchData={fetchData}
          total={total}
          setTotal={setTotal}
        />
      </Wrapper>
    </>
  );
};

export default ReserveProperty;
