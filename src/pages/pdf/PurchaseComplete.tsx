import { FunctionComponent, useEffect, useState } from "react";
import { TextBold, TextSemiBold } from "../../components/text";
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  Font,
  Image,
} from "@react-pdf/renderer";
import { PDFViewer, PDFDownloadLink, BlobProvider } from "@react-pdf/renderer";
import ButtonRadius from "../../components/Button/ButtonRadiusIconL";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
const THBText = require("thai-baht-text");

interface PurchaseComplete {
  reserveno: any;
  assetNo: any;
  fullname: any;
  price: any;
  reserveDate: any;
  currentDate: any;
  adminName: any;
}
const PurchaseComplete: FunctionComponent<PurchaseComplete> = ({
  reserveno,
  fullname,
  price,
  reserveDate,
  currentDate,
  assetNo,
  adminName,
}) => {
  let defaultData = {
    id: "",
    reserve_no: "",
    package_renovate: "",
    department: "",
    asset_id: "",
    asset_type: "",
    fullname: "",
    phone: "",
    email: "",
    price: "",
    status_payment: "pending",
    status_approve: "",
    reserve_date: "",
    create_date: "",
    update_date: "",
  };

  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    setIsClient(true);
  }, []);

  Font.register({
    family: "Sarabun",
    src: "/new/fonts/Sarabun-Regular.ttf",
  });

  return (
    <div className="container">
      <div></div>
      <Flex className="flex-column">
        <Flex className="flex-row justify-content-center">
          {isClient && (
            <PDFDownloadLink
              document={
                <Document>
                  <Page size="A4" style={styles.page}>
                    <View
                      style={{
                        flexDirection: "row",
                      }}
                    >
                      <Image
                        src="/new/images/bam_logo.png"
                        style={{ width: "50px", height: "50px" }}
                      />
                      <View
                        style={{ flexDirection: "column", marginLeft: "5px" }}
                      >
                        <Text style={{ fontSize: "10px" }}>
                          บริษัทบริหารสินทรัพย์ กรุงเทพพาณิชย์ จำกัด (มหาชน))
                        </Text>
                        <View
                          style={{
                            marginTop: "5px",
                            backgroundColor: Colors.mainBlack,
                            width: "100%",
                            height: "3px",
                          }}
                        />
                        <Text style={{ fontSize: "10px", marginTop: "5px" }}>
                          Bangkok Commercial Asset Management Public Company
                          Limited
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: "column",
                      }}
                    >
                      <Text style={{ fontSize: "10px", marginTop: "10px" }}>
                        99 ถนน สุรศักดิ์ แขวง สีลม เขตบางรัก กรุงเทพมหานคร 10500
                      </Text>
                      <Text style={{ fontSize: "10px", marginTop: "10px" }}>
                        ทะเบียนเลขที่/เลขประจำตัวผู้เสียภาษีอากร 0107558000482
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        marginTop: "20px",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: "25px",
                          marginTop: "10px",
                          fontWeight: 30,
                        }}
                      >
                        หลักฐานการรับเงินจองซื้อออนไลน์
                      </Text>
                      <View
                        style={{ flexDirection: "column", marginLeft: "15px" }}
                      >
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                          }}
                        >
                          <Text
                            style={{
                              fontSize: "15px",
                              marginTop: "10px",
                              fontWeight: 30,
                            }}
                          >
                            เลขที่
                          </Text>
                          <Text
                            style={{
                              marginLeft: "10px",
                              fontSize: "15px",
                              marginTop: "10px",
                              fontWeight: 30,
                              borderBottom: "1px",
                              borderStyle: "dotted",
                              width: "120px",
                              color: Colors.mainDarkBlue,
                            }}
                          >
                            {/* {order?.reserve_no} */}
                            {reserveno}
                          </Text>
                        </View>

                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                          }}
                        >
                          <Text
                            style={{
                              fontSize: "15px",
                              marginTop: "10px",
                              fontWeight: 30,
                            }}
                          >
                            วันที่
                          </Text>
                          <Text
                            style={{
                              marginLeft: "10px",
                              fontSize: "15px",
                              marginTop: "10px",
                              fontWeight: 30,
                              borderBottom: "1px",
                              borderStyle: "dotted",
                              width: "120px",
                              color: Colors.mainDarkBlue,
                            }}
                          >
                            {reserveDate}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <View style={{ flexDirection: "column" }}>
                      <View style={{ flexDirection: "row" }}>
                        <Text
                          style={{
                            fontSize: "15px",
                            marginTop: "10px",
                            fontWeight: 30,
                          }}
                        >
                          ได้รับเงินจาก
                        </Text>
                        <Text
                          style={{
                            marginLeft: "20px",
                            fontSize: "15px",
                            marginTop: "10px",
                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "100%",
                            color: Colors.mainDarkBlue,
                          }}
                        >
                          คุณ {fullname}
                        </Text>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <Text
                          style={{
                            fontSize: "15px",
                            marginTop: "10px",
                            fontWeight: 30,
                          }}
                        >
                          เป็นค่า
                        </Text>
                        <Text
                          style={{
                            marginLeft: "20px",
                            fontSize: "15px",
                            marginTop: "10px",
                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "100%",
                            color: Colors.mainDarkBlue,
                          }}
                        >
                          จองซื้อทรัพย์ออนไลน์ รหัสทรัพย์ {assetNo}
                        </Text>
                      </View>
                    </View>
                    <View style={{ marginTop: "10px" }}>
                      <Text
                        style={{
                          fontSize: "15px",

                          fontWeight: 30,
                        }}
                      >
                        ชำระโดยย
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "20px",
                      }}
                    >
                      <View
                        style={{ flexDirection: "row", alignItems: "center" }}
                      >
                        <Image
                          src="/new/images/checkbox.png"
                          style={{ width: "15px", height: "15px" }}
                        />
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          เงินสด
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          marginLeft: "20px",
                        }}
                      >
                        <Image
                          src="/new/images/checkbox.png"
                          style={{ width: "15px", height: "15px" }}
                        />
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          โอนเงิน
                        </Text>
                      </View>
                      <View
                        style={{ flexDirection: "row", marginLeft: "20px" }}
                      >
                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          ธนาคาร
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",

                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "80px",
                            color: Colors.mainDarkBlue,
                          }}
                        ></Text>
                      </View>
                      <View
                        style={{ flexDirection: "row", marginLeft: "20px" }}
                      >
                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          สาขา
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",

                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "80px",
                            color: Colors.mainDarkBlue,
                          }}
                        ></Text>
                      </View>
                      <View
                        style={{ flexDirection: "row", marginLeft: "20px" }}
                      >
                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          ลงวันที่
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",

                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "80px",
                            color: Colors.mainDarkBlue,
                          }}
                        ></Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "20px",
                      }}
                    >
                      <View style={{ flexDirection: "row" }}>
                        <Image
                          src="/new/images/checkbox.png"
                          style={{ width: "15px", height: "15px" }}
                        />
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          เช็คธนาคาร
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",

                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "80px",
                            color: Colors.mainDarkBlue,
                          }}
                        ></Text>
                      </View>

                      <View
                        style={{ flexDirection: "row", marginLeft: "20px" }}
                      >
                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          สาขา
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",

                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "80px",
                            color: Colors.mainDarkBlue,
                          }}
                        ></Text>
                      </View>
                      <View
                        style={{ flexDirection: "row", marginLeft: "20px" }}
                      >
                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          เลขที่
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",

                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "80px",
                            color: Colors.mainDarkBlue,
                          }}
                        ></Text>
                      </View>
                      <View
                        style={{ flexDirection: "row", marginLeft: "20px" }}
                      >
                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          ลงวันที่
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",

                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "80px",
                            color: Colors.mainDarkBlue,
                          }}
                        ></Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "20px",
                      }}
                    >
                      <View
                        style={{ flexDirection: "row", alignItems: "center" }}
                      >
                        <Image
                          src="/new/images/checkbox-checked.png"
                          style={{ width: "15px", height: "15px" }}
                        />
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          อื่นๆ
                        </Text>
                        <Text
                          style={{
                            marginLeft: "10px",
                            fontSize: "10px",
                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "100%",
                            color: Colors.mainDarkBlue,
                          }}
                        >
                          ชำระเงินออนไลน์์
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "20px",
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          flex: 0.4,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: "15px",
                            fontWeight: 30,
                          }}
                        >
                          จำนวนเงินน
                        </Text>

                        <Text
                          style={{
                            textAlign: "center",
                            marginLeft: "10px",
                            fontSize: "15px",
                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "40%",
                            color: Colors.mainDarkBlue,
                          }}
                        >
                          {parseFloat(price).toLocaleString(undefined, {
                            maximumFractionDigits: 2,
                            minimumFractionDigits: 2,
                          })}
                        </Text>

                        <Text
                          style={{
                            fontSize: "15px",
                            fontWeight: 30,
                            marginLeft: "10px",
                          }}
                        >
                          {"  "}บาท
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          flex: 0.6,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: "15px",
                            fontWeight: 30,
                          }}
                        >
                          (
                        </Text>

                        <Text
                          style={{
                            textAlign: "center",
                            marginLeft: "10px",
                            fontSize: "15px",
                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "100%",
                            color: Colors.mainDarkBlue,
                          }}
                        >
                          {THBText(price)}
                        </Text>

                        <Text
                          style={{
                            fontSize: "15px",
                            fontWeight: 30,
                          }}
                        >
                          )
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "50px",
                        justifyContent: "flex-end",
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          flex: 0.4,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          ลงชื่อ
                        </Text>

                        <Text
                          style={{
                            textAlign: "center",
                            marginLeft: "10px",
                            fontSize: "10px",
                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "100%",
                            color: Colors.mainDarkBlue,
                          }}
                        >
                          {" "}
                        </Text>

                        <Text
                          style={{
                            fontSize: "10px",
                            fontWeight: 30,
                          }}
                        >
                          ผู้รับเงิน
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "20px",
                        justifyContent: "flex-end",
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          flex: 0.4,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: "15px",
                            fontWeight: 30,
                          }}
                        >
                          (
                        </Text>

                        <Text
                          style={{
                            textAlign: "center",
                            marginLeft: "10px",
                            fontSize: "10px",
                            fontWeight: 30,
                            borderBottom: "1px",
                            borderStyle: "dotted",
                            width: "100%",
                            color: Colors.mainDarkBlue,
                          }}
                        >
                          ชนัฐ สำเนียงง
                        </Text>

                        <Text
                          style={{
                            fontSize: "15px",
                            fontWeight: 30,
                          }}
                        >
                          )
                        </Text>
                      </View>
                    </View>
                  </Page>
                </Document>
              }
              fileName="หลักฐานการรับเงินจองซื้อออนไลน์.pdf"
            >
              {({ blob, url, loading, error }) =>
                loading ? (
                  "Loading document..."
                ) : (
                  <img
                    src="new/images/Icon feather-printer 1.svg"
                    width="20px"
                    height="20px"
                    style={{
                      marginLeft: "4px",
                      marginRight: "4px",
                      marginBottom: "4px",
                    }}
                  />
                )
              }
            </PDFDownloadLink>
          )}
        </Flex>
      </Flex>
      {/* <TextBold>status : {order.status_payment} </TextBold> */}
      {/* <ModalLoading visible={visible} /> */}
    </div>
  );
};

export default PurchaseComplete;

const styles = StyleSheet.create({
  page: {
    flexDirection: "column",
    backgroundColor: "white",
    border: "none",
    width: "100%",
    fontFamily: "Sarabun",
    padding: "20px",
  },
  section: {
    fontSize: "15px",
    margin: 10,
    padding: 10,
  },
});
