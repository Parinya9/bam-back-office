import type { NextPage } from "next";
import { TextBold, TextSemiBold } from "../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../utils/Colors";
import ButtonRadius from "../components/Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../components/Layout/InputRadius";
import callApi from "./api/callApi";
import Router from "next/router";
import Cookies from "universal-cookie";
const Login: NextPage = () => {
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    Router.push({
      pathname: process.env.NEXT_PUBLIC_API_END_POINT + "auth/ad/login",
    });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="row" style={{ height: "100vh", overflow: "hidden" }}>
        <div className="col-6" style={{ backgroundColor: Colors.mainDarkBlue }}>
          <div style={{ marginTop: "30%" }}>
            <div className="d-flex justify-content-center ">
              <img src="new/images/bam_logo.png" width="100px"></img>
            </div>
            <div className="d-flex justify-content-center mt-5">
              <div className="col-12">
                <TextSemiBold
                  fontSize="40px"
                  style={{ display: "block" }}
                  weight="normal"
                  color={Colors.mainWhite}
                  align="center"
                >
                  คัดสรร คุ้มค่า เพื่อคุณ
                </TextSemiBold>
              </div>
            </div>
          </div>
        </div>
        <div className="col-6" style={{ backgroundColor: Colors.mainWhite }}>
          <div style={{ marginTop: "15%" }}></div>
          <div className="d-flex justify-content-center mt-2">
            <div className="col-9">
              <TextSemiBold
                fontSize="40px"
                style={{ display: "block" }}
                color={Colors.mainDarkBlue}
                align="left"
              >
                เข้าสู่ระบบ
              </TextSemiBold>
            </div>
          </div>
          <div className="d-flex justify-content-center mt-2">
            <div className="col-9">
              <TextBold
                fontSize="32px"
                color={Colors.mainDarkBlue}
                style={{ display: "block" }}
              >
                Backoffice Management System.
              </TextBold>
            </div>
          </div>

          {/* <div className="d-flex justify-content-center mt-2">
            <div className="col-9">
              <div className="mt-3">
                <TextSemiBold
                  fontSize="17px"
                  weight="normal"
                  color={Colors.mainDarkBlue}
                >
                  อีเมล
                </TextSemiBold>
                <Controller
                  name="email"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      fontSize="18px"
                      field={field as any}
                      {...register("email", {
                        pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
                      })}
                    />
                  )}
                />
                {errors.email && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-center mt-2">
            <div className="col-9">
              <div className="mt-2">
                <TextSemiBold
                  fontSize="17px"
                  weight="normal"
                  color={Colors.mainDarkBlue}
                >
                  รหัสผ่าน
                </TextSemiBold>
                <Controller
                  name="password"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      type="password"
                      fontSize="18px"
                      field={field as any}
                      {...register("password", {
                        required: true,
                      })}
                    />
                  )}
                />
                {errors.password && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
          </div> */}
          <div className="d-flex justify-content-center mt-2 mb-5">
            <div className="col-9">
              <ButtonRadius
                type="submit"
                title="เข้าสู่ระบบ "
                backgroundColor="#F8E47D"
                color={Colors.mainDarkBlue}
                width="100%"
                height="50px"
                fontSize="24px"
                className=" mt-4 "
                borderWidth="thin"
              />
              {/* <ButtonRadius
                type="button"
                title="test login ad"
                backgroundColor="#F8E47D"
                color={Colors.mainDarkBlue}
                width="100%"
                height="50px"
                fontSize="24px"
                className=" mt-4 "
                borderWidth="thin"
                onClick={testLoginAd}
              /> */}
            </div>
          </div>
          <div
            className="d-flex justify-content-center mt-2 mb-5"
            style={{ paddingTop: "15%" }}
          >
            <div className="col-9 ">
              <TextBold fontSize="18px" weight="normal" color={Colors.mainGray}>
                &copy; 2021 สงวนสิทธิ์ บริษัทบริหารสินทรัพย์ กรุงเทพพาณิชย์
                จำกัด (มหาชน)
              </TextBold>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Login;
