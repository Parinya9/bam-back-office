import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import "../../styles/globals.css";
import type { AppProps } from "next/app";
import "bootstrap/dist/css/bootstrap.css";
import "react-pro-sidebar/dist/css/styles.css";
import "../../styles/SideBar.scss";
import "../../styles/Pagination.scss";

import "react-datepicker/dist/react-datepicker.css";

import Aside from "../components/Layout/SideBar";

import { PersistGate } from "redux-persist/integration/react";
import configureStore from "../redux/store";
import { Provider } from "react-redux";

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();

  const { store, persistor } = configureStore();

  const [rtl, setRtl] = useState(false);
  const [collapsed, setCollapsed] = useState(false);
  const [image, setImage] = useState(true);
  const [toggled, setToggled] = useState(false);

  const handleToggleSidebar = (value: any) => {
    setToggled(value);
  };

  return (
    <div className="row no-gutters" style={{ maxWidth: "100%"}}>
      <div className={router.pathname === "/" ? "d-none" : "col-2 px-0"}>
        <Aside
          collapsed={collapsed}
          rtl={rtl}
          toggled={toggled}
          handleToggleSidebar={handleToggleSidebar}
        />
      </div>
      <div
        className={router.pathname === "/" ? "col-12" : "col-10"}
        style={{ backgroundColor: "#F4F5F7", overflowY: "auto" }}
      >
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </div>
    </div>
  );
}

export default MyApp;
