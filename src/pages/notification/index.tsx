import { TextBold, TextSemiBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import Wrapper from "../../components/Layout/Wrapper";
import CardDetailNotification from "../../components/Card/CardDetailNotification";
import callApi from "../../pages/api/callApi";
import Cookies from "universal-cookie";
import Auth from "../auth/index";
import Router from "next/router";

const mockData = [
  {
    id: 652,
    asset_id: 316517,
    market_code: "DEUDNSH0105001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: false,
    create_date: "2022-03-22T17:17:38.922Z",
    update_date: "2022-03-22T17:17:38.922Z",
  },
  {
    id: 638,
    asset_id: 315117,
    market_code: "DDLPAAB0100001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: false,
    create_date: "2022-03-22T17:17:33.859Z",
    update_date: "2022-03-22T17:17:33.859Z",
  },
  {
    id: 624,
    asset_id: 291001,
    market_code: "KBKKCB0006000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: false,
    create_date: "2022-03-22T17:17:31.834Z",
    update_date: "2022-03-22T17:17:31.834Z",
  },
  {
    id: 610,
    asset_id: 284342,
    market_code: "OCHRVL0137000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: false,
    create_date: "2022-03-22T17:17:29.687Z",
    update_date: "2022-03-22T17:17:29.687Z",
  },
  {
    id: 596,
    asset_id: 276191,
    market_code: "ABKKVL0507000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: false,
    create_date: "2022-03-22T17:17:26.577Z",
    update_date: "2022-03-22T17:17:26.577Z",
  },
  {
    id: 582,
    asset_id: 274597,
    market_code: "LKPPVL0101001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: false,
    create_date: "2022-03-22T17:17:03.317Z",
    update_date: "2022-03-22T17:17:03.317Z",
  },
  {
    id: 568,
    asset_id: 274362,
    market_code: "LCHMSH0101001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: false,
    create_date: "2022-03-22T17:16:59.170Z",
    update_date: "2022-03-22T17:16:59.170Z",
  },
  {
    id: 538,
    asset_id: 286865,
    market_code: "LBKKCB0100000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:47:08.597Z",
    update_date: "2022-03-22T15:47:08.597Z",
  },
  {
    id: 524,
    asset_id: 285575,
    market_code: "ABKKSH0033000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:47:06.197Z",
    update_date: "2022-03-22T15:47:06.197Z",
  },
  {
    id: 510,
    asset_id: 284342,
    market_code: "OCHRVL0137000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:47:03.390Z",
    update_date: "2022-03-22T15:47:03.390Z",
  },
  {
    id: 496,
    asset_id: 283329,
    market_code: "BPYOSH0129000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:47:00.961Z",
    update_date: "2022-03-22T15:47:00.961Z",
  },
  {
    id: 482,
    asset_id: 276612,
    market_code: "ABKKVL0500000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:56.066Z",
    update_date: "2022-03-22T15:46:56.066Z",
  },
  {
    id: 468,
    asset_id: 287591,
    market_code: "BPCHFT0001001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:53.778Z",
    update_date: "2022-03-22T15:46:53.778Z",
  },
  {
    id: 454,
    asset_id: 276193,
    market_code: "ABKKVL0509000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:50.881Z",
    update_date: "2022-03-22T15:46:50.881Z",
  },
  {
    id: 440,
    asset_id: 276192,
    market_code: "ABKKVL0508000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:47.832Z",
    update_date: "2022-03-22T15:46:47.832Z",
  },
  {
    id: 426,
    asset_id: 276191,
    market_code: "ABKKVL0507000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:42.373Z",
    update_date: "2022-03-22T15:46:42.373Z",
  },
  {
    id: 412,
    asset_id: 276188,
    market_code: "ARAYCU0001000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:39.485Z",
    update_date: "2022-03-22T15:46:39.485Z",
  },
  {
    id: 398,
    asset_id: 274597,
    market_code: "LKPPVL0101001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:35.761Z",
    update_date: "2022-03-22T15:46:35.761Z",
  },
  {
    id: 384,
    asset_id: 274362,
    market_code: "LCHMSH0101001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:33.014Z",
    update_date: "2022-03-22T15:46:33.014Z",
  },
  {
    id: 370,
    asset_id: 274070,
    market_code: "LUBNCB0100001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:31.185Z",
    update_date: "2022-03-22T15:46:31.185Z",
  },
  {
    id: 356,
    asset_id: 273968,
    market_code: "PRAYVL0110000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:29.406Z",
    update_date: "2022-03-22T15:46:29.406Z",
  },
  {
    id: 342,
    asset_id: 273300,
    market_code: "BCNTTH0105000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:27.271Z",
    update_date: "2022-03-22T15:46:27.271Z",
  },
  {
    id: 328,
    asset_id: 273249,
    market_code: "BCNTTH0101000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:23.209Z",
    update_date: "2022-03-22T15:46:23.209Z",
  },
  {
    id: 314,
    asset_id: 272587,
    market_code: "PNSMVL0121000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:21.302Z",
    update_date: "2022-03-22T15:46:21.302Z",
  },
  {
    id: 300,
    asset_id: 272355,
    market_code: "PCHMVL0138000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T15:46:19.652Z",
    update_date: "2022-03-22T15:46:19.652Z",
  },
  {
    id: 250,
    asset_id: null,
    market_code: null,
    type: "ทรัพย์มาใหม่",
    total: "70357",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-22T11:23:13.177Z",
    update_date: "2022-03-22T11:23:13.177Z",
  },
  {
    id: 236,
    asset_id: null,
    market_code: null,
    type: "ทรัพย์มาใหม่",
    total: "10",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-18T14:10:45.861Z",
    update_date: "2022-03-18T14:20:59.782Z",
  },
  {
    id: 222,
    asset_id: 200699,
    market_code: "22NONSH0023000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-15T16:41:19.248Z",
    update_date: "2022-03-15T16:41:19.248Z",
  },
  {
    id: 203,
    asset_id: 139973,
    market_code: "TRAYSH0100001",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-10T02:08:59.688Z",
    update_date: "2022-03-10T02:08:59.688Z",
  },
  {
    id: 190,
    asset_id: 139777,
    market_code: "PBKKSH0100000",
    type: "ตรวจสอบแล้ว",
    total: "1",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-08T15:35:56.021Z",
    update_date: "2022-03-08T15:35:56.021Z",
  },
  {
    id: 167,
    asset_id: 139804,
    market_code: "",
    type: "ทรัพย์มาใหม่",
    total: "2",
    email: "websitesit1@lleafhotmail.onmicrosoft.com",
    read: true,
    create_date: "2022-03-07T17:10:31.423Z",
    update_date: "2022-03-07T17:12:17.000Z",
  },
];

const Notification = () => {
  const cookies = new Cookies();
  const email = cookies.get("email");
  const [listNotification, setListNotification] = useState([]);
  const [triggerRead, setTriggerRead] = useState(false);
  const [triggerIcon, setTriggerIcon] = useState(false);

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    getNotification();
    setReadNoti();
  }, []);

  const getNotification = async () => {
    try {
      const condition = { email: email };
      const response = await callApi.apiPost(
        "notification/findByEmail",
        condition
      );
      if (response.length > 0) {
        setListNotification(response);
      }
    } catch (error) {
      console.log("error get notification", error);
    }
  };

  const setReadNoti = async () => {
    if (!triggerRead) {
      setTriggerRead(!triggerRead);
      const condition = { email: email };
      await callApi.apiPost("notification/upateNoti", condition);
      setTriggerIcon(false);
    } else {
      getNotification();
    }
  };

  return (
    <Wrapper isLogout={true} stepIcon={null} title="ข้อมูลประเภททรัพย์">
      <div>
        <TextSemiBold color={Colors.mainBlack} fontSize="20px">
          ข้อความ
        </TextSemiBold>
        &nbsp;&nbsp;
        <TextSemiBold color={Colors.mainBlue} fontSize="20px">
          {listNotification.length}
        </TextSemiBold>
        &nbsp;&nbsp;
        <TextSemiBold color={Colors.mainBlack} fontSize="20px">
          รายการ
        </TextSemiBold>
      </div>
      <div className="mt-3">
        {listNotification.map((item: any, index: any) => {
          return (
            <CardDetailNotification
              key={item.id}
              title={item.type}
              date={item.update_date}
              total={item.total}
              read={item.read}
              marketCode={item.market_code}
              asset_id={item.asset_id}
            />
          );
        })}
      </div>
    </Wrapper>
  );
};

export default Notification;
