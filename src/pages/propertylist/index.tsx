import type { NextPage } from "next";
import Router from "next/router";
import { TextBold, TextSemiBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import Wrapper from "../../components/Layout/Wrapper";
import SearchProperty from "../../components/Search/SearchProperty";
import HeaderTabelPropertyList from "../../components/Table/HeaderTabelPropertyList";
import TableAssetList from "../../components/Table/TableAssetList";
import callApi from "../api/callApi";
import Cookies from "universal-cookie";
import Auth from "../auth/index";

const PropertyList: NextPage = () => {
  // const [numData, setNumData] = useState("");
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  let deptCode = cookies.get("deptCode");

  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [conditionExport, setConditionExport] = useState([] as any);
  const [search, setSearch] = useState({});
  const filterRoles = (role: any) => {};
  const fetchData = async (start: any, limit: any) => {
    let item = search;
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(item, {
        start: start,
        limit: limit,
        ao_name: firstname + " " + lastname,
      });
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      Object.assign(item, {
        start: start,
        limit: limit,
        dept_code: deptCode,
      });
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      Object.assign(item, {
        start: start,
        limit: limit,
        v_name: firstname + " " + lastname,
      });
    } else {
      Object.assign(item, {
        start: start,
        limit: limit,
      });
    }
    setSearch(item);
    const response = await callApi.apiPost("property-detail/find", item);
    if (response.length > 0) {
      setData(response);
      setTotal(response[0].total);
    } else {
      setData([]);
      setTotal(0);
    }
  };

  const actionSearch = async (data: string) => {
    // ao_name v_name m_name
    var item = { market_code: data, start: 0, limit: 10 };
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(item, {
        ao_name: firstname + " " + lastname,
      });
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      Object.assign(item, {
        dept_code: deptCode,
      });
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      Object.assign(item, {
        v_name: firstname + " " + lastname,
      });
    }
    setSearch(item);
    setConditionExport(item);
    const response = await callApi.apiPost("property-detail/find", item);
    if (response.length > 0) {
      setData(response);
      setTotal(response[0].total);
      setCurrentPage(1);
    } else {
      setData([]);
      setTotal(0);
    }
  };
  const filterSearch = async (data: any) => {
    var item = data;
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(item, {
        ao_name: firstname + " " + lastname,
      });
    }
    Object.assign(item, { start: 0, limit: 10 });
    setSearch(item);
    setConditionExport(item);

    const response = await callApi.apiPost("property-detail/find", item);
    if (response.length != 0) {
      setData(response);
      setTotal(response[0].total);
    } else {
      setData([]);
      setTotal(0);
    }
    setCurrentPage(1);
  };
  const setDataForExport = (item: any) => {
    // var item = search;
    // Object.assign(item, { start: 0, limit: total });
    // const response = await callApi.apiPost("property-detail/find", item);
    var listDataExport = [];
    // for (var i = 0; i < item.length; ++i) {
    var objData = {
      ประเภททรัพย์สิน: item["npa_type"],
      รหัสทรัพย์สิน: item["market_code"],
      ชื่อทรัพย์สิน: item["project_th"],
      ชื่อผู้ดูแล: item["ao_name"],
      ละติจูด: item["gps_lat1"],
      ลองติจูด: item["gps_long1"],
      อำเภอ: item["city_name"],
      จังหวัด: item["province_name"],
      ราคา: item["center_price"],
      สถานะทรัพย์สิน: item["asset_state"],
    };

    listDataExport.push(objData);
    // }
    return listDataExport;
  };
  const exportExcel = async () => {
    const response = await callApi.apiExport(
      "property-detail/export",
      conditionExport
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "Property_detail" + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };
  const exportExcelRows = async (item: any) => {
    const dataExport = {
      filename: "Property_List",
      data: setDataForExport(item),
    };
    const response = await callApi.apiExport("excel/export", dataExport);
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );

    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", dataExport.filename + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    // if (Auth()) {
    //   Router.push("/");
    // }
    fetchData(0, 10);
  }, []);
  return (
    <Wrapper
      isLogout={true}
      stepIcon={
        <>
          <img src="new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ทรัพย์สิน
          </TextBold>
        </>
      }
      title="ทรัพย์สิน"
    >
      <SearchProperty
        isFilter={true}
        typeProperty="propertyList"
        onClickSearch={actionSearch}
        onClickExport={exportExcel}
        onClickSearchFilter={filterSearch}
      />
      <HeaderTabelPropertyList
        setSearch={setSearch}
        setData={setData}
        setCurrentPage={setCurrentPage}
        setTotal={setTotal}
        number={total.toString()}
      />
      <TableAssetList
        data={data}
        total={total}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        fetchData={fetchData}
        exportExcel={exportExcelRows}
        setTotal={setTotal}
        setData={setData}
      />
    </Wrapper>
  );
};

export default PropertyList;
