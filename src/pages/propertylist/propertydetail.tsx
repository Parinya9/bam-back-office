import React, { FunctionComponent, useEffect, useState } from "react";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import Router from "next/router";
import { useForm, Controller } from "react-hook-form";

import Switch from "react-switch";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Cookies from "universal-cookie";
import AssetDetail from "../../components/assetDetail/assetDetail";
import AssetStatus from "../../components/assetDetail/assetStatus";
import TrackingLog from "../../components/assetDetail/trackingLog";
import ReserveAssetData from "../../components/assetDetail/reserveAssetData";
import HistoryLog from "../../components/assetDetail/historyLog";
import Renovate from "../../components/assetDetail/renovate";
import dynamic from "next/dynamic";
import ButtonRadius from "../../components/Button/ButtonNoBorder";
import ButtonBorderRadius from "../../components/Button/ButtonRadius";
import callApi from "../api/callApi";
import { useDispatch } from "react-redux";
import RoleDisplay from "../../../utils/RolesDisplay";
import {
  saveEditImageRenovate1,
  saveEditImageRenovate2,
  saveEditImageRenovate3,
  saveEditMapImage,
  saveEditAssetImage,
  saveEdit360Image,
} from "../../redux/assetDetail/action";
import ModalPropertyDisplay from "../../components/Modal/ModalPropertyDisplay";
import ModalRejectProperty from "../../components/Modal/ModalRejectProperty";
import Auth from "../auth/index";

const AssetImage = dynamic(import("../../components/assetDetail/assetImage"));

interface PropertyDetail {
  mode: string;
  id: string;
  market_code: string;
}

const PropertyDetail: FunctionComponent<PropertyDetail> = ({
  id,
  mode,
  market_code,
}) => {
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const dispatch = useDispatch();
  const [roleAction, setRoleAction] = useState([
    { action: "", approveAsset: "" },
  ]);
  const [desc, setDesciption] = useState("");
  const [display_property, setDisplayProperty] = useState(false);
  const [statusField, setStatusField] = useState(true);
  const [txtTitle, setTxtTitle] = useState("");
  const [showDisplayProperty, setShowDisplayProperty] = useState(false);
  const [showDisplayReject, setShowDisplayReject] = useState(false);
  const [assetStatus, setAssetStatus] = useState("");
  const [assetApprove, setAssetApprove] = useState("");
  const [statusApprove, setStatusApprove] = useState("");
  const [flagTab, setFlagTab] = useState(false);
  const [keyTab, setKeyTab] = useState(0);
  const [wrapTxt, setWrapTxt] = useState("รายละเอียดทรัพย์สิน");
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");

  const returnPage = () => {
    Router.push("/propertylist");
  };
  const changeTab = (key: any) => {
    if (flagTab) {
      alert("คุณยังไม่ได้บันทึกข้อมูล กรุณากดปุ่มบันทึกข้อมูล");
      setFlagTab(false);
    } else {
      setKeyTab(key);
    }
  };
  const setData = async () => {
    setTxtTitle("รหัสทรัพย์ " + market_code);
    const conditionSearch = { id: parseInt(id) };
    const response = await callApi.apiPost(
      "property-detail/findKey",
      conditionSearch
    );
    if (response.length > 0) {
      if (response[0].asset_state !== "ทรัพย์พร้อมขาย" && mode == "edit") {
        Router.push({
          pathname: "/propertylist",
        });
      }
      setDisplayProperty(response[0].display_property);
      setStatusApprove(response[0].status_approve);
      setDesciption(response[0].display_property_desc);
      setAssetStatus(response[0].asset_state);
      setAssetApprove(response[0].status_approve);
    }
  };
  const getRoleAction = async () => {
    const action = await callApi.apiPost("rolepermission/findAction", {
      role: role,
    });
    if (action.length > 0) {
      setRoleAction(action);
    }
  };
  const checkApprove = (item: any) => {
    if (item == "divButtonApprove" && roleAction[0]?.approveAsset) {
      return true;
    }
  };
  const checkSubmit = (item: any) => {
    if (
      item == "buttonSubmitAsset" &&
      roleAction[0]?.action?.search("add") != -1 &&
      roleAction[0]?.action?.search("edit") != -1
    ) {
      return true;
    }
  };
  const onChangeDisplayProperty = async () => {
    setDisplayProperty(!display_property);
    if (!display_property == false) {
      setShowDisplayProperty(true);
    } else {
      // setDesciption("");
      //   const objEdit = {
      //     display_property_desc: "",
      //     display_property: !display_property ? "true" : "false",
      //     id: parseInt(id),
      //   };
      //   const item = await callApi.apiPut("property-detail/update", objEdit);
      //   if (item.affected == 1) {
      //     setTimeout(async () => {
      //       const response = await apiSyncAssetDetail("api/asset-detail");
      //       if (response.status === "success") {
      //         alert("Update Success");
      //       } else {
      //         alert("Cant'not update asset");
      //       }
      //     }, 2000);
      //   }
      // }
    }
  };

  const checkStatus = (status: string) => {
    if (status === "ทรัพย์พร้อมขาย") {
      return (
        <img
          src="/new/images/Ellipse-green.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    } else if (status === "ขายแล้ว") {
      return (
        <img
          src="/new/images/Ellipse-yellow.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    } else if (status === "ระหว่างดำเนินการ") {
      return (
        <img
          src="/new/images/Ellipse-red.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    }
  };
  const checkStatusAssetApprove = (status: any) => {
    if (status === "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      return (
        <img
          src="/new/images/Ellipse-orange.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    } else if (status === "รอการตรวจสอบ จนท.ฝ่ายการตลาด") {
      return (
        <img
          src="/new/images/Ellipse-pink.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    } else if (status === "รอการตรวจสอบ ผจก.ฝ่ายการตลาด") {
      return (
        <img
          src="/new/images/Ellipse-blue.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    } else if (status === "ตรวจสอบแล้ว") {
      return (
        <img
          src="/new/images/Ellipse-green.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    } else {
      return (
        <img
          src="/new/images/Ellipse-gray.svg"
          width="10px"
          height="10px"
          style={{ paddingBottom: "2.5px" }}
        />
      );
    }
  };
  const approveProperty = async () => {
    const objEdit = {
      status_approve: nextStatus(),
      id: parseInt(id),
    };
    let objLog = {
      asset_id: parseInt(id),
      status_approve: true,
      user: firstname + " " + lastname,
      position: role,
      title: statusApprove,
      note: "",
    };
    await callApi.apiPut("property-detail/update", objEdit);
    await callApi.apiPost("history-log/addLog", objLog);

    window.location.reload();
  };
  const rejectProperty = () => {
    setShowDisplayReject(true);
  };
  const nextStatus = () => {
    switch (statusApprove as any) {
      case "รอการตรวจสอบ ผจก.ฝ่ายจำหน่ายฯ / สนง.ภูมิภาคทรัพย์":
        return "รอการตรวจสอบ จนท.ฝ่ายการตลาด";
      case "รอการตรวจสอบ จนท.ฝ่ายการตลาด":
        return "รอการตรวจสอบ ผจก.ฝ่ายการตลาด";
      case "รอการตรวจสอบ ผจก.ฝ่ายการตลาด":
        return "ตรวจสอบแล้ว";
      default:
        return "";
    }
  };
  const setInitialPage = () => {
    dispatch(saveEditImageRenovate1([]));
    dispatch(saveEditImageRenovate2([]));
    dispatch(saveEditImageRenovate3([]));
    dispatch(saveEditMapImage([]));
    dispatch(saveEditAssetImage([]));
    dispatch(saveEdit360Image([]));
    setData();
    getRoleAction();
    if (mode == "edit") {
      setStatusField(false);
      setWrapTxt("แก้ไขทรัพย์สิน");
    } else {
      setStatusField(true);
    }
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    setInitialPage();
  }, []);

  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ทรัพย์สิน
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            {wrapTxt}
          </TextBold>
        </>
      }
      title={txtTitle}
      titleIcon="/new/images/Back.svg"
      onClick={returnPage}
    >
      <div className="row mt-3 mb-4">
        <Flex className="flex-column">
          <Flex className="flex-row mt-2">
            <Flex>
              <TextBold
                fontSize="18px"
                color={Colors.mainBlackLight}
                className="ms-2"
                style={{ paddingRight: "2em" }}
              >
                สถานะการแสดงทรัพย์บนหน้าเว็บไซต์
              </TextBold>

              <Controller
                name="display_property"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <Switch
                    onChange={() => onChangeDisplayProperty()}
                    checked={display_property}
                    checkedIcon={false}
                    uncheckedIcon={false}
                    disabled={statusField}
                    onColor={Colors.green}
                  />
                )}
              />
              <TextSemiBold
                fontSize="16px"
                weight="normal"
                color={Colors.mainBlackLight}
                className="ms-3"
              >
                {display_property ? "แสดงทรัพย์" : "ซ่อนทรัพย์"}
              </TextSemiBold>
            </Flex>
          </Flex>
        </Flex>
      </div>
      <div className="mb-2">
        <Flex className="flex-column">
          <Flex className="flex-row mt-2">
            <Flex className="justify-content-end">
              <TextBold
                fontSize="17px"
                weight="400"
                color={Colors.mainBlack}
                className="ms-2"
              >
                สถานะทรัพย์สิน :{""} {checkStatus(assetStatus)}
                {""} {assetStatus}
              </TextBold>
              <TextBold
                fontSize="17px"
                weight="400"
                color={Colors.mainGrayLight}
                className="ms-2"
              >
                {" "}
                |{" "}
              </TextBold>
              <TextBold
                fontSize="17px"
                weight="400"
                color={Colors.mainBlack}
                className="ms-2"
              >
                สถานะรอการตรวจสอบ : {checkStatusAssetApprove(assetApprove)}
                {assetApprove}
              </TextBold>
            </Flex>
          </Flex>
        </Flex>
      </div>
      <div
        className="container pb-5"
        style={{
          backgroundColor: "white",
          minHeight: "450px",
        }}
      >
        <Tabs selectedIndex={keyTab} onSelect={(index) => changeTab(index)}>
          <TabList>
            <Tab
            // onClick={() => {
            //   changeTab(0);
            // }}
            >
              รายละเอียด
            </Tab>
            <Tab>รูปทรัพย์สิน</Tab>
            <Tab>สถานะทรัพย์สิน</Tab>
            <Tab
            // onClick={() => {
            //   changeTab(3);
            // }}
            >
              Tracking Log
            </Tab>
            {/* <Tab>ข้อมูลจองซื้อทรัพย์</Tab> */}
            <Tab>History Log</Tab>
            {/* <Tab>รีโนเวท</Tab> */}
          </TabList>
          <TabPanel id={"1"} key={1}>
            <AssetDetail
              id={id}
              mode={mode}
              checkSubmit={checkSubmit}
              flagTab={false}
              setFlagTab={setFlagTab}
              display_property={display_property}
            />
          </TabPanel>
          <TabPanel id={"2"} key={2}>
            <AssetImage
              id={id}
              mode={mode}
              checkSubmit={checkSubmit}
              flagTab={false}
              setFlagTab={setFlagTab}
              display_property={display_property}
            />{" "}
          </TabPanel>
          <TabPanel id={"3"} key={3}>
            <AssetStatus
              id={id}
              mode={mode}
              checkSubmit={checkSubmit}
              flagTab={false}
              setFlagTab={setFlagTab}
              display_property={display_property}
            />
          </TabPanel>
          <TabPanel id={"4"} key={4}>
            <TrackingLog
              id={id}
              mode={mode}
              statusField={statusField}
              checkSubmit={checkSubmit}
              flagTab={false}
              setFlagTab={setFlagTab}
              display_property={display_property}
            />
          </TabPanel>
          {/* <TabPanel id={"5"} key={5}>
            <ReserveAssetData
              id={id}
              mode={mode}
              statusField={statusField}
              checkSubmit={checkSubmit}
              flagTab={false}
              setFlagTab={setFlagTab}
              display_property={display_property}
            />
          </TabPanel> */}
          <TabPanel id={"6"} key={6}>
            <HistoryLog id={id} />
          </TabPanel>
          {/* <TabPanel id={"7"} key={7}>
            <Renovate
              id={id}
              mode={mode}
              statusField={statusField}
              checkSubmit={checkSubmit}
              flagTab={false}
              setFlagTab={setFlagTab}
              display_property={display_property}
            />
          </TabPanel> */}
        </Tabs>
      </div>

      {RoleDisplay.checkStatusApprove(role, statusApprove) &&
        checkApprove("divButtonApprove") && (
          <div className="mt-5 mb-5 row ">
            <div className="col-4 ">
              <ButtonBorderRadius
                type="button"
                title="ยกเลิก"
                background="inherit"
                color={Colors.mainDarkBlue}
                width="200px"
                fontSize="18px"
                className="mx-1 py-2"
                borderWidth="1px"
                border="solid"
                borderColor="#004C85"
                onClick={() => Router.back()}
              />
            </div>
            <div className="col-8">
              <ButtonRadius
                type="button"
                title="ไม่อนุมัติ"
                background="#FF2727"
                color={Colors.mainWhite}
                width="200px"
                fontSize="18px"
                className="mx-1 py-2"
                border="none"
                onClick={rejectProperty}
              />{" "}
              <ButtonRadius
                type="button"
                title="อนุมัติ"
                onClick={approveProperty}
                background={Colors.mainBlue}
                color={Colors.mainWhite}
                width="200px"
                fontSize="18px"
                className="mx-1 py-2"
                border="none"
              />
            </div>
          </div>
        )}
      <ModalPropertyDisplay
        show={showDisplayProperty}
        setShow={setShowDisplayProperty}
        display_property={display_property}
        setDisplayProperty={setDisplayProperty}
        id={id}
        reload={false}
        fetchData={() => {}}
        currentPage={0}
      />
      <ModalRejectProperty
        show={showDisplayReject}
        setShow={setShowDisplayReject}
        id={id}
        statusApprove={statusApprove}
        market_code={market_code}
      ></ModalRejectProperty>
    </Wrapper>
  );
};

export default PropertyDetail;

export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      mode: query.mode || "edit",
      id: query.id || null,
      market_code: query.market_code || null,
    },
  };
}
