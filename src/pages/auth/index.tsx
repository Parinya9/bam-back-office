import Cookies from "universal-cookie";
const withAuth = () => {
  const cookies = new Cookies();
  const token = cookies.get("token");
  if (token == undefined) {
    return true;
  }
  return false;
};

export default withAuth;
