import type { NextPage } from "next";
import { TextBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import { useForm } from "react-hook-form";
import SearchProperty from "../../components/Search/SearchProperty";
import TableInterestProperty from "../../components/Table/TableInterestProperty";
import Wrapper from "../../components/Layout/Wrapper";
import HeaderTable from "../../components/Table/HeaderTable";
import callApi from "../api/callApi";
import Cookies from "universal-cookie";

import moment from "moment";
import Auth from "../auth/index";
import Router from "next/router";
import ModalLoading from "../../components/Modal/ModalLoading";

const InterestProperty: NextPage = () => {
  const {
    control,
    formState: { errors },
  } = useForm();
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  let deptCode = cookies.get("deptCode");
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState({});
  const [conditionExport, setConditionExport] = useState([] as any);
  const [visible, setVisible] = useState(false);
  const fetchData = async (start: any, limit: any) => {
    setVisible(true);
    let item = search;
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(item, {
        ao_name: firstname + " " + lastname,
        start: start,
        limit: limit,
      });
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      Object.assign(item, {
        dept_code: deptCode,
        start: start,
        limit: limit,
      });
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      Object.assign(item, {
        v_name: firstname + " " + lastname,
        start: start,
        limit: limit,
      });
    } else {
      Object.assign(item, {
        start: start,
        limit: limit,
      });
    }
    setSearch(item);
    const response = await callApi.apiPost("interestproperty/find", item);
    setData(genRunningNo(response.item, start));
    setTotal(response.total);
    setVisible(false);
  };
  const genRunningNo = (data: any, start: any = 0) => {
    if (data.length > 0) {
      for (var i = 0; i < data.length; ++i) {
        data[i].no = start + i + 1;
      }

      return data;
    }
    return data;
  };
  const filterSearch = async (data: any) => {
    setVisible(true);
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(data, {
        ao_name: firstname + " " + lastname,
      });
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      Object.assign(data, {
        dept_code: deptCode,
      });
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      Object.assign(data, {
        v_name: firstname + " " + lastname,
      });
    }
    Object.assign(data, { start: 0, limit: 10 });
    setSearch(data);
    setConditionExport(data);

    const response = await callApi.apiPost("interestproperty/find", data);
    setData(genRunningNo(response.item));
    setTotal(response.total);
    setVisible(false);
  };
  const actionSearch = async (data: string) => {
    setVisible(true);

    const conditionSearch = {
      asset_id: data,
      fullname: data,
      admin_name: data,
    };
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(conditionSearch, {
        ao_name: firstname + " " + lastname,
      });
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      Object.assign(conditionSearch, {
        dept_code: deptCode,
      });
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      Object.assign(conditionSearch, {
        v_name: firstname + " " + lastname,
      });
    }
    setSearch(conditionSearch);
    setConditionExport(conditionSearch);

    const response = await callApi.apiPost(
      "interestproperty/findSearch",
      conditionSearch
    );
    setData(genRunningNo(response.item));

    setTotal(response.total);
    setVisible(false);
  };

  const exportExcel = async () => {
    setVisible(true);
    const response = await callApi.apiExport(
      "interestproperty/export",
      conditionExport
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "Interest_property" + ".xlsx");
    document.body.appendChild(link);
    link.click();
    setVisible(false);
  };

  const deleteData = async (id: number) => {
    if (confirm("ยืนยันการลบข้อมูล")) {
      await callApi.apiDelete("interestproperty/delete/" + id);
      fetchData((currentPage - 1) * 10, 10);
    }
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);
  return (
    <>
      <Wrapper
        isLogout={true}
        stepIcon={
          <>
            <img src="new/images/Home2.svg" width="20px" height="20px" />
            <img
              src="new/images/Expand_right_double_light.svg"
              width="20px"
              height="20px"
              className="ms-2"
            />
            <TextBold
              fontSize="16px"
              color={Colors.mainGrayLight}
              className="ms-2"
            >
              สนใจทรัพย์สิน
            </TextBold>
          </>
        }
        title="สนใจทรัพย์สิน"
      >
        <SearchProperty
          isFilter={true}
          typeProperty="interestProperty"
          onClickSearch={actionSearch}
          onClickExport={exportExcel}
          onClickSearchFilter={filterSearch}
        />
        <HeaderTable number={total.toString()} />
        <div style={{ overflowX: "auto" }}>
          <TableInterestProperty
            data={data}
            onClickDelete={deleteData}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            fetchData={fetchData}
            // exportExcel={exportExcelRows}
            total={total}
            setTotal={setTotal}
          />
        </div>
        <ModalLoading visible={visible} />
      </Wrapper>
    </>
  );
};

export default InterestProperty;
