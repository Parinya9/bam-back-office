import dynamic from "next/dynamic";

const DynamicComponentWithNoSSR = dynamic(() => import("./buttonKbank"), {
  ssr: false,
});

export default function DynamicComponentWithNoSSRCB() {
  return <DynamicComponentWithNoSSR />;
}
