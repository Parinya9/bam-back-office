import Script from "next/script";

export default function buttonKbank() {
  return (
    <Script
      type="text/javascript"
      src="https://dev-kpaymentgateway.kasikornbank.com/ui/v2/kpayment.min.js"
      data-apikey="pkey_test_242sdX4aEJC4aliRFnaOMQPcO7qt2OgtM8O"
      data-amount="734.00"
      data-payment-methods="card"
      data-mid="401001184984001"
    />
  );
}
