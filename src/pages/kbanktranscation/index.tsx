import type { NextPage } from "next";
import { TextBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import { useForm } from "react-hook-form";
import SearchProperty from "../../components/Search/SearchProperty";
import TableKbankTranscation from "../../components/Table/TableKbankTranscation";
import Wrapper from "../../components/Layout/Wrapper";
import HeaderTable from "../../components/Table/HeaderTable";
import ButtonKBank from "./noSSRButtonKbank";
import callApi from "../api/callApi";
import moment from "moment";
import Auth from "../auth/index";
import Router from "next/router";
import Cookies from "universal-cookie";

// import axios from "axios";
const KbankTranscation: NextPage = () => {
  const {
    control,
    formState: { errors },
  } = useForm();
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState({});
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const [conditionExport, setConditionExport] = useState([] as any);
  const cookies = new Cookies();
  let role = cookies.get("role");
  let firstname = cookies.get("firstname");
  let lastname = cookies.get("lastname");
  const fetchData = async (start: any, limit: any) => {
    let item = search;
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(item, {
        ao_name: firstname + " " + lastname,
      });
    }
    Object.assign(item, { start: start, limit: limit });
    setSearch(item);
    setConditionExport(item);

    const response = await callApi.apiPost("reserve-asset/find", item);

    setData(genRunningNo(response.item, start));
    setTotal(response.total);
  };
  const genRunningNo = (data: any, start: any = 0) => {
    if (data.length > 0) {
      for (var i = 0; i < data.length; ++i) {
        data[i].no = start + i + 1;
      }

      return data;
    }
    return data;
  };
  const actionSearch = async (data: string) => {
    const conditionSearch = {
      fullname: "",
      reserve_no: data,
      start: 0,
      limit: 10,
    };
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(conditionSearch, {
        ao_name: firstname + " " + lastname,
      });
    }
    setSearch(conditionSearch);
    setConditionExport(conditionSearch);
    const response = await callApi.apiPost(
      "reserve-asset/find",
      conditionSearch
    );
    setData(genRunningNo(response.item));
    setTotal(response.total);
  };
  const filterSearch = async (data: any) => {
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      Object.assign(data, {
        ao_name: firstname + " " + lastname,
      });
    }
    Object.assign(data, {
      start: 0,
      limit: 10,
    });
    setSearch(data);
    setConditionExport(data);
    const response = await callApi.apiPost("reserve-asset/find", data);
    setData(response.item);
    setTotal(response.total);
  };

  const exportExcel = async () => {
    const response = await callApi.apiExport(
      "reserve-asset/exportKbank",
      conditionExport
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "KbankTranscation" + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);

  return (
    <>
      <Wrapper
        isLogout={true}
        stepIcon={
          <>
            <img src="new/images/Home2.svg" width="20px" height="20px" />
            <img
              src="new/images/Expand_right_double_light.svg"
              width="20px"
              height="20px"
              className="ms-2"
            />
            <TextBold
              fontSize="16px"
              color={Colors.mainGrayLight}
              className="ms-2"
            >
              Transaction KBank
            </TextBold>
          </>
        }
        title="Transaction KBank"
      >
        <SearchProperty
          isFilter={true}
          typeProperty="searchKbank"
          onClickSearch={actionSearch}
          onClickExport={exportExcel}
          onClickSearchFilter={filterSearch}
        />

        <HeaderTable number={total.toString()} />
        <TableKbankTranscation
          data={data}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          fetchData={fetchData}
          total={total}
          setTotal={setTotal}
        />
      </Wrapper>
    </>
  );
};

export default KbankTranscation;
