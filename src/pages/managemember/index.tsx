import type { NextPage } from "next";
import Router from "next/router";
import { TextBold, TextSemiBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import ButtonRadius from "../../components/Button/ButtonRadius";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Layout/InputRadius";
import Wrapper from "../../components/Layout/Wrapper";
import { Flex } from "../../components/layout";
import SearchProperty from "../../components/Search/SearchProperty";
import HeaderTable from "../../components/Table/HeaderTable";
import TableManageMember2 from "../../components/Table/TableManageMember2";
import callApi from "../api/callApi";
import moment from "moment";
import Auth from "../auth/index";
import Cookies from "universal-cookie";

const ManageMember: NextPage = () => {
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [tableType, setTableType] = useState("1");
  const [paramSearch, setParamSearch] = useState({});
  const cookies = new Cookies();
  const token = cookies.get("token");
  const genRunningNo = (data: any) => {
    if (data?.length > 0) {
      for (var i = 0; i < data.length; ++i) {
        data[i].no = i + 1;
      }

      return data;
    }
    return data;
  };
  const fetchData = async (page: any) => {
    let item = {
      start: (page - 1) * pageSize,
      pageSize: pageSize,
    };

    const response = await callApi.apiPost("admin/searchAll", item, token);
    setTotal(response.total);
    setNumData(response.total || "0");
    setData(genRunningNo(response.data));
    setTableType(response.type);
  };
  const filterSearch = async (data: any, page: number = 1) => {
    data.start = (page - 1) * pageSize;
    data.pageSize = pageSize;
    const response = await callApi.apiPost("admin/searchAll", data, token);
    setTotal(response.total);
    setNumData(response.total || "0");
    setData(genRunningNo(response.data));
    setTableType(response.type);
    setParamSearch({ data: data, typeSearch: "1" });
  };
  const actionSearch = async (data: string, page: number = 1) => {
    let conditionSearch = {
      fullname: data != undefined ? data : "",
      start: (page - 1) * pageSize,
      pageSize: pageSize,
    };
    const response = await callApi.apiPost(
      "admin/searchAll",
      conditionSearch,
      token
    );
    setTotal(response.total);
    setNumData(response.total || "0");
    setData(genRunningNo(response.data));
    setTableType(response.type);
    setParamSearch({ data: data, typeSearch: "2" });
  };
  const setDataForExport = () => {
    var listDataExport = [];
    for (var i = 0; i < data.length; ++i) {
      var objData = {
        "No.": data[i]["no"],
        "ชื่อ - นามสกุล": data[i]["firstname"] + " " + data[i]["lastname"],
        Role: data[i]["role"],
        "Job position ": data[i]["position"],
        "Organization ": data[i]["organization"],
        "create date ": moment(data[i]["create_date"]).format(
          "DD/MM/YYYY HH:mm"
        ),
        "last update	":
          data[i]["update_date"] != undefined
            ? moment(data[i]["update_date"]).format("DD/MM/YYYY HH:mm")
            : data[i]["update_date"],
      };
      listDataExport.push(objData);
    }
    return listDataExport;
  };
  const exportExcel = async () => {
    const dataExport = { filename: "admin", data: setDataForExport() };
    const response = await callApi.apiExport("excel/export", dataExport);
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );

    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", dataExport.filename + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(1);
  }, []);
  return (
    <Wrapper
      isLogout={true}
      stepIcon={
        <>
          <img src="new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            จัดการผู้ใช้งาน
          </TextBold>
        </>
      }
      title="จัดการผู้ใช้งาน"
    >
      <SearchProperty
        isFilter={true}
        isExport={false}
        typeProperty="managemember"
        onClickAddBtn={() => Router.push("/managemember/addmember")}
        onClickSearch={actionSearch}
        onClickExport={exportExcel}
        onClickSearchFilter={filterSearch}
      />
      {/* <HeaderTable number="5,000" /> */}
      <HeaderTable number={numData} />
      <TableManageMember2
        data={data}
        totalCount={total}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        fetchData={fetchData}
        pageSize={pageSize}
        siblingCount={1}
        tableType={tableType}
        filterSearch={filterSearch}
        actionSearch={actionSearch}
        paramSearch={paramSearch}
      />
    </Wrapper>
  );
};

export default ManageMember;
