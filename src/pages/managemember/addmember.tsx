import { useState, useEffect, FunctionComponent } from "react";
import Router from "next/router";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import Switch from "react-switch";
import ButtonRadius from "../../components/Button/ButtonRadius";
import callApi from "../api/callApi";
import Auth from "../auth/index";
import { apiSyncAssetDetail } from "../api/elsSync";
import ModalLoading from "../../components/Modal/ModalLoading";
import Cookies from "universal-cookie";

interface AddMemberInfo {
  id: string;
  mode: string;
}
const AddMemberInfo: FunctionComponent<AddMemberInfo> = ({ id, mode }) => {
  const {
    register,
    handleSubmit,
    control,
    watch,
    formState: { errors },
    reset,
    setValue,
    setError,
    clearErrors,
  } = useForm();

  const [approve, setApprove] = useState(false);
  const [title, setTitle] = useState("เพิ่มข้อมูลผู้ใช้งาน");
  const [oldEmail, setOldEmail] = useState("");
  const [dropdownRoles, setDropdownRoles] = useState([]);
  const [displayManager, setDisplayManager] = useState(false);
  const [displayDirector, setDisplayDirector] = useState(false);
  const [disabledDirector, setDisabledDirector] = useState(true);
  const [listManager, setListManager] = useState([]);
  const [listDirector, setListDirector] = useState([]);
  const [requiredManager, setRequiredManager] = useState(false);
  const [requiredDirector, setRequiredDirector] = useState(false);
  const [disabledStatus, setDisabledStatus] = useState(false);
  const [disabledRole, setDisabledRole] = useState(false);
  const [roleId, setRoleId] = useState("");
  const [visible, setVisible] = useState(false);
  const cookies = new Cookies();
  const token = cookies.get("token");
  const checkDropdownHeader = (value: any, key: any) => {
    if (value === "" || value === undefined) {
      setError(key, {
        type: "manual",
        message: "",
      });
      return false;
    } else {
      clearErrors(key);
      return true;
    }
  };
  const checkValdiate = (data: any) => {
    let role = data.role.label;
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      let valueDirector = checkDropdownHeader(data.director?.value, "director");
      let valueManager = checkDropdownHeader(data.manager?.value, "manager");
      if (valueDirector && valueManager) {
        return true;
      }
      return false;
    } else if (role === "จนท.ฝ่ายการตลาด") {
      let valueManager = checkDropdownHeader(data.manager?.value, "manager");
      if (valueManager) {
        return true;
      }
      return false;
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      let valueDirector = checkDropdownHeader(data.director?.value, "director");
      if (valueDirector) {
        return true;
      }
      return false;
    } else {
      clearErrors();
      return true;
    }
  };
  const onSubmit = (data: any) => {
    if (checkValdiate(data)) {
      mode == "edit" ? editSubmitData(data) : addSubmitData(data);
    }
  };
  const editSubmitData = async (data: any) => {
    data.role = data.role.label;
    data.oldEmail = oldEmail;
    data.status = approve;
    data.role_id = roleId;
    data.director = data.director?.value;
    data.manager = data.manager?.value;
    data.id = parseInt(id);
    setVisible(true);
    const item = await callApi.apiPut("admin/update", data, token);
    if (item.error == undefined) {
      if (roleId == "4") {
        const response = await apiSyncAssetDetail("api/asset-detail");
        if (response.status === "success") {
          setVisible(false);
          alert("Update Success");
        } else {
          setVisible(false);
          alert("Update Failed");
        }
      } else {
        setVisible(false);
        alert("Update Success");
      }
    } else {
      setVisible(false);
      alert("Error is " + item.error);
    }
  };
  const addSubmitData = async (data: any) => {
    data.role = data.role.label;
    data.status = approve;
    data.role_id = roleId;
    data.director = data.director?.value;
    data.manager = data.manager?.value;
    setVisible(true);
    const item = await callApi.apiPost("admin/register", data, token);
    if (item.error == undefined) {
      if (roleId == "4") {
        const response = await apiSyncAssetDetail("api/asset-detail");
        if (response.status === "success") {
          setVisible(false);
          alert("Update Success");
        } else {
          setVisible(false);
          alert("Update Failed");
        }
      } else {
        setVisible(false);
        alert("Update Success");
      }
    } else {
      alert(item.error);
    }
  };
  const clearDropdown = () => {
    setValue("manager", {
      value: "",
      label: "",
    });
    setValue("director", {
      value: "",
      label: "",
    });
  };
  const setDisplayField = (role: any) => {
    if (role === "จำหน่ายทรัพย์ (Sale)") {
      setDisplayDirector(true);
      setDisplayManager(true);
      setDisabledDirector(true);
    } else if (role === "จนท.ฝ่ายการตลาด") {
      setDisplayDirector(false);
      setDisplayManager(true);
      setDisabledDirector(true);
    } else if (role === "ผจก.ฝ่ายการตลาด") {
      setDisplayDirector(false);
      setDisplayManager(false);
      setDisabledDirector(false);
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      setDisplayDirector(true);
      setDisplayManager(false);
      setDisabledDirector(false);
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      setDisplayDirector(false);
      setDisplayManager(false);
      setDisabledDirector(true);
    } else {
      setDisplayDirector(false);
      setDisplayManager(false);
      setDisabledDirector(true);
    }
  };
  const onChangeManager = async (event: any) => {
    const dropdownDirector = await callApi.apiGet(
      "master/directorByManager/" + event.value
    );

    setListDirector(dropdownDirector.data);
    setValue("director", {
      value: dropdownDirector.data[0].value,
      label: dropdownDirector.data[0].label,
    });
    clearErrors("director");
  };
  const onChangeRole = async (event: any) => {
    clearDropdown();
    setRoleId(event.value);
    setDisplayField(event.label);

    if (event.label === "จำหน่ายทรัพย์ (Sale)") {
      const dropdownManager = await callApi.apiGet(
        "master/manager/dropdown/" + event.value
      );
      setRequiredManager(true);
      setRequiredDirector(true);

      setListManager(dropdownManager.data);
    } else if (event.label === "จนท.ฝ่ายการตลาด") {
      const dropdownManager = await callApi.apiGet(
        "master/manager/dropdown/" + event.value
      );
      setListManager(dropdownManager.data);
      setListDirector([]);
      setRequiredManager(true);
      setRequiredDirector(false);
    } else if (event.label === "ผจก.ฝ่ายการตลาด") {
      setListManager([]);
      setListDirector([]);
      setRequiredManager(false);
      setRequiredDirector(false);
    } else if (event.label === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      const dropdownDirector = await callApi.apiGet(
        "master/director/dropdown/" + event.value
      );
      setListManager([]);
      setListDirector(dropdownDirector.data);
      setRequiredManager(false);
      setRequiredDirector(true);
    } else if (event.label === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      setListManager([]);
      setListDirector([]);
      setRequiredManager(false);
      setRequiredDirector(false);
    } else {
      setListManager([]);
      setListDirector([]);
      setRequiredManager(false);
      setRequiredDirector(false);
    }
  };
  const setDropDown = async () => {
    const listRoles = await callApi.apiGet("master/roles/dropdown");

    setDropdownRoles(listRoles);
  };
  const watchToggle = watch("statusAdmin");
  const countUnderUser = async (role: any, id: any) => {
    if (role === "ผจก.ฝ่ายการตลาด") {
      const item = await callApi.apiGet("master/count/manager/" + id);
      if (item.data[0].value !== "0") {
        setDisabledStatus(true);
        setDisabledRole(true);
      }
    } else if (role === "ผจก.ฝ่ายจําหน่ายฯ / สนง.ภูมิภาคทรัพย์") {
      const item = await callApi.apiGet("master/count/manager/" + id);
      if (item.data[0].value !== "0") {
        setDisabledStatus(true);
        setDisabledRole(true);
      }
    } else if (role === "ผู้อำนวยการฝ่ายจำหน่าย/สนง.ภูมิภาค") {
      const item = await callApi.apiGet("master/count/director/" + id);
      if (item.data[0].value !== "0") {
        setDisabledStatus(true);
        setDisabledRole(true);
      }
    }
  };
  const setEditData = async () => {
    const conditionSearch = { id: id, fullname: "" };
    const response = await callApi.apiPost(
      "admin/find",
      conditionSearch,
      token
    );
    if (response.length > 0) {
      await countUnderUser(response[0].role, response[0].id);
      const dropdownManager = await callApi.apiGet(
        "master/manager/dropdown/" + response[0].role_id
      );
      const dropdownDirector = await callApi.apiGet(
        "master/director/dropdown/" + response[0].role_id
      );
      setListManager(dropdownManager.data);
      setListDirector(dropdownDirector.data);
      setTitle("แก้ไขข้อมูลผู้ใช้งาน");
      setDisplayField(response[0].role);
      setValue("email", response[0].email);
      setOldEmail(response[0].email);
      setValue("firstname", response[0].firstname);
      setValue("lastname", response[0].lastname);
      setValue("emp_id", response[0].emp_id);
      setValue("tel_number", response[0].tel_number);
      setValue("txt", response[0].txt);
      setValue("mobile_number", response[0].mobile_number);
      setRoleId(response[0].role_id);
      setValue("role", {
        label: response[0].role,
        value: response[0].role,
      });

      setValueDropDown(dropdownManager.data, "manager", response[0].manager);

      if (response[0].director !== "") {
        const director = await callApi.apiGet(
          "master/director/" + response[0].director
        );
        setListDirector(director.data);

        setValue("director", {
          value: director?.data[0]?.value,
          label: director?.data[0]?.label,
        });
      }

      setApprove(response[0].status);
    }
  };
  const setValueDropDown = (list: any, key: any, value: any) => {
    for (let index = 0; index < list.length; ++index) {
      if (list[index].value == value) {
        setValue(key, {
          label: list[index].label,
          value: value,
        });
        return;
      }
    }
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    mode == "edit" ? setEditData() : "";
    setDropDown();
  }, []);

  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            เพิ่มข้อมูลผู้ใช้งาน
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            {title}
          </TextBold>
        </>
      }
      title={title}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Flex
          style={{
            backgroundColor: "white",
          }}
          height="auto"
          className="flex-column container py-3 mt-3"
        >
          <div id="member-section2" className="mt-3">
            <Flex>
              <TextSemiBold fontSize="24px">ข้อมูลส่วนตัว</TextSemiBold>
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  อีเมล &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="email"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("email", {
                        pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
                      })}
                    />
                  )}
                />
                {errors.email && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              {/* <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  Job Position &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="position"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      styles={customStyles}
                      instanceId="position"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      options={dropdownJob}
                    />
                  )}
                />
                {errors.position && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex> */}
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  รหัสพนักงาน &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="emp_id"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("emp_id", {
                        pattern: /^[0-9]*$/,
                      })}
                    />
                  )}
                />
                {errors.emp_id && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  ชื่อ &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="firstname"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("firstname", {
                        pattern: /^[A-Za-zก-๙\s]+$/,
                      })}
                    />
                  )}
                />
                {errors.firstname && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  นามสกุล&nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="lastname"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("lastname", {
                        pattern: /^[A-Za-zก-๙\s]+$/,
                      })}
                    />
                  )}
                />
                {errors.lastname && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  เบอร์โทรศัพท์มือถือ &nbsp;
                </TextSemiBold>

                <Controller
                  name="mobile_number"
                  control={control}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
                {errors.mobile_number && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.3}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  เบอร์โทรศัพท์&nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="tel_number"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
                {errors.tel_number && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.17}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  ต่อ&nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="txt"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
                {errors.txt && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  Role &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="role"
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <Select
                        onChange={(event: any) => {
                          onChangeRole(event);
                          onChange({ value: event.value, label: event.label });
                        }}
                        value={value}
                        placeholder="โปรดเลือก"
                        styles={customStyles}
                        isDisabled={disabledRole}
                        instanceId="role"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        options={dropdownRoles}
                      />
                    );
                  }}
                />
                {errors.role && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              {/* <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  Organization &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="organization"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      styles={customStyles}
                      instanceId="organization"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      options={dropdownOrganization}
                    />
                  )}
                />
                {errors.organization && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex> */}
            </Flex>

            <Flex className="flex-row mt-3">
              {displayManager && (
                <Flex className="flex-column" flex={0.49}>
                  <TextSemiBold
                    fontSize="16px"
                    weight="500"
                    color={Colors.mainBlackLight}
                  >
                    ผู้จัดการ &nbsp;
                  </TextSemiBold>

                  <Controller
                    name="manager"
                    control={control}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <Select
                          onChange={(event: any) => {
                            onChangeManager(event);
                            onChange({
                              value: event.value,
                              label: event.label,
                            });
                          }}
                          value={value}
                          placeholder="โปรดเลือก"
                          styles={customStyles}
                          instanceId="manager"
                          components={{
                            IndicatorSeparator: () => null,
                          }}
                          options={listManager}
                        />
                      );
                    }}
                  />
                  {errors.manager && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </Flex>
              )}
              {displayDirector && displayManager && <Flex flex={0.02} />}
              {displayDirector && (
                <Flex className="flex-column" flex={0.49}>
                  <TextSemiBold
                    fontSize="16px"
                    weight="500"
                    color={Colors.mainBlackLight}
                  >
                    ผู้อำนวยการ (ผอ.)&nbsp;
                  </TextSemiBold>

                  <Controller
                    name="director"
                    control={control}
                    // rules={{ required: true }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="โปรดเลือก"
                        styles={customStyles}
                        isDisabled={disabledDirector}
                        instanceId="director"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        options={listDirector}
                      />
                    )}
                  />
                  {errors.director && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </Flex>
              )}
            </Flex>
          </div>
          <div id="member-section-3" className="mt-5">
            <Flex className="flex-column">
              <TextSemiBold fontSize="16px">สถานะผู้ใช้งาน</TextSemiBold>
              <Flex className="flex-row mt-2">
                <Controller
                  name="status"
                  control={control}
                  render={({ field }) => (
                    <Switch
                      {...field}
                      checked={approve}
                      onChange={() => {
                        setApprove(!approve);
                      }}
                      disabled={disabledStatus}
                      checkedIcon={false}
                      uncheckedIcon={false}
                      onColor={Colors.mainBlue}
                    />
                  )}
                />

                <TextSemiBold
                  fontSize="16px"
                  color={Colors.mainBlack}
                  className="ms-3"
                >
                  อนุมัติการใช้งาน
                </TextSemiBold>
              </Flex>
            </Flex>
          </div>
        </Flex>
        <Flex
          style={{ height: "180px" }}
          className="flex-row justify-content-center align-items-center"
        >
          <ButtonRadius
            onClick={() => Router.back()}
            type="button"
            title="ยกเลิก"
            backgroundColor="transparent"
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            borderColor={Colors.mainDarkBlue}
            borderWidth="1px"
            border="solid"
          />
          <ButtonRadius
            type="submit"
            title="บันทึก"
            backgroundColor={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            border="none"
          />
        </Flex>
      </form>
      <ModalLoading visible={visible} />
    </Wrapper>
  );
};

export default AddMemberInfo;

export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      id: query.id || null,
      mode: query.mode || "add",
    },
  };
}
