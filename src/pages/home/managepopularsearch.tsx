import { useEffect, useState } from "react";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold } from "../../components/text";
import Router from "next/router";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import SearchWithNoFilter from "../../components/Search/SearchWithNoFilter";
import TableMangePopularSearch from "../../components/Table/TableMangePopularSearch";
import HeaderTable from "../../components/Table/HeaderTable";
import ModalPopularSearch from "../../components/Modal/ModalPopularSearch";
import callApi from "../api/callApi";
import Auth from "../auth/index";

const ManagePopularSearch = () => {
  const [show, setShow] = useState(false);
  const [mode, setMode] = useState("add");
  const [searchName, setSearchName] = useState("");
  const [url, setUrl] = useState("");
  const [id, setId] = useState("");
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const genRunningNo = (data: any) => {
    if (data.length > 0) {
      for (var i = 0; i < data.length; ++i) {
        data[i].no = i + 1;
      }

      return data;
    }
    return data;
  };
  const handleShow = () => {
    setShow(true);
    setMode("add");
  };
  const fetchData = async () => {
    const response = await callApi.apiGet("popularsearch");
    setNumData(response.length);
    setData(genRunningNo(response));
  };
  const actionSearch = async (data: string) => {
    const conditionSearch = { search_name: data };
    const response = await callApi.apiPost(
      "popularsearch/search",
      conditionSearch
    );
    setNumData(response.length);
    setData(genRunningNo(response));
  };

  const deleteData = async (id: number) => {
    if (confirm("ยืนยันการลบข้อมูล")) {
      await callApi.apiDelete("popularsearch/" + id);
      fetchData();
    }
  };
  const editData = async (searchName: string, url: string, id: string) => {
    setSearchName(searchName);
    setUrl(url);
    setId(id);
    setMode("edit");
    setShow(true);
  };

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData();
  }, []);
  return (
    <Wrapper
      isLogout={false}
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            Home
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            จัดการคำค้นหายอดนิยม
          </TextBold>
        </>
      }
      titleIcon="/new/images/Back.svg"
      title="จัดการคำค้นหายอดนิยม"
      onClick={() => Router.back()}
    >
      <Flex
        style={{
          backgroundColor: "white",
        }}
        height="auto"
        className="flex-column container py-3"
      >
        <SearchWithNoFilter
          onClickAddBtn={handleShow}
          onClickSearch={actionSearch}
          typeProperty=""
        />
      </Flex>
      <div className="mt-3">
        <HeaderTable number={numData} />
        <TableMangePopularSearch
          data={data}
          onClickDelete={deleteData}
          onClickEdit={editData}
        />
      </div>
      <ModalPopularSearch
        show={show}
        setShow={setShow}
        fetchData={fetchData}
        mode={mode}
        search_name={searchName}
        url={url}
        id={id}
      />
    </Wrapper>
  );
};

export default ManagePopularSearch;
