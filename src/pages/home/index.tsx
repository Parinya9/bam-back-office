import { useEffect, useState } from "react";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import CardHome from "../../components/Card/CardHome";
import { Flex } from "../../components/layout";
import { useDispatch, useSelector } from "react-redux";
import { setInitial } from "../../redux/initial/action";
import Auth from "../auth/index";
import Router, { useRouter } from "next/router";
import Cookies from "universal-cookie";
import callApi from "../api/callApi";
import { NavItem } from "react-bootstrap";

const dataCard = [
  {
    title: "รูปภาพหน้าปก",
    detail: "จัดการ รูปภาพหน้าปก",
    linkPath: "/home/managecoverphoto",
  },
  {
    title: "คำค้นหา ยอดนิยม",
    detail: "จัดการข้อมูล คำค้นหา ยอดนิยม",
    linkPath: "/home/managepopularsearch",
  },
  {
    title: "Banner Slide",
    detail: "จัดการ รูปโฆษณา",
    linkPath: "/home/managebannerslide",
  },
];

const Home = () => {
  const router = useRouter();

  const query = router.query;
  const [listMenu, setListMenu] = useState([]);
  const cookies = new Cookies();
  const role = cookies.get("role");
  const token = cookies.get("token");

  const getMenuByRoles = async () => {
    if (role != undefined) {
      const data = await callApi.apiPost("rolepermission/find", {
        role: role,
      });
      setListMenu(data);
    }
  };
  const checkMenu = (menu: any) => {
    let flag = false;
    listMenu.map((item: any, index: any) => {
      if (menu == item.menu) {
        flag = true;
        return;
      }
    });
    return flag;
  };
  const setCookies = (item: any) => {
    item = JSON.parse(item);
    cookies.set("token", item?.access_token, { maxAge: 60 * 60 * 24 });
    cookies.set("firstname", item?.firstname, { maxAge: 60 * 60 * 24 });
    cookies.set("lastname", item?.lastname, { maxAge: 60 * 60 * 24 });
    cookies.set("role", item.role, { maxAge: 60 * 60 * 24 });
    cookies.set("email", item.email, { maxAge: 60 * 60 * 24 });
    cookies.set("deptCode", item.deptCode, { maxAge: 60 * 60 * 24 });

    router.replace("/home");
  };
  useEffect(() => {
    getMenuByRoles();
    if (query?.item) {
      setCookies(query?.item);
    } else if (token !== undefined) {
    } else {
      Router.push("/");
    }
    // if (Auth()) {
    //   Router.push("/");
    // }
    // dispatch(setInitial("ssss"));
  }, [query?.item]);

  return (
    <div
      className="container"
      style={{
        height: "100vh",
        overflowY: "scroll",
      }}
    >
      <Wrapper
        isLogout={true}
        isDisplayTitle={checkMenu("จัดการ HomePage") ? true : false}
        stepIcon={
          <>
            <img src="new/images/Home2.svg" width="20px" height="20px" />
            <img
              src="new/images/Expand_right_double_light.svg"
              width="20px"
              height="20px"
              className="ms-2"
            />
            <TextBold
              fontSize="16px"
              color={Colors.mainGrayLight}
              className="ms-2"
            >
              Home
            </TextBold>
          </>
        }
        title="จัดการ Homepage"
      >
        {checkMenu("จัดการ HomePage") && (
          <div>
            <TextSemiBold fontSize="24px" color={Colors.mainBlackLight}>
              จัดการข้อมูลหน้า Home
            </TextSemiBold>
            <Flex className="flex-row">
              {dataCard.map((item: any, index: number) => {
                return (
                  <CardHome
                    linkPath={item.linkPath}
                    key={item.title}
                    title={item.title}
                    detail={item.detail}
                    className={index === 0 ? "" : "ms-5"}
                  />
                );
              })}
            </Flex>
          </div>
        )}
      </Wrapper>
    </div>
  );
};
export default Home;
export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      item: query.item || null,
    },
  };
}
