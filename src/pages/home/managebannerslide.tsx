import { useState, useEffect } from "react";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold } from "../../components/text";
import Router from "next/router";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import SearchWithNoFilter from "../../components/Search/SearchWithNoFilter";
import TableBannerSlide from "../../components/Table/TableBannerSlide";
import HeaderTable from "../../components/Table/HeaderTable";
import ModalBannerSlide from "../../components/Modal/ModalBannerSlide";
import callApi from "../api/callApi";
import Auth from "../auth/index";

const ManageBannerSlide = () => {
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [mode, setMode] = useState("add");
  const [itemEdit, setItemEdit] = useState({});
  const handleShow = () => {
    setShow(true);
    setEdit(false);
    setMode("add");
  };

  const editBanner = async (
    id: string,
    img_name: string,
    pdf_name: string,
    link: string,
    start_date: string,
    end_date: string
  ) => {
    setShow(true);
    setEdit(true);
    setMode("edit");
    setItemEdit({
      id: id,
      img_name: img_name,
      pdf_name: pdf_name,
      link: link,
      start_date: start_date,
      end_date: end_date,
    });
    fetchData();
  };
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const genRunningNo = (data: any) => {
    if (data.length > 0) {
      for (var i = 0; i < data.length; ++i) {
        data[i].no = i + 1;
      }

      return data;
    }
    return data;
  };

  const fetchData = async () => {
    const response = await callApi.apiGet("banner/searchAll");
    setNumData(response.length);
    setData(genRunningNo(response));
  };
  const actionSearch = async (data: string) => {
    const conditionSearch = { image_name: data };
    const response = await callApi.apiPost("banner/search", conditionSearch);
    setNumData(response.length);
    setData(genRunningNo(response));
  };

  const deleteData = async (id: number) => {
    if (confirm("ยืนยันการลบข้อมูล")) {
      await callApi.apiDelete("banner/" + id);
      fetchData();
    }
  };

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData();
  }, []);
  return (
    <Wrapper
      isLogout={false}
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            Home
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            จัดการ Banner Slide
          </TextBold>
        </>
      }
      titleIcon="/new/images/Back.svg"
      title="Banner slide"
      onClick={() => Router.back()}
    >
      <Flex
        style={{
          backgroundColor: "white",
        }}
        height="auto"
        className="flex-column container py-3"
      >
        <SearchWithNoFilter
          onClickSearch={actionSearch}
          onClickAddBtn={handleShow}
          typeProperty=""
        />
      </Flex>
      <div className="mt-3">
        {/* <HeaderTable number="3" />
        <TableBannerSlide onClickEditBtn={editBanner} /> */}
        <HeaderTable number={numData} />
        <TableBannerSlide
          data={data}
          onClickDelete={deleteData}
          onClickEdit={editBanner}
        />
      </div>
      <ModalBannerSlide
        show={show}
        setShow={setShow}
        edit={edit}
        fetchData={fetchData}
        mode={mode}
        itemEdit={itemEdit}
      />
    </Wrapper>
  );
};

export default ManageBannerSlide;
