import { useEffect, useRef, useState } from "react";
import Wrapper from "../../components/Layout/Wrapper";

import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import styled from "styled-components";
import ButtonRightSideRadius from "../../components/Button/ButtonRightSideRadius";
import ButtonRadius from "../../components/Button/ButtonRadius";
import Router from "next/router";
import callApi from "../api/callApi";
import Auth from "../auth/index";

const ManageCoverPhoto = () => {
  const [inputEvent, setInputEvent] = useState(null as any);
  const [imageBanner, setImageBanner] = useState("" as any);
  const inputImg: any = useRef<HTMLInputElement>(null);
  const [image, setImage] = useState(null as any);

  const {
    register,
    handleSubmit,
    control,
    watch,
    setValue,
    formState: { errors },
  } = useForm();

  const watchImageUpload = watch("uploadCover");

  const handleImage = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      // 10485760
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue("uploadCover", "");
        setImage("");
        setImageBanner("");
      } else {
        var listType = ["image/png", "image/jpg", "image/jpeg"];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setImage(event.target.files[0]);
          setImageBanner(URL.createObjectURL(event.target.files[0]));
        } else {
          alert("Incorrect Type File (Support png , jpg and jpeg only )");
          setValue("uploadCover", "");
          setImage("");
          setImageBanner("");
        }
      }
    }
  };

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    setData();
  }, []);

  const setData = async () => {
    const item = await callApi.apiGet("coverphoto/");
    if (item.length > 0) {
      setValue("messageCoverPhoto1", item[0].messageCoverPhoto1);
      setValue("messageCoverPhoto2", item[0].messageCoverPhoto2);

      item[0].image_url != null && item[0].image_url.url != undefined
        ? setValue("uploadCover", {
            name: item[0].image_url.name,
          })
        : setValue("uploadCover", "");

      setImageBanner(item[0].image_url != null ? item[0].image_url.url : "");
    }
  };
  const onSubmit = async (data: any) => {
    let formData = new FormData();
    image != null ? formData.append("image", image, image.name) : "";
    formData.append("messageCoverPhoto1", data.messageCoverPhoto1);
    formData.append("messageCoverPhoto2", data.messageCoverPhoto2);
    await callApi.apiPost("coverphoto/", formData);
    alert("Update Success");
  };

  return (
    <Wrapper
      isLogout={false}
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            Home
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            จัดการรูปภาพหน้าปก
          </TextBold>
        </>
      }
      titleIcon="/new/images/Back.svg"
      title="จัดการรูปภาพหน้าปก"
      onClick={() => Router.back()}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Flex
          style={{
            backgroundColor: "white",
          }}
          height="auto"
          className="flex-column container py-3"
        >
          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                ข้อความบนรูป 1
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  &nbsp; *
                </TextSemiBold>
              </TextSemiBold>

              <Controller
                name="messageCoverPhoto1"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    // {...register("messageCoverPhoto1", {
                    //   pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
                    // })}
                  />
                )}
              />
              {errors.messageCoverPhoto1 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex flex={0.02} />
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                ข้อความบนรูป 2
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  &nbsp; *
                </TextSemiBold>
              </TextSemiBold>

              <Controller
                name="messageCoverPhoto2"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                  />
                )}
              />
              {errors.messageCoverPhoto2 && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Flex>
          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="17px"
                weight="normal"
                color={Colors.mainBlackLight}
              >
                เลือกไฟล์
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  &nbsp; *
                </TextSemiBold>
              </TextSemiBold>

              <RadiusBox>
                <Flex widthMobile="100%" className="justify-content-between">
                  <Flex className="ms-3 mt-1">
                    {watchImageUpload?.name || ""}
                  </Flex>
                  <ButtonRightSideRadius
                    onClick={() => inputImg.current.click()}
                    backgroundColor={Colors.mainBlue}
                    title="เลือกไฟล์"
                    width="20%"
                    height="100%"
                    color={Colors.mainWhite}
                  />

                  <Controller
                    name="uploadCover"
                    control={control}
                    rules={{ required: true }}
                    render={({ field: { onChange } }) => (
                      <input
                        onChange={(e: any) => {
                          onChange(e.target.files[0]);
                          handleImage(e);
                        }}
                        type="file"
                        style={{ display: "none" }}
                        id="uploadCover"
                        accept="image/png, image/jpg, image/jpeg"
                        ref={inputImg}
                      />
                    )}
                  />
                </Flex>
              </RadiusBox>
              {errors.uploadCover && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex flex={0.02} />
            <Flex className="flex-column" flex={0.49} />
          </Flex>
          <Flex
            height="200px"
            style={{ backgroundColor: "rgba(0, 146, 255, 0.05)" }}
            className="flex-column justify-content-center align-items-center mt-5 "
          >
            <TextBold fontSize="16px" color={Colors.mainBlue}>
              ขนาดรูปภาพ 1920x500 px
            </TextBold>
            <TextBold fontSize="16px" color={Colors.mainBlue}>
              ไฟล์นามสกุล .png .jpeg ,jpg เท่านั้น และขนาดไฟล์ต้องไม่เกิน 10 MB
            </TextBold>
          </Flex>
          {imageBanner && (
            <Flex
              height="auto"
              className="mt-4"
              style={{
                borderStyle: "solid",
                borderWidth: "1px",
                borderRadius: "10px",
                borderColor: "#CED4DA",
              }}
            >
              <img
                src={imageBanner}
                alt="previewimage"
                width="100%"
                height="100%"
                style={{ borderRadius: "10px" }}
              />
            </Flex>
          )}
        </Flex>

        <Flex
          style={{ height: "180px" }}
          className="flex-row justify-content-center align-items-center"
        >
          <ButtonRadius
            type="button"
            title="ยกเลิก"
            backgroundColor="transparent"
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            borderColor={Colors.mainDarkBlue}
            borderWidth="1px"
            border="solid"
            onClick={() => Router.back()}
          />
          <ButtonRadius
            type="submit"
            title="บันทึก"
            backgroundColor={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            border="none"
          />
        </Flex>
      </form>
    </Wrapper>
  );
};

export default ManageCoverPhoto;

const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;
