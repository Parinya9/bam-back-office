import type { NextPage } from "next";
import Router from "next/router";
import { TextBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import Wrapper from "../../components/Layout/Wrapper";
import SearchProperty from "../../components/Search/SearchProperty";
import HeaderTablePropertySales from "../../components/Table/HeaderTablePropertySales";
import TablePropertyForSales from "../../components/Table/TablePropertyForSales";
import callApi from "../api/callApi";
import Auth from "../auth/index";

const PropertyForSales: NextPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState({});
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const [searchCase, setSearchCase] = useState("");
  const [conditionExport, setConditionExport] = useState([] as any);
  const genRunningNo = (data: any, start: any = 0) => {
    if (data.length > 0) {
      for (var i = 0; i < data.length; ++i) {
        data[i].no = start + i + 1;
      }

      return data;
    }
    return data;
  };
  const fetchData = async (start: any, limit: any) => {
    let item = search;

    Object.assign(item, { start: start, limit: limit });
    setSearch(item);
    setConditionExport(item);

    const response = await callApi.apiPost("property-auction/find", item);

    setData(genRunningNo(response.item, start));
    setTotal(response.total);
  };
  const filterSearch = async (item: any) => {
    Object.assign(item, {
      start: 0,
      limit: 10,
    });
    setSearch(item);
    setConditionExport(item);

    const response = await callApi.apiPost("property-auction/find", item);
    setData(genRunningNo(response.item));
    setTotal(response.total);
  };
  const actionSearch = async (search: string) => {
    const conditionSearch = { caseno: search, start: 0, limit: 10 };
    setSearchCase(search);
    setSearch(conditionSearch);
    setConditionExport(conditionSearch);
    const response = await callApi.apiPost(
      "property-auction/find",
      conditionSearch
    );
    setData(genRunningNo(response.item));
    setTotal(response.total);
  };
  const downloadQR = async (url: any, caseno: any) => {
    const dataQR = { url: url };
    const response = await callApi.apiPost("property-auction/qrcode", dataQR);
    const link = document.createElement("a");
    link.href = response;
    link.setAttribute("download", "Qrcode_" + caseno + ".png");

    document.body.appendChild(link);
    link.click();
  };
  const exportExcel = async () => {
    const response = await callApi.apiExport(
      "property-auction/export",
      conditionExport
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "Property_Auction" + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };

  // const deleteData = async (id: number) => {
  //   const response = await callApi.apiDelete("property-auction/delete/" + id);
  //   fetchData();
  // };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);
  return (
    <Wrapper
      isLogout={true}
      stepIcon={
        <>
          <img src="new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี
          </TextBold>
        </>
      }
      title="ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี"
    >
      <SearchProperty
        isFilter={true}
        typeProperty="propertyForsales"
        onClickSearch={actionSearch}
        onClickExport={exportExcel}
        onClickSearchFilter={filterSearch}
        onClickAddBtn={() =>
          Router.push({
            pathname: "/propertyforsales/addpropertyforsales",
            query: {
              mode: "add",
              title: "เพิ่มข้อมูล - ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี ",
            },
          })
        }
      />
      <HeaderTablePropertySales number={total} caseno={searchCase} />
      <TablePropertyForSales
        data={data}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        fetchData={fetchData}
        total={total}
        setTotal={setTotal}
        onClickDownloadQR={downloadQR}
      />
    </Wrapper>
  );
};

export default PropertyForSales;
