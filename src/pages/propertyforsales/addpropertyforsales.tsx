import React, {
  forwardRef,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from "react";
import InputRadiusAsset from "../../components/Layout/InputRadiusAsset";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import Router from "next/router";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import TextAreaRadius from "../../components/textarea";
import { toggle } from "slidetoggle";
import ButtonRadius from "../../components/Button/ButtonRadius";
import DatePicker from "../../components/Datepicker/DatePicker";
import "react-datepicker/dist/react-datepicker.css";
import styled from "styled-components";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import ButtonRightSideRadius from "../../components/Button/ButtonRightSideRadius";
import Switch from "react-switch";
import callApi from "../api/callApi";
import moment from "moment";
import { apiSyncAssetDetailAuction } from "../api/elsSync";
import Auth from "../auth/index";
import ModalLoading from "../../components/Modal/ModalLoading";

interface AddPropertyForSales {
  mode: string;
  id: string;
}
const AddPropertyForSales: FunctionComponent<AddPropertyForSales> = ({
  id,
  mode,
}) => {
  const {
    control,
    handleSubmit,
    watch,
    setValue,
    register,
    getValues,
    clearErrors,
    setError,
    formState: { errors },
  } = useForm();
  const [approve, setApprove] = useState(false);
  const watchToggle = watch("statusApprove");
  const [imageProperty, setImageProperty] = useState(null as any);
  const [imageMap, setImageMap] = useState(null as any);
  const [inputEvent, setInputEvent] = useState(null as any);
  const [visible, setVisible] = useState(false);
  const [inputEventMap, setInputEventMap] = useState(null as any);
  const [statusField, setStatusField] = useState(false);
  const [txtTitle, setTxtTitle] = useState("");
  const watchImagePropertyUpload = watch("uploadPicProperty");
  const watchImageMapUpload = watch("uploadPicMap");
  const [slideCaseInfo, setSlideCaseInfo] = useState(true);
  const [slidePropertyInfo, setSlidePropertyInfo] = useState(true);
  const [slidePrice, setSlidePrice] = useState(true);
  const [slideSalesInfo, setSlideSalesInfo] = useState(true);
  const [dropdownProvince, setDropdownProvince] = useState([]);
  const [dropdownDistrict, setDropdownDistrict] = useState([]);
  const [dropdownAsset, setDropdownAsset] = useState([]);
  const [disabledDistrict, setDisabledDistrict] = useState(true);
  const [hiddenBtnSubmit, setHiddenBtnSubmit] = useState(false);
  const [priceAppraiser, setPriceAppraiser] = useState("");
  const [priceAppraiserShow, setPriceAppraiserShow] = useState("");
  const [priceRecivorShip, setPriceRecivorShip] = useState("");
  const [priceRecivorShipShow, setpriceRecivorShipShow] = useState("");
  const [priceProfesstional, setPriceProfesstional] = useState("");
  const [priceProfesstionalShow, setPriceProfesstionalShow] = useState("");
  const [priceLegal, setpriceLegal] = useState("");
  const [priceLegalShow, setPriceLegalShow] = useState("");
  const [priceCommittee, setpriceCommittee] = useState("");
  const [priceCommitteeShow, setPriceCommitteeShow] = useState("");

  const [txtBtnCancel, setTxtBtnCancel] = useState("ยกเลิก");
  const inputFile: any = useRef<HTMLInputElement>(null);
  const inputFile1: any = useRef<HTMLInputElement>(null);

  const setPropertyDistrict = async (event: any) => {
    var objData = { province: event.value };
    const listDistrict = await callApi.apiPost(
      "master/District/Dropdown/find",

      objData
    );
    setValue("province", {
      value: event.value,
      label: event.value,
    });
    clearErrors("province");
    setDropdownDistrict(listDistrict);
  };

  const toggleDivCaseInfo = () => {
    setSlideCaseInfo(!slideCaseInfo);
    toggle("div.toggle-divcaseinfo", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const toggleDivPropertyInfo = () => {
    setSlidePropertyInfo(!slidePropertyInfo);
    toggle("div.toggle-divpropertyinfo", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const toggleDivPrice = () => {
    setSlidePrice(!slidePrice);
    toggle("div.toggle-divprice", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };
  const toggleDivSalesInfo = () => {
    setSlideSalesInfo(!slideSalesInfo);
    toggle("div.toggle-divsalesinfo", {
      miliseconds: 300,
      transitionFunction: "ease-in",
      elementDisplayStyle: "inline-block",
    });
  };

  const handlePicProperty = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      // 10485760
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue("uploadPicProperty", {
          name: "",
        });
        setImageProperty("");
      } else {
        var listType = ["image/png", "image/jpg", "image/jpeg"];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setImageProperty(event.target.files[0]);
        } else {
          alert("Incorrect Type File (Support png , jpg and jpeg only )");
          setValue("uploadPicProperty", "");
          setImageProperty("");
        }
      }
    }
  };
  const handlePicMap = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      // 10485760
      if (event.target.files[0].size > 10485760) {
        alert("Limit Upload File 10 MB");
        setValue("uploadImg", {
          name: "",
        });
        setImageMap("");
      } else {
        var listType = ["image/png", "image/jpg", "image/jpeg"];
        var flagType = false;
        for (var index = 0; index < listType.length; ++index) {
          if (event.target.files[0].type == listType[index]) {
            flagType = true;
          }
        }
        if (flagType) {
          setImageMap(event.target.files[0]);
        } else {
          alert("Incorrect Type File (Support png , jpg and jpeg only )");
          setValue("uploadPicMap", "");
          setImageMap("");
        }
      }
    }
  };

  const returnPage = () => {
    Router.back();
  };
  const prePareData = async () => {
    setDropDown();
    if (mode == "add") {
      setTxtTitle("เพิ่มข้อมูล - ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี");
    } else {
      const conditionSearch = { id: id };
      const response = await callApi.apiPost(
        "property-auction/find",
        conditionSearch
      );
      const listDistrict = await callApi.apiPost(
        "master/District/Dropdown/find",

        { province: response.item[0].province }
      );
      setDropdownDistrict(listDistrict);
      setTxtTitle("คดีแดงเลขที่ - " + response.item[0].caseno);
      setApprove(response.item[0].show_display);
      setValue("caseno", response.item[0].caseno);
      setValue("telephone", response.item[0].telephone);
      setValue("work_phone", response.item[0].work_phone);
      setValue("work_phone_nxt", response.item[0].work_phone_nxt);
      setValue("ref_debt", response.item[0].ref_debt);
      setValue("dept_name", response.item[0].dept_name);

      setValue("annoucement", response.item[0].annoucement);
      response.item[0].startdate != null
        ? setValue(
            "startdate",
            new Date(moment(response.item[0].startdate).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].enddate != null
        ? setValue(
            "enddate",
            new Date(moment(response.item[0].enddate).format("yyyy-MM-DD"))
          )
        : "";

      response.item[0].sale_date1 != null
        ? setValue(
            "sale_date1",
            new Date(moment(response.item[0].sale_date1).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date2 != null
        ? setValue(
            "sale_date2",
            new Date(moment(response.item[0].sale_date2).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date3 != null
        ? setValue(
            "sale_date3",
            new Date(moment(response.item[0].sale_date3).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date4 != null
        ? setValue(
            "sale_date4",
            new Date(moment(response.item[0].sale_date4).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date5 != null
        ? setValue(
            "sale_date5",
            new Date(moment(response.item[0].sale_date5).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date1 != null
        ? setValue(
            "sale_date1",
            new Date(moment(response.item[0].sale_date1).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date6 != null
        ? setValue(
            "sale_date6",
            new Date(moment(response.item[0].sale_date6).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date7 != null
        ? setValue(
            "sale_date7",
            new Date(moment(response.item[0].sale_date7).format("yyyy-MM-DD"))
          )
        : "";
      response.item[0].sale_date8 != null
        ? setValue(
            "sale_date8",
            new Date(moment(response.item[0].sale_date8).format("yyyy-MM-DD"))
          )
        : "";
      setValue("asset_url", response.item[0].asset_url);
      setValue("area", response.item[0].area);
      setValue("address", response.item[0].address);
      setValue("contact", response.item[0].contact);
      setValue("licenseno", response.item[0].licenseno);
      setValue("place_auction", response.item[0].place_auction);
      setValue("claimant", response.item[0].claimant);

      setpriceRecivorShipShow(
        parseFloat(
          response.item[0].price_estimate_of_reciveorship
        ).toLocaleString("en-US")
      );
      setPriceProfesstionalShow(
        parseFloat(
          response.item[0].price_estimate_of_professtional
        ).toLocaleString("en-US")
      );
      setPriceLegalShow(
        parseFloat(
          response.item[0].price_estimate_of_legal_officer
        ).toLocaleString("en-US")
      );
      setPriceAppraiserShow(
        parseFloat(response.item[0].price_estimate_of_appraiser).toLocaleString(
          "en-US"
        )
      );
      setPriceCommitteeShow(
        parseFloat(response.item[0].price_set_by_committee).toLocaleString(
          "en-US"
        )
      );

      setValue("condition_bidder", response.item[0].condition_bidder);

      setValue("asset_type", {
        value: response.item[0].asset_type,
        label: response.item[0].asset_type,
      });
      setValue("province", {
        value: response.item[0].province,
        label: response.item[0].province,
      });
      setValue("district", {
        value: response.item[0].district,
        label: response.item[0].district,
      });
      response.item[0].asset_img != null
        ? setValue("uploadPicProperty", {
            name: response.item[0].asset_img.name,
          })
        : "";
      response.item[0].map_img != null
        ? setValue("uploadPicMap", { name: response.item[0].map_img.name })
        : "";

      if (mode == "readonly") {
        setStatusField(true);
        setHiddenBtnSubmit(true);
        setTxtBtnCancel("ปิด");
      } else {
        setStatusField(false);
      }
    }
  };
  const onChangePrice = (e: any, setPrice: any, setPriceShow: any) => {
    let wording = e.target.value;

    if (wording.includes(",")) {
      const data = wording.split("").filter((item: any) => item !== ",");
      wording = data.join("");
    }
    const re = /^(?:\d*\.\d{1,2}|\d+)$/;
    const re1 = /^(?:\d*\.\d{0,2}|\d+)$/;
    if (e.target.value == "" || re1.test(wording)) {
      if (wording !== "") {
        if (re.test(e.target.value)) {
          setPrice(parseFloat(wording).toLocaleString("en-US"));
          let splitStr = wording.split(".");
          if (splitStr.length > 1) {
            if (splitStr[1].length == 0) {
              //15.
              setPriceShow(wording);
            } else {
              //15.1 15.23
              const loadData = parseFloat(wording).toFixed(splitStr[1].length);
              setPriceShow(loadData.toString());
            }
          } else {
            // no digit
            const loadData = parseFloat(wording).toLocaleString("en-US");
            setPriceShow(loadData.toString());
          }
        } else {
          //>1000
          let splitStr = wording.split(".");
          if (splitStr.length > 1 && splitStr[1] === "") {
            //15.
            setPriceShow(parseFloat(wording).toLocaleString("en-US") + ".");
            setPrice(wording);
          } else if (splitStr.length > 1) {
            let str = wording.split(".");
            let num = parseFloat(wording).toFixed(splitStr[1].length);
            let numSplit = num.split(".");
            let numShow =
              parseFloat(str[0]).toLocaleString("en-US") + "." + numSplit[1];
            setPriceShow(numShow);
            setPrice(wording);
          } else {
            setPriceShow(parseFloat(wording).toLocaleString("en-US"));
            setPrice(wording);
          }
        }
      } else {
        setPriceShow("");
        setPrice("");
      }
    }
  };
  const setDropDown = async () => {
    const listProvince = await callApi.apiGet("master/Province/Dropdown");
    const listAsset = await callApi.apiGet("master/AssetType/Dropdown");
    setDropdownProvince(listProvince);
    setDropdownAsset(listAsset);
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    prePareData();
  }, []);

  interface CustomInput {
    onClick: any;
    value: string;
    placeholder: string;
  }
  const CustomInput: FunctionComponent<CustomInput> = forwardRef(
    ({ value, placeholder, onClick }, ref: any) => {
      return (
        <CustomDatePickDiv
          style={{ background: statusField ? "#f8f9fa" : "white" }}
        >
          <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
            {value || placeholder}

            <img
              src="/new/images/Calendar_navyblue.svg"
              width="25px"
              height="25px"
              style={{ float: "right", marginRight: "10px" }}
            />
          </label>
        </CustomDatePickDiv>
      );
    }
  );
  CustomInput.displayName = "CustomInput";

  const onSubmit = async (data: any) => {
    setVisible(true);
    mode == "Edit" ? editData(data) : addData(data);
  };

  const setFormData = (formData: any, key: any, data: any, type: any) => {
    if (type == "date") {
      if (data != undefined) {
        formData.append(key, setFormatDate(data));
      }
    } else if (type == "dropdown") {
      if (data != undefined) {
        formData.append(key, data.value);
      }
    } else {
      if (data != undefined) {
        formData.append(key, data);
      }
    }
  };
  const genFormData = async (formData: any, data: any) => {
    let listDate = [
      "startdate",
      "enddate",
      "sale_date1",
      "sale_date2",
      "sale_date3",
      "sale_date4",
      "sale_date5",
      "sale_date6",
      "sale_date7",
      "sale_date8",
    ];
    let listString = [
      "caseno",
      "asset_url",
      "area",
      "address",
      "contact",
      "licenseno",
      "place_auction",
      "claimant",

      "condition_bidder",
      "telephone",
      "work_phone",
      "work_phone_nxt",
      "ref_debt",
      "dept_name",
    ];
    let listDropDown = ["asset_type", "province", "district"];
    if (!isNaN(parseFloat(priceRecivorShipShow))) {
      formData.append(
        "price_estimate_of_reciveorship",
        parseFloat(priceRecivorShipShow.replace(/,/g, ""))
      );
    }

    if (!isNaN(parseFloat(priceProfesstionalShow))) {
      formData.append(
        "price_estimate_of_professtional",
        parseFloat(priceProfesstionalShow.replace(/,/g, ""))
      );
    }
    if (!isNaN(parseFloat(priceLegalShow))) {
      formData.append(
        "price_estimate_of_legal_officer",
        parseFloat(priceLegalShow.replace(/,/g, ""))
      );
    }

    if (!isNaN(parseFloat(priceCommitteeShow))) {
      formData.append(
        "price_set_by_committee",
        parseFloat(priceCommitteeShow.replace(/,/g, ""))
      );
    }
    if (!isNaN(parseFloat(priceAppraiserShow))) {
      formData.append(
        "price_estimate_of_appraiser",
        parseFloat(priceAppraiserShow.replace(/,/g, ""))
      );
    }

    imageProperty != null
      ? formData.append("asset_image", imageProperty, imageProperty.name)
      : "";
    imageMap != null
      ? formData.append("map_image", imageMap, imageMap.name)
      : "";

    formData.append("show_display", approve ? "true" : "false");

    mode == "Edit" ? formData.append("id", id) : "";

    listDate.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "date");
    });
    listDropDown.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "dropdown");
    });
    listString.map((item: any, index: number) => {
      setFormData(formData, item, data[item], "string");
    });
  };
  const addData = async (data: any) => {
    let formData = new FormData();
    genFormData(formData, data);
    formData.append("annoucement", data.annoucement);

    const item = await callApi.apiPost("property-auction/add", formData);
    if (item.error) {
      setVisible(false);
      alert("Error is " + item.error);
    } else {
      const response = await apiSyncAssetDetailAuction(
        "api/asset-detail-auction"
      );

      if (response.status === "success") {
        // Router.back();
        alert("Update Success");
        setVisible(false);
      } else {
        alert("Can't not update asset");
        setVisible(false);
      }
    }
  };

  const editData = async (data: any) => {
    let formData = new FormData();
    genFormData(formData, data);
    formData.append("annoucement", data.annoucement);
    const item = await callApi.apiPut("property-auction/update", formData);
    if (item.error) {
      setVisible(false);
      alert("Error is " + item.error);
    }

    const response = await apiSyncAssetDetailAuction(
      "api/asset-detail-auction"
    );

    if (response.status === "success") {
      // Router.back();
      alert("Update Success");
      setVisible(false);
    } else {
      alert("Can't not update asset");
      setVisible(false);
    }
  };

  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ทรัพย์ขายทอดตลาดโดยกรมบังคับคดี
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            เพิ่มข้อมูล
          </TextBold>
        </>
      }
      title={txtTitle}
      titleIcon="/new/images/Back.svg"
      onClick={returnPage}
    >
      <div className="row mt-3 mb-4">
        <Flex className="flex-column">
          <Flex className="flex-row mt-2">
            <TextBold
              fontSize="18px"
              color={Colors.mainBlackLight}
              className="ms-2"
              style={{ paddingRight: "2em" }}
            >
              สถานะการแสดงทรัพย์บนหน้าเว็บไซต์
            </TextBold>
            <Controller
              name="statusApprove"
              control={control}
              render={({ field }) => (
                <Switch
                  {...field}
                  disabled={statusField}
                  checked={approve}
                  checkedIcon={false}
                  onChange={() => {
                    setApprove(!approve);
                  }}
                  uncheckedIcon={false}
                  onColor={Colors.mainBlue}
                />
              )}
            />

            <TextSemiBold
              fontSize="16px"
              weight="normal"
              color={Colors.mainBlackLight}
              className="ms-3"
            >
              แสดง
            </TextSemiBold>
          </Flex>
        </Flex>
      </div>

      <form onSubmit={handleSubmit(onSubmit)}>
        <div
          className="container pb-5"
          style={{
            backgroundColor: "white",
            minHeight: "450px",
          }}
        >
          <div>
            {" "}
            <div className="row mt-3 mx-1">
              <div
                style={{
                  borderBottom: " 1px solid #CED4DA",
                  marginTop: "1.5rem",
                  paddingBottom: "0.5rem",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <TextSemiBold fontSize="24px">
                  ข้อมูลคดี{" "}
                  {slideCaseInfo ? (
                    <img
                      src="/new/images/expand.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivCaseInfo}
                    />
                  ) : (
                    <img
                      src="/new/images/collaspe.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivCaseInfo}
                    />
                  )}
                </TextSemiBold>
              </div>
            </div>
            <div className="toggle-divcaseinfo" style={{ width: "100%" }}>
              <div className="row mt-3">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    คดีแดงเลขที่ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="caseno"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        disabled={statusField}
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                      />
                    )}
                  />
                  {errors.caseno && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ชื่อประกาศ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="annoucement"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        disabled={statusField}
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                      />
                    )}
                  />
                  {errors.annoucement && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ เริ่มต้นประกาศ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="startdate"
                        rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ สิ้นสุดประกาศ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="enddate"
                        rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              disabled={statusField}
                              value={value || ""}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div>
            <div className="row mt-3 mx-1">
              <div
                style={{
                  borderBottom: " 1px solid #CED4DA",
                  marginTop: "1.5rem",
                  paddingBottom: "0.5rem",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <TextSemiBold fontSize="24px">
                  ข้อมูลทรัพย์{" "}
                  {slidePropertyInfo ? (
                    <img
                      src="/new/images/expand.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivPropertyInfo}
                    />
                  ) : (
                    <img
                      src="/new/images/collaspe.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivPropertyInfo}
                    />
                  )}
                </TextSemiBold>
              </div>
            </div>
            <div className="toggle-divpropertyinfo" style={{ width: "100%" }}>
              <div className="row mt-3">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    URL ทรัพย์ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="asset_url"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        disabled={statusField}
                        border="solid"
                        {...register("asset_url", {
                          pattern:
                            /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi,
                        })}
                        field={field}
                      />
                    )}
                  />
                  {errors.asset_url && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6 mt-1">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ประเภททรัพย์สิน &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="asset_type"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="โปรดเลือก"
                        isDisabled={statusField}
                        styles={customStyles}
                        instanceId="type"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        options={dropdownAsset}
                      />
                    )}
                  />
                  {errors.asset_type && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    เลขที่เอกสารสิทธิ์ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="licenseno"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        disabled={statusField}
                        border="solid"
                        field={field}
                      />
                    )}
                  />
                  {errors.licenseno && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    เนื้อที่ / พื้นที่ใช้สอย &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="area"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                      />
                    )}
                  />
                  {errors.area && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    รูปทรัพย์ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <RadiusBox>
                    <Flex
                      widthMobile="100%"
                      className="justify-content-between"
                    >
                      <Flex className="ms-3 mt-1">
                        {watchImagePropertyUpload?.name || ""}
                      </Flex>
                      <ButtonRightSideRadius
                        onClick={() => inputFile.current.click()}
                        backgroundColor={Colors.mainBlue}
                        title="เลือกไฟล์"
                        width="20%"
                        height="100%"
                        disable={statusField}
                        color={Colors.mainWhite}
                      />

                      <Controller
                        name="uploadPicProperty"
                        control={control}
                        rules={{ required: true }}
                        render={({ field: { onChange } }) => (
                          <input
                            onChange={(e: any) => {
                              onChange(e.target.files[0]);

                              handlePicProperty(e);
                            }}
                            type="file"
                            accept="image/png, image/jpg, image/jpeg"
                            style={{ display: "none" }}
                            ref={inputFile}
                          />
                        )}
                      />
                    </Flex>
                  </RadiusBox>
                  <TextSemiBold
                    weight="300"
                    fontSize="16px"
                    className="mt-2"
                    style={{ display: "block" }}
                    color={Colors.gray9b}
                  >
                    ไฟล์นามสกุล .png .jpeg ,jpg เท่านั้น และขนาดไฟล์ต้องไม่เกิน
                    10 MB
                  </TextSemiBold>
                  {errors.uploadPicProperty && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณาอัพโหลดรูปภาพ
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    รูปแผนที่ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <RadiusBox>
                    <Flex
                      widthMobile="100%"
                      className="justify-content-between"
                    >
                      <Flex className="ms-3 mt-1">
                        {watchImageMapUpload?.name || ""}
                      </Flex>
                      <ButtonRightSideRadius
                        onClick={() => inputFile1.current.click()}
                        backgroundColor={Colors.mainBlue}
                        title="เลือกไฟล์"
                        width="20%"
                        disable={statusField}
                        height="100%"
                        color={Colors.mainWhite}
                      />

                      <Controller
                        name="uploadPicMap"
                        rules={{ required: true }}
                        control={control}
                        render={({ field: { onChange } }) => (
                          <input
                            onChange={(e: any) => {
                              onChange(e.target.files[0]);

                              handlePicMap(e);
                            }}
                            type="file"
                            style={{ display: "none" }}
                            id="uploadPicMap"
                            accept="image/png, image/jpg, image/jpeg"
                            ref={inputFile1}
                          />
                        )}
                      />
                    </Flex>
                  </RadiusBox>
                  <TextSemiBold
                    weight="300"
                    fontSize="16px"
                    className="mt-2"
                    color={Colors.gray9b}
                    style={{ display: "block" }}
                  >
                    ไฟล์นามสกุล .png .jpeg ,jpg เท่านั้น และขนาดไฟล์ต้องไม่เกิน
                    10 MB
                  </TextSemiBold>
                  {errors.uploadPicMap && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณาอัพโหลดรูปภาพ
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    จังหวัด &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="province"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="โปรดเลือก"
                        styles={customStyles}
                        isDisabled={statusField}
                        instanceId="province"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        onChange={(e) => {
                          setPropertyDistrict(e);
                        }}
                        options={dropdownProvince}
                      />
                    )}
                  />
                  {errors.province && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    อำเภอ / เขต &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="district"
                    rules={{ required: true }}
                    control={control}
                    render={({ field }) => (
                      <Select
                        {...field}
                        placeholder="โปรดเลือก"
                        isDisabled={statusField}
                        styles={customStyles}
                        instanceId="district"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        options={dropdownDistrict}
                      />
                    )}
                  />
                  {errors.district && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ที่ตั้งทรัพย์ &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="address"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <TextAreaRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={statusField}
                        field={field}
                      />
                    )}
                  />
                  {errors.address && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    สถานที่ขายทอดตลาด &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="place_auction"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <TextAreaRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        disabled={statusField}
                        field={field}
                      />
                    )}
                  />
                  {errors.place_auction && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    โจทก์
                  </TextSemiBold>
                  <Controller
                    name="claimant"
                    control={control}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                      />
                    )}
                  />
                  {errors.claimant && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ผู้ประสานงาน &nbsp;
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="contact"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                      />
                    )}
                  />
                  {errors.contact && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    เบอร์โทรศัพท์ &nbsp;
                  </TextSemiBold>
                  <Controller
                    name="telephone"
                    control={control}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                      />
                    )}
                  />
                  {errors.telephone && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-3">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    เบอร์โทรศัพท์สำนักงาน
                  </TextSemiBold>
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                  <Controller
                    name="work_phone"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                      />
                    )}
                  />
                  {errors.work_phone && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div
                  style={{
                    padding: "0px",
                    marginTop: "2rem",
                    flex: "0 0 auto",
                    width: "3%",
                  }}
                >
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    {" "}
                  </TextSemiBold>
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ต่อ
                  </TextSemiBold>
                </div>
                <div className="col-2 mt-4" style={{ paddingLeft: "0px" }}>
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  ></TextSemiBold>
                  <Controller
                    name="work_phone_nxt"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                      />
                    )}
                  />
                  {errors.work_phone_nxt && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ส่วนงานที่รับผิดชอบ
                  </TextSemiBold>
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                  <Controller
                    name="dept_name"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                      />
                    )}
                  />
                  {errors.dept_name && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    อ้างอิงรหัสลูกหนี้
                  </TextSemiBold>
                  <Controller
                    name="ref_debt"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        border="solid"
                        field={field}
                        disabled={statusField}
                        {...register("ref_debt", {
                          pattern: /^[0-9]*$/,
                          maxLength: 6,
                          minLength: 6,
                        })}
                      />
                    )}
                  />
                  {errors.ref_debt && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
              </div>
            </div>
          </div>

          <div>
            <div className="row mt-3 mx-1">
              <div
                style={{
                  borderBottom: " 1px solid #CED4DA",
                  marginTop: "1.5rem",
                  paddingBottom: "0.5rem",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <TextSemiBold fontSize="24px">
                  ราคาประเมินทรัพย์สิน{" "}
                  {slidePrice ? (
                    <img
                      src="/new/images/expand.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivPrice}
                    />
                  ) : (
                    <img
                      src="/new/images/collaspe.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivPrice}
                    />
                  )}
                </TextSemiBold>
              </div>
            </div>
            <div className="toggle-divprice" style={{ width: "100%" }}>
              <div className="row mt-3">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ราคาประเมินโดยเจ้าพนักงานพิทักษ์ทรัพย์ &nbsp;
                  </TextSemiBold>

                  <Controller
                    name="price_estimate_of_reciveorship"
                    control={control}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <InputRadiusAsset
                          width="100%"
                          borderWidth="1px"
                          borderColor="#CED4DA"
                          border="solid"
                          {...register("price_estimate_of_reciveorship", {
                            pattern: /(^[0-9]+(.[0-9]{1,2})?$)|,|(^\s*$)/gm,
                          })}
                          disabled={statusField}
                          value={priceRecivorShipShow}
                          onChange={(event: any) => {
                            onChangePrice(
                              event,
                              setPriceRecivorShip,
                              setpriceRecivorShipShow
                            );
                            onChange(event.target.value);
                          }}
                        />
                      );
                    }}
                  />
                  {errors.price_estimate_of_reciveorship &&
                    getValues("price_estimate_of_reciveorship") != undefined &&
                    getValues("price_estimate_of_reciveorship") != "" && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                </div>
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ราคาประเมินของผู้เชี่ยวชาญ &nbsp;
                  </TextSemiBold>

                  <Controller
                    name="price_estimate_of_professtional"
                    control={control}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <InputRadiusAsset
                          width="100%"
                          borderWidth="1px"
                          borderColor="#CED4DA"
                          border="solid"
                          {...register("price_estimate_of_professtional", {
                            pattern: /(^[0-9]+(.[0-9]{1,2})?$)|,|(^\s*$)/gm,
                          })}
                          disabled={statusField}
                          value={priceProfesstionalShow}
                          onChange={(event: any) => {
                            onChangePrice(
                              event,
                              setPriceProfesstional,
                              setPriceProfesstionalShow
                            );
                            onChange(event.target.value);
                          }}
                        />
                      );
                    }}
                  />
                  {errors.price_estimate_of_professtional &&
                    getValues("price_estimate_of_professtional") != undefined &&
                    getValues("price_estimate_of_professtional") != "" && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ราคาประเมินของเจ้าพนักงานบังคับคดี
                    <TextSemiBold color={Colors.mainRed} fontSize="16px">
                      *
                    </TextSemiBold>
                  </TextSemiBold>
                  <Controller
                    name="price_estimate_of_legal_officer"
                    control={control}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <InputRadiusAsset
                          width="100%"
                          borderWidth="1px"
                          borderColor="#CED4DA"
                          border="solid"
                          {...register("price_estimate_of_legal_officer", {
                            pattern: /(^[0-9]+(.[0-9]{1,2})?$)|,|(^\s*$)/gm,
                          })}
                          disabled={statusField}
                          value={priceLegalShow}
                          onChange={(event: any) => {
                            onChangePrice(
                              event,
                              setpriceLegal,
                              setPriceLegalShow
                            );
                            onChange(event.target.value);
                          }}
                        />
                      );
                    }}
                  />

                  {errors.price_estimate_of_legal_officer && (
                    // getValues("price_estimate_of_legal_officer") != undefined &&
                    // getValues("price_estimate_of_legal_officer") != "" && (
                    <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                      กรุณากรอกข้อมูลให้ถูกต้อง
                    </TextSemiBold>
                  )}
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ราคาประเมินของเจ้าพนักงานประเมินราคาทรัพย์ กรมบังคับคดี
                  </TextSemiBold>
                  <Controller
                    name="price_set_by_committee"
                    control={control}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <InputRadiusAsset
                          width="100%"
                          borderWidth="1px"
                          borderColor="#CED4DA"
                          border="solid"
                          {...register("price_set_by_committee", {
                            pattern: /(^[0-9]+(.[0-9]{1,2})?$)|,|(^\s*$)/gm,
                          })}
                          disabled={statusField}
                          value={priceCommitteeShow}
                          onChange={(event: any) => {
                            onChangePrice(
                              event,
                              setpriceCommittee,
                              setPriceCommitteeShow
                            );
                            onChange(event.target.value);
                          }}
                        />
                      );
                    }}
                  />
                  {errors.price_set_by_committee &&
                    getValues("price_set_by_committee") != undefined &&
                    getValues("price_set_by_committee") != "" && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                </div>
              </div>
              <div className="row mt-3 mb-4">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    ราคาที่กำหนดโดยคณะกรรมการกำหนดราคาทรัพย์
                  </TextSemiBold>
                  <Controller
                    name="price_estimate_of_appraiser"
                    control={control}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <InputRadiusAsset
                          width="100%"
                          borderWidth="1px"
                          borderColor="#CED4DA"
                          border="solid"
                          {...register("price_estimate_of_appraiser", {
                            pattern: /(^[0-9]+(.[0-9]{1,2})?$)|,|(^\s*$)/gm,
                          })}
                          disabled={statusField}
                          value={priceAppraiserShow}
                          onChange={(event: any) => {
                            onChangePrice(
                              event,
                              setPriceAppraiser,
                              setPriceAppraiserShow
                            );
                            onChange(event.target.value);
                          }}
                        />
                      );
                    }}
                  />

                  {errors.price_estimate_of_appraiser &&
                    getValues("price_estimate_of_appraiser") != undefined &&
                    getValues("price_estimate_of_appraiser") != "" && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                </div>
                <div className="col-6">
                  {" "}
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    เงื่อนไขผู้เข้าสู้ราคา
                  </TextSemiBold>
                  <Controller
                    name="condition_bidder"
                    control={control}
                    render={({ field }) => (
                      <InputRadius
                        width="100%"
                        borderWidth="1px"
                        borderColor="#CED4DA"
                        disabled={statusField}
                        border="solid"
                        field={field}
                      />
                    )}
                  />
                  {errors.condition_bidder &&
                    getValues("condition_bidder") != undefined &&
                    getValues("condition_bidder") != "" && (
                      <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                        กรุณากรอกข้อมูลให้ถูกต้อง
                      </TextSemiBold>
                    )}
                </div>
              </div>
            </div>
          </div>

          <div>
            <div className="row mt-3 mx-1">
              <div
                style={{
                  borderBottom: " 1px solid #CED4DA",
                  marginTop: "1.5rem",
                  paddingBottom: "0.5rem",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <TextSemiBold fontSize="24px">
                  ข้อมูลการขายทอดตลาด{" "}
                  {slideSalesInfo ? (
                    <img
                      src="/new/images/expand.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivSalesInfo}
                    />
                  ) : (
                    <img
                      src="/new/images/collaspe.svg"
                      width="20px"
                      height="20px"
                      style={{ float: "right" }}
                      onClick={toggleDivSalesInfo}
                    />
                  )}
                </TextSemiBold>
              </div>
            </div>
            <div className="toggle-divsalesinfo" style={{ width: "100%" }}>
              <div className="row mt-3">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 1 &nbsp;
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date1"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 5
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date5"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
              </div>
              <div className="row mt-3">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 2 &nbsp;
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date2"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 6
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date6"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
              </div>
              <div className="row mt-3">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 3 &nbsp;
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date3"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 7
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date7"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
              </div>
              <div className="row mt-3">
                {" "}
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 4 &nbsp;
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date4"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
                <div className="col-6">
                  <TextSemiBold
                    fontSize="16px"
                    weight="normal"
                    color={Colors.mainBlackLight}
                  >
                    วันที่ ขายนัดที่ 8
                  </TextSemiBold>
                  <div className="mt-1 ">
                    <DatePickerDiv>
                      <Controller
                        control={control}
                        name="sale_date8"
                        // rules={{ required: true }}
                        render={({
                          field: { onChange, name, value },
                          formState: { errors },
                        }) => (
                          <>
                            <DatePicker
                              value={value || ""}
                              disabled={statusField}
                              onchange={(date: any) => {
                                onChange(date);
                              }}
                            />
                            {errors &&
                              errors[name] &&
                              errors[name].type === "required" && (
                                <TextSemiBold
                                  fontSize="14px"
                                  color={Colors.mainRed}
                                >
                                  กรุณากรอกข้อมูลให้ถูกต้อง
                                </TextSemiBold>
                              )}
                          </>
                        )}
                      />
                    </DatePickerDiv>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div></div>
        </div>

        <Flex
          style={{ height: "180px" }}
          className="flex-row justify-content-center align-items-center"
        >
          <ButtonRadius
            type="button"
            title={txtBtnCancel}
            backgroundColor="transparent"
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            borderColor={Colors.mainDarkBlue}
            borderWidth="1px"
            border="solid"
            onClick={returnPage}
          />
          {hiddenBtnSubmit ? (
            ""
          ) : (
            <ButtonRadius
              type="submit"
              title="บันทึก"
              backgroundColor={Colors.mainYellow}
              color={Colors.mainDarkBlue}
              width="200px"
              fontSize="18px"
              className="mx-1 mt-1 mt-3 py-2"
              border="none"
            />
          )}
        </Flex>
      </form>
      <ModalLoading visible={visible} />
    </Wrapper>
  );
};

export default AddPropertyForSales;
const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
const RadiusBox = styled.div.attrs(() => ({
  className: "d-flex  mt-2 flex-row",
}))`
  background-color: #ffff;
  width: 100%;
  height: 35px;
  border-radius: 60px;
  border-style: solid;
  border-width: 1px;
  border-color: #ced4da;
`;

export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      id: query.id || null,
      mode: query.mode || "add",
    },
  };
}
