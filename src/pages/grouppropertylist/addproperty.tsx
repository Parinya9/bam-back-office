import React, { FunctionComponent, useEffect, useState } from "react";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import Router from "next/router";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import { toggle } from "slidetoggle";
import ButtonRadius from "../../components/Button/ButtonRadius";
import callApi from "../api/callApi";
import Auth from "../auth/index";
import { apiSyncAssetDetail } from "../api/elsSync";
import ModalLoading from "../../components/Modal/ModalLoading";

interface AddProperty {
  id: string;
  asset_name_th: string;
  asset_name_en: string;
  asset_no: string;
  group: string;
  mode: string;
}

const AddProperty: FunctionComponent<AddProperty> = ({
  id,
  asset_name_th,
  asset_name_en,
  asset_no,
  group,
  mode,
}) => {
  const {
    register,
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();
  const [visible, setVisible] = useState(false);

  const [dropdownCategory, setDropDownCategory] = useState([]);
  const [title, setTitle] = useState("เพิ่มข้อมูล - ประเภททรัพย์");
  const [wrapText, setWrapText] = useState("เพิ่มข้อมูลประเภททรัพย์");
  const [oldAsset, setOldAsset] = useState("");
  const [slide, setSlide] = useState(true);
  const returnPage = () => {
    Router.push("/grouppropertylist");
  };
  const onSubmit = (data: any) => {
    mode == "Edit" ? editData(data) : addData(data);
  };
  const addData = async (data: any) => {
    const objAdd = {
      asset_name_th: data.asset_name_th,
      asset_name_en: data.asset_name_en,
      asset_no: data.asset_no,
      group: data.group.value,
    };
    const item = await callApi.apiPost("property-list/add", objAdd);
    if (item.id != undefined) {
      setVisible(true);
      setTimeout(async () => {
        const resAssetType = await apiSyncAssetDetail(
          "api/mastersearchasset/assetType"
        );

        if (resAssetType.status === "success") {
          alert("Update Success");
          setVisible(false);

          returnPage();
        } else {
          setVisible(false);

          alert("Cant'not update asset");
        }
      }, 1500);
    } else {
      alert("Duplicate Asset No");
    }
  };
  const editData = async (data: any) => {
    const objEdit = {
      asset_name_th: data.asset_name_th,
      asset_name_en: data.asset_name_en,
      asset_no: data.asset_no,
      old_asset: oldAsset,
      id: parseInt(id),
      group: data.group.value,
    };
    const item = await callApi.apiPut("property-list/update", objEdit);
    if (item.affected == 1) {
      setTimeout(async () => {
        const resAssetType = await apiSyncAssetDetail(
          "api/mastersearchasset/assetType"
        );

        if (resAssetType.status === "success") {
          alert("Update Success");
          returnPage();
        } else {
          alert("Cant'not update asset");
        }
      }, 1500);
    } else {
      alert("Duplicate Asset No");
    }
  };

  const genDropDownCategory = async () => {
    const item = await callApi.apiGet("master/category/dropdown");
    setDropDownCategory(item);
  };

  const toggleDiv = () => {
    setSlide(!slide);
    toggle("div.toggle-div", {
      miliseconds: 300,
      transitionFunction: "ease-in",

      elementDisplayStyle: "inline-block",
    });
  };
  const setData = () => {
    setOldAsset(asset_name_th);
    setValue("asset_name_th", asset_name_th);
    setValue("asset_name_en", asset_name_en);
    setValue("group", { label: group, value: group });
    setValue("asset_no", asset_no);
    setTitle("แก้ไขข้อมูล - ประเภททรัพย์");
    setWrapText("แก้ไขข้อมูล");
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    genDropDownCategory();
    mode == "Edit" ? setData() : "";
  }, []);
  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลประเภททรัพย์
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            {wrapText}
          </TextBold>
        </>
      }
      title={title}
      titleIcon="/new/images/Back.svg"
      onClick={returnPage}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <div
          className="container"
          style={{
            backgroundColor: "white",
            height: "450px",
            overflowY: "auto",
          }}
        >
          <div className="row mt-3 mx-1">
            <div
              style={{
                borderBottom: " 1px solid #CED4DA",
                marginTop: "1.5rem",
                paddingBottom: "0.5rem",
                paddingLeft: "0px",
                paddingRight: "0px",
              }}
            >
              <TextSemiBold fontSize="24px">
                ข้อมูลประเภททรัพย์{" "}
                {slide ? (
                  <img
                    src="/new/images/expand.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDiv}
                  />
                ) : (
                  <img
                    src="/new/images/collaspe.svg"
                    width="20px"
                    height="20px"
                    style={{ float: "right" }}
                    onClick={toggleDiv}
                  />
                )}
              </TextSemiBold>
            </div>
          </div>
          <div className="toggle-div" style={{ width: "100%" }}>
            <div className="row mt-3">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  รหัสประเภททรัพย์สิน &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="asset_no"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
                {errors.asset_no && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  หมวดหมู่หลัก &nbsp;
                </TextSemiBold>
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
                <Controller
                  name="group"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      styles={customStyles}
                      instanceId="type"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      options={dropdownCategory}
                    />
                  )}
                />
                {errors.group && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
            <div className="row mt-3 mb-4">
              {" "}
              <div className="col-6">
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ชื่อประเภททรัพย์สิน (TH) &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="asset_name_th"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
                {errors.asset_name_th && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
              <div className="col-6">
                {" "}
                <TextSemiBold
                  fontSize="16px"
                  weight="normal"
                  color={Colors.mainBlackLight}
                >
                  ชื่อประเภททรัพย์สิน (EN) &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>
                <Controller
                  name="asset_name_en"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("asset_name_en", {
                        pattern:
                          /^[a-zA-Z0-9!@#\$%\^\&*\)\(+._-\{\}\[\]\~\-\?\<\>\,\/\'\"\:\;\\\|\=\ ]+$/,
                      })}
                    />
                  )}
                />
                {errors.asset_name_en && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </div>
            </div>
          </div>
        </div>
        <Flex
          style={{ height: "180px" }}
          className="flex-row justify-content-center align-items-center"
        >
          <ButtonRadius
            type="button"
            title="ยกเลิก"
            backgroundColor="transparent"
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            borderColor={Colors.mainDarkBlue}
            borderWidth="1px"
            border="solid"
            onClick={returnPage}
          />
          <ButtonRadius
            type="submit"
            title="บันทึก"
            backgroundColor={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            border="none"
          />
        </Flex>
      </form>
      <ModalLoading visible={visible} />
    </Wrapper>
  );
};

export default AddProperty;
export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      id: query.id || null,
      mode: query.mode || "add",
      asset_name_th: query.asset_name_th || null,
      asset_name_en: query.asset_name_en || null,
      group: query.group || null,
      asset_no: query.asset_no || true,
    },
  };
}
