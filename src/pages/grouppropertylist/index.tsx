import type { NextPage } from "next";
import Router from "next/router";
import { TextBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import Wrapper from "../../components/Layout/Wrapper";
import SearchProperty from "../../components/Search/SearchProperty";
import HeaderTable from "../../components/Table/HeaderTable";
import TablePropertyList from "../../components/Table/TablePropertyList";
import callApi from "../api/callApi";
import Auth from "../auth/index";
import { apiSyncAssetDetail } from "../api/elsSync";
import ModalLoading from "../../components/Modal/ModalLoading";

const GroupPropertyList: NextPage = () => {
  const [visible, setVisible] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState({});
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const [conditionExport, setConditionExport] = useState([] as any);

  const fetchData = async (start: any, limit: any) => {
    let item = search;
    Object.assign(item, {
      start: start,
      limit: limit,
    });
    setSearch(item);
    const response = await callApi.apiPost("property-list/find", item);
    setData(response.item);
    setTotal(response.total);
  };
  const actionSearch = async (data: string) => {
    let conditionSearch = {
      asset_name_th: data,
      asset_name_en: data,
      group: data,
    };
    Object.assign(conditionSearch, { start: 0, limit: 10 });
    setSearch(conditionSearch);
    setConditionExport(conditionSearch);
    const response = await callApi.apiPost(
      "property-list/find",
      conditionSearch
    );
    setData(response.item);
    setTotal(response.total);
  };

  const exportExcel = async () => {
    const response = await callApi.apiExport(
      "property-list/export",
      conditionExport
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "Propertylist" + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };

  const deleteData = async (id: number) => {
    if (confirm("ยืนยันการลบข้อมูล")) {
      setVisible(true);
      await callApi.apiDelete("property-list/delete/" + id);
      setTimeout(async () => {
        const resAssetType = await apiSyncAssetDetail(
          "api/mastersearchasset/assetType"
        );
        if (resAssetType.status === "success") {
          setVisible(false);
          alert("Update Success");
        } else {
          setVisible(false);
          alert("Cant'not update asset");
        }
        fetchData((currentPage - 1) * 10, 10);
      }, 1500);
    }
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);
  return (
    <Wrapper
      isLogout={true}
      stepIcon={
        <>
          <img src="new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลประเภททรัพย์
          </TextBold>
        </>
      }
      title="ข้อมูลประเภททรัพย์"
    >
      <SearchProperty
        onClickAddBtn={() => Router.push("/grouppropertylist/addproperty")}
        onClickSearch={actionSearch}
        onClickExport={exportExcel}
        onClickSearchFilter={() => {}}
      />
      <HeaderTable number={total.toString()} />
      <TablePropertyList
        data={data}
        onClickDelete={deleteData}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        fetchData={fetchData}
        total={total}
        setTotal={setTotal}
      />

      <ModalLoading visible={visible} />
    </Wrapper>
  );
};

export default GroupPropertyList;
