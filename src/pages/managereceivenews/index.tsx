import type { NextPage } from "next";
import Router from "next/router";
import { TextBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import Wrapper from "../../components/Layout/Wrapper";
import SearchProperty from "../../components/Search/SearchProperty";
import HeaderTable from "../../components/Table/HeaderTable";
import callApi from "../api/callApi";
import Auth from "../auth/index";
import TableManageReceiveNews from "../../components/Table/TableManageReceiveNews";

const ManageReceiveNews: NextPage = () => {
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [conditionExport, setConditionExport] = useState([] as any);
  const [search, setSearch] = useState({});
  const fetchData = async (start: any, limit: any) => {
    var item = search;
    Object.assign(item, { start: start, limit: limit });
    setSearch(item);
    const response = await callApi.apiPost("news/find", item);
    if (response.length > 0) {
      setData(response);
      setTotal(response[0].total);
    }
  };

  const actionSearch = async (data: string) => {
    var item = { fullname: data != undefined ? data : "", start: 0, limit: 10 };
    setSearch(item);
    setConditionExport(item);
    const response = await callApi.apiPost("news/find", item);
    setData(response);
    setTotal(response[0].total);
    setCurrentPage(1);
  };
  const filterSearch = async (data: any) => {
    var item = data;
    Object.assign(item, { start: 0, limit: 5 });
    setSearch(item);
    setConditionExport(item);

    const response = await callApi.apiPost("news/find", item);
    if (response.length != 0) {
      setData(response);
      setTotal(response[0].total);
    } else {
      setData([]);
      setTotal(0);
    }
    setCurrentPage(1);
  };
  const setDataForExport = (item: any) => {
    // var item = search;
    // Object.assign(item, { start: 0, limit: total });
    // const response = await callApi.apiPost("property-detail/find", item);
    var listDataExport = [];
    // for (var i = 0; i < item.length; ++i) {
    var objData = {
      ประเภททรัพย์สิน: item["npa_type"],
      รหัสทรัพย์สิน: item["market_code"],
      ชื่อทรัพย์สิน: item["project_th"],
      ชื่อผู้ดูแล: item["ao_name"],
      ละติจูด: item["gps_lat1"],
      ลองติจูด: item["gps_long1"],
      อำเภอ: item["city_name"],
      จังหวัด: item["province_name"],
      ราคา: item["center_price"],
      สถานะทรัพย์สิน: item["asset_state"],
    };

    listDataExport.push(objData);
    // }
    return listDataExport;
  };
  const exportExcel = async () => {
    const response = await callApi.apiExport("news/export", conditionExport);
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "MemberReceiveNews" + ".xlsx");
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);
  return (
    <Wrapper
      isLogout={true}
      stepIcon={
        <>
          <img src="new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลผู้รับข่าวสาร
          </TextBold>
        </>
      }
      title="ข้อมูลผู้รับข่าวสาร"
    >
      <SearchProperty
        isFilter={false}
        typeProperty="searchMemberInfo"
        onClickAddBtn={false}
        onClickSearch={actionSearch}
        onClickExport={exportExcel}
        onClickSearchFilter={filterSearch}
      />
      <HeaderTable number={total.toString()} />
      <TableManageReceiveNews
        data={data}
        total={total}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        fetchData={fetchData}
        setTotal={setTotal}
        setData={setData}
      />
    </Wrapper>
  );
};

export default ManageReceiveNews;
