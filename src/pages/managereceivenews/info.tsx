import React, {
  useState,
  useEffect,
  forwardRef,
  FunctionComponent,
} from "react";
import Router from "next/router";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import Switch from "react-switch";
import ButtonRadius from "../../components/Button/ButtonRadius";
import styled from "styled-components";
import callApi from "../api/callApi";
import dayjs from "dayjs";
import DatePicker from "../../components/Datepicker/DatePicker";
// interface CustomInput {
//   onClick: any;
//   value: string;
//   placeholder: string;
// }
interface Info {
  id: any;
  mode: string;
}
// const CustomInput: FunctionComponent<CustomInput> = forwardRef(
//   ({ value, placeholder, onClick }, ref: any) => {
//     return (
//       <CustomDatePickDiv>
//         <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
//           {value || placeholder}

//           <img
//             src="/new/images/Calendar_navyblue.svg"
//             width="25px"
//             height="25px"
//             style={{ float: "right", marginRight: "10px" }}
//           />
//         </label>
//       </CustomDatePickDiv>
//     );
//   }
// );
// CustomInput.displayName = "CustomInput";

const Info: FunctionComponent<Info> = ({ id, mode }) => {
  const {
    register,
    handleSubmit,
    control,
    watch,
    setValue,
    getValues,
    clearErrors,
    formState: { errors },
  } = useForm();
  const [title, setTitle] = useState("รายละเอียด");
  const [disabledField, setDisplayField] = useState(true);

  const setData = async () => {
    const conditionSearch = { id: id };
    const response = await callApi.apiPost("news/find", conditionSearch);
    if (response.length > 0) {
      setValue("firstname", response[0].firstname);
      setValue("lastname", response[0].lastname);
      setValue("phone", response[0].telephone);
      setValue("email", response[0].email);
      setValue("address", response[0].address);
      setValue("price_range", response[0].price_range);
      setValue("province", response[0].province);
      setValue("district", response[0].district);
      setValue("asset_type", response[0].asset_type);
      setValue(
        "create_date",
        dayjs(response[0].create_date.replace("Z", "").replace("T", "")).format(
          "DD/MM/YYYY HH:mm"
        )
      );
    }
  };

  useEffect(() => {
    mode == "View" ? setData() : "";
  }, []);

  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลผู้รับข่าวสาร
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            รายละเอียด
          </TextBold>
        </>
      }
      title={title}
    >
      <Flex
        style={{
          backgroundColor: "white",
        }}
        height="auto"
        className="flex-column container py-3"
      >
        <div id="member-section2" className="mt-1">
          <Flex>
            <TextSemiBold fontSize="24px">ข้อมูลส่วนตัว</TextSemiBold>
          </Flex>

          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                ชื่อ สมาชิก
              </TextSemiBold>

              <Controller
                name="firstname"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
              {errors.firstname && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex flex={0.02} />
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                นามสกุล สมาชิก
              </TextSemiBold>

              <Controller
                name="lastname"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    disabled={disabledField}
                    field={field}
                  />
                )}
              />
              {errors.lastname && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Flex>

          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                อีเมล
              </TextSemiBold>

              <Controller
                name="email"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
              {errors.id_card && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex flex={0.02} />
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                เบอร์โทรศัพท์
              </TextSemiBold>

              <Controller
                name="phone"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
              {errors.phone && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Flex>

          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                ประเภททรัพย์สิน
              </TextSemiBold>

              <Controller
                name="asset_type"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
            </Flex>
            <Flex flex={0.02} />
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                ช่วงราคา
              </TextSemiBold>

              <Controller
                name="price_range"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
            </Flex>
          </Flex>

          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                จังหวัด
              </TextSemiBold>

              <Controller
                name="province"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
            </Flex>
            <Flex flex={0.02} />
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                อำเภอ
              </TextSemiBold>

              <Controller
                name="district"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
            </Flex>
          </Flex>
          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={0.49}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                วันที่ติดต่อ
              </TextSemiBold>

              <Controller
                name="create_date"
                control={control}
                render={({ field }) => (
                  <InputRadius
                    width="100%"
                    borderWidth="1px"
                    borderColor="#CED4DA"
                    border="solid"
                    field={field}
                    disabled={disabledField}
                  />
                )}
              />
            </Flex>
            <Flex flex={0.02} />
          </Flex>
          <Flex className="flex-row mt-3">
            <Flex className="flex-column" flex={1}>
              <TextSemiBold
                fontSize="16px"
                weight="500"
                color={Colors.mainBlackLight}
              >
                ที่อยู่
              </TextSemiBold>

              <Controller
                name="address"
                control={control}
                render={({ field }) => (
                  <textarea
                    {...field}
                    style={{
                      borderRadius: 10,
                      marginTop: 5,
                      height: "70px",
                      borderWidth: "1px",
                      borderColor: "#CED4DA",
                    }}
                    disabled={disabledField}
                  />
                )}
              />
              {errors.address && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
          </Flex>
        </div>
      </Flex>
      <Flex
        style={{ height: "180px" }}
        className="flex-row justify-content-center align-items-center"
      >
        <ButtonRadius
          onClick={() => Router.back()}
          type="button"
          title="ย้อนกลับ"
          backgroundColor="transparent"
          color={Colors.mainDarkBlue}
          width="200px"
          fontSize="18px"
          className="mx-1 mt-1 mt-3 py-2"
          borderColor={Colors.mainDarkBlue}
          borderWidth="1px"
          border="solid"
        />
      </Flex>
    </Wrapper>
  );
};

export default Info;

export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      id: query.id || null,
      mode: query.mode || "add",
    },
  };
}
