import React, {
  useState,
  useEffect,
  forwardRef,
  FunctionComponent,
} from "react";
import Router from "next/router";
import Wrapper from "../../components/Layout/Wrapper";
import { TextBold, TextSemiBold } from "../../components/text";
import Colors from "../../../utils/Colors";
import { Flex } from "../../components/layout";
import { useForm, Controller } from "react-hook-form";
import InputRadius from "../../components/Input";
import Select from "react-select";
import { customStyles } from "../../../utils/ReactSelectStyle";
import Switch from "react-switch";
import ButtonRadius from "../../components/Button/ButtonRadius";
import styled from "styled-components";
import callApi from "../api/callApi";
import moment from "moment";
import DatePicker from "../../components/Datepicker/DatePicker";
import Auth from "../auth/index";
import Cookies from "universal-cookie";

interface CustomInput {
  onClick: any;
  value: string;
  placeholder: string;
}
interface AddMemberInfo {
  id: string;
  mode: string;
}
const CustomInput: FunctionComponent<CustomInput> = forwardRef(
  ({ value, placeholder, onClick }, ref: any) => {
    return (
      <CustomDatePickDiv>
        <label style={{ width: "100%" }} onClick={onClick} ref={ref}>
          {value || placeholder}

          <img
            src="/new/images/Calendar_navyblue.svg"
            width="25px"
            height="25px"
            style={{ float: "right", marginRight: "10px" }}
          />
        </label>
      </CustomDatePickDiv>
    );
  }
);
CustomInput.displayName = "CustomInput";

const AddMemberInfo: FunctionComponent<AddMemberInfo> = ({ id, mode }) => {
  const {
    register,
    handleSubmit,
    control,
    watch,
    setValue,
    getValues,
    clearErrors,
    formState: { errors },
  } = useForm();
  const [radioGender, setRadioGender] = useState("");
  const [title, setTitle] = useState("เพิ่มข้อมูลสมาชิก");
  const [approve, setApprove] = useState(false);
  const [dropdownStatusMarry, setDropdownStatusMarry] = useState([]);
  const cookies = new Cookies();
  const token = cookies.get("token");
  const onChangeRadio = (event: any) => {
    setValue("gender", event.target.value);
    setRadioGender(event.target.value);
    clearErrors("gender");
  };

  const onSubmit = async (data: any) => {
    mode == "Edit" ? editData(data) : addData(data);
  };
  const editData = async (data: any) => {
    data.status = approve;
    data.gender = radioGender;
    data.birthday = setFormatDate(data.birthday);

    if (data.marry_status != undefined) {
      data.marry_status = data.marry_status.value;
    }
    data.occupation = data.occupation;
    data.id = parseInt(id);
    const item = await callApi.apiPut("member/update", data, token);
    if (item.affected == 1) {
      Router.back();
    } else {
      alert("Duplicate email");
    }
  };
  const addData = async (data: any) => {
    data.status = approve;
    data.gender = radioGender;
    data.birthday = setFormatDate(data.birthday);

    if (data.marry_status != undefined) {
      data.marry_status = data.marry_status.value;
    }
    data.occupation = data.occupation;

    const item = await callApi.apiPost("member/register", data, token);
    if (item.error == undefined) {
      Router.back();
    } else {
      alert("Duplicate email");
    }
  };
  const setFormatDate = (date: any) => {
    if (date == undefined) {
      return "";
    }
    //formtdate -
    if (date.length <= 10) {
      var dateSplit = date.split("/");
      return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
    // in case length == undefined
    return moment(date).format("yyyy-MM-DD");
  };
  const setData = async () => {
    const conditionSearch = { id: id, fullname: "" };
    const response = await callApi.apiPost(
      "member/find",
      conditionSearch,
      token
    );

    if (response.item.length > 0) {
      setValue("email", response.item[0].email);
      setValue("password", response.item[0].password);
      setValue("firstname", response.item[0].firstname);
      setValue("lastname", response.item[0].lastname);
      setValue("phone", response.item[0].phone);
      setValue("id_card", response.item[0].id_card);
      setValue("address", response.item[0].address);
      setValue(
        "birthday",
        moment(response.item[0].birthday).format("DD/MM/YYYY")
      );
      setValue("marry_status", {
        label: response.item[0].marry_status,
        value: response.item[0].marry_status,
      });
      setValue("occupation", response.item[0].occupation);
      setRadioGender(response.item[0].gender);
      setValue("gender", response.item[0].gender);

      setTitle(response.item[0].refno);
      setApprove(response.item[0].status);
    }

    // setApprove(show_display);
  };
  const setDropDown = async () => {
    const item = await callApi.apiGet("master/statusmarry");
    setDropdownStatusMarry(item);
  };
  const watchToggle = watch("status");

  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    setDropDown();
    mode == "Edit" ? setData() : "";
  }, []);

  return (
    <Wrapper
      stepIcon={
        <>
          <img src="/new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            เพิ่มข้อมูลสมาชิก
          </TextBold>
          <img
            src="/new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            เพิ่มข้อมูลสมาชิก
          </TextBold>
        </>
      }
      title={title}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Flex
          style={{
            backgroundColor: "white",
          }}
          height="auto"
          className="flex-column container py-3"
        >
          <div id="member-section1">
            <Flex>
              <TextSemiBold fontSize="24px">
                ตั้งชื่อผู้ใช้งาน และ รหัสผ่าน
              </TextSemiBold>
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  ชื่อในการเข้าสู่ระบบ (อีเมล) &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="email"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("email", {
                        pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
                      })}
                    />
                  )}
                />
                {errors.email && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  รหัสผ่าน &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="password"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("password", {
                        required: true,
                        minLength: 8,
                        pattern:
                          /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -/:-@\[-`{-~]).{6,64}$/,
                      })}
                    />
                  )}
                />
                {errors.password && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>
          </div>
          <div id="member-section2" className="mt-5">
            <Flex>
              <TextSemiBold fontSize="24px">ข้อมูลส่วนตัว</TextSemiBold>
            </Flex>
            <Flex className="flex-column">
              <TextSemiBold fontSize="16px" color={Colors.mainBlackLight}>
                เพศ &nbsp;
                <TextSemiBold color={Colors.mainRed} fontSize="16px">
                  *
                </TextSemiBold>
              </TextSemiBold>
              <Controller
                name="gender"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Flex className="flex-row mt-3">
                    <Flex width="100px">
                      <input
                        className="form-check-input"
                        type="radio"
                        value="ชาย"
                        checked={radioGender === "ชาย"}
                        onChange={onChangeRadio}
                        aria-label="..."
                        width="100%"
                      />
                      <TextSemiBold fontSize="16px" className="ms-3">
                        ชาย
                      </TextSemiBold>
                    </Flex>
                    <Flex width="100px">
                      <input
                        className="form-check-input"
                        type="radio"
                        value="หญิง"
                        checked={radioGender === "หญิง"}
                        onChange={onChangeRadio}
                        aria-label="..."
                      />
                      <TextSemiBold fontSize="16px" className="ms-3">
                        หญิง
                      </TextSemiBold>
                    </Flex>
                  </Flex>
                )}
              />

              {errors.gender && (
                <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                  กรุณากรอกข้อมูลให้ถูกต้อง
                </TextSemiBold>
              )}
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  ชื่อ สมาชิก &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="firstname"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("firstname", {
                        pattern: /^[A-Za-zก-๙\s]+$/,
                      })}
                    />
                  )}
                />
                {errors.firstname && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  นามสกุล สมาชิก &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="lastname"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("lastname", {
                        pattern: /^[A-Za-zก-๙\s]+$/,
                      })}
                    />
                  )}
                />
                {errors.lastname && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>

            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  เลขบัตรประชาชน &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="id_card"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("id_card", {
                        maxLength: 13,
                        minLength: 13,
                        pattern: /^[0-9]*$/,
                      })}
                    />
                  )}
                />
                {errors.id_card && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  เบอร์โทรศัพท์ &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="phone"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                      {...register("phone", {
                        maxLength: 10,
                        minLength: 10,
                        pattern: /^[0-9]*$/,
                      })}
                    />
                  )}
                />
                {errors.phone && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  วัน/เดือน/ปี ที่เกิด &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <div className="mt-1" style={{ borderRadius: "50px" }}>
                  <DatePickerDiv>
                    <Controller
                      control={control}
                      name="birthday"
                      rules={{ required: true }}
                      render={({
                        field: { onChange, name, value },
                        formState: { errors },
                      }) => (
                        <>
                          <DatePicker
                            value={value || ""}
                            onchange={(date: any) => {
                              onChange(date);
                            }}
                          />

                          {errors &&
                            errors[name] &&
                            errors[name].type === "required" && (
                              <TextSemiBold
                                fontSize="14px"
                                color={Colors.mainRed}
                              >
                                กรุณากรอกข้อมูลให้ถูกต้อง
                              </TextSemiBold>
                            )}
                        </>
                      )}
                    />
                  </DatePickerDiv>
                </div>
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  สถานะการแต่งงาน &nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="marry_status"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="โปรดเลือก"
                      styles={customStyles}
                      instanceId="marry_status"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      options={dropdownStatusMarry}
                    />
                  )}
                />
                {errors.marry_status && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>
            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={0.49}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  อาชีพ&nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="occupation"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <InputRadius
                      width="100%"
                      borderWidth="1px"
                      borderColor="#CED4DA"
                      border="solid"
                      field={field}
                    />
                  )}
                />
                {errors.occupation && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
              <Flex flex={0.02} />
              <Flex className="flex-column" flex={0.49} />
            </Flex>

            <Flex className="flex-row mt-3">
              <Flex className="flex-column" flex={1}>
                <TextSemiBold
                  fontSize="16px"
                  weight="500"
                  color={Colors.mainBlackLight}
                >
                  ที่อยู่&nbsp;
                  <TextSemiBold color={Colors.mainRed} fontSize="16px">
                    *
                  </TextSemiBold>
                </TextSemiBold>

                <Controller
                  name="address"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <textarea
                      {...field}
                      style={{
                        borderRadius: 10,
                        marginTop: 5,
                        height: "70px",
                        borderWidth: "1px",
                        borderColor: "#CED4DA",
                      }}
                    />
                  )}
                />
                {errors.address && (
                  <TextSemiBold fontSize="14px" color={Colors.mainRed}>
                    กรุณากรอกข้อมูลให้ถูกต้อง
                  </TextSemiBold>
                )}
              </Flex>
            </Flex>
          </div>
          <div id="member-section-3" className="mt-5">
            <Flex className="flex-column">
              <TextSemiBold fontSize="16px">สถานะการใช้งาน</TextSemiBold>
              <Flex className="flex-row mt-2">
                <Controller
                  name="status"
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                    <Switch
                      {...field}
                      checked={approve}
                      onChange={() => {
                        setApprove(!approve);
                      }}
                      checkedIcon={false}
                      uncheckedIcon={false}
                      onColor={Colors.mainBlue}
                    />
                  )}
                />

                <TextSemiBold
                  fontSize="16px"
                  color={Colors.mainBlack}
                  className="ms-3"
                >
                  อนุมัติการใช้งาน
                </TextSemiBold>
              </Flex>
            </Flex>
          </div>
        </Flex>
        <Flex
          style={{ height: "180px" }}
          className="flex-row justify-content-center align-items-center"
        >
          <ButtonRadius
            onClick={() => Router.back()}
            type="button"
            title="ยกเลิก"
            backgroundColor="transparent"
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            borderColor={Colors.mainDarkBlue}
            borderWidth="1px"
            border="solid"
          />
          <ButtonRadius
            type="submit"
            title="บันทึก"
            backgroundColor={Colors.mainYellow}
            color={Colors.mainDarkBlue}
            width="200px"
            fontSize="18px"
            className="mx-1 mt-1 mt-3 py-2"
            border="none"
          />
        </Flex>
      </form>
    </Wrapper>
  );
};

export default AddMemberInfo;

const DatePickerDiv = styled.div`
  position: relative;
`;

const CustomDatePickDiv = styled.div`
  background-color: white;
  border: solid 0.1em #cbd4c9;

  border-radius: 0.25em;
  padding: 0.3em 1.6em 0 1.6em;
`;
export async function getServerSideProps({ query }: { query: any }) {
  return {
    props: {
      id: query.id || null,
      mode: query.mode || "add",
    },
  };
}
