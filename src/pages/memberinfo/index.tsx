import type { NextPage } from "next";
import Router from "next/router";
import { TextBold } from "../../components/text";
import React, { useEffect, useState } from "react";
import Colors from "../../../utils/Colors";
import Wrapper from "../../components/Layout/Wrapper";
import SearchProperty from "../../components/Search/SearchProperty";
import HeaderTable from "../../components/Table/HeaderTable";
import TableMemberInfo from "../../components/Table/TableMemberInfo";
import callApi from "../api/callApi";
import Auth from "../auth/index";
import Cookies from "universal-cookie";

const MemberInformation: NextPage = () => {
  const [numData, setNumData] = useState("");
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState({});
  const [conditionExport, setConditionExport] = useState([] as any);
  const cookies = new Cookies();
  const token = cookies.get("token");
  const fetchData = async (start: any, limit: any) => {
    let item = search;
    Object.assign(item, {
      start: start,
      limit: limit,
    });
    setSearch(item);
    const response = await callApi.apiPost("member/find", item, token);

    setData(response.item);
    setTotal(response.total);
  };
  const filterSearch = async (data: any) => {
    Object.assign(data, { start: 0, limit: 10 });
    setSearch(data);
    setConditionExport(data);
    const response = await callApi.apiPost("member/find", data, token);
    setData(response.item);
    setTotal(response.total);
  };
  const actionSearch = async (data: string) => {
    const conditionSearch = {
      fullname: data != undefined ? data : "",
      refno: data,
    };
    setSearch(conditionSearch);
    setConditionExport(conditionSearch);
    const response = await callApi.apiPost(
      "member/findCriteria",
      conditionSearch,
      token
    );
    setData(response.item);
    setTotal(response.total);
  };
  const exportExcel = async () => {
    const response = await callApi.apiExport(
      "member/export",
      conditionExport,
      token
    );
    const blob = window.URL.createObjectURL(
      new Blob([response], {
        type: "application/ms-excel",
      })
    );
    const link = document.createElement("a");
    link.href = blob;
    link.setAttribute("download", "Member" + ".xlsx");
    document.body.appendChild(link);

    link.click();
  };
  const deleteData = async (id: number) => {
    if (confirm("ยืนยันการลบข้อมูล")) {
      await callApi.apiDelete("member/delete/" + id, token);
      fetchData((currentPage - 1) * 10, 10);
    }
  };
  useEffect(() => {
    if (Auth()) {
      Router.push("/");
    }
    fetchData(0, 10);
  }, []);
  return (
    <Wrapper
      isLogout={true}
      stepIcon={
        <>
          <img src="new/images/Home2.svg" width="20px" height="20px" />
          <img
            src="new/images/Expand_right_double_light.svg"
            width="20px"
            height="20px"
            className="ms-2"
          />
          <TextBold
            fontSize="16px"
            color={Colors.mainGrayLight}
            className="ms-2"
          >
            ข้อมูลสมาชิก
          </TextBold>
        </>
      }
      title="ข้อมูลสมาชิก"
    >
      <SearchProperty
        isFilter={true}
        typeProperty="searchMemberInfo"
        onClickAddBtn={() => Router.push("/memberinfo/addmemberinfo")}
        onClickSearch={actionSearch}
        onClickExport={exportExcel}
        onClickSearchFilter={filterSearch}
      />
      <HeaderTable number={total.toString()} />
      <TableMemberInfo
        data={data}
        onClickDelete={deleteData}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        fetchData={fetchData}
        // exportExcel={exportExcelRows}
        total={total}
        setTotal={setTotal}
      />
    </Wrapper>
  );
};

export default MemberInformation;
