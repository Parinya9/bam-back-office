import axios from "axios";

const apiGet = async (url: any) => {
  try {
    const res = await axios.get(process.env.NEXT_PUBLIC_API_END_POINT + url);
    return res.data;
  } catch (error) {
    return { error };
  }
};
const apiGetWithHeader = async (url: any) => {
  try {
    const res = await axios.get(process.env.NEXT_PUBLIC_API_END_POINT + url, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    return res.data;
  } catch (error) {
    return { error };
  }
};
const apiGetCookie = async (url: any, email: any) => {
  try {
    const res = await axios.get(
      process.env.NEXT_PUBLIC_API_END_POINT + url,

      {
        params: { email: email },
      }
    );
  } catch (error) {
    return { error };
  }
};
const apiClearCookie = async (url: any) => {
  try {
    const res = await axios.get(
      process.env.NEXT_PUBLIC_API_END_POINT + url,

      {
        withCredentials: true,
      }
    );
  } catch (error) {
    return { error };
  }
};
const apiPost = async (url: any, objData: any, accessToken: string = "") => {
  try {
    const config = {
      headers: { Authorization: `Bearer ${accessToken}` },
    };
    const res = await axios.post(
      process.env.NEXT_PUBLIC_API_END_POINT + url,
      objData,
      config
    );
    return res.data;
  } catch (error) {
    return { error };
  }
};
const apiExport = async (url: any, objData: any, accessToken: string = "") => {
  try {
    const res = await axios.post(
      process.env.NEXT_PUBLIC_API_END_POINT + url,
      objData,
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
        responseType: "arraybuffer",
      }
    );

    return res.data;
  } catch (error) {
    return { error };
  }
};
const apiPut = async (url: any, objData: any, accessToken: string = "") => {
  try {
    const config = {
      headers: { Authorization: `Bearer ${accessToken}` },
    };
    const res = await axios.put(
      process.env.NEXT_PUBLIC_API_END_POINT + url,
      objData,
      config
    );

    return res.data;
  } catch (error) {
    return { error };
  }
};
const apiDelete = async (url: any, accessToken: string = "") => {
  try {
    const config = {
      headers: { Authorization: `Bearer ${accessToken}` },
    };
    const res = await axios.delete(
      process.env.NEXT_PUBLIC_API_END_POINT + url,
      config
    );

    return res.data;
  } catch (error) {
    return { error };
  }
};

export default {
  apiGet,
  apiPost,
  apiGetCookie,
  apiDelete,
  apiPut,
  apiExport,
  apiClearCookie,
  apiGetWithHeader,
};
