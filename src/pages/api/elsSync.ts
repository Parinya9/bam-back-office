import axios from "axios";

const apiSyncAssetDetail = async (url: any) => {
  try {
    const res = await axios.get(process.env.NEXT_PUBLIC_ELS_SYNC_SERVICE + url);
    return res.data;
  } catch (error) {
    return { error };
  }
};

const apiSyncAssetDetailAuction = async (url: any) => {
  try {
    const res = await axios.get(process.env.NEXT_PUBLIC_ELS_SYNC_SERVICE + url);
    return res.data;
  } catch (error) {
    return { error };
  }
};

export { apiSyncAssetDetail, apiSyncAssetDetailAuction };
